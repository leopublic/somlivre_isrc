@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.mensagens')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-gift"></i> {{$titulo}}</div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body form">
                {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                {{Form::hidden('id_coletivo', $objeto->id_coletivo, array("id"=> 'id_coletivo'))}}
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Código</label>
                        <div class="col-md-9">
                            {{Form::text('id_coletivo', $objeto->id_coletivo, array("class" => "form-control", "disabled"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nome</label>
                        <div class="col-md-9">
                            {{Form::text('nome', Input::old('nome', $objeto->nome), array("class" => "form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Pseudônimo</label>
                        <div class="col-md-9">
                            {{Form::text('pseudonimo', Input::old('nome', $objeto->pseudonimo), array("class" => "form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Origem</label>
                        <div class="col-md-9">
                            {{Form::hidden('is_nacional', 0)}}
                            <div class="radio-list">
                                <label class="radio-inline">
                                    {{ Form::radio('is_nacional', 1, Input::old('is_nacional', $objeto->is_nacional) )}} Nacional
                                </label>
                                <label class="radio-inline">
                                    {{ Form::radio('is_nacional', 0, !Input::old('is_nacional', $objeto->is_nacional) )}} Estrangeira
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Código CAE</label>
                        <div class="col-md-9">
                            {{Form::text('cod_cae', $objeto->cod_cae, array("class" => "form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tipo</label>
                        <div class="col-md-9">
                            {{Form::select('id_tipo_coletivo', $tipos_coletivo, Input::old('id_tipo_coletivo', $objeto->id_tipo_coletivo), array("class" => "form-control select2me"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Sigla</label>
                        <div class="col-md-9">
                            {{Form::select('id_sigla_coletivo', $siglas, Input::old('id_sigla_coletivo', $objeto->id_sigla_coletivo), array("class" => "form-control select2me"))}}
                        </div>
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green">Salvar</button>
                        <a href="{{URL::to('/admin/coletivo')}}" class="btn default">Cancelar</a>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
@if ($objeto->id_coletivo > 0)
<div class="row">
    <div class="col-md-12" id="containerTitulares">
        @include ('coletivo.titulares_tabela')
    </div>
</div>

<div id="modTitularPesquisar" class="modal fade" tabindex="-1"  data-width="650" data-height="600">
</div>

        
<div id="modTitularEditar" class="modal fade" tabindex="-1"  data-width="650" data-height="650">
</div>        
@endif
@stop

@section('scripts_full')
<script>
    $('#menuCadastros').addClass('active');
    $('#menuCadastros').addClass('open');
    $('#itemColetivos').addClass('active');

        
    jQuery(document).ready(function () {
        $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner = 
          '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
            '<div class="progress progress-striped active">' +
              '<div class="progress-bar" style="width: 100%;"></div>' +
            '</div>' +
          '</div>';

        $.fn.modalmanager.defaults.resize = true;
    });

    function btnTitularAdicionarClick(){
        var url = "/admin/coletivo/formpesquisa/";
        $('body').modalmanager('loading');
        $("#modTitularPesquisar" ).load( url, '', function(){
            $('#modTitularPesquisar').modal();
        });
    }
    // Recupera a pesquisa de titulares
    function btnTitularPesquisarClick(){
        Metronic.blockUI({
            target: '#modTitularPesquisar',
            boxed: true,
            message: '(aguarde...)',
        });
        var id_coletivo = $('#id_coletivo').val();
        var url = "/admin/coletivo/pesquisartitulares/"+id_coletivo+"/"+$('#modTitularPesquisar #nome_razao').val();
        $("#modTitularPesquisar" ).load( url,'' , function(){
            Metronic.unblockUI('#modTitularPesquisar');    
        });
    }

    function getAdicionarTitular(id_coletivo, id_titular){
        Metronic.blockUI({
            target: '#modTitularPesquisar',
            boxed: true,
            message: '(aguarde...)'
        });
        var url = '/admin/coletivo/adicionartitular/'+id_coletivo+'/'+id_titular;
        $('#modTitularPesquisar').load( url, '', function(){
            $('.date-pickerx').datepicker({autoclose: true});            
            $('.select2').select2();            
            Metronic.unblockUI('#modTitularPesquisar');    
        });
    }
        
    function getEditarTitular(id_fonograma_titular){
        var url = '/admin/coletivo/editartitular/'+id_fonograma_titular;
        $('body').modalmanager('loading');
        $('#modTitularPesquisar').load( url, '', function(){
            $('.date-pickerx').datepicker({autoclose: true});
            $('.select2').select2();
            $('#modTitularPesquisar').modal();
        });
    }
    
    function postAdicionarTitular(){
        Metronic.blockUI({
            target: '#modTitularPesquisar',
            boxed: true,
            message: '(aguarde...)'
        });
        $.post( "/admin/coletivo/adicionartitular", $( "#form_titular" ).serialize()
            , function(data){
                if (data.codret == 0){
                    $('#modTitularPesquisar').modal('hide');
                    $(document.body).animate({
                        'scrollTop':  $('#portlet-body-titulares').offset().top
                    }, 2000);
                    $('#containerTitulares').html(data.html);
                    $('#linha'+data.id_titular_coletivo).stop().animate({backgroundColor: "#95A5A6"}, 100)
                        .animate({backgroundColor: "#FFFFFF"}, 4500);
                } else {
                    $('#modal-msg').html(data.msg);
                }
            }        
            , "json" )
        ;
    }
    
    function postExcluirTitular(id_titular_coletivo){
        Metronic.blockUI({
                target: '#portlet-body-titulares',
                boxed: true,
                message: 'Excluindo... (aguarde)'
        });
        $.post( "/admin/coletivo/excluirtitular", {id_titular_coletivo:id_titular_coletivo, id_coletivo: $('#id_coletivo').val()}
            , function(data){
                Metronic.unblockUI('#portlet-body-titulares');
                if (data.codret == 0){
                    $('#ajax-modal').modal('hide');
                    $(document.body).animate({
                        'scrollTop':  $('#containerTitulares').offset().top
                    }, 1000);
                    $('#containerTitulares').html(data.html);
                } else {
                    $('#modal-msg').html(data.msg);
                }
            }        
            , "json" )
        ;
    }
</script>
@stop