<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePais extends Migration {

    public function up() {
        Schema::create('pais', function($table) {
            $table->increments('id_pais');
            $table->string('nome', 30)->nullable();
            $table->string('sigla', 2)->nullable();
            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->boolean('is_convroma')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        //
    }

    public function down() {
        if (Schema::hasTable('pais')) {
            Schema::drop('pais');
        }
        //
    }

}
