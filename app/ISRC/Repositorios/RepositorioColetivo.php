<?php

namespace ISRC\Repositorios;

class RepositorioColetivo extends RepositorioCadastro {
    
    public function tituloModelo(){
        return 'coletivo';
    }
    
    public function nomeModelo(){
        return '\Coletivo';
    }
    
    public function ordenacaoPadrao(){
        return 'nome';
    }
    
    public function registrosConsulta($filtros = null){
        if (key_exists('filtro', $filtros)) {
            $filtro = $filtros['filtro'];
        } else {
            $filtro = '';
        }
               
    	return \Coletivo::wherefiltro($filtro)
                ->with('integrantes', 'integrantes.titular')
                ->orderBy('nome')
                ->paginate(20);    
    }
    
    public function adicionarTitular($post){
        if (intval($post['id_titular_coletivo']) == 0) {
            $tit_coletivo = new \TitularColetivo;
            $tit_coletivo->id_coletivo = $post['id_coletivo'];
            $tit_coletivo->id_titular = $post['id_titular'];
        } else {
            $tit_coletivo = \TitularColetivo::find($post['id_titular_coletivo']);
        }
        $tit_coletivo->dt_entrada_fmt = $post['dt_entrada_fmt'];
        $tit_coletivo->dt_saida_fmt = $post['dt_saida_fmt'];
        $tit_coletivo->save();
        return $tit_coletivo;
        
    }
    public function excluirTitular($id_titular_coletivo){
        \TitularColetivo::where('id_titular_coletivo', '=', $id_titular_coletivo)->delete();
    }
    
    public function instanciaEditado($id) {
        $obj = parent::instanciaEditado($id);
        if (intval($id) == 0 ){
            $obj->is_nacional = 1;
        }
        return $obj;
    }
}
