@extends('base')

@section('estilos')
@stop

@section('conteudo')
<div class="row">
    <div class="col-md-12">
        <h3 class="page-title">Criar fonograma</h3>
    </div>
</div>
@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <div class="portlet box {{$portlet_obra}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-music"></i> Obra/pot-pourrit</div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body">
                <div class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group" style="margin-bottom: 0px;">
                                <div class="col-md-10">
                                    {{Form::text('nome_obra', $nome_obra, array("class" => "form-control ", "disabled"))}}
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
                    <div class="portlet box yellow-gold">
                        <div class="portlet-title">
                            <div class="caption"><i class="fa fa-gift"></i> Dados do fonograma</div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body form">
                            {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                            {{Form::hidden('id_fonograma', 0)}}
                            {{Form::hidden('id_obra', $objeto->id_obra)}}
                            {{Form::hidden('id_poutporrit', $objeto->id_poutporrit)}}
                            <div class="form-body">
                                <h4>Códigos</h4>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">ISRC</label>
                                        <div class="col-md-9">
                                            {{Form::text('Cod_ISRC', Input::old('Cod_ISRC', $objeto->Cod_ISRC), array("class" => "form-control input-small"))}}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Gravadora</label>
                                        <div class="col-md-9">
                                            {{Form::text('cod_GRA', Input::old('cod_GRA', $objeto->cod_GRA), array("class" => "form-control input-small"))}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Data de emissão</label>
                                        <div class="col-md-9">
                                            {{Form::text('dt_emissao_fmt', Input::old('dt_emissao_fmt', $objeto->dt_emissao_fmt), array("class" => "form-control form-control-inline input-small date-picker"))}}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Catálogo</label>
                                        <div class="col-md-9">
                                            {{Form::text('num_catalogo', Input::old('num_catalogo', $objeto->num_catalogo), array("class" => "form-control input-small"))}}
                                        </div>
                                    </div>
                                </div>
                                <h4>Dados do fonograma</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Gênero</label>
                                            <div class="col-md-9">
                                                {{Form::select('id_genero', $generos, Input::old('id_genero', $objeto->id_genero), array("class" => "form-control select2me"))}}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Classificação</label>
                                            <div class="col-md-9">
                                                {{Form::select('id_tipo_fonograma', $tipos_fonograma, Input::old('id_tipo_fonograma', $objeto->id_tipo_fonograma), array("class" => "form-control select2me"))}}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Tipo mídia</label>
                                            <div class="col-md-9">
                                                {{Form::select('id_tipo_midia', $tipos_midia, Input::old('id_tipo_midia', $objeto->id_tipo_midia), array("class" => "form-control select2me"))}}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Arranjo</label>
                                            <div class="col-md-9">
                                                {{Form::select('id_arranjo', $arranjos, Input::old('id_arranjo', $objeto->id_arranjo), array("class" => "form-control select2me"))}}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">País origem</label>
                                            <div class="col-md-9">
                                                {{Form::select('id_pais_origem', $paises, Input::old('id_pais_origem', $objeto->id_pais_origem), array("class" => "form-control select2me"))}}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">País lançamento</label>
                                            <div class="col-md-9">
                                                {{Form::select('id_pais_publicacao', $paises, Input::old('id_pais_publicacao', $objeto->id_pais_publicacao), array("class" => "form-control select2me"))}}
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Tipo gravação</label>
                                            <div class="col-md-8">
                                                <div class="radio-list">
                                                    <label class="radio-inline">
                                                        {{ Form::radio('is_gravacao_propria', 0, !Input::old('is_gravacao_propria', $objeto->is_gravacao_propria) )}} Terceiros
                                                    </label>
                                                    <label class="radio-inline">
                                                        {{ Form::radio('is_gravacao_propria', 1, Input::old('is_gravacao_propria', $objeto->is_gravacao_propria) )}} Própria
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Gravação original</label>
                                            <div class="col-md-8">
                                                {{Form::text('dt_grav_original_fmt', Input::old('dt_grav_original_fmt', $objeto->dt_grav_original_fmt), array("class" => "form-control form-control-inline input-small date-picker", "data-date-format"=>"dd/mm/yyyy"))}}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Lançamento</label>
                                            <div class="col-md-8">
                                                {{Form::text('dt_lancamento_fmt', Input::old('dt_lancamento_fmt', $objeto->dt_lancamento_fmt), array("class" => "form-control form-control-inline input-small date-picker", "data-date-format"=>"dd/mm/yyyy"))}}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Duração</label>
                                            <div class="col-md-8">
                                                <div class="input-group" >
                                                    <span class="input-group-addon">Min </span>
                                                    {{Form::text('tmp_duracao_min', Input::old('tmp_duracao_min', $objeto->tmp_duracao_min), array("class" => "form-control  form-control-inline input-xsmall"))}}
                                                    <span class="input-group-addon">Seg </span>
                                                    {{Form::text('tmp_duracao_seg', Input::old('tmp_duracao_seg', $objeto->tmp_duracao_seg), array("class" => "form-control form-control-inline input-xsmall"))}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Complemento</label>
                                            <div class="col-md-8">
                                                {{Form::text('dsc_complemento_arranjo', Input::old('dsc_complemento_arranjo', $objeto->dsc_complemento_arranjo), array("class" => "form-control"))}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h4>Observação</h4>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        {{Form::textarea('obs', Input::old('obs', $objeto->obs), array("class" => "form-control"))}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Salvar</button>
                                    <a href="{{URL::to('/admin/perfil')}}" class="btn default">Cancelar</a>
                                </div>
                            </div>
                            {{Form::close()}}
                        </div>
                    </div>

                </div>
</div>
</div>
@stop
@section('scripts')
ComponentsPickers.init();
$('#itemFonogramas').addClass('active');
@stop
