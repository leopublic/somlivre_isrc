<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">{{$titulo}}</h4>
</div>
<div class="modal-body">
    {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "form_titular"))}}
    {{Form::hidden('id_pessoa', $reg->id_pessoa)}}
    {{Form::hidden('id_pseudonimo', $reg->id_pseudonimo)}}
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @if ($reg->id_pseudonimo > 0)
                <div class="form-group">
                    <label class="col-md-3 control-label">ID</label>
                    <div class="col-md-9">
                        {{Form::text('id', $reg->id_pseudonimo, array("class" => "form-control", 'disabled'))}}
                    </div>
                </div>
                @endif
                <div class="form-group">
                    <label class="col-md-3 control-label">Pseudônimo</label>
                    <div class="col-md-9">
                        {{Form::text('pseudonimo', $reg->pseudonimo, array("class" => "form-control"))}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Principal?</label>
                    <div class="col-md-9">
                        {{Form::checkbox('is_principal', 1, $reg->is_principal, array("class" => "form-control"))}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}
    <div id="modal-msg" style="color:red;"></div>
</div>
<div class="modal-footer">
    <button type="button" class="btn blue" onclick="javascript:btnSalvarPseudonimo();">Salvar</button>
    <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
</div>
