<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSiglaColetivo extends Migration {

    public function up() {
        Schema::create('sigla_coletivo', function($table) {
            $table->increments('id_sigla_coletivo');
            $table->string('descricao', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        
    }

    public function down() {
        if (Schema::hasTable('sigla_coletivo')) {
            Schema::drop('sigla_coletivo');
        }
    }

}
