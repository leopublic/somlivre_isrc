<?php

class PoutporritController extends CadastroController {

    public function repo() {
        return new \ISRC\Repositorios\RepositorioPoutporrit;
    }

    public function diretorioViews() {
        return 'poutporrit';
    }

    public function msgSucesso() {
        return 'Potpourri atualizado com sucesso';
    }

    public function getEdit($id = 0) {
        $rep = new \ISRC\Repositorios\RepositorioPoutporrit();
        $objeto = $rep->instanciaEditado($id);
        $titulo = $rep->tituloTelaEdicao($objeto);
        $poutporrit_obras = $objeto->obras;
        return View::make('poutporrit.edit')
                        ->with('objeto', $objeto)
                        ->with('titulo', $titulo)
                        ->with('poutporrit_obras', $poutporrit_obras)
        ;
    }

    public function postEdit($id = '') {
        $post = Input::all();
        $id_antes = Input::get('id_poutporrit');
        try {
            $rep = new \ISRC\Repositorios\RepositorioPoutporrit();
            $poutporrit = $rep->salve($post);
            Session::flash('msg', 'Pot-pourri atualizado com sucesso.');
            if ($id_antes > 0) {
                return Redirect::to('/admin/potpourri/');
            } else {
                return Redirect::to('/admin/potpourri/edit/' . $poutporrit->id_poutporrit);
            }
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', $ex->getMessage());
            return Redirect::to('/admin/potpourri/edit/' . $post['id_poutporrit']);
        }
    }

    public function getPesquisarobras($id_poutporrit, $pesquisar) {
        if ($pesquisar != '') {
            $pesquisar = str_replace('*', '%', $pesquisar);
            $sql_count = "select count(*) as qtd from obra where  titulo_original like ?";
            $sql = "select id_obra, titulo_original from obra where titulo_original like ? limit 15";
            $res_quantos = \DB::select($sql_count, array('%' . $pesquisar . '%'));
            $rs_quantos = $res_quantos[0];
            $obras = \DB::select($sql, array('%' . $pesquisar . '%'));
            if ($rs_quantos->qtd > 15) {
                $ignorados = $rs_quantos->qtd - 15;
                $msg_limite = '(' . $ignorados . ' resultados não exibidos. Refine sua pesquisa)';
            } else {
                $msg_limite = '';
            }
            return View::make('poutporrit.obras_pesquisa')
                            ->with('id_poutporrit', $id_poutporrit)
                            ->with('obras', $obras)
                            ->with('msg_limite', $msg_limite)
            ;
        } else {
            $quantos = 0;
            $msg_limite = '';
            $titulares = array();
            return View::make('poutporrit.obras_pesquisa')
                            ->with('id_poutporrit', $id_poutporrit)
                            ->with('obras', $obras)
                            ->with('msg_limite', $msg_limite)
            ;
        }
    }

    public function getFormpesquisa() {
        return View::make('poutporrit.obras_form_pesquisa');
    }

    public function postAdicionarobra() {
        $ret = array();
        try {
            $rep = new \ISRC\Repositorios\RepositorioPoutporrit();
            $poutporrit_obra = $rep->adicionarObra(Input::get('id_poutporrit'), Input::get('id_obra'));

            $objeto = \Poutporrit::find(Input::get('id_poutporrit'));
            $poutporrit_obras = $objeto->obras;
            $html = View::make('poutporrit.obras_tabela')
                    ->with('poutporrit_obras', $poutporrit_obras)
                    ->with('msg', 'Obra adicionada com sucesso')
            ;
            $ret['html'] = iconv('UTF-8', 'UTF-8', $html);
            $ret['codret'] = 0;
            $ret['msg'] = 'Obra adicionada com sucesso!';
            $ret['id_poutporrit_obra'] = $poutporrit_obra->id_poutporrit_obra;
        } catch (Exception $ex) {
            $ret['codret'] = -1;
            $ret['msg'] = "Não foi possível adicionar essa obra: " . $ex->getMessage() . $ex->getTraceAsString();
        }
        return json_encode($ret);
    }

    public function postExcluirobra() {
        $ret = array();
        try {
            $rep = new \ISRC\Repositorios\RepositorioPoutporrit();
            $rep->excluirObra(Input::get('id_poutporrit_obra'));

            $objeto = \Poutporrit::find(Input::get('id_poutporrit'));
            $poutporrit_obras = $objeto->obras;
            $html = View::make('poutporrit.obras_tabela')
                    ->with('poutporrit_obras', $poutporrit_obras)
                    ->with('msg', 'Obra adicionada com sucesso')
            ;
            $ret['html'] = iconv('UTF-8', 'UTF-8', $html);
            $ret['codret'] = 0;
            $ret['msg'] = 'Obra excluída com sucesso!';
        } catch (Exception $ex) {
            $ret['codret'] = -1;
            $ret['msg'] = "Não foi possível adicionar essa obra: " . $ex->getMessage() . $ex->getTraceAsString();
        }
        return json_encode($ret);
    }

}
