<?php
namespace ISRC\Validadores;
class Pessoa extends Validador {
    public $id_unique;
    public static $rules = array(
        'nome_razao'       => 'required',
    );
    public static $messages = array(
        'nome_razao.required'       => 'O nome do titular é obrigatório',
    );

//    public function getRules(){
//        $x = array_merge(static::$rules , array('email' => 'unique:usuario,email,'.$this->id_unique.',id_usuario'));
//        return $x;
//    }

}
