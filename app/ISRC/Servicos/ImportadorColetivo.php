<?php

namespace ISRC\Servicos;

class ImportadorColetivo extends Importador{

    public function sqlSelectMdb() {
        return "select id_coletivo
                        ,nome
                        ,cod_coletivo
                        ,cod_coletivo_ECAD
                        ,pseudonimo
                        ,is_nacional
                        ,cod_cae
                        ,ID_COLETIVO_TIPO
                        ,SIGLA
                        ,dt_inclusao
                        ,dt_alteracao
                        ,usuario
                        from ISRC_coletivo_tb where is_deleted = 0";
    }

    public function novoObjeto(){
        return new \Coletivo();
    }
    
    public function nome_id_local(){
        return 'id_coletivo';
    }

    public function tabela(){
        return "ISRC_coletivo_tb";
    }

    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            'nome',
            'cod_coletivo',
            'cod_coletivo_ECAD',
            'pseudonimo',
            'is_nacional',
            'cod_cae',
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);

        $obj->id_tipo_coletivo = $regMDB['ID_COLETIVO_TIPO'];
        $obj->sigla = $regMDB['SIGLA'];
        $obj->usuario = $regMDB['usuario'];
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);

        $obj->id_coletivo_original = $regMDB['id_coletivo'];
        $obj->id_empresa = \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa;
       return $obj;
    }

    public function finaliza(){
        $sql = "insert into sigla_coletivo (descricao) select distinct sigla from coletivo";
        \DB::insert(\DB::raw($sql));

        $sql = "update coletivo , sigla_coletivo set coletivo.id_sigla_coletivo = sigla_coletivo.id_sigla_coletivo where sigla_coletivo.descricao = coletivo.sigla";
        \DB::update(\DB::raw($sql));
    }
}