<?php

class FonogramaObrasController extends BaseController {
    public function getShow($id_fonograma) {
        $objeto = \Fonograma::find($id_fonograma);
        $pesquisar = Input::get('pesquisar', Input::old('pesquisar'));
        $obras = \Obra::where('titulo_original', 'like', '%'.$pesquisar.'%')
                    ->with(array('titulares', 'titulares.titular', 'titulares.titular.pessoa'))
                    ->orderBy('titulo_original')
                    ->paginate(10);
        Input::flash();
        return View::make('fonograma.obras')
                ->with('objeto', $objeto)
                ->with('obras', $obras)
                ->with('pesquisar', $pesquisar)
            ;
    }
    
    public function postShow($id_fonograma){
        Input::flash();
        $objeto = \Fonograma::find($id_fonograma);
        $pesquisar = Input::get('pesquisar', Input::old('pesquisar'));
        $obras = \Obra::where('titulo_original', 'like', '%'.$pesquisar.'%')
                    ->with(array('titulares', 'titulares.titular', 'titulares.titular.pessoa'))
                    ->orderBy('titulo_original')
                    ->paginate(10);
        return View::make('fonograma.obras')
                ->with(compact('objeto', 'obras', 'pesquisar'))
            ;
    }
    
    public function getSet($id_fonograma, $id_obra){
        try{
            $rep = new \ISRC\Repositorios\RepositorioFonograma;
            $rep->atualizaObra($id_fonograma, $id_obra);
            Session::flash('msg', 'Fonograma atualizado com sucesso');
        } catch (Exception $ex) {
            Session::flash('error', $ex->getMessage());
        }
        return Redirect::to('/admin/fonogramaobras/show/'.$id_fonograma);
    }

}
