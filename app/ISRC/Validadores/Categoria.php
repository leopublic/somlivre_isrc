<?php
namespace ISRC\Validadores;
class Categoria extends Validador {
    public $id_unique;
    public static $rules = array(
        'cod_categoria'       => 'required',
        'desc_categoria'       => 'required',
    );
    public static $messages = array(
        'cod_categoria.required'       => 'O código da categoria é obrigatório',
        'desc_categoria.required'       => 'O nome da categoria é obrigatório',
        'desc_categoria.unique'       => 'Já existe outra categoria com esse nome',
    );
    public function getRules(){
        $x = array_merge(static::$rules , array('desc_categoria' => 'required|unique:categoria,nome,'.$this->id_unique.",id_categoria"));
        return $x;
    }
}