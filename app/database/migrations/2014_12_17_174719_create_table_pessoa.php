<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePessoa extends Migration {

    public function up() {
        Schema::create('pessoa', function($table) {
            $table->increments('id_pessoa');
            $table->integer('id_pessoa_original')->unsigned()->nullable();
            $table->integer('id_empresa')->unsigned()->nullable();
            $table->string('tp_pessoa', 1)->nullable();
            $table->integer('cod_pessoa')->nullable();
            $table->string('cod_ecad', 20)->nullable();
            $table->string('nome_razao', 60)->nullable();
            $table->string('fantasia', 60)->nullable();
            $table->string('tp_cpf', 1)->nullable();
            $table->string('cpf_cnpj', 14)->nullable();
            $table->string('email', 50)->nullable();
            $table->date('dt_nascimento')->nullable();
            $table->string('estado_civil', 2)->nullable();
            $table->string('sexo', 1)->nullable();
            $table->string('insc_estadual', 18)->nullable();
            $table->string('insc_municipal', 18)->nullable();
            $table->string('identidade', 16)->nullable();
            $table->string('orgao_emissor', 10)->nullable();
            $table->string('profissao', 15)->nullable();
            $table->string('nacionalidade', 20)->nullable();
            $table->string('is_editora', 1)->nullable();
            $table->string('is_produtor_fono', 1)->nullable();
            $table->string('is_autor', 1)->nullable();
            $table->string('is_interprete', 1)->nullable();
            $table->string('is_musico', 1)->nullable();
            $table->text('observacao')->nullable();

            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::table('pessoa', function ($table) {
           $table->index('nome_razao'); 
           $table->index('fantasia'); 
        });
    }

    public function down() {
        if (Schema::hasTable('pessoa')) {
            Schema::drop('pessoa');
        }
    }

}
