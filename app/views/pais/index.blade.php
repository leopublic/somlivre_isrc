@extends('base')

@section('estilos')
@stop

@section('conteudo')
<div class="col-md-12">
    <h3 class="page-title">Países</h3>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>Registros encontrados
            </div>
           <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-scrollable">
                @include('_padroes.table')
                <thead>
                    <tr>
                        <th class="center">Ações</th>
                        <th class="center">Id</th>
                        <th>Nome</th>
                        <th class="center">Sigla</th>
                        <th class="center">Convenção de Roma</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($registros as $pais)
                    <tr>
                        <td class="center">
                            <a href="{{URL::to('/pais/edit/'.$pais->id_pais)}}" class="btn btn-sm green-seagreen" title="Editar o país"><i class="fa fa-edit"></i></a>
                        </td>
                        <td class="center">{{$pais->id_pais}}</td>
                        <td>{{$pais->nome}}</td>
                        <td class="center">{{$pais->sigla}}</td>
                        <td class="center">
                            @if ($pais->is_convroma)
                            <i class="fa fa-check"></i>
                            @else
                            &nbsp;
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END BORDERED TABLE PORTLET-->
</div>
@stop
@section('scripts')
    $('#menuTabelas').addClass('active');
    $('#menuTabelas').addClass('open');
    $('#itemPaises').addClass('active');
@stop
