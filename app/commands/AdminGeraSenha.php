<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AdminGeraSenha extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'admin:gera-senha';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Renova a senha de um usuário';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		try {
            $email = $this->argument('email');
			$usuario = \Usuario::where('email', '=', $email)->first();
			$rep = new \ISRC\Repositorios\RepositorioUsuario;
			$post['id_usuario'] = $usuario->id_usuario;
			$post['password'] = $this->argument('senha');
			$rep->altereSenha($post);
			$this->info('Senha alterada com sucesso');
        } catch (Exception $e) {
            print $e->getMessage();
            print $e->getTraceAsString();
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('email', InputArgument::REQUIRED, 'Email'),
			array('senha', InputArgument::REQUIRED, 'Senha'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
