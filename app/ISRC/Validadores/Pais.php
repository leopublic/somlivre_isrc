<?php
namespace ISRC\Validadores;
class Pais extends Validador {
    public $id_unique;
    public static $rules = array(
        'sigla'       => 'required',
        'nome'       => 'required',
    );
    public static $messages = array(
        'sigla.required'       => 'A sigla do País é obrigatória',
        'nome.required'       => 'O nome do País é obrigatório',
    );

//    public function getRules(){
//        $x = array_merge(static::$rules , array('email' => 'unique:usuario,email,'.$this->id_unique.',id_usuario'));
//        return $x;
//    }

}
