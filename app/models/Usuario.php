<?php

/**
 * @property int $id_usuario Chave
 * @property int $id_usuario_cadastro
 * @property string $nome Nome 
 * @property string $email Email e login
 * @property int $qtd_tentativas_erradas Quantidade de vezes que o usuário errou a senha
 * @property datetime $dt_ultimo_acesso
 * @property datetime $dthr_primeiro_erro
 * @property boolean $acessa_backoffice Indica se o usuário tem acesso ao backoffice
 * @property string $observacao Observações
 */
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Usuario extends Modelo implements UserInterface, RemindableInterface {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usuario';
    protected $primaryKey = 'id_usuario';
    protected $guarded = array('password_confirmation');
    protected $fillable = array('nome', 'username', 'email', 'observacao');
    protected $connection = 'mysql';
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier() {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword() {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken() {
        return $this->remember_token;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value) {
        $this->remember_token = $value;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName() {
        return 'remember_token';
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail() {
        return $this->email;
    }

    public function empresa(){
        return $this->belongsTo('Empresa', 'id_empresa','id_empresa');
    }

    public function atualizaUltimoAcesso() {
        $this->dt_ultimo_acesso = date('Y-m-d H:i:s');
        $this->dthr_primeiro_erro = null;
        $this->qtd_tentativas_erradas = 0;
        $this->save();
    }

    public function getAcessaBackofficeFmtAttribute($valor) {
        if ($this->acessa_backoffice) {
            return "Sim";
        } else {
            return "Não";
        }
    }

    public function getDtUltimoAcessoFmtAttribute($valor) {
        return $this->dtHrFmt('dt_ultimo_acesso');
    }

    public function getPerfisAttribute($valor) {
        $id_usuario = $this->id_usuario;
        $perfis = Perfil::whereIn('id_perfil', function($query) use($id_usuario) {
            $query->select('id_perfil')
                    ->from('usuario_perfil')
                    ->where('id_usuario', '=', $id_usuario);
        })->lists('descricao');
        return implode('<br/>', $perfis);
    }

    public function perfis(){
        return $this->belongsToMany('Perfil', 'usuario_perfil', 'id_usuario', 'id_perfil');
    }

    public function getNomeEmpresaAttribute(){
        if (count($this->empresa) > 0){
            return $this->empresa->nome;
        } else {
            return '--';
        }
    }
}
