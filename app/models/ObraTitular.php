<?php
/**
 * @property int $id_obra_titular
 * @property int $id_categoria
 * @property int $id_obra
 * @property int $id_titular
 * @property int $id_pseudonimo
 * @property float $pct_autoral
 * @property date $dt_inclusao
 * @property date $dt_alteracao
 * @property datetime created_at 
 * @property datetime updated_at 
 * @property string $usuario
 */
class ObraTitular extends Modelo {
    protected $table = 'obra_titular';
    protected $primaryKey = 'id_obra_titular';
    protected $guarded = array();

    public function titular(){
        return $this->belongsTo('Titular', 'id_titular', 'id_titular');
    }

    public function obra(){
        return $this->belongsTo('Obra', 'id_obra', 'id_obra');
    }
    
    /**
     * @return \Pseudonimo Description
     */
    public function pseudonimo(){
        return $this->belongsTo('TitularPseudonimo', 'id_pseudonimo', 'id_pseudonimo');
    }

    public function categoria(){
        return $this->belongsTo('Categoria', 'id_categoria', 'id_categoria');
    }
    
    public function getPctAutoralFmtAttribute(){
        return number_format($this->pct_autoral, '8', ',','.');
    }
    public function setPctAutoralFmtAttribute($valor){
        $this->attributes['pct_autoral'] = str_replace(',', '.', str_replace('.', '', trim($valor)));
    }
    public function getDescCategoriaAttribute(){
        if ($this->categoria){
            return $this->categoria->desc_categoria;
        } else {
            return '--';
        }
    }
    
    public function getNomePseudonimoAttribute(){
        if ($this->pseudonimo){
            return $this->pseudonimo->pseudonimo;
        } else {
            return '--';
        }
    }
    
    public function getNomeRazaoAttribute(){
        if ($this->id_titular > 0){
            return $this->titular->pessoa->nome_razao;
        } else {
            return '--';
        }
    }
}