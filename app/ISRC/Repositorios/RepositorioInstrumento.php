<?php

namespace ISRC\Repositorios;

class RepositorioInstrumento extends RepositorioCadastro {
    
    public function nomeModelo(){
        return '\Instrumento';
    }
    
    public function ordenacaoPadrao(){
        return 'nome';
    }
    
    public function registrosConsulta($filtros = null){
               
    	return \Instrumento::with('grupo')
                ->orderBy('nome')
                ->get();
    }
    
}
