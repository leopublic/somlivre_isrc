<?php

namespace ISRC\Servicos;

class ImportadorSociedade extends Importador{

    public function sqlSelectMdb() {
        return "select id_sociedade, sgl_sociedade, nome, dt_inclusao, dt_alteracao, usuario, ind_nacional  from ISRC_sociedade_tb where is_deleted = 0";
    }

    public function novoObjeto(){
        return new \Sociedade();
    }

    public function tabela(){
        return "ISRC_sociedade_tb";
    }

    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            'sgl_sociedade',
            'nome',
            'ind_nacional',
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);
        $obj->id_sociedade_original = $regMDB['id_sociedade'];
        $obj->usuario = $regMDB['usuario'];
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        $obj->id_empresa = \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa;
       return $obj;
    }
}