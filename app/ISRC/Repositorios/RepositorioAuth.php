<?php

namespace ISRC\Repositorios;

class RepositorioAuth extends Repositorio {

    public function login($email, $password) {
        if (trim($email) == '') {
            $this->registreErro('Informe o login do usuário.');
            return false;
        }
        if (trim($password) == '') {
            $this->registreErro('Informe a senha do usuário.');
            return false;
        }
        $ip = \Request::getClientIp();
        // Testa o limite de tentativas erradas
        if ($this->podeTentarAcesso($email, $ip)) {
            if (\Auth::attempt(array('email' => $email, 'password' => $password), false)) {
                $this->registraTentativaComSucesso(\Auth::user(), $ip);
                return true;
            } else {
                $this->registraTentativaSemSucesso($email, $ip);
                $this->registreErro('Usuário ou senha inválidos..');
                return false;
            }
        } else {
            //Notifica responsáveis
            $this->notificaExcessoTentativas($email, $ip);
            $this->registreErro('Você excedeu o número permitido de tentativas e seu acesso foi bloqueado. Por favor, solicite uma nova senha.');
            return false;
        }
    }

    /**
     * Verifica se o username /ip pode continuar a tentar acesso
     * @param type $username
     * @param type $ip
     */
    public function podeTentarAcesso($email, $ip) {
        // Primeiro verifica se está tentando acessar um usuário válido
        $usuario = \Usuario::where('email', '=', $email)->first();
        if (is_object($usuario)) {
            if ($usuario->qtd_tentativas_erradas <= 5) {
                return true;
            } else {
                return false;
            }
        } else {
            //Se não é um usuário válido, então verifica se é DDOS pelo IP
            $tentativa = \TentativaAcesso::where('ip', '=', $ip)->first();
            if (is_object($tentativa)) {
                if ($tentativa->qtd > 10) {
                    return false;
                } else {
                    return true;
                }
            } else {
                $tentativa = new \TentativaAcesso();
                $tentativa->ip = $ip;
                $tentativa->qtd = 0;
                $tentativa->save();
                return true;
            }
        }
    }

    public function registraTentativaComSucesso(\Usuario $usuario, $ip) {
        $tentativa = \TentativaAcesso::where('ip', '=', $ip)->first();
        if (is_object($tentativa)) {
            $tentativa->qtd = 0;
            $tentativa->save();
        } else {
            $tentativa = new \TentativaAcesso();
            $tentativa->ip = $ip;
            $tentativa->qtd = 0;
            $tentativa->save();
        }
        $usuario->atualizaUltimoAcesso();
    }

    public function registraTentativaSemSucesso($email, $ip) {
        $tentativa = \TentativaAcesso::where('ip', '=', $ip)->first();
        if (is_object($tentativa)) {
            $tentativa->qtd = $tentativa->qtd + 1;
            $tentativa->save();
        } else {
            $tentativa = new \TentativaAcesso();
            $tentativa->ip = $ip;
            $tentativa->qtd = 1;
            $tentativa->save();
        }

        $usuario = \Usuario::where('email', '=', $email)->first();
        if (is_object($usuario)) {
            if ($usuario->dthr_primeiro_erro == '') {
                $usuario->dthr_primeiro_erro = date('Y-m-d H:i:s');
            }
            $usuario->qtd_tentativas_erradas = $usuario->qtd_tentativas_erradas + 1;
            $usuario->save();
        }
    }

    public function notificaExcessoTentativas($email, $ip) {
        $data = array('email' => $email, 'ip' => $ip);

        $email = Config::get('isrc.notificar_em_caso_ataque_email');
        $nome = Config::get('isrc.notificar_em_caso_ataque_nome');

        \Mail::send('emails.usuario_bloqueado', $data, function($message) use($email, $nome) {
            $message->to($email, $nome)->subject('Som Livre ISRC - Usuário bloqueado');
        });
    }

}
