<?php
namespace ISRC\Validadores;
class Senha extends Validador {
    public $id_unique;
    public static $rules = array(
        'password'       => 'required',
    );
    public static $messages = array(
        'password.required'       => 'A senha é obrigatória',
    );

}
