<?php

//================================================
// Rotas públicas
//================================================
Route::controller('auth', 'AuthController');
Route::get('/', function() {
    return Redirect::to('auth/login');
});

//================================================
// Rotas restritas ao usuário autenticado
//================================================

Route::group(array('before' => 'usuario-externo'), function() {

    Route::controller('home', 'PublicaHomeController');

});

//================================================
// Rotas restritas ao usuário com acesso ao back-office
//================================================
Route::group(array('before' => 'backoffice', 'prefix' => 'admin'), function() {

    Route::get('/', function() {
        return Redirect::to('admin/home');
    });

    Route::controller('home', 'HomeController');

    Route::group(array('before' => 'empresa'), function() {

        Route::controller('pessoa', 'PessoaController');
        Route::controller('potpourri', 'PoutporritController');
        Route::controller('sociedade', 'SociedadeController');
        Route::controller('titular', 'TitularController');

        // Cadastros
        Route::controller('categoria', 'CategoriaController');
        Route::controller('contato', 'ContatoController');
        Route::controller('coletivo', 'ColetivoController');
        Route::controller('empresa', 'EmpresaController');
        Route::controller('endereco', 'EnderecoController');
        Route::controller('exportacao', 'ExportacaoController');
        Route::controller('fonograma', 'FonogramaController');
        Route::controller('fonogramaobras', 'FonogramaObrasController');
        Route::controller('fonogramapoutporrit', 'FonogramaPoutporritController');
        Route::controller('fonogramacoletivos', 'FonogramaColetivosController');
        Route::controller('fonogramatitulares', 'FonogramaTitularesController');
        Route::controller('genero', 'GeneroController');
        Route::controller('instrumento', 'InstrumentoController');
        Route::controller('obra', 'ObraController');
        Route::controller('obratitulares', 'ObraTitularesController');
        Route::controller('pais', 'PaisController');
        Route::controller('pseudonimo', 'PseudonimoController');
        Route::controller('telefone', 'TelefoneController');
        Route::controller('tipomidia', 'TipoMidiaController');
        Route::controller('tipoproduto', 'TipoProdutoController');

        // Controle de acesso
        Route::controller('perfil', 'PerfilController');
        Route::controller('usuario', 'UsuarioController');
    });
});

//================================================
// Rotas restritas ao back-office
//================================================
Route::group(array('before' => 'usuario-externo'), function() {

});

