<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGrupoInstrumento extends Migration {

    public function up() {
        Schema::create('grupo_instrumento', function($table) {
            $table->increments('id_grupo_instrumento');
            $table->string('nome', 45)->nullable();
            $table->date('data')->nullable();
            $table->string('ind_part_calc_musico_acomp', 1)->nullable();
            $table->boolean('fl_ind_part_calc_musico_acomp')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down() {
        if (Schema::hasTable('grupo_instrumento')) {
            Schema::drop('grupo_instrumento');
        }
    }
}
