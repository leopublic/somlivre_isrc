<?php

namespace ISRC\Servicos;

class ImportadorPoutporrit extends Importador{

    public function sqlSelectMdb() {
        return "select id_poutporrit"
                . ", nome"
                . ", cod_poutporrit"
                . ", cod_poutporrit_ECAD"
                . ", cod_IWC"
                . ", dt_inclusao"
                . ", dt_alteracao"
                . ", usuario "
                . " from ISRC_poutporrit_tb"
                . " where is_deleted = 0";
    }
    
    public function nome_id_local(){
        return 'id_poutporrit';
    }

    public function novoObjeto(){
        return new \Poutporrit();
    }

    public function tabela(){
        return "ISRC_poutporrit_tb";
    }

    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            'nome',
            'cod_poutporrit',
            'cod_poutporrit_ECAD',
            'cod_IWC',
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);

        $obj->usuario = $regMDB['usuario'];
        $obj->id_poutporrit_original = $regMDB['id_poutporrit'];
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        $obj->id_empresa = \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa;
        return $obj;
    }
}