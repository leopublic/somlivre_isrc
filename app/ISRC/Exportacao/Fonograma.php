<?php
namespace ISRC\Exportacao;
class Fonograma extends Exportacao{
	const tprDETALHE = '1';
	const tprTITULAR = '2';
	const tprINSTRUMENTO = '3';
	const tprCOLETIVO = '4';

	public static function nomeEntidade(){
		return '\Fonograma';
	}
	public function tipo_cadastro(){
		return 'FON';
	}

	public function exporta($fonograma){
		$this->abreTransacao();
		$this->detalhe($fonograma);
		$titulares = $fonograma->fonogramatitulares;
		foreach($titulares as $titular){
			if ($titular->id_coletivo > 0){
				$this->coletivo($fonograma, $titular->coletivo);
			}
			$this->titular($fonograma, $titular);
			$fonogramatitularinstrumentos = $titular->fonogramatitularinstrumentos;
			foreach($fonogramatitularinstrumentos as $fonogramatitularinstrumento){
				$this->instrumento($fonograma, $fonogramatitularinstrumento);
			}
		}
		$this->fechaTransacao();
	}

	public function detalhe($fonograma){
		$this->novo_reg(self::tpoINCLUIR, self::tprDETALHE);
		$this->adicionaCampo($fonograma->cod_fonograma_ECAD, 'N', 9, 'Código ECAD fonograma');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC fonograma');
		if (count($fonograma->obra) > 0){
			$this->adicionaCampo($fonograma->obra->cod_obra_ecad, 'N', 13, 'Código ECAD Obra');
			$this->adicionaCampo('', 'A', 15, 'Código ISRC da Obra');		
			$this->adicionaCampo('', 'N', 9, 'Código ECAD do poutporrit');
			$this->adicionaCampo('', 'A', 15, 'Código ISRC do poutporrit');			
		} elseif (count($fonograma->poutporrit) > 0) {
			$this->adicionaCampo('', 'N', 13, 'Código ECAD Obra');
			$this->adicionaCampo('', 'A', 15, 'Código ISRC Obra');		
			$this->adicionaCampo($fonograma->poutporrit->cod_poutporrit_ECAD, 'N', 9, 'Codigo ECAD poutporrit');
			$this->adicionaCampo('', 'A', 15, 'Codigo ISRC poutporrit');
		}
		$this->adicionaCampo($fonograma->cod_GRA, 'A', 8, 'Código GRA');
		$this->adicionaCampo($fonograma->cod_ISRC, 'A', 12, 'Código ISRC');

		$this->adicionaCampo($fonograma->dtFmtSemSep('dt_grav_original'), 'D', 8, 'Data da gravação original');
		$this->adicionaCampo($fonograma->dtFmtSemSep('dt_lancamento'), 'D', 8, 'Data do lançamento');
		$this->adicionaCampo($fonograma->dtFmtSemSep('dt_inclusao'), 'D', 8, 'Data de emissão do GRA');
		$this->adicionaCampo($fonograma->is_instrumental, 'B', 1, 'É instrumental');
		$this->adicionaCampo($fonograma->duracao_exportacao, 'N', 6, 'Duração');
		$this->adicionaCampo(0, 'N', 12, 'Código CRWOLEY');
		$this->adicionaCampo($fonograma->is_nacional, 'B', 1, 'É nacional');
		$this->adicionaCampo($fonograma->relacaoComDefault('paisorigem', 'sigla'), 'A', 2, 'Sigla do país de origem');
		$this->adicionaCampo($fonograma->relacaoComDefault('genero', 'cod_genero'), 'A', 10, 'Código do gênero');
		$this->adicionaCampo($fonograma->relacaoComDefault('tipofonograma', 'nome'), 'A', 3, 'Classificação do fonograma');
		$this->adicionaCampo(0, 'N', 6, 'Código do Selo');			
		$this->adicionaCampo('', 'A', 60, 'Nome do coletivo do fonograma');
		$this->adicionaCampo('', 'A', 10, 'Tipo de coletivo do fonograma');	
		$this->adicionaCampo('', 'A', 1, 'Coletivo é nacional');		
		$this->adicionaCampo('', 'A', 2, 'Sigla do país do coletivo');	
		$this->adicionaCampo($fonograma->relacaoComDefault('genero', 'nome'), 'A', 30, 'Nome do gênero');
		$this->adicionaCampo($fonograma->relacaoComDefault('paispublicacao', 'sigla'), 'A', 2, 'Sigla do país de publicação');
		$this->adicionaCampo($fonograma->relacaoComDefault('tipomidia', 'dsc_tipo_midia'), 'A', 6,'Tipo de mídia');
		$this->adicionaCampo(false, 'B', 1, 'É publicação simultanea');			//  
		$this->adicionaCampo('', 'A', 3, 'Código do arranjo');			//  Código do arranjo
		$this->adicionaCampo($fonograma->relacaoComDefault('arranjo', 'dsc_arranjo'), 'A', 50, 'Descrição do complemento do arranjo');

		$this->grava_registro();
	}

	public function titular($fonograma, $fonogramatitular){
		$this->novo_reg(self::tpoINCLUIR, self::tprTITULAR);

		if(count($fonogramatitular->titular) > 0){
			$titular = $fonogramatitular->titular;
			if (count($titular->pessoa)> 0){
				$pessoa = $titular->pessoa;
			} else {
				$pessoa = new \Pessoa;
			}
		} else {
			$titular = new \Titular;
			$pessoa = new \Pessoa;
		}


		$this->adicionaCampo($fonograma->cod_fonograma_ECAD, 'N', 9, 'Código ECAD fonograma');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC fonograma');			// Cod ISRC
		$this->adicionaCampo($pessoa->cod_ecad, 'N', 13, 'Código ECAD titular');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC titular');			// Cod ISRC
		$this->adicionaCampo($pessoa->nome_razao, 'A', 45, 'Nome do titular'); //
		$this->adicionaCampo($pessoa->tp_pessoa, 'A', 1, 'Tipo de pessoa'); //

		if ($pessoa->tp_pessoa == 'J'){
			$this->adicionaCampo(0, 'N', 11, 'CPF do titular');
			$this->adicionaCampo($pessoa->cpf_cnpj, 'N', 14, 'CNPJ do titular');
		} else {
			$this->adicionaCampo($pessoa->cpf_cnpj, 'N', 11, 'CPF do titular');
			$this->adicionaCampo(0, 'N', 14, 'CNPJ do titular');
		}
		$this->adicionaCampo($titular->cod_cae, 'N', 11, 'Código CAE do titular'); // Código CAE
		$this->adicionaCampo($fonogramatitular->relacaoComDefault('categoria', 'cod_categoria'), 'A', 2, 'Código da categoria do titular'); // Código categoria
		$this->adicionaCampo('', 'A', 2, 'Código da subcategoria'); // Código sub-categoria
		$this->adicionaCampo(true, 'B', 1, 'Interprete principal'); // Intérprete principal
		$pct = substr(str_replace(',', '', '00'.$fonogramatitular->pct_participacao_fmt), -10);
		$this->adicionaCampo($pct, 'N', 10, 'Percentual de participação do titular'); // Percentual participacao
		$this->adicionaCampo(true, 'B', 1, 'Titular é nacional'); // Titular é nacional
		$this->adicionaCampo('', 'A', 22, 'Sigla da sociedade'); // Sigla da sociedade
		$this->adicionaCampo($titular->nomepseudonimoprincipal(), 'A', 45, 'Nome do pseudônimo'); // Pseudônimo principal
		$this->adicionaCampo($fonogramatitular->relacaoComDefault('coletivo', 'cod_coletivo_ECAD'), 'A', 10, 'Código ECAD do coletivo'); // Código Ecad coletivo
		$this->adicionaCampo('', 'A', 10, 'Código ISRC do coletivo'); // Código ISRC coletivo

		$this->grava_registro();
	}

	public function instrumento($fonograma, $fonogramatitularinstrumento){
		$this->novo_reg(self::tpoINCLUIR, self::tprTITULAR);
		$this->adicionaCampo($fonograma->cod_fonograma_ECAD, 'N', 9, 'Código ECAD fonograma');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC fonograma');			// Cod ISRC
		$this->adicionaCampo($fonogramatitularinstrumento->fonogramatitular->titular->pessoa->cod_ecad, 'A', 13, 'Código ECAD titular');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC titular');			// Cod ISRC


		$this->adicionaCampo('', 'A', 4, 'Código instrumento');			// Cod instrumento
		$this->adicionaCampo($fonogramatitularinstrumento->relacaoComDefault('instrumento', 'nome'), 'A', 45, 'Nome do instrumento');			// Nome instrumento

		$this->grava_registro();
	}

	public function coletivo($fonograma, $coletivo){
		$this->novo_reg(self::tpoINCLUIR, self::tprCOLETIVO);
		$this->adicionaCampo($fonograma->cod_fonograma_ECAD, 'N', 9, 'Código ECAD fonograma');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC fonograma');			// Cod ISRC
		$this->adicionaCampo($coletivo->cod_coletivo_ECAD, 'N', 10, 'Código ECAD coletivo ');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC coletivo');			// Cod ISRC
		$this->grava_registro();
	}
}