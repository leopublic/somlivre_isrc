<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTipoMidia extends Migration {

    public function up() {
        Schema::create('tipo_midia', function($table) {
            $table->increments('id_tipo_midia');
            $table->string('dsc_tipo_midia', 100)->nullable();
            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down() {
        if (Schema::hasTable('tipo_midia')) {
            Schema::drop('tipo_midia');
        }
    }

}
