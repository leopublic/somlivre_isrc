<?php

namespace ISRC\Repositorios;

class RepositorioExportacao extends Repositorio {
	// Cria uma nova exportação
	public function exportar($id_empresa){
		$empresa = \Empresa::find($id_empresa);

		$exp = new \ISRC\Exportacao\Exportacao($empresa);
		$exp->abreArquivo();
		$exp->header();
		//Exporta coletivos
		$coletivos = \Coletivo::where('id_empresa', '=', $id_empresa)
					->whereRaw("coalesce(cod_coletivo_ECAD, '') = ''")
					->get();
		$expColetivo = new \ISRC\Exportacao\Coletivo;
		foreach($coletivos as $coletivo){
			$expColetivo->exporta($coletivo);
		}
		//Exporta titulares
		$titulares = \Titular::where('id_empresa', '=', $id_empresa)
					->whereRaw("id_pessoa in (select id_pessoa from pessoa where coalesce(cod_ecad, '') = '')")
					->get();
		$expTitular = new \ISRC\Exportacao\Titular;
		foreach($titulares as $titular){
			$expTitular->exporta($titular);
		}
		//Exporta obras
		$obras = \Obra::where('id_empresa', '=', $id_empresa)
					->whereRaw(" coalesce(cod_obra_ecad, '') = '')")
					->get();
		$expObra = new \ISRC\Exportacao\Obra;
		foreach($obras as $obra){
			$expObra->exporta($obra);
		}
		//Exporta fonogramas
		$fonogramas = \Fonograma::where('id_empresa', '=', $id_empresa)
					->whereRaw("coalesce(cod_fonograma_ECAD, '') = '')")
					->get();
		$expFonograma = new \ISRC\Exportacao\Fonograma;
		foreach($fonogramas as $fonograma){
			$expFonograma->exporta($fonograma);
		}

		$exp->fechaArquivo();
	}

	/**
	 * Cria uma nova exportação a partir do id da empresa
	 * @param  [type] $id_empresa [description]
	 * @return \Exportacao
	 */
	public function nova($id_empresa, $id_usuario){
		$exp = new \Exportacao;
		$exp->id_empresa = $id_empresa;
		$exp->id_usuario = $id_usuario;
		$exp->id_status_exportacao = 1;
		$exp->save();
		$exp->atribuiProximoSequencial();
		return $exp;
	}

	public function processar($exp){
		$expt = new \ISRC\Exportacao\Exportacao($exp->empresa);
		$expt->abreArquivo();
		$expt->header();

		$entidades = \Titular::where('id_empresa', '=', $exp->id_empresa)
						->where('exportar', '=', 1)
						->get();
		$expEnt = new \ISRC\Exportacao\Titular;
		$expEnt->exportaCursor($entidades);

		$entidades = \Coletivo::where('id_empresa', '=', $exp->id_empresa)
						->where('exportar', '=', 1)
						->get();
		$expEnt = new \ISRC\Exportacao\Coletivo;
		$expEnt->exportaCursor($entidades);

		$entidades = \Poutporrit::where('id_empresa', '=', $exp->id_empresa)
						->where('exportar', '=', 1)
						->get();
		$expEnt = new \ISRC\Exportacao\Poutporrit;
		$expEnt->exportaCursor($entidades);

		$entidades = \Obra::where('id_empresa', '=', $exp->id_empresa)
						->where('exportar', '=', 1)
						->get();
		$expEnt = new \ISRC\Exportacao\Obra;
		$expEnt->exportaCursor($entidades);

		$entidades = \Fonograma::where('id_empresa', '=', $exp->id_empresa)
						->where('exportar', '=', 1)
						->get();
		$expEnt = new \ISRC\Exportacao\Fonograma;
		$expEnt->exportaCursor($entidades);

		$expt->trailler();
		$expt->fechaArquivo();
	}
}