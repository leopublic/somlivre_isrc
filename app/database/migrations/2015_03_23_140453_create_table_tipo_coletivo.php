<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTipoColetivo extends Migration {

    public function up() {
        Schema::create('tipo_coletivo', function($table) {
            $table->increments('id_tipo_coletivo');
            $table->string('descricao', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down() {
        if (Schema::hasTable('tipo_coletivo')) {
            Schema::drop('tipo_coletivo');
        }
    }

}
