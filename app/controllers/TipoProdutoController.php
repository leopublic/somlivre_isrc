<?php

class TipoProdutoController extends CadastroController {
    public function repo(){
        return new \ISRC\Repositorios\RepositorioTipoProduto();
    }
    
    public function diretorioViews() {
        return 'tipoproduto';
    }
    
    public function raizRota() {
        return '/admin/tipoproduto';
    }
    
    public function msgSucesso(){
        return 'Tipo de produto atualizado com sucesso';
    }    

}
