@extends('base')

@section('estilos')
@stop

@section('conteudo')
<div class="row">
    <div class="col-md-12">
        <h3 class="page-title">{{$titulo}}</h3>
    </div>
</div>
@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-gift"></i> Alterar perfil</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
            {{Form::hidden('id_perfil', $objeto->id_perfil)}}
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Nome</label>
                    <div class="col-md-9">
                        {{Form::text('descricao', Input::old('descricao', $objeto->descricao), array("class" => "form-control"))}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Acessa backoffice?</label>
                    <div class="col-md-9">
                        @include('_padroes.radio_sim_nao', array('nome'=>'backoffice', 'modelo' => $objeto) ) 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Observações</label>
                    <div class="col-md-9">
                        {{Form::textarea('observacao', Input::old('observacao', $objeto->observacao), array("class" => "form-control"))}}
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">Salvar</button>
                    <a href="{{URL::to('/admin/perfil')}}" class="btn default">Cancelar</a>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
@stop
@section('scripts')
$('#menuAcesso').addClass('active');
$('#menuAcesso').addClass('open');
$('#itemPerfis').addClass('active');
@stop
