<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Adicionar titular</h4>
</div>
<div class="modal-body">
    {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "fmrTitularPesquisar"))}}
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-md-3 control-label">Nome</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            {{Form::text('nome_razao', $nome_razao, array("class" => "form-control", "id"=> 'nome_razao', "onkeypress" =>"return noenter()"))}}
                            <span class="input-group-btn">
                                <a  class="btn blue" href="javascript:btnTitularPesquisarClick();">Pesquisar</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}
    <div class="row">
        <div class="col-md-12">
            <table class="table table-stripped table-condensed table-hover">
                <colgroup>
                    <col style="width:80px;"/>
                    <col style="width:40px;"/>
                    <col style="width:auto;"/>
                </colgroup>
                <thead>
                    <tr>
                        <th class="center">Ações</th>
                        <th class="center">Id</th>
                        <th class="left">Titular</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($titulares as $titular)
                    <tr>
                        <td class="center">
                            <a onClick="javascript:getAdicionarTitular({{$id_fonograma}},{{$titular->id_titular}});" class="btn btn-xs blue" title="Incluir esse titular no fonograma"><i class="fa fa-plus"></i></a>
                        </td>
                        <td>{{$titular->id_titular}}</td>
                        <td style="text-align:left">{{$titular->nome_razao}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{$msg_limite}}
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
</div>
