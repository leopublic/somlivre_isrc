@extends('base')

@section('estilos')
@stop

@section('conteudo')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-gift"></i> Alterar titular</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
            {{Form::hidden('id_pessoa', $objeto->id_pessoa, array('id'=> 'id_pessoa'))}}
            <div class="form-body">
                @include('_campos.texto_id', array('valor'=> $reg->id_pessoa))
                @include('_campos.texto', array('label'=> 'Nome', 'nome'=>'nome_razao'))
            </div>
            <div class="form-actions fluid">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">Salvar</button>
                    <a href="{{URL::to('/admin/titular')}}" class="btn default">Cancelar</a>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
</div>
@stop