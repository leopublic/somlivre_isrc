<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGenero extends Migration {

    public function up() {
        Schema::create('genero', function($table) {
            $table->increments('id_genero');
            $table->string('nome', 100)->nullable();
            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->integer('cod_genero')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down() {
        if (Schema::hasTable('genero')) {
            Schema::drop('genero');
        }
    }

}
