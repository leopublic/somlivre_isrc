<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUf extends Migration {
    public function up() {
        Schema::create('uf', function($table) {
            $table->increments('id_uf');
            $table->string('sigla', 2);
            $table->string('descricao', 200);
            $table->timestamps();
        });

		$obj = array("descricao" => "Acre", "sigla" =>"AC");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Alagoas", "sigla" =>"AL");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Amapá", "sigla" =>"AP");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Amazônia", "sigla" =>"AM");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Bahia", "sigla" =>"BA");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Ceará", "sigla" =>"CE");
        DB::table('uf')->insert($obj);
        $obj = array("descricao" => "Distrito Federal", "sigla" =>"DF");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Espírito Santo", "sigla" =>"ES");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Goiás", "sigla" =>"GO");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Maranhão", "sigla" =>"MA");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Mato Grosso", "sigla" =>"MT");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Mato Grosso do Sul", "sigla" =>"MS");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Minas Gerais", "sigla" =>"MG");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Pará", "sigla" =>"PA");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Paraíba", "sigla" =>"PB");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Paraná", "sigla" =>"PR");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Pernambuco", "sigla" =>"PE");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Piauí", "sigla" =>"PI");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Rio de Janeiro", "sigla" =>"RJ");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Rio Grande do Norte", "sigla" =>"RN");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Rio Grande do Sul", "sigla" =>"RS");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Rondônia", "sigla" =>"RD");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Roraima", "sigla" =>"RR");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Santa Catarina", "sigla" =>"SC");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "São Paulo", "sigla" =>"SP");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Sergipe", "sigla" =>"SE");
        DB::table('uf')->insert($obj);
		$obj = array("descricao" => "Tocantins", "sigla" =>"TO");
        DB::table('uf')->insert($obj);
    }

    public function down() {
        Schema::dropIfExists('uf');
    }
}
