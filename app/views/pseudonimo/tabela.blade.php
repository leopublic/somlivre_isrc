<table class="table table-stripped table-condensed table-hover">
    <colgroup>
        <col style="width:80px;"/>
        <col style="width:80px;"/>
        <col style="width:80px;"/>
        <col style="width:auto;"/>
    </colgroup>
    <thead>
        <tr>
            <th class="center">Ações</th>
            <th class="center">Id</th>
            <th class="center">Principal?</th>
            <th class="left">Pseudônimo</th>
        </tr>
    </thead>
    <tbody>
        @foreach($regs as $reg)
        <tr>
            <td class="center">
                <a href="#" onclick="btnPseudonimoAdicionarClick('{{$reg->id_pseudonimo}}', '{{$reg->pseudonimo}}', '{{$reg->is_principal}}')" class="btn btn-sm green-seagreen" title="Editar"><i class="fa fa-edit"></i></a>
            </td>
            <td class="center">{{$reg->id_pseudonimo}}</td>
            <td class="center">@if ($reg->is_principal) <i class="fa fa-check"></i>@else &nbsp;@endif</td>
            <td>{{$reg->pseudonimo}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
