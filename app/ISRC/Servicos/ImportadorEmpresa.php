<?php

namespace ISRC\Servicos;

class ImportadorEmpresa extends Importador{

    public function sqlSelectMdb() {
        return "select  id_parametro,
            id_produtor_fonografico,
            id_pais,
            sgl_IFPI,
            ano_base,
            seq_ISRC,
            dt_inclusao,
            dt_alteracao,
            usuario,
            check, 
            versao
            from ISRC_parametro_tb where is_deleted = 0";
    }
    
    public function novoObjeto(){
        return new \Empresa();
    }
    
    public function nome_id_local(){
        return '';
    }
            
    public function tabela(){
        return "ISRC_parametro_tb";
    }

    
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = \Empresa::where('sgl_IFPI', '=', $regMDB['sgl_IFPI'])->first();
        if (!is_object($obj)){
            $obj = $this->novoObjeto();
        }
        $campos = array(
            'id_produtor_fonografico',
            'id_pais',
            'sgl_IFPI',
            'ano_base',
            'seq_ISRC',
            'versao'
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos);

        $obj->usuario = $regMDB['usuario'];
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        return $obj;
    }
}