<?php

namespace ISRC\Servicos;

class ImportadorTitular extends Importador{

    public function sqlSelectMdb() {
        return "select id_titular, id_sociedade, is_nacional, id_pessoa, cod_cae, cod_omb, dt_inclusao, dt_alteracao, usuario from ISRC_titular_tb where is_deleted = 0";
    }
    
    public function novoObjeto(){
        return new \Titular();
    }

    public function nome_id_local(){
        return 'id_titular';
    }

    public function tabela(){
        return "ISRC_titular_tb";
    }
        
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            // 'id_pessoa', 
            // 'id_sociedade', 
            'is_nacional', 
            'cod_cae', 
            'cod_omb', 
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);


        $pessoa = \Pessoa::where('id_empresa', '=', \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa)
                        ->where('id_pessoa_original', '=', $regMDB['id_pessoa'])
                        ->first();
        if (count($pessoa) > 0){
            $obj->id_pessoa = $pessoa->id_pessoa;
        } else {
            \Log::error('Titular (id='.$regMDB['id_titular'].' empresa='.\ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa.'): Pessoa não encontrada id='.$regMDB['id_pessoa']);
        }


        $sociedade = \Sociedade::where('id_empresa', '=', \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa)
                        ->where('id_sociedade_original', '=', $regMDB['id_sociedade'])
                        ->first();
        if (count($sociedade) > 0){
            $obj->id_sociedade = $sociedade->id_sociedade;
        } else {
            \Log::error('Titular (id='.$regMDB['id_titular'].' empresa='.\ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa.'): sociedade não encontrada id='.$regMDB['id_sociedade']);
        }

        $obj->id_titular_original = $regMDB['id_titular'];
        $obj->usuario = $regMDB['usuario'];
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        $obj->id_empresa = \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa;
        return $obj;
    }
}