<?php

namespace ISRC\Repositorios;

class RepositorioTitular extends RepositorioCadastro {
    
    public function nomeModelo(){
        return '\Titular';
    }
    
    public function ordenacaoPadrao(){
        return 'nome_razao';
    }
    
    
    public function registrosConsulta($filtros = null){
        if (key_exists('filtro', $filtros)) {
            $filtro = $filtros['filtro'];
        } else {
            $filtro = '';
        }

    	return \Titular::wherefiltro($filtro)
                ->paginate(20);    
    }

    public function Excluir($id){
        $titular = \Titular::find($id);
        \Pessoa::where('id_pessoa', '=', $titular->id_pessoa)->delete();
        \Titular::where('id_titular', '=', $id)->delete();
        return true;
    }    
}
