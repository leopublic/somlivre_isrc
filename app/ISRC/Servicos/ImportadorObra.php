<?php

namespace ISRC\Servicos;

class ImportadorObra extends Importador{

    public function sqlSelectMdb() {
        return "select id_obra, id_genero, titulo_original, cod_obra, cod_obra_ecad, subtitulo, versao, cod_ISWC, is_nacional, is_instrumental, dt_inclusao, dt_alteracao, usuario from ISRC_obra_tb where is_deleted = 0";
    }

    public function novoObjeto(){
        return new \Obra();
    }
    
    public function nome_id_local(){
        return 'id_obra';
    }

    public function tabela(){
        return "ISRC_obra_tb";
    }

    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            'id_genero',
            'titulo_original',
            'cod_obra',
            'cod_obra_ecad',
            'subtitulo',
            'versao',
            'cod_ISWC',
            'is_nacional',
            'is_instrumental',
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);
        $obj->id_obra_original = $regMDB['id_obra'];
        $obj->usuario = $regMDB['usuario'];
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        $obj->id_empresa = \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa;
        return $obj;
    }
}