@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.titulo_cadastro', array('titulo'=> 'Coletivos', 'rota'=>'coletivo'))
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>Registros encontrados
            </div>
        </div>
        <div class="portlet-body">
            {{Form::open()}}
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group">
                        <input type="search" id="filtro" name="filtro" class="form-control" value="{{Input::old('filtro')}}">
                        <span class="input-group-btn"><button type="submit" class="btn">Buscar</button></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">{{$regs->appends(Input::get())->links()}}</div>
            </div>
            {{Form::close()}}
            <div class="table-scrollable">
                @include('_padroes.table')
                <thead>
                    <tr>
                        <th class="center">Ações</th>
                        <th class="center">Id</th>
                        <th class="left">Nome</th>
                        <th class="left">Integrantes</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($regs as $reg)
                    <tr>
                        <td class="center">
                            <a href="{{URL::to('/admin/coletivo/edit/'.$reg->id_coletivo)}}" class="btn btn-sm green-seagreen" title="Editar"><i class="fa fa-edit"></i></a>
                        </td>
                        <td class="center">{{$reg->id_coletivo}}</td>
                        <td>{{ $reg->nome }}
                        @if ($reg->pseudonimo)
                            <br/><span class="subitem">{{$reg->pseudonimo}}</span>
                        @endif
                        </td>
                        <td>
                        <? $br = '';?>
                        @if($reg->integrantes)
                            @foreach ($reg->integrantes as $integrante)
                                {{$br.$integrante->titular->pessoa->nome_razao}}
                                <? $br = '<br/>';?>
                            @endforeach
                        @else 
                        --
                        @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END BORDERED TABLE PORTLET-->
</div>
@stop

@section('scripts')
    $('#menuCadastros').addClass('active');
    $('#menuCadastros').addClass('open');
    $('#menuCadastros').addClass('selected');
    $('#itemColetivos').addClass('active');
@stop
