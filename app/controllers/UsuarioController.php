<?php

class UsuarioController extends BaseController {
    
    public function getIndex() {
        $rep = new \ISRC\Repositorios\RepositorioUsuario();
        $usuarios = $rep->registrosConsulta();

        return View::make('usuario.index')
                ->with('registros', $usuarios);
    }

    public function getEdit($id_usuario = 0) {
        $perfis = \UsuarioPerfil::perfisDoUsuarioParaEdicao($id_usuario);
        if($id_usuario > 0){
            $usuario = \Usuario::find($id_usuario);
        } else {
            $usuario = new \Usuario;
        }

        return View::make('usuario.edit')
                ->with('usuario', $usuario)
                ->with('perfis', $perfis)
            ;
    }
    
    public function postEdit($id_usuario = ''){
        $post = Input::all();
        try{
            $rep = new \ISRC\Repositorios\RepositorioUsuario();
            $usuario = $rep->salve($post);
            Session::flash('msg', 'Usuário atualizado com sucesso');
            return Redirect::to('/admin/usuario/edit/'.$usuario->id_usuario);
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', $ex->getMessage());
            return Redirect::to('/admin/usuario/edit/'.$post['id_usuario']);
        }
    }

}
