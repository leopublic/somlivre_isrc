<?php

class CategoriaController extends CadastroController {
    public function repo(){
        return new \ISRC\Repositorios\RepositorioCategoria;
    }
    
    public function diretorioViews() {
        return 'categoria';
    }
    
    public function raizRota() {
        return '/admin/categoria';
    }
    
    public function msgSucesso(){
        return 'Categoria atualizada com sucesso';
    }    
}
