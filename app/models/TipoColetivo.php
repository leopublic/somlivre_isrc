<?php
/**
 * @property int $id_tipo_coletivo
 * @property string $descricao Nome 
 * @property datetime created_at 
 * @property datetime updated_at 
 * 
 */
class TipoColetivo extends Modelo {
    protected $table = 'tipo_coletivo';
    protected $primaryKey = 'id_tipo_coletivo';
    protected $guarded = array();

    public function carregaPeloMDB($registro){
        
    }
    
    public static function combo($filtro = ''){
        return \TipoColetivo::orderBy('descricao')->lists('descricao', 'id_tipo_coletivo');
    }
    
}