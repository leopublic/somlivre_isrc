<?php
class HomeController extends BaseController {
    public function getIndex() {
        return View::make('base');
    }

    public function getEmpresa(){
        return View::make('home_empresas');
    }

    public function getSelecioneempresa($id_empresa){

        $user = Usuario::find(\Auth::user()->id_usuario);
        $user->id_empresa = $id_empresa;
        $user->save();
        \Auth::setUser($user);

        return Redirect::to(URL::previous());
    }

}
