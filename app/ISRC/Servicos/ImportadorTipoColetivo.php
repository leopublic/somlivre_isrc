<?php

namespace ISRC\Servicos;

class ImportadorTipoColetivo extends Importador{

    public function sqlSelectMdb() {
        return "select ID_COLETIVO_TIPO, DESCRICAO from COLETIVO_TIPO where id_coletivo_tipo > 0";
    }
    
    public function novoObjeto(){
        return new \TipoColetivo();
    }
            
    public function tabela(){
        return "COLETIVO_TIPO";
    }
        
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        if ($preservar_id){
            $obj->id_tipo_coletivo   = $regMDB['ID_COLETIVO_TIPO'];
        }
        $obj->descricao       = $regMDB['DESCRICAO'];
        return $obj;
    }
}