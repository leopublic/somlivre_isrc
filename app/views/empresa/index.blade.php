@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.titulo_cadastro', array('titulo'=> 'Empresas', 'rota'=>'empresa'))

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>Registros encontrados
                </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-stripped table-condensed table-hover">
                        <thead>
                            <tr>
                                <th class="center">Ações</th>
                                <th class="center">Id</th>
                                <th class="left">Nome</th>
                                <th class="center">Prefixo</th>
                                <th class="center">Sigla</th>
                                <th class="center">Ano base</th>
                                <th class="center">Último ISRC</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($regs as $reg)
                            <tr>
                                <td class="center">
                                    <a href="{{URL::to('/admin/empresa/edit/'.$reg->id_empresa)}}" class="btn btn-sm green-seagreen" title="Editar"><i class="fa fa-edit"></i></a>
                                </td>
                                <td class="center">{{$reg->id_empresa}}</td>
                                <td>{{$reg->nome}}</td>
                                <td class="center">{{$reg->sgl_radical_IFPI}}</td>
                                <td class="center">{{$reg->sgl_IFPI}}</td>
                                <td class="center">{{$reg->ano_base}}</td>
                                <td class="center">{{$reg->seq_ISRC}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END BORDERED TABLE PORTLET-->
    </div>
</div>
@stop
@section('scripts')
$('#itemEmpresas').addClass('active');
@stop
