<?php

namespace ISRC\Servicos;

class ImportadorInstrumento extends Importador{

    public function sqlSelectMdb() {
        return "select id_instrumento, id_grupo_instrumento, nome, dt_inclusao, dt_alteracao, usuario from ISRC_instrumento_tb where is_deleted = 0";
    }
    
    public function novoObjeto(){
        return new \Instrumento();
    }
    
    public function nome_id_local(){
        return 'id_instrumento';
    }
            
    public function tabela(){
        return "ISRC_instrumento_tb";
    }
        
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            'nome',
            'id_grupo_instrumento',
            'usuario',
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        return $obj;
    }
}