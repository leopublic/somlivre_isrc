<?php
namespace ISRC\Repositorios;

class RepositorioEndereco extends RepositorioCadastro {

    public function nomeModelo(){
        return '\Endereco';
    }

    public function ordenacaoPadrao(){
        return 'endereco';
    }

    public function excluir($id_endereco){
        \Endereco::where('id_endereco', '=', $id_endereco)->delete();
    }
}