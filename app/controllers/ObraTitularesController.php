<?php

class ObraTitularesController extends BaseController {

    public function getAdd($id_fonograma, $id_titular) {
        $fono_titular = new \FonogramaTitular;
        $fono_titular->id_fonograma = $id_fonograma;
        $fono_titular->id_titular = $id_titular;
    }

    public function getGridresultadopesquisa($id_obra, $pesquisar = '') {
        if ($pesquisar != '') {
            $pesquisar = str_replace('*', '%', $pesquisar);
            $sql_count = "select count(*) as qtd "
                    . " from titular"
                    . " join pessoa on pessoa.id_pessoa = titular.id_pessoa"
                    . " left join titular_pseudonimo on titular_pseudonimo.id_titular = titular.id_titular"
                    . " where nome_razao like ? or pseudonimo like ?";

            $sql = "select titular.id_titular, pessoa.nome_razao, pseudonimo "
                    . " from titular"
                    . " join pessoa on pessoa.id_pessoa = titular.id_pessoa"
                    . " left join titular_pseudonimo on titular_pseudonimo.id_titular = titular.id_titular"
                    . " where nome_razao like ? or pseudonimo like ?"
                    . " order by nome_razao"
                    . " limit 15";

            $res_quantos = \DB::select($sql_count, array('%' . $pesquisar . '%', '%' . $pesquisar . '%'));
            $rs_quantos = $res_quantos[0];
            $titulares = \DB::select($sql, array('%' . $pesquisar . '%', '%' . $pesquisar . '%'));
            if ($rs_quantos->qtd > 15) {
                $ignorados = $rs_quantos->qtd - 15;
                $msg_limite = '(' . $ignorados . ' resultados não exibidos. Refine sua pesquisa)';
            } else {
                $msg_limite = '';
            }
        } else {
            $quantos = 0;
            $msg_limite = '';
            $titulares = array();
        }
        return View::make('obra.titulares_pesquisa_novo')
                        ->with('id_obra', $id_obra)
                        ->with('titulares', $titulares)
                        ->with('msg_limite', $msg_limite)
        ;
    }

    public function getAdicionar($id_obra, $id_titular) {
        $categorias = \Categoria::comboComDefault('(selecione...)');
        $titular = \Titular::find($id_titular);
        $pseudonimos = $titular->combopseudonimos();
        $obra_titular = new \ObraTitular;
        $obra_titular->id_obra = $id_obra;
        $obra_titular->id_obra_titular = 0;
        $obra_titular->id_titular = $id_titular;
        return View::make('obra.titulares_adicionar')
                        ->with('titular', $titular)
                        ->with('categorias', $categorias)
                        ->with('pseudonimos', $pseudonimos)
                        ->with('obra_titular', $obra_titular)
                        ->with('titulo', 'Adicionar titular')
        ;
    }

    public function getAdicionarnovo($i, $id_obra, $id_titular) {
        $categorias = \Categoria::comboComDefault('(selecione...)');
        $titular = \Titular::find($id_titular);
        $obra_titular = new \ObraTitular;
        $obra_titular->id_obra = $id_obra;
        $obra_titular->id_obra_titular = 0;
        $obra_titular->id_titular = $id_titular;
        return View::make('obra.linha_titular')
                        ->with('titular', $titular)
                        ->with('categorias', $categorias)
                        ->with('obra_titular', $obra_titular)
                        ->with('i', $i)
                        ->with('titulo', 'Adicionar titular')
        ;
    }

    public function getEditarold($id_obra_titular) {
        $obra_titular = \ObraTitular::find($id_obra_titular);

        $categorias = \Categoria::comboComDefault('(selecione...)');
        $titular = $obra_titular->titular;
        $pseudonimos = $titular->combopseudonimos();
        return View::make('obra.titulares_adicionar')
                        ->with('titular', $titular)
                        ->with('categorias', $categorias)
                        ->with('pseudonimos', $pseudonimos)
                        ->with('obra_titular', $obra_titular)
                        ->with('titulo', 'Editar titular')
        ;
    }

    public function getEditar($id_obra){
        $obra = \Obra::find($id_obra);
        $obra_titulares = $obra->obratitulares;
        $categorias = \Categoria::comboComDefault('(selecione...)');

        $data = array(
                'obra'              => $obra
            ,   'obra_titulares'    => $obra_titulares
            ,   'categorias'        => $categorias
            ,   'titulo'            => 'Editar titulares da obra'
            );
        return View::make('obra.titulares_edit', $data);
    }

    public function postEditar($id_obra){
        $ret = array();
        try{
            $all = Input::all();
            $qtd = Input::get('qtd');
            if ($qtd > 0){
                for ($i=0; $i < $qtd; $i++) {
                    $id_obra_titular = Input::get('id_'.$i);
                    $id_titular = Input::get('id_titular_'.$i);
                    $id_categoria = Input::get('id_categoria_'.$i);
                    $id_pseudonimo = Input::get('id_pseudonimo_'.$i);
                    $pct_autoral = Input::get('pct_autoral_'.$i);
                    if ($id_obra_titular > 0){
                        $obratitular = ObraTitular::find($id_obra_titular);
                    } else {
                        $obratitular = new ObraTitular;
                        $obratitular->id_obra = Input::get('id_obra');
                    }
                    $obratitular->id_titular = $id_titular;
                    $obratitular->id_categoria = $id_categoria;
                    $obratitular->id_pseudonimo = $id_pseudonimo;
                    $obratitular->pct_autoral_fmt = $pct_autoral;
                    $obratitular->save();
                }
            }
            $ret['status'] = '1';
        } catch(Exception $e){
            $ret['status'] = '-1';
            $ret['erro'] = $e->getMessage().'<br/><br/>'.$e->getTraceAsString();
        }
        return json_encode($ret);
    }

    public function postAdicionar() {
        $ret = array();
        try {
            $rep = new \ISRC\Repositorios\RepositorioObra;
            $post = Input::all();
            $obra_titular = $rep->adicionarTitular($post);
            $ret['codret'] = 0;
            $ret['msg'] = 'Titular adicionado com sucesso!';
            $reg = \Obra::find(Input::get('id_obra'));
            $obra_titulares = $reg->obratitulares;
            $html = View::make('obra.titulares_tabela')
                    ->with('obra_titulares', $obra_titulares)
                    ->with('msg', 'Titular adicionado com sucesso')
            ;

            $ret['html'] = iconv('UTF-8', 'UTF-8', $html);
            $ret['id_obra_titular'] = $obra_titular->id_obra_titular;
        } catch (Exception $ex) {
            $ret['codret'] = -1;
            $ret['msg'] = "Não foi possível adicionar esse titular: " . $ex->getMessage();
        }
        return json_encode($ret);
    }

    public function postExcluir(){
        $id_obra_titular = Input::get('id_obra_titular');
        $ret = array();
        try {
            $obra_tit = \ObraTitular::find($id_obra_titular);
            $reg = \Obra::find($obra_tit->id_obra);
            $obra_tit->delete();
            $obra_titulares = $reg->obratitulares;

            $html = View::make('obra.titulares_tabela')
                    ->with('obra_titulares', $obra_titulares)
                    ->with('msg', 'Titular excluído com sucesso')
            ;
            $ret['html'] = iconv('UTF-8', 'UTF-8', $html);
            $ret['msg'] = '';
            $ret['codret'] = 0;
        } catch (Exception $ex) {
            $ret['codret'] = -1;
            $ret['msg'] = "Não foi possível excluir esse titular: " . $ex->getMessage();
        }
        return json_encode($ret);
    }
}
