<?php

namespace ISRC\Servicos;

class ImportadorCategoria extends Importador{

    public function sqlSelectMdb() {
        return "select id_categoria
                    ,cod_categoria
                    ,desc_categoria
                    ,tp_direito
                    ,pct_max
                    ,dt_inclusao
                    ,dt_alteracao
                    ,usuario
                    ,is_deleted
                    from ISRC_categoria_tb";
    }
    
    public function novoObjeto(){
        return new \Categoria();
    }
    
    public function nome_id_local(){
        return 'id_categoria';
    }
            
    public function tabela(){
        return "ISRC_categoria_tb";
    }
        
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            'cod_categoria',
            'desc_categoria',
            'tp_direito',
            'pct_max',
            'usuario'
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);

        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        return $obj;
    }
}