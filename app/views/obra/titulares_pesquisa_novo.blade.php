<table class="table table-stripped table-condensed table-hover">
    <colgroup>
        <col style="width:60px;"/>
        <col style="width:40px;"/>
        <col style="width:auto;"/>
    </colgroup>
    <thead>
        <tr>
            <th class="center">Ações</th>
            <th class="center">Id</th>
            <th class="left">Titular</th>
        </tr>
    </thead>
    <tbody>
        @foreach($titulares as $titular)
        <tr>
            <td class="center">
                <a onClick="javascript:getAdicionarTitular({{$id_obra}},{{$titular->id_titular}});" class="btn btn-xs blue" title="Incluir esse titular na obra"><i class="fa fa-plus"></i></a>
            </td>
            <td>{{$titular->id_titular}}</td>
            <td style="text-align:left">{{$titular->nome_razao}}@if($titular->pseudonimo)<br/><span style="color:#999999; font-style:italic; ">{{$titular->pseudonimo}}</span>@endif</td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$msg_limite}}
