<?php
namespace ISRC\Validadores;
class TipoProduto extends Validador {
    public $id_unique;
    public static $rules = array(
        'nome'       => 'required',
    );
    public static $messages = array(
        'nome.required'       => 'O nome do tipo de produto é obrigatório',
        'nome.unique'       => 'Já existe outro tipo de produto com esse nome',
    );
    public function getRules(){
        $y = array_merge(static::$rules , array('nome' => 'required|unique:tipo_produto,nome,'.$this->id_unique.",id_tipo_produto"));
        return $y;
    }
}