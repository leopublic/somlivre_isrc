<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Adicionar titular</h4>
</div>
<div class="modal-body">
    {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "form_titular"))}}
    {{Form::hidden('id_obra_titular', $obra_titular->id_obra_titular)}}
    {{Form::hidden('id_obra', $obra_titular->id_obra)}}
    {{Form::hidden('id_titular', $obra_titular->id_titular)}}
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-md-3 control-label">Titular</label>
                    <div class="col-md-9">
                        {{Form::text('nome_razao', $titular->pessoa->nome_razao, array("class" => "form-control", 'disabled'))}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Pseudônimo</label>
                    <div class="col-md-9">
                        {{Form::select('id_pseudonimo', $pseudonimos, Input::old('id_pseudonimo', $obra_titular->id_pseudonimo), array("class" => "form-control select2"))}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Categoria</label>
                    <div class="col-md-9">
                        {{Form::select('id_categoria', $categorias, Input::old('id_categoria', $obra_titular->id_categoria), array("class" => "form-control select2"))}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">% autoral</label>
                    <div class="col-md-9">
                        {{Form::text('pct_autoral_fmt', $obra_titular->pct_autoral_fmt, array("class" => "form-control"))}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}
    <div id="modal-msg" style="color:red;"></div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
    <button type="button" class="btn blue" onclick="javascript:postAtualizaTitular();">Adicionar</button>
</div>
