<?php

class PaisController extends BaseController {
    public function getIndex() {
        $rep = new \ISRC\Repositorios\RepositorioPais;
        $paises = $rep->registrosConsulta();

        return View::make('pais.index')
                ->with('registros', $paises);
    }

    public function getEdit($id = 0) {
        $rep = new \ISRC\Repositorios\RepositorioPais();
        $objeto = $rep->instanciaEditado($id);
        $titulo = $rep->tituloTelaEdicao($objeto);
        var_dump($titulo);
        return View::make('pais.edit')
                ->with('objeto', $objeto)
                ->with('titulo', $titulo)
            ;
    }
    
    public function postEdit($id_pais = ''){
        $post = Input::all();
        try{
            $rep = new \ISRC\Repositorios\RepositorioPais();
            $rep->salve($post);
            Session::flash('msg', 'País atualizado com sucesso');
            return Redirect::to('/admin/pais/');
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', $ex->getMessage());
            return Redirect::to('/admin/pais/edit/'.$post['id_pais']);
        }
    }
}
