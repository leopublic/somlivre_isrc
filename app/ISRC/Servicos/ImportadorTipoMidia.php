<?php

namespace ISRC\Servicos;

class ImportadorTipoMidia extends Importador{

    public function sqlSelectMdb() {
        return "select id_tipo_midia, dsc_tipo_midia, dt_inclusao, dt_alteracao, usuario from ISRC_tipo_midia_tb where is_deleted = 0";
    }
    
    public function novoObjeto(){
        return new \TipoMidia();
    }
            
    public function tabela(){
        return "ISRC_tipo_midia_tb";
    }
        
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        if ($preservar_id){
            $obj->id_tipo_midia   = $regMDB['id_tipo_midia'];
        }
        $obj->dsc_tipo_midia       = $regMDB['dsc_tipo_midia'];
        $obj->usuario = $regMDB['usuario'];
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        return $obj;
    }
}