@extends('base')

@section('estilos')
@stop

@section('conteudo')
<div class="row">
    <div class="col-md-12">
        <h3 class="page-title">Perfis de acesso</h3>
    </div>
</div>
@include('_padroes.mensagens')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>Registros encontrados
            </div>
            <div class="actions">
                <a href="{{URL::to('/admin/perfil/edit/0')}}" class="btn blue btn-sm">
                    <i class="fa fa-plus"></i> Novo </a>
            </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-scrollable">
                @include('_padroes.table')
                <thead>
                    <tr>
                        <th class="center">Ações</th>
                        <th class="center">Id</th>
                        <th>Nome</th>
                        <th style="text-align:center;">Acessa o backoffice?</th>
                        <th style="text-align:center;">Qtd usuários</th>
                        <th>Observação</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($registros as $perfil)
                    <tr>
                        <td class="center">
                            <a href="{{URL::to('/admin/perfil/edit/'.$perfil->id_perfil)}}" class="btn btn-sm green-seagreen" title="Editar o perfil"><i class="fa fa-edit"></i></a>
                        </td>
                        <td class="center">{{$perfil->id_perfil}}</td>
                        <td>{{$perfil->descricao}}</td>
                        <td style="text-align: center;">{{$perfil->backoffice_fmt}}</td>
                        <td style="text-align:center;">{{$perfil->qtd_usuarios}}</td>
                        <td>{{$perfil->observacao}}</td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END BORDERED TABLE PORTLET-->
</div>
@stop
@section('scripts')
$('#menuAcesso').addClass('active');
$('#menuAcesso').addClass('open');
$('#itemPerfis').addClass('active');
@stop
