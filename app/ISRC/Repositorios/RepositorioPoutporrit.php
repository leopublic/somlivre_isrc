<?php

namespace ISRC\Repositorios;

class RepositorioPoutporrit extends RepositorioCadastro {
    public function tituloModelo(){
        return 'pot-pourri';
    }
    
    
    public function nomeModelo(){
        return '\Poutporrit';
    }
    
    public function ordenacaoPadrao(){
        return 'nome';
    }
    public function registrosConsulta($filtros = null){
        if (key_exists('filtro', $filtros)) {
            $filtro = $filtros['filtro'];
        } else {
            $filtro = '';
        }
        
    	return \Poutporrit::wherefiltro($filtro)
                ->with('obras', 'obras.obra')
                ->orderBy('nome')
                ->paginate(20);    
    }

    public function adicionarObra($id_poutporrit, $id_obra){
        $poutporri_obra = new \PoutporritObra;
        $poutporri_obra->id_poutporrit = $id_poutporrit;
        $poutporri_obra->id_obra = $id_obra;
        $poutporri_obra->save();
        return $poutporri_obra;
    }
    
    public function excluirObra($id_poutporrit_obra){
        \PoutporritObra::where('id_poutporrit_obra', '=', $id_poutporrit_obra)->delete();
    }
    
}
