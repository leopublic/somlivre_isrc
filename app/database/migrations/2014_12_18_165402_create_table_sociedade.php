<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSociedade extends Migration {

    public function up() {
        Schema::create('sociedade', function ($table) {
            $table->increments('id_sociedade');
            $table->integer('id_sociedade_original')->unsigned()->nullable();
            $table->integer('id_empresa')->unsigned()->nullable();
            $table->string('sgl_sociedade', 20)->nullable();
            $table->string('nome', 60)->nullable();
            $table->boolean('ind_nacional')->nullable();
            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down() {
        if (Schema::hasTable('sociedade')) {
            Schema::drop('sociedade');
        }
    }

}
