<?php

class FonogramaController extends BaseController {

    public function getIndex() {
        $rep = new \ISRC\Repositorios\RepositorioFonograma;
        $regs = $rep->registrosConsulta();
        return View::make('fonograma.index')
                        ->with('regs', $regs);
    }

    public function postIndex() {
        Input::flash();
        $rep = new \ISRC\Repositorios\RepositorioFonograma;
        $regs = $rep->registrosConsulta(Input::get('filtro'));
        return View::make('fonograma.index')
                        ->with('regs', $regs);
    }

    public function getEdit($id = 0, $id_obra = null, $id_poutporrit = null) {
        if ($id > 0) {
            $reg = \Fonograma::find($id);
            $titulo = 'Editar fonograma';
        } else {
            $titulo = 'Cadastrar novo fonograma';
            $reg = new \Fonograma;
            $reg->id_obra = $id_obra;
            $reg->id_poutporrit = $id_poutporrit;
            $reg->is_gravacao_propria = 1;
        }
        $generos = \Genero::comboComDefault();
        $tipos_midia = \TipoMidia::comboComDefault();
        $tipos_fonograma = \TipoFonograma::comboComDefault();
        $tipos_produto = \TipoProduto::comboComDefault();
        $arranjos = \Arranjo::comboComDefault();
        $paises = \Pais::comboComDefault();
        $fonograma_titulares = $reg->fonogramatitulares;
        $tipos_fonograma_trilha = \TipoFonograma::trilhas();
        $arranjos_precisam_complemento = \Arranjo::precisam_complemento();

        return View::make('fonograma.edit')
                        ->with('objeto', $reg)
                        ->with('titulo', $titulo)
                        ->with('generos', $generos)
                        ->with('tipos_midia', $tipos_midia)
                        ->with('tipos_fonograma', $tipos_fonograma)
                        ->with('tipos_produto', $tipos_produto)
                        ->with('arranjos', $arranjos)
                        ->with('paises', $paises)
                        ->with('fonograma_titulares', $fonograma_titulares)
                        ->with('tipos_fonograma_trilha', $tipos_fonograma_trilha)
                        ->with('arranjos_precisam_complemento', $arranjos_precisam_complemento)
        ;
    }

    public function postEdit() {
        $post = Input::all();
        try {
            $rep = new \ISRC\Repositorios\RepositorioFonograma;
            $fono = $rep->salve($post);
            if ($fono) {
                Session::flash('msg', 'Fonograma atualizado com sucesso');
                return Redirect::to('/admin/fonograma/edit/' . $fono->id_fonograma);
            } else {
                Input::flash();
                Session::flash('error', $rep->getErrors());
                return Redirect::to('/admin/fonograma/edit/'  . $post['id_fonograma']);
            }
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', $ex->getMessage().$ex->getTraceAsString());
            return Redirect::to('/admin/fonograma/edit/' . $post['id_fonograma']);
        }
    }

    public function getSelecionarobra($id_fonograma) {
        $fonograma = \Fonograma::find($id_fonograma);
        $obra = $fonograma->obra;
        return View::make('obra.seleciona')
                        ->with('obra', $obra)
        ;
    }

    public function getCriar($tipo, $id) {
        $titulo = 'Cadastrar novo fonograma';
        $reg = new \Fonograma;
        if ($tipo == 'obra') {
            $reg->id_obra = $id;
            $obra = \Obra::find($id);
            $nome_obra = '(' . $id . ') ' . $obra->titulo_original;
        } else {
            $reg->id_poutporrit = $id;
            $pot = \Poutporrit::find($id);
            $nome_obra = '(' . $id . ') ' . $pot->nome;
        }


        $generos = \Genero::comboComDefault();
        $tipos_midia = \TipoMidia::comboComDefault();
        $tipos_fonograma = \TipoFonograma::comboComDefault();
        $arranjos = \Arranjo::comboComDefault();
        $paises = \Pais::comboComDefault();

        return View::make('fonograma.criar')
                        ->with('objeto', $reg)
                        ->with('titulo', $titulo)
                        ->with('generos', $generos)
                        ->with('tipos_midia', $tipos_midia)
                        ->with('tipos_fonograma', $tipos_fonograma)
                        ->with('arranjos', $arranjos)
                        ->with('paises', $paises)
                        ->with('nome_obra', $nome_obra)
        ;
    }

    public function postCriar() {
        Input::flash();
        $post = Input::all();
        try {
            $rep = new \ISRC\Repositorios\RepositorioFonograma;
            $rep->salve($post);
            Session::flash('msg', 'Fonograma atualizado com sucesso');
            return Redirect::to('/admin/fonograma/edit/' . $post['id_fonograma']);
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', $ex->getMessage());
            return Redirect::to('/admin/fonograma/edit/' . $post['id_fonograma']);
        }
    }

    public function postAlterarobra() {
        $id_fonograma = Input::get('id_fonograma');
        $id_obra = Input::get('id_obra');
        $fono = \Fonograma::find($id_fonograma);
        $fono->id_obra = $id_obra;
        $fono->save();
        $ret['codret'] = 0;
        $ret['msg'] = 'Obra alterada com sucesso!';
        return $ret;
    }

    public function postArquivo() {
        try {
            if (Input::hasFile('arquivo')) {
                $arquivo = Input::file('arquivo');
                $fono = \Fonograma::find(Input::get('id_fonograma'));
                $fono->armazenarArquivo($arquivo);
                Session::flash('msg', 'Fonograma atualizado com sucesso');
            } else {
                Session::flash('msg', 'Arquivo não recebido. Tente novamente.');
            }
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', $ex->getMessage());
        }
        return Redirect::to('/admin/fonograma/edit/' . Input::get('id_fonograma'));
    }

    public function getDownload($id_fonograma) {
        $fono = \Fonograma::find($id_fonograma);
        $headers = array(
            'Content-Type: ' . $fono->mimetype_arquivo,
        );
        return Response::download($fono->caminhoArquivo(), "fonograma_" . $id_fonograma . "." . $fono->extensao_arquivo, $headers);
    }

    public function getRecalcular($id_fonograma){
        try{
            $rep = new \ISRC\Repositorios\RepositorioFonograma;
            $rep->calculaRateio($id_fonograma);
            Session::flash('msg', 'Percentuais recalculados com sucesso!');
        }
        catch(Exception $ex){
            Session::flash('error', $ex->getMessage());
        }
        return Redirect::to('/admin/fonograma/edit/' . $id_fonograma. "#containerFonogramaTitulares");
     }
}
