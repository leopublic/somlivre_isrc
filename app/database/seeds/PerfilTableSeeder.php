<?php

class PerfilTableSeeder extends Seeder {

    public function run() {
        // Uncomment the below to wipe the table clean before populating
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('usuario_perfil')->truncate();
        DB::table('perfil')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $obj = array("descricao" => "administrador", "observacao" => "Perfil com acesso a todas as funções do sistema", "backoffice" => 1);
        DB::table('perfil')->insert($obj);
    }
}
