<?php
/**
 * @property int $id_telefone
 * @property int $id_pessoa
 * @property varchar $ddi
 * @property varchar $ddd
 * @property varchar $telefone
 * @property varchar $ramal
 * @property int $id_tipo_telefone
 * @property int $id_usuario
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property timestamp $deleted_at
 */
class Telefone extends Modelo {
    protected $table = 'telefone';
    protected $primaryKey = 'id_telefone';
    protected $guarded = array();

    public function tipotelefone(){
        return  $this->belongsTo('TipoTelefone', 'id_tipo_telefone', 'id_tipo_telefone');
    }

    public function getNumeroCompletoAttribute($valor){
    	$ret = '';
    	$ret = $this->attributes['telefone'];
    	if ($this->attributes['ddd'] != ''){
    		$ret = '('.$this->attributes['ddd'].') '.$ret;
    	}
    	if ($this->attributes['ramal']!= ''){
    		$ret = $ret.' - ramal '.$this->attributes['ramal'];
    	}
    	if ($this->attributes['ddi'] != ''){
    		$ret = '(+'.$this->attributes['ddi'].') '.$ret;
    	}
    	return $ret;
    }
}