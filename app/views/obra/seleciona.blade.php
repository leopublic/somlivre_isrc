<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Selecionar obra</h4>
    </div>
    <div class="modal-body">
        <div class="scroller" style="" data-always-visible="1" data-rail-visible1="1">
            <div class="row">
                <div class="col-md-12">
                    <h4>Obra atual: "{{$obra->titulo_original}}"</h4>
                </div>
            </div>

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn default">Fechar</button>
        <button type="button" class="btn green">Salvar</button>
    </div>
</div>
<script>

</script>
