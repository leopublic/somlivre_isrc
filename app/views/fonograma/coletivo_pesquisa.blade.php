<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Adicionar coletivo</h4>
</div>
<div class="modal-body">
    {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "fmrTitularPesquisar"))}}
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-md-3 control-label">Nome</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        {{Form::text('nome_razao', $nome_razao, array("class" => "form-control", "id"=> 'nome_razao', "onkeypress" =>"return noenter()"))}}
                            <span class="input-group-btn">
                                <a  class="btn blue" href="javascript:btnColetivoPesquisarClick();">Pesquisar</a>
                            </span>  
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}
    <div class="row">
        <div class="col-md-12">
            <table class="table table-stripped table-condensed table-hover">
                <colgroup>
                    <col style="width:60px;"/>
                    <col style="width:40px;"/>
                    <col style="width:auto;"/>
                </colgroup>
                <thead>
                    <tr>
                        <th class="center">Ações</th>
                        <th class="center">Id</th>
                        <th class="left">Coletivo</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($coletivos as $coletivo)
                    <tr>
                        <td class="center">
                            <a onClick="javascript:getDetalharColetivo({{$id_fonograma}},{{$coletivo->id_coletivo}});" class="btn btn-xs green-meadow" title="Incluir esse coletivo no fonograma"><i class="fa fa-arrow-right"></i></a>
                        </td>
                        <td>{{$coletivo->id_coletivo}}</td>
                        <td style="text-align:left">{{$coletivo->nome}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$msg_limite}}
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
</div>
