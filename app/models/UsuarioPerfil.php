<?php
/**
 * Indica os perfis que o usuário tem acesso
 * @property int $id_usuario
 * @property int $id_perfil
 * @property timestamp $created_at 
 * @property timestamp $updated_at 
 */
class UsuarioPerfil extends Modelo {

    protected $table = 'usuario_perfil';
    protected $primaryKey = 'id_usuario_perfil';
    protected $guarded = array();
    protected $connection = 'mysql';

    /**
     * Gera uma lista de perfis, com o id do usuário onde ele tiver acesso
     * @param type $id_usuario
     */
    public static function perfisDoUsuarioParaEdicao($id_usuario){
        $sql = "select p.id_perfil , p.descricao, up.id_usuario"
                . " from perfil p"
                . " left join usuario_perfil up on up.id_perfil = p.id_perfil and up.id_usuario = ".$id_usuario
                . " order by p.descricao";
        $ret = \DB::select(\DB::raw($sql));
        return $ret;
        
    }
}
