<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePoutporritObra extends Migration {

    public function up() {
        Schema::create('poutporrit_obra', function ($table) {
            $table->increments('id_poutporrit_obra');
            $table->integer('id_obra')->unsigned()->nullable();
            $table->integer('id_poutporrit')->unsigned()->nullable();
            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('poutporrit_obra', function ($table) {
            $table->index('id_obra');
            $table->index('id_poutporrit');
        });
    }

    public function down() {
        if (Schema::hasTable('poutporrit_obra')) {
            Schema::drop('poutporrit_obra');
        }
    }

}
