@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.titulo_cadastro', array('titulo'=> 'Instrumentos', 'rota'=>'instrumento'))

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>Registros encontrados
            </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-scrollable">
                @include('_padroes.table')
                <thead>
                    <tr>
                        <th class="center">Ações</th>
                        <th class="center">Id</th>
                        <th class="left">Grupo</th>
                        <th class="left">Nome</th>
                        </tr>
                </thead>
                <tbody>
                @foreach($regs as $reg)
                    <tr>
                        <td class="center">
                            <a href="{{URL::to('/admin/instrumento/edit/'.$reg->id_instrumento)}}" class="btn btn-sm green-seagreen" title="Editar"><i class="fa fa-edit"></i></a>
                        </td>
                        <td class="center">{{$reg->id_instrumento}}</td>
                        <td class="left">
                                {{$reg->grupo->nome}}
                        </td>
                        <td>{{$reg->nome}}</td>
                    </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END BORDERED TABLE PORTLET-->
</div>
@stop
@section('scripts')
    $('#menuTabelas').addClass('active');
    $('#menuTabelas').addClass('open');
    $('#itemInstrumentos').addClass('active');
@stop
