<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTitularPseudonimo extends Migration {

    public function up() {
        Schema::create('titular_pseudonimo', function ($table) {
            $table->increments('id_pseudonimo');
            $table->integer('id_pseudonimo_original')->unsigned()->nullable();
            $table->integer('id_empresa')->unsigned()->nullable();
            $table->integer('id_titular')->unsigned()->nullable();
            $table->boolean('is_principal')->nullable();
            $table->string('pseudonimo', 50)->nullable();
            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::table('titular_pseudonimo', function ($table) {
           $table->index('pseudonimo'); 
           $table->index('id_titular'); 
        });
        
        
    }

    public function down() {
        if (Schema::hasTable('titular_pseudonimo')) {
            Schema::drop('titular_pseudonimo');
        }
    }

}
