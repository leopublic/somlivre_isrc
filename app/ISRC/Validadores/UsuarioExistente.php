<?php
namespace ISRC\Validadores;
class UsuarioExistente extends Validador {
    public $id_unique;
    public static $rules = array(
        'nome'       => 'required',
        'email'       => array('required', 'regex:/^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,3})$/'),
    );
    public static $messages = array(
        'nome.required'       => 'O nome do usuário é obrigatório',
        'email.required'       => 'O e-mail é obrigatório',
        'email.regex'            => 'O formato do e-mail informado não é válido',
        'email.unique'       => 'Já existe outro usuário com o e-mail escolhido',
    );

    public function getRules(){
        $x = array_merge(static::$rules , array('email' => 'unique:usuario,email,'.$this->id_unique.',id_usuario'));
        return $x;
    }

}
