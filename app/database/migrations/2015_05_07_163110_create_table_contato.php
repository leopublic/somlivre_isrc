<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContato extends Migration {

    public function up() {
        Schema::create('contato', function($table) {
            $table->increments('id_contato');
            $table->integer('id_pessoa');
            $table->integer('id_telefone')->nullable();
            $table->integer('id_usuario')->nullable();
            $table->string('nome', 60)->nullable();
            $table->string('email', 60)->nullable();
            $table->string('observacao', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('contato');
        //
    }

}
