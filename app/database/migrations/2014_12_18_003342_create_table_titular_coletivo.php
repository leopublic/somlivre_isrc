<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTitularColetivo extends Migration {

    public function up() {
        Schema::create('titular_coletivo', function ($table) {
            $table->increments('id_titular_coletivo');
            $table->integer('id_coletivo')->unsigned()->nullable();
            $table->integer('id_titular')->unsigned()->nullable();
            $table->date('dt_entrada')->nullable();
            $table->date('dt_saida')->nullable();
            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('titular_coletivo', function ($table) {
            $table->index('id_titular');
            $table->index('id_coletivo');
        });
    }

    public function down() {
        if (Schema::hasTable('titular_coletivo')) {
            Schema::drop('titular_coletivo');
        }
    }

}
