<table class="table table-stripped table-condensed table-hover">
    <colgroup>
        <col style="width:100px;"/>
        <col style="width:50px;"/>
        <col style="width:auto;"/>
        <col style="width:auto;"/>
        <col style="width:auto;"/>
        <col style="width:auto;"/>
        <col style="width:auto;"/>
    </colgroup>
    <thead>
        <tr>
            <th class="center">Ações</th>
            <th class="center">Id</th>
            <th class="left">DDI</th>
            <th class="left">DDD</th>
            <th class="left">Número</th>
            <th class="left">Ramal</th>
            <th class="left">Tipo</th>
        </tr>
    </thead>
    <tbody>
        @foreach($telefones as $tel)
        <tr id="telefone{{$tel->id_telefone}}">
            <td class="center">
                <a href="javascript:getEditarTelefone('{{$tel->id_pessoa}}', '{{$tel->id_telefone}}');" class="btn btn-xs {{$botao_edicao_cor}}" title="Editar esse telefone">{{$botao_edicao_icon}}</a>
                <a href="javascript:postExcluirTelefone('{{$tel->id_pessoa}}', '{{$tel->id_telefone}}');" class="btn btn-xs {{$botao_exclusao_cor}}" title="Excluir esse telefone">{{$botao_exclusao_icon}}</a>
            </td>
            <td class="center">{{$tel->id_telefone}}</td>
            <td class="left">{{$tel->ddi}}</td>
            <td class="left">{{$tel->ddd}}</td>
            <td class="left">{{$tel->telefone}}</td>
            <td class="left">{{$tel->ramal}}</td>
            <td class="left">@if(count($tel->tipotelefone)>0){{$tel->tipotelefone->descricao}}@else--@endif</td>
        </tr>
        @endforeach
    </tbody>
</table>