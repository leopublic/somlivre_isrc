<div class="portlet box {{$portlet_titular}}">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-star"></i> Titulares</div>
        <div class="actions">
                <a href="/admin/obratitulares/editar/{{$reg->id_obra}}"  class="btn btn-sm default">Alterar</a>
        </div>
        <div class="tools">
        </div>
    </div>
    <div class="portlet-body" id="portlet-body-titulares">
        <table class="table table-stripped table-condensed table-hover" id="tblObraTitular">
            <thead>
                <tr>
                    <th class="center">Id</th>
                    <th class="left">Titular</th>
                    <th class="left">Pseud&ocirc;nimo</th>
                    <th class="left">Categoria</th>
                    <th class="right">Part. %</th>
                </tr>
            </thead>
            <tbody>
                <? $total = 0; ?>
                @foreach($obra_titulares as $obra_titular)
                <? $total += $obra_titular->pct_autoral; ?>
                <tr id="tabela{{$obra_titular->id_obra_titular}}">
                    <td class="center">{{$obra_titular->id_titular}}</td>
                    <td>
                        @if ($obra_titular->id_coletivo > 0)
                            <i class="fa fa-group"></i>                        
                        @else
                            <i class="fa fa-user"></i>
                        @endif
                        {{$obra_titular->nome_razao}}</td>
                    <td>{{$obra_titular->nome_pseudonimo}}</td>
                    <td>{{$obra_titular->desc_categoria}}</td>
                    <td class="right">{{$obra_titular->pct_autoral_fmt}}</td>
                </tr>
                @endforeach
                <tr @if ($total != 100) class="red" @endif>
                    <td colspan="4" style="text-align: right;">Total:</td>
                    <td class="right"><? print $total; ?>%</td>
                </tr>
            </tbody>
        </table>
        @include('_padroes.mensagens_locais')
    </div>
</div>