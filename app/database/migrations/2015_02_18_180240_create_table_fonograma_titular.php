<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFonogramaTitular extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('fonograma_titular', function ($table) {
            $table->increments('id_fonograma_titular');
            $table->integer('id_fonograma_titular_original')->unsigned()->nullable();
            $table->integer('id_empresa')->unsigned()->nullable();
            $table->integer('id_categoria')->unsigned()->nullable();
            $table->integer('id_fonograma')->unsigned()->nullable();
            $table->integer('id_titular')->unsigned()->nullable();
            $table->integer('id_pseudonimo')->unsigned()->nullable();
            $table->integer('id_coletivo')->unsigned()->nullable();

            $table->decimal('pct_participacao', 13, 10)->unsigned()->nullable();
            
            $table->boolean('ind_calc_auto')->nullable();
            
            $table->dateTime('dt_inicontrato')->nullable();
            $table->dateTime('dt_fimcontrato')->nullable();
            
            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down() {
        if (Schema::hasTable('fonograma_titular')) {
            Schema::drop('fonograma_titular');
        }
    }

}
