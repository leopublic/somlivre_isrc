<?php

namespace ISRC\Repositorios;

class RepositorioArranjo extends RepositorioCadastro {
    
    public function nomeModelo(){
        return '\Arranjo';
    }
    
    public function ordenacaoPadrao(){
        return 'dsc_arranjo';
    }
    
}
