<?php

class ColetivoController extends CadastroController {
    public function repo(){
        return new \ISRC\Repositorios\RepositorioColetivo;
    }
    
    public function diretorioViews() {
        return 'coletivo';
    }
    
    public function raizRota() {
        return 'coletivo';
    }
    
    public function msgSucesso(){
        return 'Coletivo atualizado com sucesso';
    }    


    public function getEdit($id = 0) {
        $rep = new \ISRC\Repositorios\RepositorioColetivo();
        $objeto = $rep->instanciaEditado($id);
        $titulo = $rep->tituloTelaEdicao($objeto);
        $tipos = \TipoColetivo::comboComDefault();
        $siglas = \SiglaColetivo::comboComDefault();
        $titularescoletivo = $objeto->integrantes;
        return View::make('coletivo.edit')
                ->with('objeto', $objeto)
                ->with('titulo', $titulo)
                ->with('tipos_coletivo', $tipos)
                ->with('siglas', $siglas)
                ->with('titularescoletivo', $titularescoletivo)
            ;
    }
    
    public function postEdit($id = ''){
        $post = Input::all();
        try{
            $id_antes = Input::get('id_coletivo');
            $rep = new \ISRC\Repositorios\RepositorioColetivo();
            if ($obj = $rep->salve($post)){
                if ($id_antes > 0){
                    $msg = 'Coletivo atualizado com sucesso';
                } else {
                    $msg = 'Coletivo criado com sucesso. Adicione os titulares';                    
                }
                Session::flash('msg', $msg);
                return Redirect::to('/admin/coletivo/edit/'.$obj->id_coletivo);            
            } else {
                Input::flash();
                Session::flash('error', $rep->getErrors());
                return Redirect::to('/admin/coletivo/edit/'.$id_antes);
            }
            return Redirect::to('/admin/coletivo/');
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', $ex->getMessage());
            return Redirect::to('/admin/coletivo/edit/'.$id_antes);
        }
    }
    
    public function getPesquisartitulares($id_coletivo, $pesquisar){
        if ($pesquisar != '') {
                $pesquisar = str_replace('*', '%', $pesquisar);
                $sql_count = "select count(*) as qtd from titular 
                            join pessoa on pessoa.id_pessoa = titular.id_pessoa 
                            left join titular_pseudonimo tp on tp.id_titular = titular.id_titular
                            where nome_razao like ? or tp.pseudonimo like ?";
                $sql = "select titular.id_titular, nome_razao from titular 
                            join pessoa on pessoa.id_pessoa = titular.id_pessoa 
                            left join titular_pseudonimo tp on tp.id_titular = titular.id_titular
                            where nome_razao like ? or tp.pseudonimo like ? limit 15";
                $res_quantos = \DB::select($sql_count, array('%' . $pesquisar . '%', '%' . $pesquisar . '%'));
                $rs_quantos = $res_quantos[0];
                $titulares = \DB::select($sql, array('%' . $pesquisar . '%', '%' . $pesquisar . '%'));
                if ($rs_quantos->qtd > 15) {
                    $ignorados = $rs_quantos->qtd - 15;
                    $msg_limite = '(' . $ignorados . ' resultados não exibidos. Refine sua pesquisa)';
                } else {
                    $msg_limite = '';
                }
                return View::make('coletivo.titulares_pesquisa')
                                ->with('id_coletivo', $id_coletivo)
                                ->with('titulares', $titulares)
                                ->with('msg_limite', $msg_limite)
                ;
        } else {
            $quantos = 0;
            $msg_limite = '';
            $titulares = array();
            return View::make('coletivo.titulares_pesquisa')
                            ->with('id_coletivo', $id_coletivo)
                            ->with('titulares', $titulares)
                            ->with('msg_limite', $msg_limite)
            ;
        }
        
    }
    
    public function getAdicionartitular($id_coletivo, $id_titular){
        $titular = \Titular::find($id_titular);
        $titular_coletivo = new \TitularColetivo;
        $titular_coletivo->id_coletivo_titular = 0;
        $titular_coletivo->id_coletivo = $id_coletivo;
        $titular_coletivo->id_titular = $id_titular;
        return View::make('coletivo.titulares_adicionar')
                        ->with('titular', $titular)
                        ->with('titular_coletivo', $titular_coletivo)
                        ->with('titulo', 'Adicionar titular')
        ;
        
    }    
    
    public function getFormpesquisa(){
        return View::make('coletivo.titulares_form_pesquisa')
        ;
        
    }    
    
    public function getEditartitular($id_titular_coletivo){
        $titular_coletivo = \TitularColetivo::find($id_titular_coletivo);
        $titular = \Titular::find($titular_coletivo->id_titular);
        return View::make('coletivo.titulares_adicionar')
                        ->with('titular', $titular)
                        ->with('titular_coletivo', $titular_coletivo)
                        ->with('titulo', 'Adicionar titular')
        ;
        
    }
    
    public function postAdicionartitular(){
        $ret = array();
        try {
            $rep = new \ISRC\Repositorios\RepositorioColetivo();
            $post = Input::all();
            $titular_coletivo = $rep->adicionarTitular($post);

            $objeto = \Coletivo::find(Input::get('id_coletivo'));
            $titularescoletivo = $objeto->integrantes;
            $html = View::make('coletivo.titulares_tabela')
                    ->with('titularescoletivo', $titularescoletivo)
                    ->with('msg', 'Titular adicionado com sucesso')
            ;
            $ret['html'] = iconv('UTF-8', 'UTF-8', $html);
            $ret['codret'] = 0;
            $ret['msg'] = 'Titular adicionado com sucesso!';
            $ret['id_titular_coletivo'] = $titular_coletivo->id_titular_coletivo;
        } catch (Exception $ex) {
            $ret['codret'] = -1;
            $ret['msg'] = "Não foi possível adicionar esse titular: " . $ex->getMessage() . $ex->getTraceAsString();
        }
        return json_encode($ret);        
    }
    
    public function postExcluirtitular(){
        $ret = array();
        try {
            $rep = new \ISRC\Repositorios\RepositorioColetivo();
            $rep->excluirTitular(Input::get('id_titular_coletivo'));

            $objeto = \Coletivo::find(Input::get('id_coletivo'));
            $titularescoletivo = $objeto->integrantes;
            $html = View::make('coletivo.titulares_tabela')
                    ->with('titularescoletivo', $titularescoletivo)
                    ->with('msg', 'Titular adicionado com sucesso')
            ;
            $ret['html'] = iconv('UTF-8', 'UTF-8', $html);
            $ret['codret'] = 0;
            $ret['msg'] = 'Titular excluído com sucesso!';
        } catch (Exception $ex) {
            $ret['codret'] = -1;
            $ret['msg'] = "Não foi possível adicionar esse titular: " . $ex->getMessage() . $ex->getTraceAsString();
        }
        return json_encode($ret);        
        
    }
}
