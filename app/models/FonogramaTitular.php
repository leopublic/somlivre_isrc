<?php
/**
 * @property int $id_fonograma_titular 
 * @property int $id_categoria 
 * @property int $id_fonograma 
 * @property int $id_titular 
 * @property int $id_pseudonimo 
 * @property int $id_coletivo 
 * @property decimal $pct_participacao 
 * @property tinyint $ind_calc_auto 
 * @property datetime $dt_inicontrato 
 * @property datetime $dt_fimcontrato 
 * @property datetime $dt_inclusao 
 * @property datetime $dt_alteracao 
 * @property varchar $usuario 
 * @property timestamp $created_at 
 * @property timestamp $updated_at 
 * @property timestamp $deleted_at 
 */
class FonogramaTitular extends Modelo {
    protected $table = 'fonograma_titular';
    protected $primaryKey = 'id_fonograma_titular';
    protected $guarded = array();

    public function carregaPeloMDB($registro){
        
    }
    /**
     * 
     * @return Categoria
     */
    public function categoria(){
        return $this->belongsTo('Categoria', 'id_categoria', 'id_categoria');
    }
    /**
     * 
     * @return Titular
     */
    public function titular(){
        return $this->belongsTo('Titular', 'id_titular', 'id_titular');
    }
    
    public function coletivo(){
        return $this->belongsTo('Coletivo', 'id_coletivo', 'id_coletivo');
    }
    
    public function fonograma(){
        return $this->belongsTo('Fonograma', 'id_fonograma', 'id_fonograma');
    }
    /**
     * @return \Pseudonimo Description
     */
    public function pseudonimo(){
        return $this->belongsTo('TitularPseudonimo', 'id_pseudonimo', 'id_pseudonimo');
    }

    public function fonogramatitularinstrumentos(){
        return $this->hasMany('FonogramaTitularInstrumento', 'id_fonograma_titular', 'id_fonograma_titular');
    }

    public function instrumentos(){
        return $this->belongsToMany('Instrumento', 'fonograma_titular_instrumento', 'id_fonograma_titular', 'id_instrumento');
    }
    
    public function getDescCategoriaAttribute(){
        if ($this->categoria){
            return $this->categoria->desc_categoria;
        } else {
            return '--';
        }
    }
    
    public function getNomePseudonimoAttribute(){
        if ($this->pseudonimo){
            return $this->pseudonimo->pseudonimo;
        } else {
            return '--';
        }
    }
    
    public function getNomeRazaoAttribute(){
        if ($this->id_titular > 0){
            return $this->titular->pessoa->nome_razao;
        } else {
            return '--';
        }
    }
    
    public function getNomeColetivoAttribute(){
        if ($this->id_coletivo > 0){
            return $this->coletivo->nome;
        } else {
            return '';
        }
    }
    
    public function getPctParticipacaoFmtAttribute(){
        return number_format($this->pct_participacao, '8', ',','.');
    }
    public function setPctParticipacaoFmtAttribute($valor){
        $this->attributes['pct_participacao'] = str_replace(',', '.', str_replace('.', '', trim($valor)));
    }
 
    public function getDtInicontratoFmtAttribute(){
        return $this->dtFmt('dt_inicontrato');
    }
    public function setDtInicontratoFmtAttribute($valor){
        $this->attributes['dt_inicontrato'] = $this->dtViewParaModel($valor);
    }
 
    public function getDtFimcontratoFmtAttribute(){
        return $this->dtFmt('dt_fimcontrato');
    }
    public function setDtFimcontratoFmtAttribute($valor){
        $this->attributes['dt_fimcontrato'] = $this->dtViewParaModel($valor);
    }
    
    public function id_instrumentos(){
        return \FonogramaTitularInstrumento::where('id_fonograma_titular', '=', $this->id_fonograma_titular)->lists('id_instrumento');
        
    }

    public function lista_instrumentos(){
        $instrumentos = $this->instrumentos;
        $ret = "";
        $br = "";
        foreach($instrumentos as $instrumento){
            $ret .= $br.$instrumento->nome;
            $br = "<br/>";
        }
        if ($ret == ""){
            $ret = "(nenhum informado)";
        }
        return $ret;
    }

}