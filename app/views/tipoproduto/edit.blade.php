@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.mensagens')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-tags"></i> {{$titulo}}</div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body form">
                {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                {{Form::hidden('id_tipo_produto', $reg->id_tipo_produto)}}
                <div class="form-body">
                    @include('_campos.texto_id', array('valor' => $reg->id_tipo_produto))
                    @include('_campos.texto', array("label"=> "Nome", "nome"=> "nome"))
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green">Salvar</button>
                        <a href="{{URL::to('/tipoproduto')}}" class="btn default">Cancelar</a>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
$('#menuTabelas').addClass('active');
$('#menuTabelas').addClass('open');
    $('#itemTiposProduto').addClass('active');
@stop
