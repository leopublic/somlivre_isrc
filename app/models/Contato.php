<?php
/**
 * @property int $id_contato
 * @property int $id_pessoa
 * @property int $id_telefone
 * @property int $id_usuario
 * @property varchar $nome
 * @property varchar $email
 * @property varchar $observacao
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property timestamp $deleted_at
 */
class Contato extends Modelo {
    protected $table = 'contato';
    protected $primaryKey = 'id_contato';
    protected $guarded = array();

    public function telefone(){
        return  $this->belongsTo('Telefone', 'id_telefone', 'id_telefone');
    }

}