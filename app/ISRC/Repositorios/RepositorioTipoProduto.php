<?php

namespace ISRC\Repositorios;

class RepositorioTipoProduto extends RepositorioCadastro {

    public function tituloModelo() {
        return 'tipo de produto';
    }

    public function nomeModelo() {
        return '\TipoProduto';
    }

    public function ordenacaoPadrao() {
        return 'nome';
    }

    public function registrosConsulta($filtros = null) {
        return \TipoProduto::paginate(20);
    }
}
