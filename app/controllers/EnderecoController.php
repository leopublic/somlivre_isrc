<?php
class EnderecoController extends BaseController
{

    public function repo() {
        return new ISRC\Repositorios\RepositorioEndereco;
    }

    public function getIndex($id_pessoa) {
        $rep = $this->repo();
    }

    public function getEditar($id_pessoa, $id_endereco) {
        if ($id_endereco > 0) {
            $reg = \Endereco::find($id_endereco);
            $titulo = 'Alterar endereço';
        }
        else {
            $reg = new \Endereco;
            $reg->id_pessoa = $id_pessoa;
            $titulo = "Adicionar endereço";
        }
        $paises = \Pais::comboComDefault();
        $ufs = \Uf::comboComDefault();
        $data = array('paises' => $paises, 'ufs' => $ufs, 'reg' => $reg, 'titulo' => $titulo);
        return View::make('endereco/edit', $data);
    }

    public function postEditar() {
        $ret = array();
        try {
            $rep = $this->repo();
            $post = Input::all();
            $endereco = $rep->salve($post);

            $enderecos = \Endereco::where('id_pessoa', '=', $post['id_pessoa'])
                            ->get();
            $html = View::make('pessoa.tabela_enderecos')->with('enderecos', $enderecos);
            $ret['html'] = iconv('UTF-8', 'UTF-8', $html);
            $ret['codret'] = 0;
            $ret['msg'] = iconv('UTF-8', 'UTF-8', 'Endereço adicionado com sucesso!');
            $ret['id_endereco'] = $endereco->id_endereco;
        }
        catch(Exception $ex) {
            $ret['codret'] = - 1;
            $ret['msg'] = "Não foi possível adicionar esse endereço: " . $ex->getMessage() . $ex->getTraceAsString();
        }
        return json_encode($ret);
    }


    public function postExcluir() {
        $id_pessoa = Input::get('id_pessoa');
        $id_endereco = Input::get('id_endereco');
        $ret = array();
        try {
            $rep = $this->repo();
            $end = $rep->exclua($id_endereco);

            $enderecos = \Endereco::where('id_pessoa', '=', $id_pessoa)->get();
            $html = View::make('pessoa.tabela_enderecos')->with('enderecos', $enderecos);
            $ret['html'] = iconv('UTF-8', 'UTF-8', $html);
            $ret['codret'] = 0;
            $ret['msg'] = iconv('UTF-8', 'UTF-8', 'Endereço excluído com sucesso!');
        }
        catch(Exception $ex) {
            $ret['codret'] = - 1;
            $ret['msg'] = "Não foi possível excluir esse endereço: " . $ex->getMessage() . $ex->getTraceAsString();
        }
        return json_encode($ret);
    }
}
