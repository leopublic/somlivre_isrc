<div class="row">
    <div class="col-md-10">
        <h3 class="page-title">{{$titulo}}</h3>
    </div>
    <div class="col-md-2" style="text-align: right;">
        <a href="{{URL::to('/admin/'.$rota.'/edit/0')}}" class="btn blue"><i class="fa fa-plus"></i> Novo</a>
    </div>
</div>
@include('_padroes.mensagens')
