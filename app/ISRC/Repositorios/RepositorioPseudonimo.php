<?php
namespace ISRC\Repositorios;

class RepositorioPseudonimo extends RepositorioCadastro {
    
    public function nomeModelo(){
        return '\Pseudonimo';
    }
    
    public function ordenacaoPadrao(){
        return 'pseudonimo';
    }
    
    public function registrosConsulta($filtros = null){
    	return \Pseudonimo::orderBy('pseudonimo')->paginate(20);
    }
}