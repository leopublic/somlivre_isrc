@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.titulo_cadastro', array('titulo'=> 'Categorias', 'rota'=>'categoria'))

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>Registros encontrados
                </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-stripped table-condensed table-hover">
                        <thead>
                            <tr>
                                <th class="center">Ações</th>
                                <th class="center">Id</th>
                                <th class="center">Código</th>
                                <th class="left">Descrição</th>
                                <th class="center">Tipo</th>
                                <th class="center">% máximo</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($regs as $reg)
                            <tr>
                                <td class="center">
                                    <a href="{{URL::to('/admin/categoria/edit/'.$reg->id_categoria)}}" class="btn btn-sm green-seagreen" title="Editar"><i class="fa fa-edit"></i></a>
                                </td>
                                <td class="center">{{$reg->id_categoria}}</td>
                                <td class="center">{{$reg->cod_categoria}}</td>
                                <td>{{$reg->desc_categoria}}</td>
                                <td class="center">
                                    @if ($reg->tp_direito == 'A')
                                    Autoral
                                    @else
                                    Conexo
                                    @endif
                                </td>
                                <td class="center">{{number_format($reg->pct_max, 2, '.', ',')}}%</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END BORDERED TABLE PORTLET-->
    </div>
</div>
@stop
@section('scripts')
$('#menuTabelas').addClass('active');
$('#menuTabelas').addClass('open');
$('#itemCategorias').addClass('active');
@stop
