@extends('base')

@section('estilos')
@stop

@section('conteudo')

@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <div class="tabbable-custom nav-justified">
            @include('fonograma.abas', array('abaAtiva' => 'Titulares'))
            {{Form::hidden('id_fonograma', $objeto->id_fonograma, array("id" => 'id_fonograma'))}}
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1_1">
                    <div class="portlet box {{$portlet_titular}}">
                        <div class="portlet-title">
                            <div class="caption"><i class="fa fa-user"></i>Adicionar outro titular</div>
                            <div class="tools">
                                <a href="javascript:;" class="expand"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-12">
                                    {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "form_titulares"))}}
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    {{Form::text('pesquisar', Input::old('pesquisar'), array("class" => "form-control", "id" => "txtPesquisarTitular"))}}
                                                    <span class="input-group-btn"><button id="btnPesquisarTitular" type="button" class="btn"><i class="fa fa-search"></i> Pesquisar</button></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{Form::close()}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="containerTitulares" style="text-align: center;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption"><i class="fa fa-group"></i>Adicionar coletivo aos titulares</div>
                            <div class="tools">
                                <a href="javascript:;" class="expand"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-12">
                                    {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "form_coletivos"))}}
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    {{Form::text('pesquisar_coletivo', Input::old('pesquisar_coletivo'), array("class" => "form-control", "id" => "txtPesquisarColetivo"))}}
                                                    <span class="input-group-btn"><button id="btnPesquisarColetivo" type="button" class="btn" onClick="pesquisaColetivos({{$objeto->id_fonograma}});"><i class="fa fa-search"></i> Pesquisar</button></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{Form::close()}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="containerColetivos" style="text-align: center;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" id="containerFonogramaTitulares" name="containerFonogramaTitulares">
                        @include('fonograma.titulares_tabela')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('_padroes.modal-container', array('id'=> 'ajax-modal', 'largura'=>'550'));
</div>
@stop
@section('scripts')
    $('#itemFonogramas').addClass('active');
    
    jQuery(document).ready(function () {
        $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner = 
          '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
            '<div class="progress progress-striped active">' +
              '<div class="progress-bar" style="width: 100%;"></div>' +
            '</div>' +
          '</div>';

        $.fn.modalmanager.defaults.resize = true;
        
        $('#btnPesquisarTitular').click(function(event){
            event.preventDefault();
            $(this).html('<i class="fa fa-spin fa-spinner"></i> Aguarde');
            $(this).attr('disabled', 'disabled');
            $('#containerTitulares').html('<br/><i class="fa fa-spin fa-spinner"></i> carregando...');
            var id_fonograma = $('#id_fonograma').val();
            var url = "/admin/fonogramatitulares/gridresultadopesquisa/"+id_fonograma+"/"+$('#txtPesquisarTitular').val();
            $("#containerTitulares" ).load( url );
            $(this).html('<i class="fa fa-search"></i> Pesquisar');
            $(this).removeAttr('disabled');
        });
    });
        
    function getAdicionarTitular(id_fonograma, id_titular){
        var url = '/admin/fonogramatitulares/adicionar/'+id_fonograma+'/'+id_titular;
        $('body').modalmanager('loading');
        setTimeout(function(){
            $('#ajax-modal').load( url, '', function(){
                $('.date-pickerx').datepicker({autoclose: true});
                $('.select2').select2();
                $('#ajax-modal').modal();
            });
        }, 1000);
    }
        
    function getEditarTitular(id_fonograma_titular){
        var url = '/admin/fonogramatitulares/editar/'+id_fonograma_titular;
        $('body').modalmanager('loading');
        setTimeout(function(){
            $('#ajax-modal').load( url, '', function(){
                $('.date-pickerx').datepicker({autoclose: true});
                $('.select2').select2();
                $('#ajax-modal').modal();
            });
        }, 1000);
    }
    
    function pesquisaColetivos(id_fonograma){
        $('#containerColetivos').html('<br/><i class="fa fa-spin fa-spinner"></i> carregando...');
        var url = "/admin/fonogramacoletivos/gridresultadopesquisa/"+id_fonograma+"/"+$('#txtPesquisarColetivo').val();
        $( "#containerColetivos" ).load( url );
    }
    
    function adicionarColetivo(id_fonograma, id_coletivo){
        var url = '/admin/fonogramacoletivos/adicionar/'+id_fonograma+'/'+id_coletivo;
        $('body').modalmanager('loading');
        setTimeout(function(){
            $('#ajax-modal').load( url, '', function(){
                $('.date-pickerx').datepicker({autoclose: true});
                $('.select2').select2();
                $('#ajax-modal').modal();
            });
        }, 1000);
    }
    
    function postAdicionarTitular(){
        $.post( "/admin/fonogramatitulares/adicionar", $( "#form_titular" ).serialize()
            , function(data){
                if (data.codret == 0){
                    $('#ajax-modal').modal('hide');
                    $(document.body).animate({
                        'scrollTop':  $('#containerFonogramaTitulares').offset().top
                    }, 2000);
                    $('#containerFonogramaTitulares').html(data.html);
                    $('#tabela'+data.id_fonograma_titular).stop().animate({backgroundColor: "#95A5A6"}, 100)
                        .animate({backgroundColor: "#FFFFFF"}, 4500);
                } else {
                    $('#modal-msg').html(data.msg);
                }
            }        
            , "json" )
        ;
    }
    
    function postExcluirTitular(id_fonograma_titular){
        Metronic.blockUI({
                target: '#portlet-body-titulares',
                boxed: true,
                message: 'Excluindo...(aguarde)'
        });
        $.post( "/admin/fonogramatitulares/excluir", {id_fonograma_titular:id_fonograma_titular, id_fonograma: $('#id_fonograma').val()}
            , function(data){
                Metronic.unblockUI('#portlet-body-titulares');
                if (data.codret == 0){
                    $('#ajax-modal').modal('hide');
                    $(document.body).animate({
                        'scrollTop':  $('#containerFonogramaTitulares').offset().top
                    }, 1000);
                    $('#containerFonogramaTitulares').html(data.html);
                } else {
                    $('#modal-msg').html(data.msg);
                }
            }        
            , "json" )
        ;
    }
    
    function postAdicionarColetivo(){
        $.post( "/admin/fonogramacoletivos/adicionar", $( "#form_coletivo" ).serialize()
            , function(data){
                console.log(data);
                if (data.codret == 0){
                    $('#ajax-modal').modal('hide');
                } else {
                    $('#modal-msg').html(data.msg);
                }
            }        
            , "json" )
        ;
    }
@stop
