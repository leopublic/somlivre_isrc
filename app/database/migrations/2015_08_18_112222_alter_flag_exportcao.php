<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFlagExportcao extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasColumn('titular', 'exportar')) {
            Schema::table('titular', function ($table) {
                $table->boolean('exportar');
            });
        }
		if (!Schema::hasColumn('poutporrit', 'exportar')) {
            Schema::table('poutporrit', function ($table) {
                $table->boolean('exportar');
            });
        }
		if (!Schema::hasColumn('obra', 'exportar')) {
            Schema::table('obra', function ($table) {
                $table->boolean('exportar');
            });
        }
		if (!Schema::hasColumn('fonograma', 'exportar')) {
            Schema::table('fonograma', function ($table) {
                $table->boolean('exportar');
            });
        }
		if (!Schema::hasColumn('coletivo', 'exportar')) {
            Schema::table('coletivo', function ($table) {
                $table->boolean('exportar');
            });
        }

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
