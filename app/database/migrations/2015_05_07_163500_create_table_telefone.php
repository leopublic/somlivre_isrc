<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTelefone extends Migration {

    public function up() {
        Schema::create('telefone', function($table) {
            $table->increments('id_telefone');
            $table->integer('id_pessoa');
            $table->string('ddi', 6)->nullable();
            $table->string('ddd', 4)->nullable();
            $table->string('telefone', 12)->nullable();
            $table->string('ramal', 10)->nullable();
            $table->integer('id_tipo_telefone')->nullable();
            $table->integer('id_usuario')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down() {
        Schema::dropIfExists('telefone');
    }

}
