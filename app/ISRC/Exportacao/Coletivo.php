<?php
namespace ISRC\Exportacao;
class Coletivo extends Exportacao{
	public $modo; 

	const tprDETALHE = '1';
	const tprTITULAR = '2';

	public static function nomeEntidade(){
		return '\Coletivo';
	}

	public function tipo_cadastro(){
		return 'COL';
	}

	public function exporta($coletivo){
		if ($coletivo->cod_coletivo_ECAD != ''){
			$this->modo = self::tpoINCLUIR;
		} else {
			$this->modo = self::tpoALTERAR;
		}
		$this->abreTransacao();
		$this->detalhe($coletivo);
		$titulares = $coletivo->integrantes;
		foreach($titulares as $titularcoletivo){
			$this->titular($coletivo, $titularcoletivo);
		}
		$this->fechaTransacao();
	}

	public function detalhe($coletivo){
		$this->novo_reg($this->modo, self::tprDETALHE);
		$this->adicionaCampo($coletivo->cod_coletivo_ECAD, 'N', 8);
		$this->adicionaCampo('', 'A', 10);
		$this->adicionaCampo('', 'A', 2);	//Sigla país
		$this->adicionaCampo($coletivo->relacaoComDefault('tipocoletivo', 'descricao'), 'A', 10);
		$this->adicionaCampo($coletivo->nome, 'A', 60);
		$this->adicionaCampo($coletivo->is_nacional, 'B', 1);
		$this->adicionaCampo($coletivo->cod_cae, 'N', 11);
		$this->adicionaCampo($coletivo->dtFmtSemSep('dt_alteracao'), 'D', 8);
		$this->adicionaCampo($coletivo->pseudonimo, 'A', 60);

		$this->grava_registro();
	}

	public function titular($coletivo, $titularcoletivo){
		$this->novo_reg($this->modo, self::tprTITULAR);
		$this->adicionaCampo($coletivo->cod_coletivo_ECAD, 'N', 10);
		$this->adicionaCampo('', 'N', 10);			// Cod ISRC
		$this->adicionaCampo($titularcoletivo->titular->pessoa->cod_ecad, 'N', 13);
		$this->adicionaCampo('', 'N', 15);			// Cod ISRC
		$this->adicionaCampo($titularcoletivo->titular->pessoa->nome_razao, 'A', 45); //
		$this->adicionaCampo($titularcoletivo->titular->pessoa->tp_pessoa, 'A', 1); //

		if ($titularcoletivo->titular->pessoa->tp_pessoa == 'J'){
			$this->adicionaCampo(0, 'N', 11);
			$this->adicionaCampo($titularcoletivo->titular->pessoa->cpf_cnpj, 'N', 14);
		} else {
			$this->adicionaCampo($titularcoletivo->titular->pessoa->cpf_cnpj, 'N', 11);
			$this->adicionaCampo(0, 'N', 14);
		}
		$this->adicionaCampo($titularcoletivo->titular->cod_cae, 'N', 11); // Código CAE
		$this->adicionaCampo($titularcoletivo->titular->is_nacional, 'B', 1); // Titular é nacional
		$this->adicionaCampo($titularcoletivo->dtFmtSemSep('dt_entrada'), 'D', 8); 
		$this->adicionaCampo($titularcoletivo->dtFmtSemSep('dt_saida'), 'D', 8); 

		$this->grava_registro();
	}

}