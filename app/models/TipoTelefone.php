<?php
/**
 * @property int $id_tipo_telefone
 * @property string $descricao Nome
 * @property datetime created_at
 * @property datetime updated_at
 *
 */
class TipoTelefone extends Modelo
{
    protected $table = 'tipo_telefone';
    protected $primaryKey = 'id_tipo_telefone';
    protected $guarded = array();

    public static function combo($filtro = '') {
        return \TipoTelefone::orderBy('descricao')
                ->lists('descricao', 'id_tipo_telefone');
    }
}
