@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.titulo_cadastro', array('titulo'=> 'Obras', 'rota'=>'obra'))

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_obra}}">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>Registros encontrados
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        {{Form::text('pesquisar', Input::old('pesquisar'), array("class" => "form-control"))}}
                                        <span class="input-group-btn"><button class="btn" ><i class="fa fa-search"></i> Pesquisar</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{$regs->links()}}
                    </div>
                </div>
                <div class="table-scrollable">
                    @include('_padroes.table')
                    <thead>
                        <tr>
                            <th class="center" rowspan="2" style="width:150px;">Ações</th>
                            <th class="center" rowspan="2">Id</th>
                            <th class="center" rowspan="2">Código</th>
                            <th class="left" rowspan="2">Título</th>
                            <th class="center" colspan="4">Titulares</th>
                        </tr>
                        <tr>
                            <th class="left">Nome</th>
                            <th class="left">Pseudônimo</th>
                            <th class="left">Categoria</th>
                            <th class="right">Part.</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($regs as $reg)
                        @if ($reg->obratitulares()->count() > 0)
                        <? $qtd = $reg->obratitulares()->count(); ?>
                        @else
                        <? $qtd = 1;?>
                        @endif
                        <tr class="grossa">
                            <td rowspan="{{$qtd}}" class="center">
                                <a href="{{URL::to('/admin/obra/edit/'.$reg->id_obra)}}" class="btn btn-sm {{$botao_edicao_cor}}" title="Editar">{{$botao_edicao_icon}}</a>
                                <a href="{{URL::to('/admin/fonograma/edit/0/'.$reg->id_obra)}}" class="btn btn-sm {{$portlet_fonograma}}" title="Criar um fonograma para essa obra"><i class="fa fa-plus"></i> {{$fonograma_icon}}</a>
                                @if($reg->pode_excluir())
                                    <div class="btn-group" style="display: ">
                                        <button class="btn btn-danger btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-trash-o"></i></button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#" class="alert-danger">Clique para confirmar a exclusão dessa obra</a></li>
                                        </ul>
                                    </div>
                                @endif
                            </td>
                            <td rowspan="{{$qtd}}" class="center">{{$reg->id_obra}}</td>
                            <td rowspan="{{$qtd}}" class="center">{{$reg->cod_obra}}</td>
                            <td rowspan="{{$qtd}}">{{$reg->titulo_original}}</td>
                            <? $tr = ''; ?>
                            <? $btr = ''; ?>
                            @if ($reg->obratitulares()->count() > 0)                    
                            @foreach ($reg->obratitulares as $titulares)
                            {{ $tr }}
                            <td>{{ $titulares->titular->pessoa->nome_razao }} </td>
                            @if ($titulares->titular->pseudonimoprincipal)
                            <td>{{ $titulares->titular->pseudonimoprincipal->pseudonimo }} </td>
                            @else
                            <td>--</td>
                            @endif
                            <td>{{ $titulares->desc_categoria }} </td>
                            <td style="text-align: right;">{{ number_format($titulares->pct_autoral, 2, ',', '.') }}% </td>
                            {{ $btr }}
                            <? $tr = '<tr>'; ?>
                            <? $btr = '</tr>'; ?>

                            @endforeach
                            @else
                            <td>--</td><td>--</td><td>--</td><td>--</td>
                            @endif
                            @if ($reg->obratitulares()->count() <= 1)
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END BORDERED TABLE PORTLET-->
    </div>
    @stop

    @section('scripts')
    $('#itemObras').addClass('active');
    @stop
