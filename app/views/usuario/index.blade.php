@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.titulo_cadastro', array('titulo'=> 'Usuários cadastrados', 'rota'=>'usuario'))

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>Registros encontrados
            </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-scrollable">
                @include('_padroes.table')
                <thead>
                    <tr>
                        <th class="center">Ações</th>
                        <th class="center">Id</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th style="text-align:center;">Último acesso</th>
                        <th style="text-align:center;">Acessa o backoffice?</th>
                        <th>Perfis</th>
                        <th>Observação</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($registros as $usuario)
                    <tr>
                        <td class="center">
                            <a href="{{URL::to('/admin/usuario/edit/'.$usuario->id_usuario)}}" class="btn btn-sm {{$botao_edicao_cor}}" title="Editar o usuário">{{$botao_edicao_icon}}</a>
                        </td>
                        <td class="center">{{$usuario->id_usuario}}</td>
                        <td>{{$usuario->nome}}</td>
                        <td>{{$usuario->email}}</td>
                        <td style="text-align: center;">{{$usuario->dt_ultimo_acesso_fmt}}</td>
                        <td style="text-align: center;">{{$usuario->acessa_backoffice_fmt}}</td>
                        <td>{{$usuario->perfis}}</td>
                        <td>{{$usuario->observacao}}</td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END BORDERED TABLE PORTLET-->
</div>
@stop
@section('scripts')
$('#menuAcesso').addClass('active');
$('#menuAcesso').addClass('open');
$('#itemUsuarios').addClass('active');
@stop
