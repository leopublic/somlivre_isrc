<?
$abas = array(
    'Fonograma' => array(
                    'titulo' => '<i class="fa fa-microphone"></i> Fonograma'    
                  , 'link' => "/admin/fonograma/edit/".$objeto->id_fonograma
                  , 'toggle' => ''
                  , 'ativo' => ''
                ),
    'Pot-pourri' => array('titulo' => '<i class="fa fa-flask"></i> Pot-pourri'    , 'link' => "/admin/fonogramapoutporrit/show/".$objeto->id_fonograma, 'toggle' => '', 'ativo' => ''),
    'Titulares' => array('titulo' => '<i class="fa fa-user"></i> Titulares'    , 'link' => "/admin/fonogramatitulares/show/".$objeto->id_fonograma, 'toggle' => '', 'ativo' => '')
);
$abas[$abaAtiva]['link'] = '#tab_1_1_1';
$abas[$abaAtiva]['toggle'] = 'data-toggle="tab"';
$abas[$abaAtiva]['ativo'] = 'class="active"';
?>

<ul class="nav nav-tabs nav-justified">
    @foreach($abas as $aba)
    <li {{$aba['ativo']}}><a href="{{$aba['link']}}" {{$aba['toggle']}}> {{$aba['titulo']}}</a></li>
    @endforeach
</ul>
