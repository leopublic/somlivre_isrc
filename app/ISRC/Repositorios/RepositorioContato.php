<?php
namespace ISRC\Repositorios;

class RepositorioContato extends RepositorioCadastro {
    
    public function nomeModelo(){
        return '\Contato';
    }
    
    public function ordenacaoPadrao(){
        return 'nome';
    }
}