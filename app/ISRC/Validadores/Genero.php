<?php
namespace ISRC\Validadores;
class Genero extends Validador {
    public $id_unique;
    public static $rules = array(
        'cod_genero'       => 'required',
        'nome'       => 'required',
    );
    public static $messages = array(
        'cod_genero.required'       => 'O código do gênero é obrigatório',
        'nome.required'       => 'O nome do gênero é obrigatório',
        'nome.unique'       => 'Já existe outro gênero com esse nome',
        'cod_genero.unique'       => 'Já existe outro gênero com esse código',
    );
    public function getRules(){
        $y = array_merge(static::$rules , array('nome' => 'required|unique:genero,nome,'.$this->id_unique.",id_genero"));
        $x = array_merge($y, array('cod_genero' => 'required|unique:genero,cod_genero,'.$this->id_unique.",id_genero"));
        return $x;
    }
}