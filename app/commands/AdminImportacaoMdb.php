<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use ISRC\Servicos\Importador;

class AdminImportacaoMdb extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin:importar-mdb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa uma base existente do ISRC para dentro do sistema';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        try {
            $mdb = "app/".$this->argument('mdb');
            $ambiente = $this->argument('ambiente');
            $preservar_idsn  = strtolower($this->argument('preservar_id'));
            if ($preservar_idsn == 's'){
                $preservar_id = true;
            } else {
                $preservar_id = false;
            }
            $aproveitar_cadastrossn  = strtolower($this->argument('aproveitar_cadastros'));

            $limpar = strtolower($this->argument('limpar'));

            if ($ambiente == 'SL'){
                $database = 'mysql_sl';
            } elseif($ambiente == 'BR'){
                $database = 'mysql_br';
            } elseif($ambiente == 'AB'){
                $database = 'mysql_br';
            } else {
                throw new Exception ('Ambiente indisponível');
            }

            Config::set('database.default', $database);

            if ($limpar == 's'){
                Artisan::call('migrate:refresh', array('--database' => $database));
            }

            $mdb_file = base_path() . DIRECTORY_SEPARATOR . $mdb;

            if (!file_exists($mdb_file)){
                $this->error('O arquivo '.$mdb_file." não foi encontrado! Verifique.");
                exit();
            }

            //==================================================
            // Abre conexão com MDB
            //==================================================
            $repMDB = new \ISRC\Repositorios\RepositorioMDB;
            $repMDB->abreConexao($mdb_file);
            Importador::$repMDB = $repMDB;
            if ($aproveitar_cadastrossn == 'n'){
                //==================================================
                // Importa o cadastro de tabelas
                //==================================================
                $importador = new \ISRC\Servicos\ImportadorTipoColetivo();
                $importador->importaRegistro(true);
                $importador = new \ISRC\Servicos\ImportadorArranjo();
                $importador->importaRegistro(true);
                $importador = new \ISRC\Servicos\ImportadorCategoria();
                $importador->importaRegistro(true);
                $importador = new \ISRC\Servicos\ImportadorPais ();
                $importador->importaRegistro(true);
                $importador = new \ISRC\Servicos\ImportadorGenero ();
                $importador->importaRegistro(true);
                $importador = new \ISRC\Servicos\ImportadorTipoFonograma ();
                $importador->importaRegistro(true);
                $importador = new \ISRC\Servicos\ImportadorTipoMidia ();
                $importador->importaRegistro(true);
                $importador = new \ISRC\Servicos\ImportadorGrupoInstrumento ();
                $importador->importaRegistro(true);
                $importador = new \ISRC\Servicos\ImportadorInstrumento ();
                $importador->importaRegistro(true);
                $importador = new \ISRC\Servicos\ImportadorSociedade();
                $importador->importaRegistro($preservar_id);
            }
            //==================================================
            // Importa o cadastro de entidades (associado a empresa)
            //==================================================
            $importador = new \ISRC\Servicos\ImportadorColetivo();
            $importador->importaRegistro($preservar_id);
            $importador = new \ISRC\Servicos\ImportadorPoutporrit();
            $importador->importaRegistro($preservar_id);
            $importador = new \ISRC\Servicos\ImportadorPessoa ();
            $importador->importaRegistro($preservar_id);
            $importador = new \ISRC\Servicos\ImportadorObra();
            $importador->importaRegistro($preservar_id);
            //==================================================
            // Importa o cadastro de entidades relacionadas a entidades (associado à empresa)
            //==================================================
            $importador = new \ISRC\Servicos\ImportadorTitular();
            $importador->importaRegistro($preservar_id);
            $importador = new \ISRC\Servicos\ImportadorFonograma();
            $importador->importaRegistro($preservar_id);
            $importador = new \ISRC\Servicos\ImportadorTitularPseudonimo();
            $importador->importaRegistro($preservar_id);
            //==================================================
            // Importa os relacionamentos
            //==================================================
            $importador = new \ISRC\Servicos\ImportadorObraTitular();
            $importador->importaRegistro($preservar_id);
            $importador = new \ISRC\Servicos\ImportadorTitularColetivo();
            $importador->importaRegistro($preservar_id);
            $importador = new \ISRC\Servicos\ImportadorPoutporritObra();
            $importador->importaRegistro($preservar_id);
            $importador = new \ISRC\Servicos\ImportadorFonogramaTitular();
            $importador->importaRegistro($preservar_id);
            $importador = new \ISRC\Servicos\ImportadorFonogramaTitularInstrumento();
            $importador->importaRegistro($preservar_id);
        } catch (Exception $e) {
            print $e->getMessage();
            print $e->getTraceAsString();
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array(
                array("mdb", InputArgument::REQUIRED, "Nome do MDB que será importado")
                ,array("ambiente", InputArgument::REQUIRED, "Ambiente onde será importado (SL ou BR)")
                ,array("limpar", InputArgument::REQUIRED, "Limpar os dados existentes? S/N")
                ,array("preservar_id", InputArgument::REQUIRED, "Preservar os IDs ? S/N")
                ,array("aproveitar_cadastros", InputArgument::REQUIRED, "Aproveitar os cadastros ? S/N")
            );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array();
    }
}