<?php

namespace ISRC\Repositorios;

class RepositorioFonograma extends RepositorioCadastro {

    public function nomeModelo() {
        return '\Fonograma';
    }

    public function ordenacaoPadrao() {
        return 'cod_isrc';
    }

    public function registrosConsulta($filtros = null) {

        $ret =  \Fonograma::filtro($filtros)
                        ->with('obra')
                        ->orderBy('cod_isrc')
                        ->paginate(20);
        $queries = \DB::getQueryLog();
        $last_query = end($queries);
        \Log::info($last_query);
        return $ret;
    }

    public function atualizaObra($id_fonograma, $id_obra) {
        $objeto = \Fonograma::find($id_fonograma);
        $objeto->id_poutporrit = null;
        $objeto->id_obra = $id_obra;
        $objeto->save();
    }

    public function atualizaPoutporrit($id_fonograma, $id_poutporrit) {
        $objeto = \Fonograma::find($id_fonograma);
        $objeto->id_poutporrit = $id_poutporrit;
        $objeto->id_obra = null;
        $objeto->save();
    }

    public function adicionarTitular($post) {
        $id_instrumentos = $post['id_instrumentos'];
        $id_fonograma = $post['id_fonograma'];
        if ($post['id_fonograma_titular'] == 0) {
            $fono_tit = new \FonogramaTitular;
            $fono_tit->id_fonograma = $post['id_fonograma'];
            $fono_tit->id_titular = $post['id_titular'];
        } else {
            $fono_tit = \FonogramaTitular::find($post['id_fonograma_titular']);
        }
        $fono_tit->id_categoria = $post['cod_categoria'];
        if (isset($post['id_pseudonimo']) && $post['id_pseudonimo'] > 0) {
            $fono_tit->id_pseudonimo = $post['id_pseudonimo'];
        } else {
            $fono_tit->id_pseudonimo = null;
        }
        $fono_tit->pct_participacao_fmt = $post['pct_participacao_fmt'];
        $fono_tit->dt_inicontrato_fmt = $post['dt_inicontrato_fmt'];
        $fono_tit->dt_fimcontrato_fmt = $post['dt_fimcontrato_fmt'];
        $fono_tit->id_coletivo = 0;
        $fono_tit->save();

        $fono_tit->instrumentos()->sync($id_instrumentos);

        $zerados = \FonogramaTitular::where('id_fonograma', '=', $id_fonograma)
                    ->where('pct_participacao', '=', 0)
                    ->count();
        if ($zerados > 0 || $post['id_fonograma_titular'] == 0 ){
            $this->calculaRateio($id_fonograma);
        }

        return $fono_tit;
    }

    public function validacao_adicional($post){
        $passou = true;
        if(intval($post['tmp_duracao_min']) == 0 && intval($post['tmp_duracao_seg']) == 0){
            $this->registreErro("A duração deve ser maior que zero");
            $passou = false;
        }
        if ($post['id_tipo_fonograma'] != ''){
            $tipo_fonograma = \TipoFonograma::find($post['id_tipo_fonograma']);
            if ($tipo_fonograma->ind_trilha == 'S'){
                if ($post['id_arranjo'] == ''){
                    $this->registreErro("O arranjo é obrigatório");
                    $passou = false;
                } else {
                    $arrajo = \Arranjo::find($post['id_arranjo']);
                    if ($arrajo->ind_complemento){
                        if ($post['dsc_complemento_arranjo'] == ''){
                            $this->registreErro("O complemento é obrigatório");
                            $passou = false;
                        }
                    }
                }
            }
        }
        // Valida ISRC
        // 
        if (!$post['is_gravacao_propria'] ){
            if (array_key_exists('Cod_ISRC', $post) ){
                $isrc = $post['Cod_ISRC'];
            } else {
                $isrc = '';
            }
            $isrc_a = explode("-", $isrc);
            if (count($isrc_a) != 4){
                $this->registreErro("ISRC inválido. Informe no formato AA-AAA-00-00000");
                $passou = false;
            } else {
                if (!preg_match('/^[A-Z]{2}$/D',$isrc_a[0])
                 || !preg_match('/^[A-Z]{3}$/D',$isrc_a[1])
                 || !preg_match('/^[0-9]{2}$/D',$isrc_a[2])
                 || !preg_match('/^[0-9]{5}$/D',$isrc_a[3]) ){
                    $this->registreErro("ISRC inválido. Informe no formato AA-AAA-00-00000 (formato dos blocos)");
                    $passou = false;
                }
            }
        }
        if ($post['dt_lancamento_fmt'] != '' && $post['dt_grav_original_fmt'] != ''){
            $dt_lancamento = \Carbon::createFromFormat('d/m/Y', $post['dt_lancamento_fmt'])->format('Y-m-d');
            $dt_grav_original = \Carbon::createFromFormat('d/m/Y', $post['dt_grav_original_fmt'])->format('Y-m-d');
            if ($dt_lancamento < $dt_grav_original){
                $this->registreErro("A data de lançamento não pode ser menor que a data da gravação original");
            }
        }

        return $passou;
    }

    public function adicionarTitularColetivo($post) {
        $fono_tit = new \FonogramaTitular;
        $fono_tit->id_fonograma = $post['id_fonograma'];
        $fono_tit->id_titular = $post['id_titular'];

        $titular = \Titular::find($post['id_titular']);
        $pseudonimo = $titular->pseudonimoprincipal;
        if (is_object($pseudonimo)) {
            $fono_tit->id_pseudonimo = $pseudonimo->id_pseudonimo;
        }
        $fono_tit->id_categoria = $post['id_categoria'];
        $fono_tit->pct_participacao_fmt = $post['pct_participacao_fmt'];

        $fono_tit->id_coletivo = $post['id_coletivo'];
        $fono_tit->save();

        $this->calculaRateio($post['id_fonograma']);

        return $fono_tit;
    }

    public function excluirTitular($id_fonograma_titular) {
        $fonotit = \FonogramaTitular::find($id_fonograma_titular);

        $id_fonograma = $fonotit->id_fonograma;
        \FonogramaTitular::where('id_fonograma_titular', '=', $id_fonograma_titular)->delete();
        $this->calculaRateio($id_fonograma);
    }

    public function preencheCampos($instancia, $post) {
        $instancia = parent::preencheCampos($instancia, $post);
        if (intval($instancia->id_fonograma) == 0 && $instancia->is_gravacao_propria) {
            $rep = new \ISRC\Repositorios\RepositorioEmpresa;
            $instancia->Cod_ISRC = $rep->geraNovoIsrc(\Auth::user()->id_empresa);
        }
        if ($instancia->id_fonograma > 0){
            $fono = \Fonograma::find($instancia->id_fonograma);
            if ($fono->Cod_ISRC != $instancia->Cod_ISRC){
                $instancia->Cod_ISRC_antes = $fono->Cod_ISRC;
            }
        }

        $instancia->Cod_ISRC = str_replace("-", "", $instancia->Cod_ISRC);

        if ($instancia->dt_emissao == ''){
            $instancia->dt_emissao = \Carbon::now('America/Sao_Paulo')->format('Y-m-d');
        }



        return $instancia;
    }

    public function calculaRateioJs($titulares){
        $tit = json_decode($titulares);
        $novo_tits = array();
        foreach($tit as $titular){
            $x = new FonogramaTitular;
            $x->id_categoria = $titular['id_categoria'];
            $x->id_coletivo = $titular['id_coletivo'];
        }
    }


    public function calculaRateio($id_fonograma){
        $titulares = $this->carregaTitularesEmArray($id_fonograma);
        $calculo = $this->classificaTitulares($titulares);
        // Calcula
        if (count($calculo['acompanhantes']) == 0){
            $total_interpretes = 50;
            $total_produtores = 50;
            $total_acompanhantes = 0;
        } else {
            $total_interpretes = 41.7;
            $total_produtores = 41.7;
            $total_acompanhantes = 16.6;
        }
        $calculo['interpretes']   = $this->rateia($calculo['interpretes'], $total_interpretes);
        $calculo['produtores']    = $this->rateia($calculo['produtores'], $total_produtores);
        $calculo['acompanhantes'] = $this->rateia($calculo['acompanhantes'], $total_acompanhantes);
        foreach($calculo as $nome => $grupos){
            if ($nome == 'interpretes' || $nome == 'produtores' || $nome == 'acompanhantes'){
                foreach($grupos as $titular ){
                    if (is_array($titular)){
                        foreach($titular as $coletivo){
                            $coletivo->save();
                        }
                    } else {
                        $titular->save();
                    }
                }
            }
        }
    }

    public function carregaTitularesEmArray($id_fonograma){
        $titulares = array();
        $avulsos = \FonogramaTitular::where('id_fonograma', '=', $id_fonograma)
                    ->whereRaw('coalesce(id_coletivo, 0) = 0')
                    ->get();
        foreach($avulsos as $titular){
            $titulares[] = $titular;
        }

        $coletivos = \FonogramaTitular::where('id_fonograma', '=', $id_fonograma)
                    ->where('id_coletivo', '<>', '0')
                    ->orderBy('id_coletivo')
                    ->get();
        if (count($coletivos) > 0){
            $id_coletivo_ant = '';

            foreach($coletivos as $titular){
                if ($id_coletivo_ant != $titular->id_coletivo){
                    if ($id_coletivo_ant > 0){
                        $titulares[] = $titDoColetivo;
                    } 
                    $id_coletivo_ant = $titular->id_coletivo;
                    $titDoColetivo = array();
                }
                $titDoColetivo[] = $titular;
            }
            $titulares[] = $titDoColetivo;
        }
        return $titulares;
    }

    public function classificaTitulares($titulares){
        $calculo = array(
            'interpretes' => array(),
            'produtores' => array(),
            'acompanhantes' => array(),
            'outros'        => array()
        );
        // Separa os titulares
        foreach($titulares as $titular){
            if (is_array($titular)){
                $temp = array();
                foreach($titular as $coletivo){
                    //$calculo = $this->classificaTitular($calculo, $coletivo);
                    $cat = $this->categoriaTitular($coletivo);
                    $temp[$cat][] = $coletivo;
                }
                if (array_key_exists('interpretes', $temp) && count($temp['interpretes']) > 0 ){
                    $calculo['interpretes'][] = $temp['interpretes'];
                }
                if (array_key_exists('produtores', $temp) && count($temp['produtores']) > 0 ){
                    $calculo['produtores'][] = $temp['produtores'];
                }
                if (array_key_exists('acompanhantes', $temp) && count($temp['acompanhantes']) > 0 ){
                    $calculo['acompanhantes'][] = $temp['acompanhantes'];
                }
            } else {
                //$calculo = $this->classificaTitular($calculo, $titular);
                $cat = $this->categoriaTitular($titular);
                $calculo[$cat][] = $titular;
            }
        }
        return $calculo;
    }

    public function classificaTitular($calculo, $titular){
        if (count($titular->categoria) > 0){
            if ($titular->categoria->pct_max == 41.7){
                if(trim($titular->categoria->cod_categoria) == 'I'){
                    $calculo['interpretes'][] = $titular;
                } else {
                    $calculo['produtores'][] = $titular;
                }
            } else {
                $calculo['acompanhantes'][] = $titular;
            }
        } else {
            $calculo['acompanhantes'][] = $titular;
        }
        return $calculo;
    }

    public function categoriaTitular( $titular){
        if (count($titular->categoria) > 0){
            if ($titular->categoria->pct_max == 41.7){
                if(trim($titular->categoria->cod_categoria) == 'I'){
                    return 'interpretes';
                } else {
                    return 'produtores';
                }
            } else {
                return 'acompanhantes';
            }
        } else {
            return 'acompanhantes';
        }
        return $calculo;
    }

    public function rateia($titulares, $valorTotal){
        if ($valorTotal > 0 && count($titulares) > 0){
            $valor = $this->round_down($valorTotal / count($titulares), 8);
            foreach($titulares as &$titular){
                if (is_array($titular)){
                    $this->rateia($titular, $valor);
                } else {
                    $titular->pct_participacao = $valor;
                }
            }
        }
        return $titulares;
    }
    public function round_down($number, $precision )
    {
        $fig = (int) str_pad('1', $precision, '0');
        return (floor($number * $fig) / $fig);
    }
}
