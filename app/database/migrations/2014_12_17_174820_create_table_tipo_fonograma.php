<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTipoFonograma extends Migration {

	public function up()
	{
		Schema::create('tipo_fonograma', function($table) {
			$table->increments('id_tipo_fonograma');
			$table->string('dsc_tipo_fonograma', 100)->nullable();
			$table->string('usuario', 20)->nullable();
			$table->string('Ind_Situacao', 1)->nullable();
			$table->string('ind_trilha', 1)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}
	
	public function down()
	{
		if (Schema::hasTable('tipo_fonograma')) {
			Schema::drop('tipo_fonograma');
		}
	}
	
}
