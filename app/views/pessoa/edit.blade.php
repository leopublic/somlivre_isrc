@extends('base')

@section('estilos')
@stop

@section('conteudo')

@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-gift"></i> Cadastro </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body form">
                {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                {{Form::hidden('id_pessoa', $reg->id_pessoa, array('id'=> 'id_pessoa'))}}
                {{Form::hidden('id_titular', $titular->id_titular, array('id'=> 'id_titular'))}}
                <div class="form-body">
                    @include('_campos.texto_id', array('valor'=> $reg->id_pessoa))
                    @include('_campos.texto', array('label'=> 'Nome', 'nome'=>'nome_razao'))
                    @include('_campos.texto', array('label'=> 'Cód. ECAD', 'nome'=>'cod_ecad'))
                    @include('_campos.texto', array('label'=> 'Email principal', 'nome'=>'email'))
                    <div class="form-group">
                        <label class="col-md-3 control-label">Origem</label>
                        <div class="col-md-9">
                            {{Form::hidden('is_nacional', 0)}}
                            <div class="radio-list">
                                <label class="radio-inline">
                                    {{ Form::radio('is_nacional', 1, Input::old('is_nacional', $titular->is_nacional) )}} Nacional
                                </label>
                                <label class="radio-inline">
                                    {{ Form::radio('is_nacional', 0, !Input::old('is_nacional', $titular->is_nacional) )}} Estrangeira
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tipo de pessoa</label>
                        <div class="col-md-9">
                            {{Form::hidden('is_pf', 0)}}
                            <div class="radio-list" id="lista_is_pf">
                                <label class="radio-inline">
                                    {{ Form::radio('is_pf', 1, Input::old('is_pf', $reg->is_pf) )}} Física
                                </label>
                                <label class="radio-inline">
                                    {{ Form::radio('is_pf', 0, !Input::old('is_pf', $reg->is_pf) )}} Jurídica
                                </label>
                            </div>
                        </div>
                    </div>
                    <div id="pessoa_fisica">
                    <h4>Pessoa física</h4>
                    @include('_campos.select', array('label'=> 'Tipo CPF', 'nome'=>'tp_cpf', 'valores'=> $tipos_cpf))
                    @include('_campos.texto', array('label'=> 'CPF', 'nome'=>'cpf', 'valor' => $reg->cpf_cnpj))
                    @include('_campos.texto', array('label'=> 'RG', 'nome'=>'identidade'))
                    @include('_campos.texto', array('label'=> 'Órgão emissor', 'nome'=>'orgao_emissor'))
                    @include('_campos.texto', array('label'=> 'Nascimento', 'nome'=>'dt_nascimento_fmt', 'class' => 'mask_data'))
                    @include('_campos.texto', array('label'=> 'Nacionalidade', 'nome'=>'nacionalidade'))
                    @include('_campos.texto', array('label'=> 'Profissão', 'nome'=>'profissao'))
                    @include('_campos.select', array('label'=> 'Estado civil', 'nome'=>'id_estado_civil', 'valores'=> $estados_civis))
                    @include('_campos.select', array('label'=> 'Sexo', 'nome'=>'sexo', 'valores'=> array("F"=>"Feminino", "M"=>"Masculino")))
                    </div>
                    <div id="pessoa_juridica">
                    <h4>Pessoa jurídica</h4>
                    @include('_campos.texto', array('label'=> 'CNPJ', 'nome'=>'cnpj', 'valor' => $reg->cpf_cnpj))
                    @include('_campos.texto', array('label'=> 'Nome fantasia', 'nome'=>'fantasia'))
                    @include('_campos.texto', array('label'=> 'Incrição estadual', 'nome'=>'insc_estadual'))
                    @include('_campos.texto', array('label'=> 'Incrição municipal', 'nome'=>'insc_municipal'))
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Classificação</label>
                        <div class="col-md-9">
                            {{Form::hidden('is_autor', 0)}}
                            {{Form::hidden('is_editora', 0)}}
                            {{Form::hidden('is_interprete', 0)}}
                            {{Form::hidden('is_musico', 0)}}
                            {{Form::hidden('is_produtor_fono', 0)}}
                            <div class="checkbox-list">
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="is_autor" name="is_autor" value="1" @if ($reg->is_autor) checked @endif  > Autor </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="is_editora" name="is_editora" value="1" @if ($reg->is_editora) checked @endif> Editora </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="is_interprete" name="is_interprete" value="1"  @if ($reg->is_interprete) checked @endif> Intérprete </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="is_musico" name="is_musico" value="1" @if ($reg->is_musico) checked @endif> Músico</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="is_produtor_fono" name="is_produtor_fono" value="1" @if ($reg->is_produtor_fono) checked @endif> Produtor fonográfico</label>
                            </div>
                        </div>
                    </div>
                    <h4>Titular</h4>
                    @include('_campos.select', array('label'=> 'Sociedade', 'nome'=>'id_sociedade', 'valores'=> $sociedades, 'valor' => $titular->id_sociedade))
                    @include('_campos.texto', array('label'=> 'Cód. CAE/IPI', 'nome'=>'cod_cae', 'valor' => $titular->cod_cae))
                    @include('_campos.texto', array('label'=> 'Cód. OMB', 'nome'=>'cod_omb', 'valor' => $titular->cod_omb))
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green">Salvar</button>
                        <a href="{{URL::to('/admin/titular')}}" class="btn default">Cancelar</a>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
@if ($reg->id_pessoa > 0)

<div class="row">
    <div class="col-md-12">
        <div class="portlet box {{$portlet_fonograma}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-microphone"></i> Pseudônimos</div>
                <div class="actions">
                    <a href="#" class="btn btn-sm {{$botao_novo_cor}}" onclick="btnPseudonimoAdicionarClick();">{{$botao_novo_icon}} Novo</a>
                </div>
            </div>
            <div class="portlet-body" id="body-pseudonimos">

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box {{$portlet_fonograma}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-microphone"></i> Endereços</div>
                <div class="actions">
                    <a href="javascript:getEditarEndereco('{{$reg->id_pessoa}}', '0');" class="btn btn-sm {{$botao_novo_cor}}" onclick="">{{$botao_novo_icon}} Novo</a>
                </div>
            </div>
            <div class="portlet-body" id="containerEnderecos">
                @include('pessoa/tabela_enderecos')
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box {{$portlet_fonograma}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-microphone"></i> Telefones</div>
                <div class="actions">
                    <a href="javascript:getEditarTelefone('{{$reg->id_pessoa}}', '0');" class="btn btn-sm {{$botao_novo_cor}}" onclick="">{{$botao_novo_icon}} Novo</a>
                </div>
            </div>
            <div class="portlet-body" id="containerTelefones">
                @include('pessoa/tabela_telefones')
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box {{$portlet_fonograma}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-microphone"></i> Contatos</div>
                <div class="actions">
                    <a href="javascript:getEditarContato('{{$reg->id_pessoa}}', '0');" class="btn btn-sm {{$botao_novo_cor}}" onclick="">{{$botao_novo_icon}} Novo</a>
                </div>
            </div>
            <div class="portlet-body"  id="containerContatos">
                @include('pessoa/tabela_contatos')
            </div>
        </div>
    </div>
</div>
@endif
<div id="frmEditPseudonimo" class="modal" tabindex="-1"  data-width="650" data-height="200">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Incluir/alterar pseudônimo</h4>
    </div>
    <div class="modal-body">
        {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "form_titular"))}}
        {{Form::hidden('id_titular', $titular->id_titular)}}
        {{Form::hidden('id_pseudonimo', 0, array('id'=> 'id_pseudonimo'))}}
        <div class="form-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group" id="id_pseu">
                        <label class="col-md-3 control-label">ID</label>
                        <div class="col-md-9">
                            <input id="id" type="text" value="" class="form-control" disabled="disabled"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Pseudônimo</label>
                        <div cla0ss="col-md-9">
                            <input id="pseudonimo" name="pseudonimo" type="text" value="" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Principal?</label>
                        <div class="col-md-9">
                            <input id="is_principal" name="is_principal" type="checkbox" value="1" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{Form::close()}}
        <div id="modal-msg" style="color:red;"></div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn blue" onclick="javascript:btnSalvarPseudonimo();">Salvar</button>
        <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
    </div>
</div>
<div id="frmEditEndereco" class="modal" tabindex="-1"  data-width="650" data-height="420">
</div>
<div id="frmEditTelefone" class="modal" tabindex="-1"  data-width="650" data-height="330">
</div>
<div id="frmEditContato" class="modal" tabindex="-1"  data-width="650" data-height="290">
</div>

@stop

@section('scripts_full')
<script>
function corrigePessoa(){
    if ($('input[name=is_pf]:checked').val() == '1'){
        $('#pessoa_juridica').hide();
        $('#pessoa_fisica').show();
    } else {
        $('#pessoa_juridica').show();
        $('#pessoa_fisica').hide();
    }
 }

$('#itemTitulares').addClass('active');

    jQuery(document).ready(function () {
        $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
          '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
            '<div class="progress progress-striped active">' +
              '<div class="progress-bar" style="width: 100%;"></div>' +
            '</div>' +
          '</div>';

        $.fn.modalmanager.defaults.resize = true;
        if ($('#id_pessoa').val() > 0){
            atualizaPseudonimo();
        }

        corrigePessoa();
        $("#cnpj").inputmask("99.999.999/9999-99", {
            "placeholder": "__.___.___/____-__"
        }); //multi-char placeholder

        $("#cpf").inputmask("999.999.999-99", {
            "placeholder": "___.___.___-__"
        }); //multi-char placeholder
    });

    $( "input[name=is_pf]:radio" ).change(function() {
        corrigePessoa();
    });


    function btnPseudonimoAdicionarClick(id_pseudonimo, pseudonimo, is_principal){
        $('body').modalmanager('loading');
        $('#frmEditPseudonimo #id_pseudonimo').val(id_pseudonimo);
        $('#frmEditPseudonimo #id').val(id_pseudonimo);
        $('#frmEditPseudonimo #pseudonimo').val(pseudonimo);
        if (is_principal == '1'){
            $('#uniform-is_principal').find('span').addClass('checked');
            $('#is_principal').attr('checked', true);
        } else {
            $('#uniform-is_principal').find('span').removeClass('checked');
            $('#is_principal').attr('checked', false);
        }
        $('#frmEditPseudonimo').modal();
    }

    function btnSalvarPseudonimo(){
        Metronic.blockUI({
            target: '#frmEditPseudonimo',
            boxed: true,
            message: '(aguarde...)',
        });
        var url = '/admin/pseudonimo/edit';
        var dados = $( "#frmEditPseudonimo form" ).serialize();
        $.post( url, dados)
            .done(function( data ) {
                Metronic.unblockUI('#frmEditPseudonimo');
                $('#frmEditPseudonimo').modal('hide');
                atualizaPseudonimo();
            });
    }

    function atualizaPseudonimo(){
        var container = $('#body-pseudonimos');
        container.html('<div style="text-align:center; margin:auto;"><br><i class="fa fa-spinner fa-spin fa-4x"></i></div>');
        var id_titular = $('#id_titular').val();
        var url = '/admin/pseudonimo/index/'+id_titular;
        container.load( url, function(response, status, xhr){
        } );
    }

//===========================================================
// Endereco
//===========================================================
    function getEditarEndereco(id_pessoa, id_endereco){
        var url = '/admin/endereco/editar/'+id_pessoa+'/'+id_endereco;
        Metronic.blockUI({boxed: true, overlayColor:'#acacac',message: 'Aguarde...'});
            $('#frmEditEndereco').empty();
            $('#frmEditEndereco').load( url, '', function(){
                Metronic.unblockUI();
                $('frmEditEndereco .select2').select2();
                $('#frmEditEndereco').modal();
            });
    }
    function postEditarEndereco(){
        Metronic.blockUI({
            target: '#frmEditEndereco',
            boxed: true,
            message: '(aguarde...)'
        });
        $.post( "/admin/endereco/editar", $( "#frmEndereco" ).serialize()
            , function(data){
                Metronic.unblockUI('#frmEditEndereco');
                if (data.codret == 0){
                    $('#frmEditEndereco').modal('hide');
                    $(document.body).animate({
                        'scrollTop':  $('#containerEnderecos').offset().top
                    }, 2000);
                    $('#containerEnderecos').html(data.html);
                    $('#endereco'+data.id_endereco).stop().animate({backgroundColor: "#95A5A6"}, 100)
                        .animate({backgroundColor: "#FFFFFF"}, 4500);
                } else {
                    $('#frmEditEndereco #modal-msg').html(data.msg);
                }
            }
            , "json" )
        ;
    }

    function postExcluirEndereco(id_pessoa, id_endereco){
        Metronic.blockUI({
            target: '#containerEnderecos',
            boxed: true,
            message: '(aguarde...)'
        });
        $.post( "/admin/endereco/excluir", {id_pessoa: id_pessoa, id_endereco: id_endereco}
            , function(data){
                Metronic.unblockUI('#containerEnderecos');
                $('#containerEnderecos').html(data.html);
                $('#frmEditEnderecos #modal-msg').html(data.msg);
            }
            , "json" )
        ;
    }

//===========================================================
// Telefone
//===========================================================
    function getEditarTelefone(id_pessoa, id_telefone){
        var url = '/admin/telefone/editar/'+id_pessoa+'/'+id_telefone;
        Metronic.blockUI({boxed: true, overlayColor:'#acacac',message: 'Aguarde...'});
            $('#frmEditTelefone').empty();
            $('#frmEditTelefone').load( url, '', function(){
                Metronic.unblockUI();
                $('frmEditTelefone .select2').select2();
                $('#frmEditTelefone').modal();
            });
    }
    function postEditarTelefone(){
        Metronic.blockUI({
            target: '#frmEditTelefone',
            boxed: true,
            message: '(aguarde...)'
        });
        $.post( "/admin/telefone/editar", $( "#frmTelefone" ).serialize()
            , function(data){
                Metronic.unblockUI('#frmEditTelefone');
                if (data.codret == 0){
                    $('#frmEditTelefone').modal('hide');
                    $(document.body).animate({
                        'scrollTop':  $('#containerTelefones').offset().top
                    }, 2000);
                    $('#containerTelefones').html(data.html);
                    $('#telefone'+data.id_telefone).stop().animate({backgroundColor: "#95A5A6"}, 100)
                        .animate({backgroundColor: "#FFFFFF"}, 4500);
                } else {
                    $('#frmEditTelefone #modal-msg').html(data.msg);
                }
            }
            , "json" )
        ;
    }

    function postExcluirTelefone(id_pessoa, id_telefone){
        Metronic.blockUI({
            target: '#containerTelefones',
            boxed: true,
            message: '(aguarde...)'
        });
        $.post( "/admin/telefone/excluir", {id_pessoa: id_pessoa, id_telefone: id_telefone}
            , function(data){
                Metronic.unblockUI('#containerTelefones');
                $('#containerTelefones').html(data.html);
                $('#frmEditTelefone #modal-msg').html(data.msg);
            }
            , "json" )
        ;
    }

//===========================================================
// Contato
//===========================================================
    function getEditarContato(id_pessoa, id_contato){
        var url = '/admin/contato/editar/'+id_pessoa+'/'+id_contato;
        Metronic.blockUI({boxed: true, overlayColor:'#acacac',message: 'Aguarde...'});
            $('#frmEditContato').empty();
            $('#frmEditContato').load( url, '', function(){
                Metronic.unblockUI();
                $('frmEditContato .select2').select2();
                $('#frmEditContato').modal();
            });
    }

    function postEditarContato(){
        Metronic.blockUI({
            target: '#frmEditContato',
            boxed: true,
            message: '(aguarde...)'
        });
        $.post( "/admin/contato/editar", $( "#frmContato" ).serialize()
            , function(data){
                Metronic.unblockUI('#frmEditContato');
                if (data.codret == 0){
                    $('#frmEditContato').modal('hide');
                    $(document.body).animate({
                        'scrollTop':  $('#containerContatos').offset().top
                    }, 2000);
                    $('#containerContatos').html(data.html);
                    $('#contato'+data.id_contato).stop().animate({backgroundColor: "#95A5A6"}, 100)
                        .animate({backgroundColor: "#FFFFFF"}, 4500);
                } else {
                    $('#frmEditContato #modal-msg').html(data.msg);
                }
            }
            , "json" )
        ;
    }

    function postExcluirContato(id_pessoa, id_contato){
        Metronic.blockUI({
            target: '#containerContatos',
            boxed: true,
            message: '(aguarde...)'
        });
        $.post( "/admin/contato/excluir", {id_pessoa: id_pessoa, id_contato: id_contato}
            , function(data){
                Metronic.unblockUI('#containerContatos');
                $('#containerContatos').html(data.html);
                $('#frmEditContato #modal-msg').html(data.msg);
            }
            , "json" )
        ;
    }

</script>
@stop