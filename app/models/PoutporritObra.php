<?php
/**
 * @property int $id_tipo_fonograma
 * @property string $dsc_tipo_fonograma Nome 
 * @property string $Ind_Situacao
 * @property string $ind_trilha
 * @property datetime created_at 
 * @property datetime updated_at 
 * 
 */
class PoutporritObra extends Modelo {
    protected $table = 'poutporrit_obra';
    protected $primaryKey = 'id_poutporrit_obra';
    protected $guarded = array();

    public function obra(){
        return $this->belongsTo('Obra', 'id_obra', 'id_obra');
    }

    public function poutporrit(){
        return $this->belongsTo('Poutporrit', 'id_poutporrit', 'id_poutporrit');
    }
}