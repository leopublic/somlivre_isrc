<?php

class UsuarioTableSeeder extends Seeder {

    public function run() {
        // Uncomment the below to wipe the table clean before populating
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('usuario')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $usuario = array("nome" => "Leonardo Medeiros (M2)", "email" => "leonardo@m2software.com.br", "created_at" => date("Y-m-d H:i:s"), "password" => Hash::make("windsurf"), "acessa_backoffice" => 1, "observacao" => "Criado na instalação da aplicação");
        DB::table('usuario')->insert($usuario);
        $usuario = array("nome" => "Victor Medeiros (M2)", "email" => "victor@m2software.com.br", "created_at" => date("Y-m-d H:i:s"), "password" => Hash::make("ojr4675"), "acessa_backoffice" => 1, "observacao" => "Criado na instalação da aplicação");
        DB::table('usuario')->insert($usuario);
        $usuario = array("nome" => "Rodrigo Moreno", "email" => "rodrigo.moreno@somlivre.com.br", "created_at" => date("Y-m-d H:i:s"), "password" => Hash::make("somlivre"), "acessa_backoffice" => 1, "observacao" => "Criado na instalação da aplicação");
        DB::table('usuario')->insert($usuario);
    }

}
