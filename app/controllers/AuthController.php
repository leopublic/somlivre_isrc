<?php
/**
 * Controla a verificação de acessos
 */
class AuthController extends \BaseController {

    public function getLogin() {
        return View::make('auth.login');
    }

    public function postLogin() {
        $email = Input::get('email');
        $password = Input::get('password');
        $rep = new \ISRC\Repositorios\RepositorioAuth();
        if ($rep->login($email, $password)){
            if (Auth::user()->acessa_backoffice){
                if (Auth::user()->id_empresa == ''){
                    return Redirect::to('/admin/home/empresa/');
                } else {
                    return Redirect::to('/admin/');
                }
            } else {
                return Redirect::to('/');
            }
        } else {
            $erros = $rep->getErrors();
            Session::flash('msg', $erros[0]);
            return Redirect::to('auth/login');
        }
    }

    public function postEsqueceusenha() {
        if(Input::has('email')){
            $usuario = \Usuario::where('email', '=', Input::get('email'))->first();
            if (is_object($usuario)){
                $rep = new \ISRC\Repositorios\RepositorioUsuario;
                $rep->enviaNovaSenha($usuario);
                Session::flash('login_errors', true);
                Session::flash('msg', 'Nova senha enviada para o e-mail "'. Input::get('email') . '". Verifique na sua caixa de entrada.');
                return Redirect::to('/auth/login');
            } else {
                Session::flash('login_errors', true);
                Session::flash('msg', 'Nenhum usuário encontrado com o e-mail informado');
                return Redirect::back();
            }
        } else {
                Session::flash('login_errors', true);
            Session::flash('msg', 'Informe o e-mail');
            return Redirect::back();
        }
    }

    public function getLogout() {
        Session::clear();
        Auth::logout();
        return Redirect::to('/');
    }

    public function getAltereminhasenha() {
        $usuario = Auth::user();
        return View::make('usuario.altereminhasenha')
                        ->with('model', $usuario)
        ;
    }
    public function postAltereminhasenha() {
        try{
            Input::flash();
            $rep = new \Westhead\Repositorios\RepositorioUsuario();
            if($rep->altereMinhaSenha(Input::all())){
                $msg = 'Senha alterada com sucesso!';
                Session::flash('flash_msg', $msg);
                return Redirect::to('/home/bemvindo');
            } else {
                return Redirect::back()->withErrors($rep->getErrors());
            }
        } catch (Exception $ex) {
            Session::flash('flash_error', $ex->getMessage());
            return Redirect::back();
        }
    }

}
