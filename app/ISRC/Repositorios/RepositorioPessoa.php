<?php
namespace ISRC\Repositorios;

class RepositorioPessoa extends RepositorioCadastro {

    public function tituloModelo() {
        return "titular";
    }
    
    public function nomeModelo(){
        return '\Pessoa';
    }
    
    public function ordenacaoPadrao(){
        return 'nome_razao';
    }
    public function registrosConsulta($filtros = null){
    	return \Pessoa::orderBy('nome_razao')->paginate(20);    
    }
    
    public function salve($post) {        
        $id = $post['id_pessoa'];
        if (intval($id) > 0) {
            $instancia = \Pessoa::find($id);
        } else {
            $instancia = new \Pessoa;
        }
        $this->validador = new \ISRC\Validadores\Pessoa($post);
        $this->validador->id_unique = $id;
        if ($this->validador->passes()) {
            $instancia = $this->preencheCampos($instancia, $post);
            $instancia->is_pf = $post['is_pf'];
            if ($post['is_pf'] == 0){
                $instancia->cpf_cnpj = preg_replace('/\D/', '', $post['cnpj']);
            } else {
                $instancia->cpf_cnpj = preg_replace('/\D/', '', $post['cpf']);
            }
            $instancia->save();
            $this->aposSalvar($instancia, $post);
            $pessoa = $instancia;
        } else {
            return false;
        }
        
        $id = $post['id_titular'];
        if (intval($id) > 0) {
            $instancia = \Titular::find($id);
        } else {
            $instancia = new \Titular;
        }
        $this->validador = new \ISRC\Validadores\Titular($post);
        $this->validador->id_unique = $id;
        if ($this->validador->passes()) {
            $instancia->id_pessoa = $pessoa->id_pessoa;
            $instancia->id_sociedade = $post['id_sociedade'];
            $instancia->cod_cae = $post['cod_cae'];
            $instancia->cod_omb = $post['cod_omb'];
            $instancia->is_nacional = $post['is_nacional'];
            $instancia->save();
            return $pessoa;
        } else {
            return false;
        }
    }
    
    public function preencheCampos($instancia, $post){
        $instancia->fill($post);
        return $instancia;
    }

}