<?php

namespace ISRC\Repositorios;

class RepositorioUsuario extends RepositorioCadastro {

    public function nomeModelo() {
        return '\Usuario';
    }

    public function ordenacaoPadrao() {
        return 'nome';
    }

    public function salve($post) {
        $id_usuario = $post['id_usuario'];
        if (intval($id_usuario) > 0) {
            $usuario = \Usuario::find($id_usuario);
        } else {
            $usuario = new \Usuario;
        }
        if (intval($post ['id_usuario']) > 0) {
            $validacao = new \ISRC\Validadores\UsuarioExistente($post);
            $validacao->id_unique = $post ['id_usuario'];
        } else {
            $validacao = new \ISRC\Validadores\UsuarioNovo($post);
        }
        $usuario = $this->instanciaUsuario($post);
        if ($validacao->passes()) {
            $usuario->fill($post);
            $usuario->save();

            $usuario->perfis()->sync($post['id_perfil']);
            return $usuario;
        } else {
            return false;
        }
    }

    public static function novaSenhaAleatoria($name_length = 8) {
        $alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789$#%&*()_+=!-[]{},.<>~^';
        return substr(str_shuffle($alpha_numeric), 0, $name_length);
    }

    /**
     * Cria usuário para atualização
     * 
     * @param unknown $post        	
     */
    public function instanciaUsuario($post) {
        if ($post ['id_usuario'] > 0) {
            $usuario = \Usuario::find($post ['id_usuario']);
        } else {
            $usuario = new \Usuario ();
            $usuario->id_usuario_cadastro = \Auth::user()->id_usuario;
            $usuario->qtd_tentativas_erradas = 0;
        }
        return $usuario;
    }

    public function altereSenha($post) {
        $id_usuario = $post['id_usuario'];
        $usuario = \Usuario::findOrFail($id_usuario);
        $this->validador = new \ISRC\Validadores\Senha($post);
        if ($this->validador->passes()) {
            $usuario->password = \Hash::make($post['password']);
            $usuario->save();
            return true;
        } else {
            return false;
        }
    }

    public function altereMinhaSenha($post) {
        $usuario = \Auth::user();
        if (isset($post['atual'])) {
            if (\Hash::check($post['atual'], $usuario->password)) {
                $this->validador = new \ISRC\Validadores\Senha($post);
                if ($this->validador->passes()) {
                    $usuario->password = \Hash::make($post['password']);
                    $usuario->fl_alterar_senha = 0;
                    $usuario->save();
                    return true;
                } else {
                    return false;
                }
            } else {
                $this->registreErro('A senha atual não confere');
                return false;
            }
        } else {
            $this->registreErro('A senha atual deve ser informada');
            return false;
        }
    }

    public function enviaNovaSenha(\Usuario $usuario) {
        $seg = new \ISRC\Servicos\Seguranca();
        $novasenha = $seg->rand_string(10);
        $usuario->password = \Hash::make($novasenha);
        $usuario->qtd_tentativas_erradas = 0;
        $usuario->save();
        $data = array('usuario' => $usuario, 'novasenha' => $novasenha);

        \Mail::send('emails.nova_senha', $data, function($message) use($usuario) {
            $message->to($usuario->email, $usuario->nome)->subject('Alteração de senha');
        });
    }

    public function edita($post) {
        $id_usuario = $post['id_usuario'];
        if ($id_usuario > 0) {
            $usuario = \Usuario::find($id_usuario);
        } else {
            $usuario = new \Usuario;
        }
        $this->validador = new \ISRC\Validadores\Usuario($post);
        $this->validador->id_unique = $id_usuario;
        if ($this->validador->passes()) {
            $usuario->fill($post);
            if (array_key_exists('senha', $post) && $post['senha'] != '') {
                $usuario->password = \Hash::make($post['senha']);
            }
            $usuario->save();
            if (isset($post['id_perfil'])) {
                $perfis = $post['id_perfil'];
            } else {
                $perfis = array();
            }
            $usuario->perfis()->sync($perfis);
            return $usuario;
        } else {
            return false;
        }
    }

}
