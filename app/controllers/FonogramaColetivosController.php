<?php

class FonogramaColetivosController extends BaseController {
    public function getShow($id_fonograma) {
        $rep = new \ISRC\Repositorios\RepositorioFonograma;
        $regs = $rep->registrosConsulta();
        $objeto = \Fonograma::find($id_fonograma);
        return View::make('fonograma.coletivos')
                ->with('objeto', $objeto)
                ->with('regs', $regs);
    }


    
    public function getFormpesquisa(){
        return View::make('fonograma.formPesquisaColetivo')
        ;
        
    }

}
