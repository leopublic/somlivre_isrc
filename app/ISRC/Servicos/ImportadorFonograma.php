<?php

namespace ISRC\Servicos;

class ImportadorFonograma extends Importador{

    public function sqlSelectMdb() {
        return "select id_fonograma, id_poutporrit, id_genero, id_coletivo, id_obra, id_tipo_fonograma, id_pais_origem, id_pais_publicacao, id_tipo_midia, cod_fonograma_ECAD, cod_GRA, num_catalogo, Cod_ISRC, dsc_complemento_arranjo, is_nacional, is_instrumental, is_gravacao_propria, is_publsimult, tmp_duracao_min, tmp_duracao_seg, cod_arranjo, dt_grav_original, dt_lancamento, dt_emissao, dt_inclusao, dt_alteracao, usuario, obs from ISRC_fonograma_tb where is_deleted = 0 and id_fonograma > 0";
    }
    
    public function novoObjeto(){
        return new \Fonograma();
    }
    
    public function nome_id_local(){
        return 'id_fonograma';
    }
    public function tabela(){
        return "ISRC_fonograma_tb";
    }
        
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            // 'id_poutporrit',
            // 'id_coletivo',
            // 'id_obra',
            'id_genero',
            'id_tipo_fonograma',
            'id_pais_origem',
            'id_pais_publicacao',
            'id_tipo_midia',
            'cod_fonograma_ECAD',
            'cod_GRA',
            'num_catalogo',
            'dsc_complemento_arranjo',
            'is_nacional',
            'is_instrumental',
            'is_gravacao_propria',
            'is_publsimult',
            'tmp_duracao_min',
            'tmp_duracao_seg',
            'obs',
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);

        $obj->id_coletivo = $this->obtem_id_coletivo($regMDB['id_coletivo'], $regMDB['id_fonograma']);
        $obj->id_poutporrit = $this->obtem_id_poutporrit($regMDB['id_poutporrit'], $regMDB['id_fonograma']);
        $obj->id_obra = $this->obtem_id_obra($regMDB['id_obra'], $regMDB['id_fonograma']);

        $obj->dt_grav_original =  $this->dataHoraOk($regMDB['dt_grav_original']);
        $obj->dt_lancamento =  $this->dataHoraOk($regMDB['dt_lancamento']);
        $obj->dt_emissao =  $this->dataHoraOk($regMDB['dt_emissao']);
        $obj->cod_isrc =  $regMDB['Cod_ISRC'];
        $obj->id_arranjo =  $regMDB['cod_arranjo'];

        $obj->usuario = $regMDB['usuario'];
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        $obj->id_fonograma_original = $regMDB['id_fonograma'];
        $obj->id_empresa = \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa;

        return $obj;
    }
}