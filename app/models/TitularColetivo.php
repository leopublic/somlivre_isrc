<?php
/**
 * @property int $id_titular_coletivo
 * @property int $id_coletivo
 * @property int $id_titular
 * @property date $dt_entrada
 * @property date $dt_saida
 * @property datetime created_at 
 * @property datetime updated_at 
 * 
 */
class TitularColetivo extends Modelo {
    protected $table = 'titular_coletivo';
    protected $primaryKey = 'id_titular_coletivo';
    protected $guarded = array();

    public function titular(){
        return $this->belongsTo('Titular', 'id_titular', 'id_titular');
    }
 
    public function getDtEntradaFmtAttribute(){
        return $this->dtFmt('dt_entrada');
    }
    public function setDtEntradaFmtAttribute($valor){
        $this->attributes['dt_entrada'] = $this->dtViewParaModel($valor);
    }
 
    public function getDtSaidaFmtAttribute(){
        return $this->dtFmt('dt_saida');
    }
    public function setDtSaidaFmtAttribute($valor){
        $this->attributes['dt_saida'] = $this->dtViewParaModel($valor);
    }
}