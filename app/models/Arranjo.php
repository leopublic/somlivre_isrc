<?php
/**
 * @property int $id_arranjo
 * @property string $dsc_arranjo Nome
 * @property boolean $ind_complemento
 * @property datetime created_at
 * @property datetime updated_at
 *
 */
class Arranjo extends Modelo {
    protected $table = 'arranjo';
    protected $primaryKey = 'id_arranjo';
    protected $guarded = array();

    public function carregaPeloMDB($registro){

    }

    public static function combo($filtro = ''){
        return \Arranjo::orderBy('dsc_arranjo')->lists('dsc_arranjo', 'id_arranjo');
    }

    public static function precisam_complemento(){
        $arranjos = \Arranjo::where('ind_complemento', '=', 1)->get();
        $ret = '';
        foreach($arranjos as $arranjo){
            $ret .= '#'.$arranjo->id_arranjo.'#';
        }
        return $ret;
    }

}