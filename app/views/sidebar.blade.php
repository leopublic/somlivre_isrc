<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <li id="itemTitulares" class="start"><a href="{{URL::to('/admin/titular')}}"><i class="fa fa-user"></i><span class="title">Titulares</span></a></li>
            <li id="itemObras"><a href="{{URL::to('/admin/obra')}}"><i class="fa fa-music"></i><span class="title">Obras</span></a></li>
            <li id="itemFonogramas"><a href="{{URL::to('/admin/fonograma')}}"><i class="fa fa-microphone"></i><span class="title">Fonogramas</span></a></li>
            <li id="menuCadastros">
                <a href="javascript:;"><i class="fa fa-database"></i><span class="title">Cadastros</span><span class="arrow "></span></a>
                <ul class="sub-menu">
                    <li id="itemSociedadesAutorais"><a href="{{URL::to('/admin/sociedade')}}"><i class="icon-home"></i>Sociedades autorais</a></li>
                    <li id="itemColetivos"><a href="{{URL::to('/admin/coletivo')}}"><i class="fa fa-users"></i>Coletivos</a></li>
                    <li id="itemPotpourri"><a href="{{URL::to('/admin/potpourri')}}"><i class="icon-home"></i>Pot-pourri</a></li>
                </ul>
            </li>
            <li id="menuTabelas">
                <a href="javascript:;"><i class="fa fa-database"></i><span class="title">Tabelas</span><span class="arrow "></span></a>
                <ul class="sub-menu">
                    <li id="itemCategorias"><a href="{{URL::to('/admin/categoria')}}"><i class="fa fa-tags"></i>Categorias</a></li>
                    <li id="itemGeneros"><a href="{{URL::to('/admin/genero')}}"><i class="icon-home"></i>Gêneros musicais</a></li>
                    <li id="itemInstrumentos"><a href="{{URL::to('/admin/instrumento')}}"><i class="fa fa-bell"></i>Instrumentos</a></li>
                    <li id="itemPaises"><a href="{{URL::to('/admin/pais')}}"><i class="icon-home"></i>Países</a></li>
                    <li id="itemTipomidias"><a href="{{URL::to('/admin/tipomidia')}}"><i class="icon-home"></i>Tipos de mídia</a></li>
                    <li id="itemTiposProduto"><a href="{{URL::to('/admin/tipoproduto')}}"><i class="icon-home"></i>Tipos de produto</a></li>
                </ul>
            </li>
            <li id="menuAcesso">
                <a href="javascript:;"><i class="icon-lock"></i><span class="title">Acesso</span><span class="arrow "></span></a>
                <ul class="sub-menu">
                    <li id="itemPerfis"><a href="{{URL::to('/admin/perfil')}}"><i class="fa fa-key"></i>Perfis</a></li>
                    <li id="itemUsuarios"><a href="{{URL::to('/admin/usuario')}}"><i class="icon-users"></i>Usuários</a></li>
                </ul>
            </li>
            <li id="itemEmpresas"><a href="{{URL::to('/admin/empresa')}}"><i class="fa fa-building"></i><span class="title">Empresas (parâmetros)</span></a></li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->
