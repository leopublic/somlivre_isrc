<?php

class DatabaseSeeder extends Seeder {

    public function run() {
        Eloquent::unguard();

        $this->call('PerfilTableSeeder');
        $this->call('UsuarioTableSeeder');
//        $this->call('EmpresaTableSeeder');
    }

}
