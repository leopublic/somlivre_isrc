<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFonoTitularInstrumento extends Migration {

    public function up() {
        Schema::create('fonograma_titular_instrumento', function ($table) {
            $table->increments('id_fonograma_titular_instrumento');
            $table->integer('id_fonograma_titular')->unsigned()->nullable();
            $table->integer('id_instrumento')->unsigned()->nullable();
            $table->integer('id_categoria')->unsigned()->nullable();
            
            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->dateTime('dt_envio')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down() {
        if (Schema::hasTable('fonograma_titular_instrumento')) {
            Schema::drop('fonograma_titular_instrumento');
        }
    }

}
