<?php

class TelefoneController extends BaseController
{

    public function repo() {
        return new ISRC\Repositorios\RepositorioTelefone;
    }

    public function getIndex($id_pessoa) {
        $rep = $this->repo();
    }

    public function getEditar($id_pessoa, $id_telefone) {
        if ($id_telefone > 0) {
            $reg = \Endereco::find($id_telefone);
            $titulo = 'Alterar telefone';
        }
        else {
            $reg = new \Endereco;
            $reg->id_pessoa = $id_pessoa;
            $titulo = "Adicionar telefone";
        }
        $tipos_telefone = \TipoTelefone::comboComDefault();
        $data = array(
        	 'tipos_telefone' => $tipos_telefone
        	, 'reg' => $reg
        	, 'titulo' => $titulo
        );
        return View::make('telefone/edit', $data);
    }

    public function postEditar() {
        $ret = array();
        try {
            $rep = $this->repo();
            $post = Input::all();
            $endereco = $rep->salve($post);

            $telefones = \Telefone::where('id_pessoa', '=', $post['id_pessoa'])
                            ->get();
            $html = View::make('pessoa.tabela_telefones')->with('telefones', $telefones);
            $ret['html'] = iconv('UTF-8', 'UTF-8', $html);
            $ret['codret'] = 0;
            $ret['msg'] = iconv('UTF-8', 'UTF-8', 'Telefone adicionado com sucesso!');
            $ret['id_telefone'] = $endereco->id_telefone;
        }
        catch(Exception $ex) {
            $ret['codret'] = - 1;
            $ret['msg'] = "Não foi possível adicionar esse telefone: " . $ex->getMessage() . $ex->getTraceAsString();
        }
        return json_encode($ret);
    }


    public function postExcluir() {
    	$id_pessoa = Input::get('id_pessoa');
    	$id_telefone = Input::get('id_telefone');
        $ret = array();
        try {
            $rep = $this->repo();
            $rep->exclua($id_telefone);

            $telefones = \Telefone::where('id_pessoa', '=', $id_pessoa)->get();
            $html = View::make('pessoa.tabela_telefones')->with('telefones', $telefones);
            $ret['html'] = iconv('UTF-8', 'UTF-8', $html);
            $ret['codret'] = 0;
            $ret['msg'] = iconv('UTF-8', 'UTF-8', 'Telefone excluído com sucesso!');
        }
        catch(Exception $ex) {
            $ret['codret'] = - 1;
            $ret['msg'] = "Não foi possível excluir esse telefone: " . $ex->getMessage() . $ex->getTraceAsString();
        }
        return json_encode($ret);
    }
}
