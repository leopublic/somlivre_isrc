<?php
/**
 * @property int $id_genero
 * @property string $nome Nome
 * @property date $dt_inclusao
 * @property date $dt_alteracao
 * @property string $usuario Usuário que cadastrou(ECAD)
 * @property datetime created_at
 * @property datetime updated_at
 * @property int $cod_genero
 *
 */
class Genero extends Modelo {
    protected $table = 'genero';
    protected $primaryKey = 'id_genero';
    protected $guarded = array();

    public function carregaPeloMDB($registro){

    }

    public static function combo($filtro = ''){
        return \Genero::orderBy('nome')->lists('nome', 'id_genero');
    }
}