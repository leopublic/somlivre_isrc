<?php
/**
 * @property integer id_empresa
 * @property integer id_produtor_fonografico
 * @property integer id_pais
 * @property string sgl_IFPI
 * @property integer ano_base
 * @property string $seq_ISRC
 * @property string $sgl_radical_IFPI
 * @property integer $seq_exportacao
 */
class Empresa extends Modelo {
    protected $table = 'empresa';
    protected $primaryKey = 'id_empresa';
    protected $guarded = array();
    protected $connection = "mysql";

    public function carregaPeloMDB($registro){

    }

    public static function combo($filtro = ''){
        return \Empresa::orderBy('nome')->lists('nome', 'id_empresa');
    }

    public function getAnoBaseParaIsrcAttribute(){
    	return substr($this->attributes['ano_base'], -2);
    }

    public function getSeqExportacaoAttribute(){
        return intval($this->attributes['seq_exportacao']);
    }

    public function incrementaExportacao(){
        $this->seq_exportacao = $this->seq_exportacao + 1;
        $this->save();
    }

    public static function outrasEmpresas($id_empresa){
        return \Empresa::where('id_empresa', '<>', $id_empresa)->orderBy('nome')->get();
    }
}