<?php

class PseudonimoController extends BaseController {
    public function getIndex($id_titular){
        $regs = \Pseudonimo::where('id_titular', '=', $id_titular)->get();
        return View::make('pseudonimo.tabela')
                ->with('regs', $regs);  
    }
    
    public function getEdit($id_pessoa, $id_pseudonimo){
        if ($id_pseudonimo > 0){
            $pseu = \Pseudonimo::find($id_pseudonimo);
        } else {
            $pseu = new \Pseudonimo;
            $pseu->id_pseudonimo =0;
            $pseu->id_pessoa = $id_pessoa;
        }
        return View::make('pseudonimo.edit')
                ->with('reg', $pseu);          
    }
    
    public function postEdit(){
        if (Input::get('id_pseudonimo')> 0){
            $pseu = \Pseudonimo::find(Input::get('id_pseudonimo'));
        } else {
            $pseu = new \Pseudonimo;
            $pseu->id_titular = Input::get('id_titular');
        }
        $pseu->pseudonimo = Input::get('pseudonimo');
        $pseu->is_principal = Input::get('is_principal');
        $pseu->save();
        
        if ($pseu->is_principal){
            \Pseudonimo::where('id_titular', '=', $pseu->id_titular)
                    ->where('id_pseudonimo', '<>', $pseu->id_pseudonimo)
                    ->update(array('is_principal' => '0'));
        }
    }
}
