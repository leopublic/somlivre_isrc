<?php
/**
 * @property int $id_tipo_midia
 * @property string $dsc_tipo_midia Nome 
 * @property date $dt_inclusao
 * @property date $dt_alteracao
 * @property string $usuario Usuário que cadastrou(ECAD)
 * @property datetime created_at 
 * @property datetime updated_at 
 * 
 */
class TipoMidia extends Modelo {
    protected $table = 'tipo_midia';
    protected $primaryKey = 'id_tipo_midia';
    protected $guarded = array();

    public function carregaPeloMDB($registro){
        
    }
    
    public static function combo($filtro = ''){
        return \TipoMidia::orderBy('dsc_tipo_midia')->lists('dsc_tipo_midia', 'id_tipo_midia');
    }
    
}