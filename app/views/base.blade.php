<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.1.1
Version: 3.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Som Livre ISRC</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="{{URL::to('/')}}/assets/global/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE STYLES -->
        <link href="{{URL::to('/')}}/assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="{{URL::to('/')}}/assets/global/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/')}}/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        @if(\Auth::user()->id_empresa > 0)
            <link href="{{URL::to('/')}}/assets/admin/layout/css/themes/empresa{{\Auth::user()->id_empresa}}.css" rel="stylesheet" type="text/css"/>
        @else
            <link href="{{URL::to('/')}}/assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
        @endif
        <link href="{{URL::to('/')}}/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="/favicon.ico"/>
        <style>
            .page-logo .logo-default {
                margin: 6px 0px 0px !important;
            }
            table.table tr th.center,
            table.table tr td.center{
                text-align: center;
            }
            table.table tr th.right,
            table.table tr td.right{
                text-align: right;
            }
            span.subitem{
                color: #557386;
                font-style: italic;
            }
            .table tbody tr.grossa td{
                border-top:solid 2px #999;
            }
            .pagination{
                margin: 0px !important;
            }
            @yield('estilos')
            
        </style>
            
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body class="page-header-fixed page-quick-sidebar-over-content">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="index.html">
                        <img src="{{URL::to('/')}}/imagens/logoh.png" alt="logo" class="logo-default"/>
                    </a> 
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <!-- BEGIN USER LOGIN DROPDOWN -->
<!--                         <li class="dropdown dropdown-user"> -->
<!--                             <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> -->
<!--                                 <img alt="" class="img-circle" src="{{URL::to('/')}}/assets/admin/layout/img/avatar3_small.jpg"/> -->
<!--                                 <span class="username"> -->
<!--                                     Bob </span> -->
<!--                                 <i class="fa fa-angle-down"></i> -->
<!--                             </a> -->
<!--                             <ul class="dropdown-menu"> -->
<!--                                 <li> -->
<!--                                     <a href="extra_profile.html"> -->
<!--                                         <i class="icon-user"></i> Meus dados </a> -->
<!--                                 </li> -->
<!--                                 <li class="divider"> -->
<!--                                 </li> -->
<!--                                 <li> -->
<!--                                     <a href="{{URL::to('/auth/lock')}}"> -->
<!--                                         <i class="icon-lock"></i> Suspender </a> -->
<!--                                 </li> -->
<!--                                 <li> -->
<!--                                     <a href="{{URL::to('/auth/logout')}}"> -->
<!--                                         <i class="icon-key"></i> Sair </a> -->
<!--                                 </li> -->
<!--                             </ul> -->
<!--                         </li> -->
                        <!-- END USER LOGIN DROPDOWN -->
                        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                        <li class="dropdown ">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true" title="Clique para sair" style="padding-top: 12px;padding-bottom: 13px;">
                                 Empresa: {{Auth::user()->nome_empresa}} 
                            </a>
                            <ul class="dropdown-menu">
                                @foreach($empresas_contexto as $empresa)
                                    <li><a href="/admin/home/selecioneempresa/{{$empresa->id_empresa}}"><i class="fa fa-arrow-right"></i> Alterar para {{$empresa->nome}}</a><li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="dropdown dropdown-user">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true" title="Clique para sair" style="padding-top: 12px;padding-bottom: 13px;">
                                 {{Auth::user()->nome}} <i class="fa fa-4x fa-sign-out"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="/auth/logout"><i class="fa fa-exclamation-circle"></i>Sair</a><li>
                            </ul>
                        </li>
                        <!-- END QUICK SIDEBAR TOGGLER -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            @include('sidebar')
            <div class="page-content-wrapper">
                <div class="page-content">
                    @section('conteudo')
                    @show
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner">
                2015 &copy; Som Livre
            </div>
            <div class="page-footer-tools">
                <span class="go-top">
                    <i class="fa fa-angle-up"></i>
                </span>
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="{{URL::to('/')}}/assets/global/plugins/respond.min.js"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{URL::to('/')}}/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js" type="text/javascript"></script>
        <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/select2/select2.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>

        <script src="{{URL::to('/')}}/assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/admin/pages/scripts/components-pickers.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
        jQuery(document).ready(function () {
            Metronic.init(); // init metronic core componets
            Layout.init(); // init layout
//            Index.init();
//            Index.initJQVMAP(); // init index page's custom scripts
            $(".mask_data").inputmask("d/m/y", {
                "placeholder": "dd/mm/aa"
            }); //multi-char placeholder
            $('.popovers').popover();
        });
        @yield('scripts')
        </script>
        @yield('scripts_full')
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>