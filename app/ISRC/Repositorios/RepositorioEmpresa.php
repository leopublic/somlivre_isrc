<?php

namespace ISRC\Repositorios;

class RepositorioEmpresa extends RepositorioCadastro {

    public function tituloModelo() {
        return 'empresa';
    }

    public function nomeModelo() {
        return '\Empresa';
    }

    public function ordenacaoPadrao() {
        return 'nome';
    }

    public function registrosConsulta($filtros = null) {
        return \Empresa::paginate(20);
    }

    public function geraNovoIsrc($id_empresa){
        $empresa = \Empresa::find($id_empresa);
        $empresa->seq_ISRC++;
        $empresa->save();
        $ret = $empresa->sgl_radical_IFPI
                .$empresa->sgl_IFPI
                .$empresa->ano_base_para_isrc
                .substr('00000'.$empresa->seq_ISRC, -5);
        return $ret;
    }
}
