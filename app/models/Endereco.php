<?php
/**
 * @property int $id_endereco
 * @property int $id_pessoa
 * @property varchar $endereco
 * @property varchar $complemento
 * @property varchar $bairro
 * @property varchar $cidade
 * @property varchar $cep
 * @property tinyint $is_correspondencia
 * @property tinyint $is_entrega
 * @property int $id_uf
 * @property int $id_pais
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property timestamp $deleted_at
 */
class Endereco extends Modelo {
    protected $table = 'endereco';
    protected $primaryKey = 'id_endereco';
    protected $guarded = array();

    public function pais(){
        return  $this->belongsTo('Pais', 'id_pais', 'id_pais');
    }

    public function uf(){
        return  $this->belongsTo('Uf', 'id_uf', 'id_uf');
    }
}