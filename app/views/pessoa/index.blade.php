@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.titulo_cadastro', array('titulo'=> 'Titulares', 'rota'=>'pessoa'))

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>Registros encontrados
            </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
       		{{$regs->links()}}
            <div class="table-scrollable">
                @include('_padroes.table')
                <thead>
                    <tr>
                        <th class="center">Ações</th>
                        <th class="center">Id</th>
                        <th>Código</th>
                        <th class="center">Nome</th>
                        <th class="center">Data cadastro</th>
                        </tr>
                </thead>
                <tbody>
                @foreach($regs as $reg)
                    <tr>
                        <td class="center">
                            <a href="{{URL::to('/admin/pessoa/edit/'.$reg->id_pessoa)}}" class="btn btn-sm green-seagreen" title="Editar"><i class="fa fa-edit"></i></a>
                        </td>
                        <td class="center">{{$reg->id_pessoa}}</td>
                        <td>{{$reg->nome_razao}}</td>
                        <td class="center">{{$reg->cpf_cnpj_formatado}}</td>
                        <td class="center">{{$reg->dtHrFmt('dt_inclusao')}}</td>
                    </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END BORDERED TABLE PORTLET-->
</div>
@stop