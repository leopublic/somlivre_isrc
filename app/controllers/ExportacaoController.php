<?php
class ExportacaoController extends BaseController
{
	public function getExportar(){
		$empresas = \Empresa::comboComDefault();
		$exp = \Exportacao::where('id_usuario', '=', \Auth::user()->id_usuario)->where('id_status_exportacao', '=', 1);
		if (!is_object($exp)){
			$exp = new \Exportacao;
		}
		return View::make('exportacao')
			->with('empresas', $empresas)
			->with('exportacao', $exp)
			;
	}

	public function postExportar(){
		$id_empresa = Input::get('id_empresa');
		$rep = new \ISRC\Repositorios\RepositorioExportacao;
		$exp = $rep->nova($id_empresa, \Auth::user()->id_usuario);
		return Redirect::to('/exportacao/exportar');
	}
	/**
	 * Processa uma exportação pendente
	 * @return [type] [description]
	 */
	public function getProcessar(){
		set_time_limit(0);
		$rep = new \ISRC\Repositorios\RepositorioExportacao;
		$exp = \Exportacao::where('id_usuario', '=', \Auth::user()->id_usuario)->where('id_status_exportacao', '=', 1);
		if (is_object($exp) && $exp->id_status_exportacao == 1){
			$rep->processar($exp);
		}

	}


	public function getDebug($id_empresa, $entidade, $reg, $id ){
		$emp = \Empresa::find($id_empresa);
		switch ($entidade){
			case "fonograma":
				$exp = new \ISRC\Exportacao\Fonograma($emp);
				$exp->debug = true;
				switch($reg){
					case "detalhe":
						$fonograma = \Fonograma::find($id);
						$exp->detalhe($fonograma);
						break;
					case "titular":
						$fonogramatitular = \FonogramaTitular::find($id);
						$fonograma = $fonogramatitular->fonograma;
						$exp->titular($fonograma, $fonogramatitular);
						break;
					case "instrumento":
						$fonogramatitularinstrumento = \FonogramaTitularInstrumento::find($id);
						$fonograma = $fonogramatitularinstrumento->fonogramatitular->fonograma;
						$exp->instrumento($fonograma, $fonogramatitularinstrumento);
						break;
					case "coletivo":
						$fonogramatitular = \FonogramaTitular::find($id);
						$fonograma = $fonogramatitular->fonograma;
						$exp->coletivo($fonograma, $fonogramatitular->coletivo);
						break;
				}
				break;
			case "titular":
				$exp = new \ISRC\Exportacao\Titular($emp);
				$exp->debug = true;
				switch($reg){
					case "detalhe":
						$titular = \Titular::find($id);
						$exp->detalhe($titular);
						break;
					case "documentacao":
						$titular = \Titular::find($id);
						$exp->documentacao($titular);
						break;
					case "pseudonimo":
						$pseudonimo = \Pseudonimo::find($id);
						$titular = $pseudonimo->titular;
						$exp->pseudonimo($titular, $pseudonimo);
						break;
				}
				break;
			case "obra":
				$exp = new \ISRC\Exportacao\Obra($emp);
				$exp->debug = true;
				switch($reg){
					case "detalhe":
						$obra = \Obra::find($id);
						$exp->detalhe($obra);
						break;
					case "titular":
						$obratitular = \ObraTitular::find($id);
						$obra = $obratitular->obra;
						$exp->titular($obra, $obratitular);
						break;
					case "subtitulo":
						$obra = \Obra::find($id);
						$exp->subtitulo($obra);
						break;
				}
				break;
			case "poutporrit":
				$exp = new \ISRC\Exportacao\Poutporrit($emp);
				$exp->debug = true;
				switch($reg){
					case "detalhe":
						$poutporrit = \Poutporrit::find($id);
						$exp->detalhe($poutporrit);
						break;
					case "obra":
						$poutporritobra = \PoutporritObra::find($id);
						$poutporrit = $poutporritobra->poutporrit;
						$exp->obra($poutporrit, $poutporritobra);
						break;
				}
				break;
		}
		$regs = $exp->saidaDebug;
		return View::make('exportacao')
			->with('empresa', $emp)
			->with('entidade', $entidade)
			->with('reg', $reg)
			->with('id', $id)
			->with('regs', $regs)
			;
	}
}