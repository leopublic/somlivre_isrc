<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEndereco extends Migration {

    public function up() {
        Schema::create('endereco', function($table) {
            $table->increments('id_endereco');
            $table->integer('id_pessoa');
            $table->string('endereco', 60)->nullable();
            $table->string('complemento', 30)->nullable();
            $table->string('bairro', 30)->nullable();
            $table->string('cidade', 30)->nullable();
            $table->string('cep', 8)->nullable();
            $table->boolean('is_correspondencia');
            $table->boolean('is_entrega');
            $table->integer('id_uf')->nullable();
            $table->integer('id_pais')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        //
    }

    public function down() {
        Schema::dropIfExists('endereco');
        //
    }

}
