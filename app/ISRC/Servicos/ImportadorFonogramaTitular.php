<?php

namespace ISRC\Servicos;

class ImportadorFonogramaTitular extends Importador{

    public function sqlSelectMdb() {
        return "select  id_fonograma_titular,
            id_categoria,
            id_fonograma,
            id_titular,
            id_pseudonimo,
            id_coletivo,
            pct_participacao,
            dt_inicontrato,
            dt_fimcontrato,
            dt_inclusao,
            dt_alteracao,
            usuario, 
            ind_calc_auto 
            from ISRC_fonograma_titular_tb where is_deleted = 0";
    }
    
    public function novoObjeto(){
        return new \FonogramaTitular();
    }
    
    public function nome_id_local(){
        return 'id_fonograma_titular';
    }

    public function tabela(){
        return "ISRC_fonograma_titular_tb";
    }
        
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            'id_fonograma_titular',
            'id_categoria',
            'id_fonograma',
            'id_titular',
            'id_pseudonimo',
            'id_coletivo',
            'pct_participacao',
            'ind_calc_auto'
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);
        $obj->usuario = $regMDB['usuario'];
        $obj->id_fonograma_titular_original = $regMDB['id_fonograma_titular'];
        $obj->dt_inicontrato =  $this->dataHoraOk($regMDB['dt_inicontrato']);
        $obj->dt_fimcontrato =  $this->dataHoraOk($regMDB['dt_fimcontrato']);
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        $obj->id_empresa = \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa;
        return $obj;
    }
}