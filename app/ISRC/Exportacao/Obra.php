<?php
namespace ISRC\Exportacao;
class Obra extends Exportacao{
	const tprDETALHE = '1';
	const tprTITULARES = '2';
	const tprSUBTITULOS = '3';
	const tprSUBSTITUICAO = '9';

	public static function nomeEntidade(){
		return '\Obra';
	}
	public function tipo_cadastro(){
		return 'OBM';
	}

	public function exporta($obra){
		$this->abreTransacao();
		$this->detalhe($obra);
		$titulares = $obra->obratitulares;
		foreach($titulares as $titular){
			$this->titular($obra, $titular);
		}
		if ($obra->subtitulo != ''){
			$this->subtitulo($obra);
		}
		$this->fechaTransacao();
	}

	public function detalhe($obra){
		$this->novo_reg(self::tpoINCLUIR, self::tprDETALHE);
		$this->adicionaCampo($obra->cod_obra_ecad, 'N', 13, 'Código ECAD obra');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC obra');			//  Código ISRC
		$this->adicionaCampo($obra->titulo_original, 'A', 95, 'Titulo principal da obra');			// Título principal
		$this->adicionaCampo($obra->is_nacional, 'B', 1, 'É nacional');			// É nacional
		$this->adicionaCampo($obra->is_instrumental, 'B', 1,'É instrumental');			// É instrumental
		$this->adicionaCampo('', 'B', 1, 'É domínio público');			// É domínio público
		$this->adicionaCampo('', 'D', 8, 'Data de registro da obra');			// Data registro da obra
		$this->adicionaCampo('', 'D', 8, 'Data de emissão');			// Data de emissão
		$this->adicionaCampo('', 'D', 8, 'Data de criação');			// Data de criação
		$this->adicionaCampo($obra->cod_ISWC, 'A', 11, 'Código ISWC');			// Código ISWC
		$this->adicionaCampo('', 'H', 6, 'Duração');			// Duração
		$this->adicionaCampo('', 'A', 2, 'Sigla país');			// Sigla pais
		$this->adicionaCampo($obra->relacaoComDefault('genero', 'cod_genero'), 'A', 10, 'Código do gênero musical');			// Código do gênero musical
		$this->adicionaCampo('', 'B', 1, 'Tem regra de varsóvia');
		$this->adicionaCampo(false, 'B', 1, 'É homonima');
		$this->adicionaCampo(false, 'B', 1, 'É obra derivada');
		$this->adicionaCampo('', 'N', 13, 'Código ECAD obra original');
		$this->adicionaCampo('', 'A', 15, 'Código SIRC obra musical original');
		$this->adicionaCampo('', 'A', 2, 'Sigla do idioma');
		$this->adicionaCampo(false, 'B', 1, 'É obra composta');
		$this->adicionaCampo('', 'A', 3, 'Código do tipo de obra composta');
		$this->adicionaCampo('', 'A', 22, 'Sociedade responsável pela informação');
		$this->adicionaCampo('', 'A', 22, 'Sociedade responsável pelo cadastro');
		$this->adicionaCampo('ID', 'A', 2, 'Situação cadastral da obra musical');
		$this->adicionaCampo(false, 'B', 1, 'É bloqueada');
		$this->adicionaCampo($obra->relacaoComDefault('genero', 'nome'), 'A', 30, 'Nome do gênero');
		$this->grava_registro();
	}

	public function titular($obra, $obratitular){
		$this->novo_reg(self::tpoINCLUIR, self::tprTITULARES);
		$this->adicionaCampo($obra->cod_obra_ecad, 'N', 13, 'Código ECAD obra');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC obra');			// Cod ISRC
		$this->adicionaCampo($obratitular->titular->pessoa->cod_ecad, 'N', 13, 'Código ECAD titular');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC titular');			// Cod ISRC
		$this->adicionaCampo($obratitular->titular->pessoa->nome_razao, 'A', 45, 'Nome titular'); //
		$this->adicionaCampo($obratitular->titular->pessoa->tp_pessoa, 'A', 1,'Tipo pessoa'); //

		if ($obratitular->titular->pessoa->tp_pessoa == 'J'){
			$this->adicionaCampo(0, 'N', 11, 'CPF');
			$this->adicionaCampo($obratitular->titular->pessoa->cpf_cnpj, 'N', 14, 'CNPJ');
		} else {
			$this->adicionaCampo($obratitular->titular->pessoa->cpf_cnpj, 'N', 11, 'CPF');
			$this->adicionaCampo(0, 'N', 14, 'CPNJ');
		}
		$this->adicionaCampo($obratitular->titular->cod_cae, 'N', 11, 'Código CAE'); // Código CAE
		$this->adicionaCampo('', 'A', 6, 'Tipo de direito');
		$this->adicionaCampo($obratitular->relacaoComDefault('categoria', 'cod_categoria'), "A", 2, 'Código categoria');
		$this->adicionaCampo('', 'A', 2, 'Código sub-categoria');
		$this->adicionaCampo(str_replace('.', ',', $obratitular->pct_autoral), 'N', 5, 'Percentual participacao');
		$this->adicionaCampo('', 'D', 8, 'Data início');
		$this->adicionaCampo('', 'D', 8, 'Data fim');
		$this->adicionaCampo('A', 'A', 1, 'Situação titular da obra');
		$this->adicionaCampo(true, 'B', 1, 'Titular é nacional');
		$this->adicionaCampo('', 'A', 22, 'Sigla da sociedade');
		$this->adicionaCampo($obratitular->relacaoComDefault('pseudonimo', 'pseudonimo'), 'A', 45, 'Nome do pseudonimo');

		$this->grava_registro();
	}

	public function subtitulo($obra){
		$this->novo_reg(self::tpoINCLUIR, self::tprSUBTITULOS);
		$this->adicionaCampo($obra->cod_obra_ecad, 'N', 13, 'Código ECAD obra');
		$this->adicionaCampo('', 'A', 15,'Código ISRC obra');			// Cod ISRC
		$this->adicionaCampo('', 'A', 2, 'Código tipo de título');			// Código tipo de titulo
		$this->adicionaCampo($obra->subtitulo, 'A', 50, 'Subtítulo');

		$this->grava_registro();
	}

}