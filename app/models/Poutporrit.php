<?php
/**
 * @property int $id_tipo_fonograma
 * @property string $dsc_tipo_fonograma Nome 
 * @property string $Ind_Situacao
 * @property string $ind_trilha
 * @property datetime created_at 
 * @property datetime updated_at 
 * 
 */
class Poutporrit extends Modelo {
    protected $table = 'poutporrit';
    protected $primaryKey = 'id_poutporrit';
    protected $guarded = array();

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            $model->id_empresa = \Auth::user()->id_empresa;
        });
    }

    public function obras(){
        return $this->hasMany('PoutporritObra', 'id_poutporrit', 'id_poutporrit');
    }
 
    public function scopeWherefiltro($query, $filtro) {
        if ($filtro != ''){
            $sql = "( nome like ?"
                    . " or cod_poutporrit_ECAD like ? "
                    . " or cod_IWC like ?";
            $sql.= " or id_poutporrit in ("
                    . " select id_poutporrit "
                    . " from poutporrit_obra po "
                    . " join obra  o on o.id_obra = po.id_obra"
                    . " where po.id_poutporrit = poutporrit.id_poutporrit"
                    . " and o.titulo_original like ? "
                    . ")";
            if (is_numeric($filtro)){
                $sql .= " or id_coletivo = ".$filtro;
            }
            $sql .= ")";
            $query->whereRaw($sql, array('%' . $filtro . '%', '%' . $filtro . '%', '%' . $filtro . '%', '%' . $filtro . '%'));
        }
        $query->where('id_empresa', '=', \Auth::user()->id_empresa);
        return $query;
    }
}