<?
if (!isset($valor)){
    $valor = Input::old($nome, $reg->$nome);
}
?>
<div class="form-group">
    <label class="col-md-3 control-label">{{$label}}</label>
    <div class="col-md-9">
        {{Form::select($nome, $valores, $valor, array("class" => "form-control select2me"))}}
    </div>
</div>
