<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">{{$titulo}}</h4>
</div>
<div class="modal-body">
    {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "frmContato"))}}
    {{Form::hidden('id_pessoa', $reg->id_pessoa, array('id' => 'id_pessoa'))}}
    {{Form::hidden('id_contato', $reg->id_contato, array('id' => 'id_contato'))}}
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @if ($reg->id_contato > 0)
                    @include('_campos.texto_id', array('valor'=> $reg->id_contato))
                @endif
                @include('_campos.texto', array('label' => 'Nome', 'nome' => 'nome'))
                @include('_campos.texto', array('label' => 'E-mail', 'nome' => 'email'))
                @include('_campos.select', array('label' => 'Telefone', 'nome' => 'id_telefone', 'valores' => $telefones))
                @include('_campos.texto', array('label' => 'Observação', 'nome' => 'observacao'))
            </div>
        </div>
    </div>
    {{Form::close()}}
    <div id="modal-msg" style="color:red;"></div>
</div>
<div class="modal-footer">
    <a href="javascript:postEditarContato();" class="btn blue">Salvar</a>
    <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
</div>
