<?php

/**
 * @property int $id_sociedade
 * @property string $sgl_sociedade
 * @property string $nome
 * @property boolean $ind_nacional
 * @property date $dt_inclusao
 * @property date $dt_alteracao
 * @property datetime created_at 
 * @property datetime updated_at 
 * @property string $usuario
 */
class Sociedade extends Modelo {

    protected $table = 'sociedade';
    protected $primaryKey = 'id_sociedade';
    protected $guarded = array('id_empresa');

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            $model->id_empresa = \Auth::user()->id_empresa;
        });
    }

    public static function combo($filtro = '') {
        return \Sociedade::orderBy('nome')->lists('nome', 'id_sociedade');
    }

    public function scopeWherefiltro($query, $filtro) {
        if ($filtro != '') {
            $query->where(function($query) use($filtro){
                $query->orWhere('nome', 'like', '%' . $filtro . '%')
                    ->orWhere('sgl_sociedade', 'like', '%' . $filtro . '%');
            });

        }
        $query->where('id_empresa', '=', \Auth::user()->id_empresa);
        return $query;
    }

}
