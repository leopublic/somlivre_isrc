<?php

class InstrumentoController extends CadastroController {
    public function repo(){
        return new \ISRC\Repositorios\RepositorioInstrumento;
    }
    
    public function diretorioViews() {
        return 'instrumento';
    }
    
    public function raizRota() {
        return '/admin/instrumento';
    }
    
    public function msgSucesso(){
        return 'Instrumento atualizado com sucesso';
    }    

    public function complementaDadosShow($dados) {
        $dados['grupos'] = \GrupoInstrumento::comboComDefault();
        return $dados;
    }
}
