<?php

class PessoaController extends CadastroController {
    public function repo(){
        return new \ISRC\Repositorios\RepositorioPessoa;
    }

    public function diretorioViews() {
        return 'pessoa';
    }

    public function raizRota() {
        return '/admin/pessoa';
    }

    public function msgSucesso(){
        return 'Titular atualizado com sucesso';
    }

    public function complementaDadosShow($dados){
        $dados['tipos_cpf'] = \TipoCpf::comboComDefault();
        $dados['sociedades'] = \Sociedade::comboComDefault();
        $dados['estados_civis'] = \EstadoCivil::comboComDefault();
        if ($dados['reg']->id_pessoa > 0){
            $titular = \Titular::where('id_pessoa', '=', $dados['reg']->id_pessoa)->first();
            $dados['enderecos'] = \Endereco::where('id_pessoa', '=', $dados['reg']->id_pessoa)->get();
            $dados['telefones'] = \Telefone::where('id_pessoa', '=', $dados['reg']->id_pessoa)->get();
            $dados['contatos'] = \Contato::where('id_pessoa', '=', $dados['reg']->id_pessoa)->get();
        } else {
            $titular = new \Titular;
            $titular->is_nacional = 1;
            $dados['enderecos'] = array();
            $dados['telefones'] = array();
            $dados['contatos'] = array();
        }


        $dados['titular'] = $titular;
        return $dados;
    }

    public function postEdit($id = ''){

        $post = Input::all();
        $id_antes = $post['id_pessoa'];
        $rep = $this->repo();
        try{
            $obj = $rep->salve($post);
            if ($obj){
                Session::flash('msg', $this->msgSucesso());
                return Redirect::to('/admin/pessoa/edit/'.$obj->id_pessoa);
            } else {
                Input::flash();
                $erro = $rep->getMsgErro();
                Session::flash('error',$erro );
                return $this->show($rep->extraiId($post));
            }
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', $ex->getMessage());
            return $this->show($id);
        }
    }
}