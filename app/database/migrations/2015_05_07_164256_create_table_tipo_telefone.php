<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTipoTelefone extends Migration {

    public function up() {
        Schema::create('tipo_telefone', function($table) {
            $table->increments('id_tipo_telefone');
            $table->string('descricao', 50);
            $table->timestamps();
            $table->softDeletes();
        });

        $obj = array("descricao" => "Residencial");
        DB::table('tipo_telefone')->insert($obj);
        $obj = array("descricao" => "Comercial");
        DB::table('tipo_telefone')->insert($obj);
        $obj = array("descricao" => "Celular");
        DB::table('tipo_telefone')->insert($obj);
        $obj = array("descricao" => "Fax");
        DB::table('tipo_telefone')->insert($obj);
    }

    public function down() {
        Schema::dropIfExists('tipo_telefone');
    }

}
