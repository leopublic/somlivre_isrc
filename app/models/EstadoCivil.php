<?php
/**
 *
 */
class EstadoCivil extends Modelo {
    protected $table = 'estado_civil';
    protected $primaryKey = 'id_estado_civil';
    protected $guarded = array();

    public function carregaPeloMDB($registro){

    }

    public static function combo($filtro = ''){
        return \EstadoCivil::orderBy('descricao')->lists('descricao', 'id_estado_civil');
    }
}