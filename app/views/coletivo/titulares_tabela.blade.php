<div class="portlet box {{$portlet_titular}}">
    <div class="portlet-title">
        <div class="caption">{{$titular_icon}} Titulares</div>
            <div class="actions">
                    <a href="javascript:btnTitularAdicionarClick();" class="btn btn-sm {{$botao_novo_cor}}">{{$botao_novo_icon}} Adicionar titular</a>
            </div>
    </div>
    <div class="portlet-body" id="portlet-body-titulares">
        <table class="table table-stripped table-condensed table-hover">
            <thead>
                <tr>
                    <th class="center">Ações</th>
                    <th class="center">Id</th>
                    <th class="left">Titular</th>
                    <th class="center">Entrada</th>
                    <th class="center">Saída</th>
                </tr>
            </thead>
            <tbody>
                @foreach($titularescoletivo as $titular_coletivo)
                <tr id="linha{{$titular_coletivo->id_titular_coletivo}}">
                    <td class="center">
                        <a href="javascript:getEditarTitular({{$titular_coletivo->id_titular_coletivo}});" class="btn btn-xs {{$botao_edicao_cor}}" title="Alterar esse titular no coletivo">{{$botao_edicao_icon}}</a>
                        <a href="javascript:postExcluirTitular({{$titular_coletivo->id_titular_coletivo}});" class="btn btn-xs {{$botao_exclusao_cor}}" title="Retirar esse titular no coletivo">{{$botao_exclusao_icon}}</a>
                    </td>
                    <td class="center">{{$titular_coletivo->id_titular}}</td>
                    <td>{{$titular_coletivo->titular->pessoa->nome_razao}}</td>
                    <td class="center">{{$titular_coletivo->dt_entrada_fmt}}</td>
                    <td class="center">{{$titular_coletivo->dt_saida_fmt}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @include('_padroes.mensagens_locais')
    </div>
</div>
