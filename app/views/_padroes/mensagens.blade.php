@if (Session::has('error') || Session::has('msg') || Session::has('info'))
<div class="row">
    <div class="col-md-12">
        @if (Session::has('error'))
        <div id="prefix_74553407" class="Metronic-alerts alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            Não foi possível executar a operação pois:
            @if (is_array(Session::get('error')))
                @foreach (Session::get('error') as $error)
                <br/>- {{$error}}
                @endforeach
            @else
                {{Session::get('error')}}
            @endif
        </div>
        @endif
        @if (Session::has('msg'))
        <div id="prefix_74553407" class="Metronic-alerts alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            {{Session::get('msg')}}
        </div>
        @endif
        @if (Session::has('info'))
        <div id="prefix_74553407" class="Metronic-alerts alert alert-info fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            {{Session::get('info')}}
        </div>
        @endif
    </div>
</div>
@endif