<?php
/**
 * @property int $id_tentativa 
 * @property string $ip Ip de origem da tentativa
 * @property int $qtd Quantidade de tentativas sem sucesso
 * @property datetime created_at 
 * 
 */
class TentativaAcesso extends Modelo {
    protected $table = 'tentativa_acesso';
    protected $primaryKey = 'ip';
    protected $guarded = array();
    protected $connection = 'mysql';

    
    public static function autorizaNovaTentativa($ip){
        $tentativa = TentativaAcesso::where('ip', '=', $ip)->first();
        if (is_object($tentativa)){
            if ($tentativa->qtd <= Config::get('isrc.qtd_tentativas_por_ip')){
                $tentativa->qtd = $tentativa->qtd+1;
                $tentativa->save();
                return true;
            } else {
                return false;
            }
        } else {
            $tentativa = new TentativaAcesso();
            $tentativa->ip = $ip;
            $tentativa->qtd = 1;
            $tentativa->save();
            return true;
        }
    }
    
    public static function acessoComSucesso($ip){
        TentativaAcesso::where('ip', '=', $ip)->update(array('qtd'=>0));
    }
}
