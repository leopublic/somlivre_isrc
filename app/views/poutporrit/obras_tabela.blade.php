<div class="portlet box {{$portlet_titular}}" id="containerObras">
    <div class="portlet-title">
        <div class="caption">{{$obra_icon}} Obras</div>
            <div class="actions">
                    <button onclick="btnObraAdicionarClick();" class="btn btn-sm {{$botao_novo_cor}}">{{$botao_novo_icon}} Adicionar obra</button>
            </div>
    </div>
    <div class="portlet-body" id="portlet-body-obras">
        <table class="table table-stripped table-condensed table-hover">
            <thead>
                <tr>
                    <th class="center">Ações</th>
                    <th class="center">Id</th>
                    <th class="left">Título original</th>
                </tr>
            </thead>
            <tbody>
                @foreach($poutporrit_obras as $poutporrit_obra)
                <tr id="linha{{$poutporrit_obra->id_obra_titular}}">
                    <td class="center">
                        <a href="javascript:postExcluirObra({{$poutporrit_obra->id_poutporrit_obra}});" class="btn btn-xs {{$botao_exclusao_cor}}" title="Retirar essa obra do pot-pourri">{{$botao_exclusao_icon}}</a>
                    </td>
                    <td class="center">{{$poutporrit_obra->id_obra}}</td>
                    <td>{{$poutporrit_obra->obra->titulo_original}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @include('_padroes.mensagens_locais')
    </div>
</div>
