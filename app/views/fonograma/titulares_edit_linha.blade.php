<tr id="tabela{{$fonograma_titular->id_fonograma_titular}}">
    <td class="left">
        <a href="javascript:postExcluirTitular({{$fonograma_titular->id_fonograma_titular}});" class="btn btn-xs {{$botao_exclusao_cor}}" title="Retirar esse titular do fonograma">{{$botao_exclusao_icon}}</a>
        @if($fonograma_titular->categoria->cod_categoria == 'MA')
            <a href="#"  class="btn btn-xs yellow-casablanca popovers" data-title="Instrumentos" data-placement="top" data-html="true" data-container="body" data-trigger="hover" data-content="{{ $fonograma_titular->lista_instrumentos() }}"><i class="fa fa-microphone"></i></a>
        @endif
    </td>
    <td class="center">{{$fonograma_titular->id_titular}}</td>
    <td>
        @if ($fonograma_titular->id_coletivo > 0)
            <i class="fa fa-group"></i>
        @else
            <i class="fa fa-user"></i>
        @endif
        {{$fonograma_titular->nome_razao}}</td>
    <td>{{Form::select('id_pseudonimo_'.$i, $fonograma_titular->titular->combopseudonimos(), $fonograma_titular->id_pseudonimo, array("class" => "form-control select2"))}}</td>
    <td>{{Form::select('id_categoria_'.$i, $categorias, $fonograma_titular->id_categoria, array("class" => "form-control select2 id_categoria"))}}</td>
    <td>{{Form::select('id_instrumentos_'.$i.'[]', $instrumentos, Input::old('id_instrumentos', $fonograma_titular->lista_instrumentos()), array("class" => "form-control select2", "multiple", 'id' => 'id_instrumentos_'.$i.'[]'))}}</td>
    <td>{{$fonograma_titular->nome_coletivo}}</td>
    <td class="right"><input type="text" value="{{$fonograma_titular->pct_participacao_fmt}}" name="pct_participacao_{{ $i }}" id="pct_participacao_{{ $i }}" class="pct_class" style="text-align: right;"/></td>
</tr>
