<?php

namespace ISRC\Repositorios;

class RepositorioGenero extends RepositorioCadastro {

    public function tituloModelo() {
        return "genero";
    }

    public function nomeModelo() {
        return '\Genero';
    }

    public function ordenacaoPadrao() {
        return 'nome';
    }

    public function registrosConsulta($filtros = null) {
        return \Genero::paginate(20);
    }

}
