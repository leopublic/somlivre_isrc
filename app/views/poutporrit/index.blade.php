@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.titulo_cadastro', array('titulo'=> 'Pot-pourri', 'rota'=>'potpourri'))

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>Registros encontrados
                </div>
            </div>
            <div class="portlet-body">
                {{Form::open()}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            {{Form::text('pesquisar', Input::old('filtro'), array("class" => "form-control"))}}
                            <span class="input-group-btn"><button class="btn" ><i class="fa fa-search"></i> Pesquisar</button></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">{{$regs->appends(Input::get())->links()}}</div>
                </div>
                {{Form::close()}}
                <div class="table-scrollable">
                    @include('_padroes.table')
                    <colgroup>
                        <col width="100px">
                        <col width="50px">
                        <col width="auto">
                        <col width="auto">
                        <col width="auto">                    
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="center">Ações</th>
                            <th class="center">Id</th>
                            <th class="left">Nome</th>
                            <th class="left">Cod ISWC</th>
                            <th class="left">Obras</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($regs as $reg)
                        <tr>
                            <td class="center">
                                <a href="{{URL::to('/admin/potpourri/edit/'.$reg->id_poutporrit)}}" class="btn btn-sm green-seagreen" title="Editar"><i class="fa fa-edit"></i></a>
                                <a href="{{URL::to('/admin/fonograma/edit/0/0/'.$reg->id_poutporrit)}}" class="btn btn-sm {{$portlet_fonograma}}" title="Criar um fonograma para essa obra"><i class="fa fa-plus"></i> {{$fonograma_icon}}</a>
                            </td>
                            <td class="center">{{$reg->id_poutporrit}}</td>
                            <td>{{$reg->nome}}</td>
                            <td>{{$reg->cod_IWC }}</td>
                            <td>
                                <? $br = ''; ?>
                                @foreach ($reg->obras as $obra)
                                {{ $br.$obra->obra->titulo_original }}
                                <? $br = '<br/>'; ?>
                                @endforeach
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END BORDERED TABLE PORTLET-->
    </div>
    @stop

    @section('scripts')
    $('#menuCadastros').addClass('active');
    $('#menuCadastros').addClass('open');
    $('#itemPotpourri').addClass('active');
    @stop
