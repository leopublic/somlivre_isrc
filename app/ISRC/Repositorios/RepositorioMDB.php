<?php

namespace ISRC\Repositorios;

/**
 * Classe responsável por ler as tabelas do acess.
 */
class RepositorioMDB {

    public static $conn;

    public static $empresa;

    public function conexao() {
        if (!is_object(self::$conn)){
            $this->abreConexao();
        }
        return self::$conn;
    }

    public function abreConexao($mdb_file) {
        $dataSourceName = "odbc:Driver=MDBTools;DBQ=" . $mdb_file . ";";
        self::$conn = new \PDO($dataSourceName);
        $this->associaEmpresa();
    }

    public function associaEmpresa(){
        $sql = "select * from ISRC_parametro_tb";
        $res = self::$conn->query($sql)->fetchAll(\PDO::FETCH_ASSOC);
        $rs = $res[0];

        self::$empresa = \Empresa::where('sgl_IFPI', '=', $rs['sgl_IFPI'])->first();
        if (count(self::$empresa) == 0){
            self::$empresa = new \Empresa();
            self::$empresa->sgl_IFPI = $rs['sgl_IFPI'];
        }
        self::$empresa->nome = $rs['id_produtor_fonografico'];
        self::$empresa->id_pais = $rs['id_pais'];
        self::$empresa->ano_base = $rs['ano_base'];
        self::$empresa->seq_ISRC = $rs['seq_ISRC'];
        self::$empresa->versao = $rs['versao'];
        self::$empresa->save();
        $usuario = new \Usuario;
        $usuario->id_empresa = self::$empresa->id_empresa;
        \Auth::login($usuario);
    }

    public function cursor($sql){
        return self::$conn->query($sql)->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function query($sql){
        return self::$conn->query($sql);
    }

    public function teste() {
        $query = 'SELECT * FROM ISRC_pais_tb';
        $result = $this->conn->query($query)->fetchAll(\PDO::FETCH_ASSOC);
        print_r($result);
    }

    public function limpa(){
        
    }


}
