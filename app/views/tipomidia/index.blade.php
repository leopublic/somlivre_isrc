@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.titulo_cadastro', array('titulo'=> 'Tipos de mídia', 'rota'=>'tipomidia'))

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>Registros encontrados
            </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-scrollable">
                @include('_padroes.table')
                <thead>
                    <tr>
                        <th class="center">Ações</th>
                        <th class="center">Id</th>
                        <th class="center">Descrição</th>
                        </tr>
                </thead>
                <tbody>
                @foreach($regs as $reg)
                    <tr>
                        <td class="center">
                            <a href="{{URL::to('/admin/tipomidia/edit/'.$reg->id_tipo_midia)}}" class="btn btn-sm green-seagreen" title="Editar"><i class="fa fa-edit"></i></a>
                        </td>
                        <td class="center">{{$reg->id_tipo_midia}}</td>
                        <td class="center">{{$reg->dsc_tipo_midia}}</td>
                    </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END BORDERED TABLE PORTLET-->
</div>
@stop
@section('scripts')
    $('#menuTabelas').addClass('active');
    $('#menuTabelas').addClass('open');
    $('#itemTipomidias').addClass('active');
@stop
