<?php

/**
 * @property int $id_fonograma
 * @property int $id_empresa
 * @property int $id_poutporrit
 * @property int $id_genero
 * @property int $id_coletivo
 * @property int $id_obra
 * @property int $id_tipo_fonograma
 * @property int $id_pais_origem
 * @property int $id_pais_publicacao
 * @property int $id_tipo_midia
 * @property int $id_tipo_produto
 * @property varchar $cod_fonograma_ECAD
 * @property varchar $cod_GRA
 * @property varchar $num_catalogo
 * @property varchar $Cod_ISRC
 * @property varchar $dsc_complemento_arranjo
 * @property tinyint $is_nacional
 * @property tinyint $is_instrumental
 * @property tinyint $is_gravacao_propria
 * @property tinyint $is_publsimult
 * @property int $tmp_duracao_min
 * @property int $tmp_duracao_seg
 * @property int $cod_arranjo
 * @property datetime $dt_grav_original
 * @property datetime $dt_lancamento
 * @property datetime $dt_emissao
 * @property datetime $dt_inclusao
 * @property datetime $dt_alteracao
 * @property varchar $usuario
 * @property varchar $extensao_arquivo
 * @property text $obs
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property timestamp $deleted_at 
 * @property  varchar $nome_produto [<description>]
 */
class Fonograma extends Modelo {

    protected $table = 'fonograma';
    protected $primaryKey = 'id_fonograma';
    protected $guarded = array('id_empresa');

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            $model->id_empresa = \Auth::user()->id_empresa;
        });
    }

    /* =====================================================*
     * Propriedades customizadas
     * ===================================================== */

    public function getTituloAttribute() {
        if (is_object($this->obra)) {
            return $this->obra->titulo_original;
        } else {
            return '--';
        }
    }

    public function getCodIsrcFmtAttribute() {
        $cod = trim(str_replace('-', '', $this->Cod_ISRC));
        if (strlen($cod) == 12) {
            return substr($cod, 0, 2) . '-' . substr($cod, 2, 3) . '-' . substr($cod, 5, 2) . '-' . substr($cod, 7, 5);
        } else {
            return $this->Cod_ISRC;
        }
    }

    public function getNomeObraPoutporritAttribute() {
        if (intval($this->id_obra) > 0) {
            $nome_obra = '(' . $this->obra->id_obra . ') ' . $this->obra->titulo_original;
        } elseif (intval($this->id_poutporrit) > 0) {
            $nome_obra = '(' . $this->poutporrit->id_poutporrit . ') ' . $this->poutporrit->nome;
        } else {
            $nome_obra = '--';
        }
        return $nome_obra;
    }

    public function getNomeObraAttribute() {
        if (intval($this->id_obra) > 0) {
            $nome_obra = '(' . $this->obra->id_obra . ') ' . $this->obra->titulo_original;
        } else {
            $nome_obra = '--';
        }
        return $nome_obra;
    }

    public function getNomePoutporritAttribute() {
        if (intval($this->id_poutporrit) > 0) {
            $nome_obra = '(' . $this->poutporrit->id_poutporrit . ') ' . $this->poutporrit->nome;
        } else {
            $nome_obra = '--';
        }
        return $nome_obra;
    }

    public function getDtEmissaoFmtAttribute() {
        return $this->dtFmt('dt_emissao');
    }

    public function setDtEmissaoFmtAttribute($valor) {
        $this->attributes['dt_emissao'] = $this->dtViewParaModel($valor);
    }

    public function getDtGravOriginalFmtAttribute() {
        return $this->dtFmt('dt_grav_original');
    }

    public function setDtGravOriginalFmtAttribute($valor) {
        $this->attributes['dt_grav_original'] = $this->dtViewParaModel($valor);
    }

    public function getDtLancamentoFmtAttribute() {
        return $this->dtFmt('dt_lancamento');
    }

    public function setDtLancamentoFmtAttribute($valor) {
        $this->attributes['dt_lancamento'] = $this->dtViewParaModel($valor);
    }

    public function getTipoObraAttribute() {
        if ($this->id_obra > 0) {
            return "Obra";
        } elseif ($this->id_poutporrit > 0) {
            return "Pout-pourrit";
        } else {
            return "Obra";
        }
    }

    public function getDuracaoExpAttribute() {
        $min = intval($this->attributes['tmp_duracao_min']);
        $seg = intval($this->attributes['tmp_duracao_seg']);

        if ($min>=60){
            $hora = floor($min/60);
            $min = $min - ($hora * 60);
        } else {
            $hora = 0;
        }
        $ret = substr('00'.$hora, -2).substr('00'.$min, -2).substr('00'.$seg, -2);
        return $ret;
    }

    /* =====================================================*
     * Relacionamentos
     * ===================================================== */

    public function arranjo() {
        return $this->belongsTo('Arranjo', 'id_arranjo', 'id_arranjo');
    }

    public function obra() {
        return $this->belongsTo('Obra', 'id_obra', 'id_obra');
    }

    public function poutporrit() {
        return $this->belongsTo('Poutporrit', 'id_poutporrit', 'id_poutporrit');
    }

    public function coletivo() {
        return $this->belongsTo('Coletivo', 'id_coletivo', 'id_coletivo');
    }

    public function genero() {
        return $this->belongsTo('Genero', 'id_genero', 'id_genero');
    }

    public function paisorigem() {
        return $this->belongsTo('Pais', 'id_paisorigem', 'id_pais');
    }

    public function paispublicacao() {
        return $this->belongsTo('Pais', 'id_paispublicacao', 'id_pais');
    }

    public function tipofonograma() {
        return $this->belongsTo('TipoFonograma', 'id_tipo_fonograma', 'id_tipo_fonograma');
    }

    public function tipomidia() {
        return $this->belongsTo('TipoMidia', 'id_tipo_midia', 'id_tipo_midia');
    }

    public function titulares_coletivos() {
        return $this->belongsToMany('Titular', 'fonograma_titular', 'id_fonograma', 'id_fonograma')->where(\DB::raw("coalesce(id_coletivo, 0) > 0"));
    }

    /**
     *
     * @return type
     */
    public function fonogramatitulares() {
        return $this->hasMany('FonogramaTitular', 'id_fonograma');
    }

    public function titulares() {
//        return $this->belongsToMany('FonogramaTitular', 'fonograma_titular', 'id_fonograma', 'id_fonograma')->where(\DB::raw("coalesce(id_coletivo, 0) = 0"));
        return $this->belongsToMany('Titular', 'fonograma_titular', 'id_fonograma', 'id_titular');
    }

    public function cursorTitulares() {
        return $this->titulares->get();
    }

    public function scopeFiltro($query, $filtro) {
        return $query->orWhere('cod_GRA', 'like', '%' . $filtro . '%')
                    ->orWhere('cod_ISRC', 'like', '%' . str_replace("-", "", $filtro) . '%')
                    ->orWhere(function($query) use($filtro) {
                        if (is_numeric($filtro)) {
                            $query->orWhere('id_fonograma', '=', $filtro);
                        }
                    })
                    ->orWhere(function($query) use($filtro) {
                        $query->whereIn('id_obra', function($query) use($filtro) {
                            $query->select('id_obra')
                                  ->from('obra')
                                  ->where('titulo_original', 'like', '%' . $filtro . '%');
                        });
                    });
    }

    public function getRaizArquivosAttribute() {
        return $this->getRaizArquivos();
    }

    public function getRaizArquivos() {
        return app_path() . '/arquivos/fonogramas';
    }

    public function getSubdirArquivoAttribute() {
        $dir1 = floor($this->attributes['id_fonograma'] / 10000);
        $dir2 = floor($this->attributes['id_fonograma'] / 1000) % 10;
        $dir3 = floor($this->attributes['id_fonograma'] / 100) % 10;
        return $dir1 . '/' . $dir2 . '/' . $dir3;
    }

    public function caminhoArquivo() {
        return $this->getRaizArquivos() . '/' . $this->getSubdirArquivoAttribute() . '/' . $this->id_fonograma;
    }

    public function getArquivoExisteAttribute() {
        if (file_exists($this->caminhoArquivo())) {
            return true;
        } else {
            return false;
        }
    }

    public function armazenarArquivo($arquivo) {
        $caminho = $this->raiz_arquivos . '/' . $this->subdir_arquivo;

        if (!file_exists($caminho)) {
            mkdir($caminho, 0755, true);
        }
        $this->mimetype_arquivo = $arquivo->getMimeType();
        $this->extensao_arquivo = $arquivo->getClientOriginalExtension();

        $this->save();
        $arquivo->move($caminho, $this->id_fonograma);
    }

}
