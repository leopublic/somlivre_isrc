<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Adicionar titulares do coletivo {{$coletivo->nome}}</h4>
</div>
<div class="modal-body">
    <table class="table table-stripped table-condensed table-hover">
        <colgroup>
            <col style="width:60px;"/>
            <col style="width:40px;"/>
            <col style="width:auto;"/>
        </colgroup>
        <thead>
            <tr>
                <th class="center">Ações</th>
                <th class="center">Id</th>
                <th class="left">Titular</th>
            </tr>
        </thead>
        <tbody>
            @foreach($titulares as $titular)
            <tr>
                <td class="center" id="acaoTitular{{$titular->id_titular}}">
                    <a onClick="javascript:postAdicionarTitularColetivo({{$id_fonograma}},{{$titular->id_titular}}, {{$coletivo->id_coletivo}});" class="btn btn-xs blue" title="Incluir esse titular no fonograma"><i class="fa fa-plus"></i></a>
                </td>
                <td>{{$titular->id_titular}}</td>
                <td style="text-align:left">{{$titular->nome_razao}}</td>
                <td>{{Form::select('id_categoria'.$titular->id_titular, $categorias, null, array("class" => "form-control select2", "id" => 'id_categoria'.$titular->id_titular))}}</td>
                <td>{{Form::text('pct_participacao_fmt', '0,0000000', array("class" => "form-control", "id" => 'pct_participacao_fmt'.$titular->id_titular))}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="modal-footer">
    <button type="button" onClick="btnColetivoAdicionarClick()" class="btn yellow-gold">Voltar</button>
    <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
</div>
<div style="visibility: hidden;" id="botaoOk">
    <button class="btn btn-xs grey disabled"><i class="fa fa-check"></i></button>
</div>
