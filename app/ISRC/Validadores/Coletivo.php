<?php
namespace ISRC\Validadores;
class Coletivo extends Validador {
    public $id_unique;
    public static $rules = array(
        'nome'               => 'required',
        'id_tipo_coletivo'               => 'required',
    );
    public static $messages = array(
        'nome.required'      => 'O nome do coletivo é obrigatório',
        'id_tipo_coletivo.required'      => 'O tipo do coletivo é obrigatório',
    );
}
