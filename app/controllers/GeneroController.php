<?php

class GeneroController extends BaseController {

    public function getIndex() {
        $rep = new \ISRC\Repositorios\RepositorioGenero;
        $regs = $rep->registrosConsulta();

        return View::make('genero.index')
                ->with('regs', $regs);
    }

    public function getEdit($id = 0) {
        $rep = new \ISRC\Repositorios\RepositorioGenero();
        $objeto = $rep->instanciaEditado($id);
        $titulo = $rep->tituloTelaEdicao($objeto);
        return View::make('genero.edit')
                        ->with('reg', $objeto)
                        ->with('titulo', $titulo)
        ;
    }

    public function postEdit($id_genero = '') {
        $post = Input::all();
        try {
            $rep = new \ISRC\Repositorios\RepositorioGenero();
            if ($rep->salve($post)) {
                Session::flash('msg', 'Gênero atualizado com sucesso');
                return Redirect::to('/admin/genero/');
            } else {
                Input::flash();
                Session::flash('error', $rep->getErrors());
                return Redirect::to('/admin/genero/edit/' . $post['id_genero']);
            }
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', $ex->getMessage());
            return Redirect::to('/admin/genero/edit/' . $post['id_genero']);
        }
    }
}
