<?php
/**
 * @property int id_tipo_cpf
 * @property string descricao 
 * @property datetime created_at 
 * @property datetime updated_at 
 * 
 */
class TipoCpf extends Modelo {
    protected $table = 'tipo_cpf';
    protected $primaryKey = 'id_tipo_cpf';
    protected $guarded = array();

    public function carregaPeloMDB($registro){
        
    }
    
    public static function combo($filtro = ''){
        return \TipoCpf::orderBy('descricao')->lists('descricao', 'id_tipo_cpf');
    }
}