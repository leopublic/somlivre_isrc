<?php

namespace ISRC\Servicos;

class ImportadorTitularColetivo extends Importador{

    public function sqlSelectMdb() {
        return "select id_titular_coletivo, id_coletivo, id_titular, dt_entrada, dt_saida, dt_inclusao, dt_alteracao, usuario from ISRC_titular_coletivo_tb where is_deleted = 0";
    }
    
    public function novoObjeto(){
        return new \TitularColetivo();
    }
            
    public function tabela(){
        return "ISRC_titular_coletivo_tb";
    }
    
    public function nome_id_local(){
        return 'id_titular_coletivo';
    }

    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            // 'id_coletivo',
        	'dt_entrada', 
        	'dt_saida', 
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);

        $this->id_titular = $this->obtem_id_titular($regMDB['id_titular'], $regMDB['id_titular_coletivo']);
        $this->id_coletivo = $this->obtem_id_coletivo($regMDB['id_coletivo'], $regMDB['id_titular_coletivo']);

        $obj->usuario = $regMDB['usuario'];
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        return $obj;
    }
}