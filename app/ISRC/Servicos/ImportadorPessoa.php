<?php

namespace ISRC\Servicos;

class ImportadorPessoa extends Importador{

    public function sqlSelectMdb() {
        return "select id_pessoa
                ,tp_pessoa
                ,cod_pessoa
                ,cod_ecad
                ,nome_razao
                ,fantasia
                ,tp_cpf
                ,cpf_cnpj
                ,email
                ,dt_nascimento
                ,estado_civil
                ,sexo
                ,insc_estadual
                ,insc_municipal
                ,identidade
                ,orgao_emissor
                ,profissao
                ,nacionalidade
                ,is_editora
                ,is_produtor_fono
                ,is_autor
                ,is_interprete
                ,is_musico
                ,observacao
                ,dt_inclusao
                ,dt_alteracao
                ,usuario
                from ISRC_pessoa_tb where is_deleted = 0";
    }
    
    public function novoObjeto(){
        return new \Pessoa();
    }
    
    public function nome_id_local(){
        return 'id_pessoa';
    }
            
    public function tabela(){
        return "ISRC_pessoa_tb";
    }
        
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            'tp_pessoa', 
            'cod_pessoa', 
            'cod_ecad', 
            'nome_razao', 
            'fantasia', 
            'tp_cpf', 
            'cpf_cnpj', 
            'email', 
            'dt_nascimento',
            'estado_civil', 
            'sexo', 
            'insc_estadual', 
            'insc_municipal', 
            'identidade', 
            'orgao_emissor', 
            'profissao', 
            'nacionalidade', 
            'is_editora', 
            'is_produtor_fono', 
            'is_autor', 
            'is_interprete',
            'is_musico',
            'observacao',
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);

        $obj->id_pessoa_original = $regMDB['id_pessoa'];
        $obj->usuario = $regMDB['usuario'];
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        $obj->id_empresa = \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa;
        return $obj;
    }
}