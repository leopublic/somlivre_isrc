@extends('base')

@section('estilos')
table tr.red td{
background-color:red;
color:#fff;
font-weight:bold;
}
@stop

@section('conteudo')
<div class="row">
    <div class="col-md-12">
        <h3 class="page-title">{{$titulo}}</h3>
    </div>
</div>

@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_obra}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-music"></i>{{$titulo}}</div>
            </div>
            <div class="portlet-body form">
                {{Form::open(array('role' => "form", "class" => "form-horizontal", "id"=>"formObra"))}}
                {{Form::hidden('id_obra', $reg->id_obra, array('id' => 'id_obra'))}}
                <div class="form-body">
                    <h4>Códigos</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">ID</label>
                                <div class="col-md-9">
                                    {{Form::text('id_obra_txt', $reg->id_obra, array("class" => "form-control input-small", 'disabled'))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Código SIGNUS</label>
                                <div class="col-md-9">
                                    {{Form::text('id_obra_txt', $reg->cod_obra, array("class" => "form-control input-small", 'disabled'))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">ISWC</label>
                                <div class="col-md-9">
                                    {{Form::text('cod_ISWC', Input::old('cod_ISWC', $reg->cod_ISWC), array("class" => "form-control input-medium"))}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Código ECAD</label>
                                <div class="col-md-9">
                                    {{Form::text('cod_obra_ecad', Input::old('cod_obra_ecad', $reg->cod_obra_ecad), array("class" => "form-control input-small"))}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <h4>Dados da obra</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Título original</label>
                                <div class="col-md-9">
                                    {{Form::text('titulo_original', Input::old('titulo_original', $reg->titulo_original), array("class" => "form-control"))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Subtítulo</label>
                                <div class="col-md-9">
                                    {{Form::text('subtitulo', Input::old('subtitulo', $reg->subtitulo), array("class" => "form-control"))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Gênero</label>
                                <div class="col-md-9">
                                    {{Form::select('id_genero', $generos, Input::old('id_genero', $reg->id_genero), array("class" => "bs-select form-control"))}}
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Versão</label>
                                <div class="col-md-9">
                                    {{Form::text('versao', Input::old('versao', $reg->versao), array("class" => "form-control"))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"> &nbsp;</label>
                                {{Form::hidden('is_nacional', '0')}}
                                {{Form::hidden('is_instrumental', '0')}}
                                <div class="checkbox-list col-md-9">
                                    <label>{{Form::checkbox('is_nacional', 1, !$reg->is_nacional)}} Estrangeiro</label>
                                    <label>{{Form::checkbox('is_instrumental', 1, $reg->is_instrumental)}} Instrumental</label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="button" class="btn green" id="submitObra" >Salvar</button>
                        <a href="{{URL::to('/admin/obra')}}" class="btn default">Cancelar</a>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
@if ($reg->id_obra > 0)
<div class="row">
    <div class="col-md-12" id="containerObraTitulares" name="containerObraTitulares">
        @include('obra.titulares_tabela')
    </div>
</div>
@endif
@include('_padroes.modal-container', array('id'=> 'ajax-modal', 'largura'=>'550'))

@stop
@section('scripts_full')
<script>
    $('#itemObras').addClass('active');
    jQuery(document).ready(function ($) {
        $(".scroll").click(function (event) {
            event.preventDefault();
            $('html,body').animate({scrollTop: $(this.hash).offset().top}, 500);
        });
    });
    jQuery(document).ready(function () {
        $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
            '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
                '<div class="progress progress-striped active">' +
                    '<div class="progress-bar" style="width: 100%;"></div>' +
                '</div>' +
            '</div>';

        $.fn.modalmanager.defaults.resize = true;

        $('#txtPesquisarTitular').keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                $('#btnPesquisarTitular').trigger('click');
            }
        });

        $('#btnPesquisarTitular').click(function (event) {
            event.preventDefault();
            $(this).html('<i class="fa fa-spin fa-spinner"></i> Aguarde');
            $(this).attr('disabled', 'disabled');
            $('#containerTitulares').html('<br/><i class="fa fa-spin fa-spinner"></i> carregando...');
            var id_obra = $('#id_obra').val();

            var url = "/admin/obratitulares/gridresultadopesquisa/" + id_obra + "/" + encodeURIComponent($('#txtPesquisarTitular').val());
            $("#containerTitulares").load(url);
            $(this).html('<i class="fa fa-search"></i> Pesquisar');
            $(this).removeAttr('disabled');
        });
    });

    function getAdicionarTitular(id_obra, id_titular) {
        var url = '/admin/obratitulares/adicionar/' + id_obra + '/' + id_titular;
        $('body').modalmanager('loading');
        setTimeout(function () {
            $('#ajax-modal').load(url, '', function () {
                $('.select2').select2();
                $('#ajax-modal').modal();
            });
        }, 1000);
    }

    function getEditarTitular(id_obra_titular) {
        var url = '/admin/obratitulares/editar/' + id_obra_titular;
        $('body').modalmanager('loading');
        setTimeout(function () {
            $('#ajax-modal').load(url, '', function () {
                $('.date-pickerx').datepicker({autoclose: true});
                $('.select2').select2();
                $('#ajax-modal').modal();
            });
        }, 1000);
    }


    function postAtualizaTitular() {
        $.post("/admin/obratitulares/adicionar", $("#form_titular").serialize()
            , function (data) {
                if (data.codret == 0) {
                    $('#ajax-modal').modal('hide');
                    $(document.body).animate({
                        'scrollTop': $('#containerObraTitulares').offset().top
                    }, 2000);
                    $('#containerObraTitulares').html(data.html);
                    $('#tabela' + data.id_obra_titular).stop().animate({backgroundColor: "#95A5A6"}, 100)
                            .animate({backgroundColor: "#FFFFFF"}, 4500);
                } else {
                    $('#modal-msg').html(data.msg);
                }
            }
        , "json")
                ;
    }

    function postExcluiTitular(id_obra_titular) {
        Metronic.blockUI({
            target: '#portlet-body-titulares',
            boxed: true,
            message: 'Excluindo...(aguarde)'
        });

        $.post("/admin/obratitulares/excluir", {id_obra_titular: id_obra_titular}
        , function (data) {
            Metronic.unblockUI('#portlet-body-titulares');

            if (data.codret == 0) {
                $('#ajax-modal').modal('hide');
                $('#modal-msg').html(data.msg);
                $('#containerObraTitulares').html(data.html);
            } else {
                $('#modal-msg').html(data.msg);
            }
        }
        , "json")
                ;
    }
</script>
@stop
