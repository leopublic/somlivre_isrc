<?php
/**
 * Arquivo de configurações da aplicação
 */
return array(
    /**
     * Caminho do MDB
     */
    'caminho_MDB' => 'app/isrc_db.mdb',
    /**
     * Indica quantas vezes um IP pode errar a senha
     */
    'qtd_tentativas_por_ip' => 15,
    /**
     * Quem deve ser notificado quando houver excesso de tentativas de acesso
     */
    'notificar_em_caso_ataque_nome' => 'Rodrigo',
    'notificar_em_caso_ataque_email' => 'rodrigo.moreno@somlivre.com.br',
    'banco_sl' => 'somlivre_isrc_sl',
    'banco_br' => 'somlivre_isrc_rge',

    
);
