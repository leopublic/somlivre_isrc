    <?php
/**
 * @property int $id_coletivo
 * @property string $nome
 * @property int $cod_coletivo
 * @property int $cod_coletivo_ECAD
 * @property string $pseudonimo
 * @property boolean $is_nacional
 * @property string $cod_cae
 * @property int $id_tipo_coletivo
 * @property string $sigla
 * @property date $dt_inclusao
 * @property date $dt_alteracao
 * @property string $usuario
 * @property datetime created_at 
 * @property datetime updated_at 
 * 
 */
class Coletivo extends Modelo {
    protected $table = 'coletivo';
    protected $primaryKey = 'id_coletivo';
    protected $guarded = array();

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            $model->id_empresa = \Auth::user()->id_empresa;
        });
    }

    /**
     * Relacionamentos
     */
    public function integrantes(){
        return $this->hasMany('TitularColetivo', 'id_coletivo', 'id_coletivo');
    }
 
    public function tipocoletivo(){
        return $this->belongsTo('TipoColetivo', 'id_tipo_coletivo', 'id_tipo_coletivo');
    }

    public function scopeWherefiltro($query, $filtro) {
        if ($filtro != ''){
            $sql = " ( nome like ?"
                    . " or cod_coletivo_ECAD like ? "
                    . " or pseudonimo like ?";
            $sql.= " or id_coletivo in ("
                    . " select id_coletivo "
                    . " from titular_coletivo tc "
                    . " join titular t on t.id_titular = tc.id_titular "
                    . " join pessoa p on p.id_pessoa = t.id_pessoa "
                    . " where tc.id_coletivo = coletivo.id_coletivo"
                    . " and (p.nome_razao like ? "
                    . " or p.fantasia like ?)"
                    . ")"
                    . ")";
            if (is_numeric($filtro)){
                $sql .= " or id_coletivo = ".$filtro;
            }
            $query->whereRaw($sql, array('%' . $filtro . '%', '%' . $filtro . '%', '%' . $filtro . '%', '%' . $filtro . '%', '%' . $filtro . '%'));
        }
        $query->where('id_empresa', '=', \Auth::user()->id_empresa);
        return $query;
    }
   
    
}