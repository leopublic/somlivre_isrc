<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableExportacao extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('exportacao', function ($table) {
            $table->increments('id_exportacao');
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_empresa')->unsigned();
            $table->integer('sequencial')->unsigned();
            $table->integer('id_status_exportacao')->unsigned();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        if (Schema::hasTable('exportacao')) {
            Schema::drop('exportacao');
        }
	}

}
