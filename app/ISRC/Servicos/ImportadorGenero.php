<?php

namespace ISRC\Servicos;

class ImportadorGenero extends Importador{

    public function sqlSelectMdb() {
        return "select id_genero"
                . ", nome"
                . ", dt_inclusao"
                . ", dt_alteracao"
                . ", usuario"
                . ", cod_genero"
        . " from ISRC_genero_tb where is_deleted = 0";
    }
    
    public function nome_id_local(){
        return 'id_genero';
    }

    public function novoObjeto(){
        return new \Genero;
    }
            
    public function tabela(){
        return "ISRC_genero_tb";
    }
        
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            'nome',
            'usuario',
            'cod_genero',
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);

        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        return $obj;
    }
}