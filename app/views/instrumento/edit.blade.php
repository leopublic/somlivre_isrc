@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-bell"></i> Alterar instrumento</div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body form">
                {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                {{Form::hidden('id_instrumento', $reg->id_instrumento)}}
                <div class="form-body">
                    @include('_campos.texto_id', array('valor'=> $reg->id_instrumento))
                    @include('_campos.texto', array('label'=> 'Nome', 'nome'=>'nome'))
                    @include('_campos.select', array('label'=> 'Grupo', 'nome'=>'id_grupo_instrumento', 'valores'=> $grupos))
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green">Salvar</button>
                        <a href="{{URL::to('/admin/instrumento')}}" class="btn default">Cancelar</a>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
    $('#menuTabelas').addClass('active');
    $('#menuTabelas').addClass('open');
    $('#itemInstrumentos').addClass('active');
@stop
