<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableArranjo extends Migration {

    public function up() {
        Schema::create('arranjo', function($table) {
            $table->increments('id_arranjo');
            $table->string('dsc_arranjo', 100)->nullable();
            $table->boolean('ind_complemento')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down() {
        if (Schema::hasTable('arranjo')) {
            Schema::drop('arranjo');
        }
    }

}
