@extends('base')

@section('estilos')
@stop

@section('conteudo')

@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-gift"></i> Alterar usuário</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
            {{Form::hidden('id_usuario', $usuario->id_usuario)}}
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Nome</label>
                    <div class="col-md-9">
                        {{Form::text('nome', Input::old('nome', $usuario->nome), array("class" => "form-control"))}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">E-mail</label>
                    <div class="col-md-9">
                        {{Form::text('email', Input::old('email', $usuario->email), array("class" => "form-control"))}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Acesso ao backoffice?</label>
                    <div class="col-md-9">
                        @include('_padroes.radio_sim_nao', array('nome'=> 'acessa_backoffice', 'modelo' => $usuario))
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Perfis</label>
                    <div class="col-md-9">
                        {{Form::hidden('id_perfil[]', '')}}
                        <div class="checkbox-list">
                            @foreach($perfis as $perfil)
                            <label class="checkbox-inline">
                                {{ Form::checkbox('id_perfil[]', $perfil->id_perfil , $perfil->id_usuario)}} {{$perfil->descricao}}
                            </label>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Observação</label>
                    <div class="col-md-9">
                        {{Form::textarea('observacao', Input::old('observacao', $usuario->observacao), array("class" => "form-control"))}}
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">Salvar</button>
                    <a href="{{URL::to('/admin/usuario')}}" class="btn default">Cancelar</a>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
</div>
@stop
@section('scripts')
$('#menuAcesso').addClass('active');
$('#menuAcesso').addClass('open');
$('#itemUsuarios').addClass('active');
@stop
