<?php

class FonogramaTitularesController extends BaseController {

    public function getShow($id_fonograma) {
        $objeto = \Fonograma::find($id_fonograma);
        $fonograma_titulares = $objeto->fonogramatitulares;
        $titulares = array();
        return View::make('fonograma.titulares')
                        ->with('objeto', $objeto)
                        ->with('fonograma_titulares', $fonograma_titulares)
                        ->with('titulares', $titulares)
        ;
    }

    public function postShow($id_fonograma) {
        $objeto = \Fonograma::find($id_fonograma);
        $fonograma_titulares = $objeto->fonogramatitulares;

        $pesquisar = Input::get('pesquisar');
        $sql = "select id_titular, nome_razao from titular, pessoa where pessoa.id_pessoa = titular.id_pessoa and nome_razao like ?";

        $titulares = \DB::select($sql, array('%' . $pesquisar . '%'));


        return View::make('fonograma.titulares')
                        ->with('objeto', $objeto)
                        ->with('fonograma_titulares', $fonograma_titulares)
                        ->with('titulares', $titulares)
        ;
    }

    public function getAdd($id_fonograma, $id_titular) {
        $fono_titular = new \FonogramaTitular;
        $fono_titular->id_fonograma = $id_fonograma;
        $fono_titular->id_titular = $id_titular;
    }

    public function getFormpesquisa(){
        return View::make('fonograma.formPesquisa')
        ;
    }

    public function getGridresultadopesquisa($tipo, $id_fonograma, $pesquisar = '') {
        $limite = 10;
        if ($pesquisar != '') {
            if ($tipo == 'titular') {
                $pesquisar = str_replace('*', '%', $pesquisar);
                $sql_count = "select count(*) as qtd from titular 
                            join pessoa on pessoa.id_pessoa = titular.id_pessoa 
                            left join titular_pseudonimo tp on tp.id_titular = titular.id_titular
                            where nome_razao like ? or tp.pseudonimo like ?";
                $sql = "select titular.id_titular, nome_razao from titular 
                            join pessoa on pessoa.id_pessoa = titular.id_pessoa 
                            left join titular_pseudonimo tp on tp.id_titular = titular.id_titular
                            where nome_razao like ? or tp.pseudonimo like ?
                             limit ".$limite;
                $res_quantos = \DB::select($sql_count, array('%' . $pesquisar . '%', '%' . $pesquisar . '%'));
                $rs_quantos = $res_quantos[0];
                $titulares = \DB::select($sql, array('%' . $pesquisar . '%', '%' . $pesquisar . '%'));
                if ($rs_quantos->qtd > $limite) {
                    $ignorados = $rs_quantos->qtd - $limite;
                    $msg_limite = '(' . $ignorados . ' resultados não exibidos. Refine sua pesquisa)';
                } else {
                    $msg_limite = '';
                }
                return View::make('fonograma.titulares_pesquisa')
                                ->with('nome_razao', $pesquisar)
                                ->with('id_fonograma', $id_fonograma)
                                ->with('titulares', $titulares)
                                ->with('msg_limite', $msg_limite)
                ;
            } else {
                $pesquisar = str_replace('*', '%', $pesquisar);
                $sql_count = "select count(*) as qtd from coletivo where nome like ?";
                $sql = "select id_coletivo, nome"
                        . " from coletivo"
                        . " where coletivo.nome like ? limit ".$limite;
                $res_quantos = \DB::select($sql_count, array('%' . $pesquisar . '%'));
                $rs_quantos = $res_quantos[0];
                $coletivos = \DB::select($sql, array('%' . $pesquisar . '%'));
                if ($rs_quantos->qtd > $limite) {
                    $ignorados = $rs_quantos->qtd - $limite;
                    $msg_limite = '(' . $ignorados . ' resultados não exibidos. Refine sua pesquisa)';
                } else {
                    $msg_limite = '';
                }
                return View::make('fonograma.coletivo_pesquisa')
                                ->with('nome_razao', $pesquisar)
                                ->with('id_fonograma', $id_fonograma)
                                ->with('coletivos', $coletivos)
                                ->with('msg_limite', $msg_limite)
                ;
            }
        } else {
            $quantos = 0;
            $msg_limite = '';
            $titulares = array();
            return View::make('fonograma.titulares_pesquisa')
                            ->with('id_fonograma', $id_fonograma)
                            ->with('titulares', $titulares)
                            ->with('msg_limite', $msg_limite)
            ;
        }
    }

    public function getDetalharcoletivo($id_fonograma, $id_coletivo) {
        $coletivo = \Coletivo::find($id_coletivo);
        $sql = "select titular.id_titular, pessoa.nome_razao"
                . " from titular_coletivo, titular, pessoa"
                . " where pessoa.id_pessoa = titular.id_pessoa"
                . " and titular.id_titular = titular_coletivo.id_titular"
                . " and titular_coletivo.id_coletivo = ?"
                ;
        $titulares = \DB::select($sql, array($id_coletivo));
        $categorias = \Categoria::comboTitular('(selecione...)');
        return View::make('fonograma.titulares_coletivo_pesquisa')
                        ->with('id_fonograma', $id_fonograma)
                        ->with('coletivo', $coletivo)
                        ->with('titulares', $titulares)
                        ->with('categorias', $categorias)

        ;
    }

    public function getAdicionar($id_fonograma, $id_titular) {
        $instrumentos = \Instrumento::combo();
        $categorias = \Categoria::comboTitular('(selecione...)');
        $id_instrumentos = array();
        $titular = \Titular::find($id_titular);
        $pseudonimos = $titular->combopseudonimos();
        $fonograma_titular = new \FonogramaTitular;
        $fonograma_titular->id_fonograma_titular = 0;
        $fonograma_titular->id_fonograma = $id_fonograma;
        $fonograma_titular->id_titular = $id_titular;
        return View::make('fonograma.titulares_adicionar')
                        ->with('instrumentos', $instrumentos)
                        ->with('id_instrumentos', $id_instrumentos)
                        ->with('titular', $titular)
                        ->with('categorias', $categorias)
                        ->with('pseudonimos', $pseudonimos)
                        ->with('fonograma_titular', $fonograma_titular)
                        ->with('titulo', 'Adicionar titular')
        ;
    }


    public function getEditar_old($id_fonograma_titular) {
        $instrumentos = \Instrumento::combo();
        $categorias = \Categoria::comboComDefault('(selecione...)');
        $fonograma_titular = \FonogramaTitular::find($id_fonograma_titular);
        $id_instrumentos = $fonograma_titular->id_instrumentos();
        $titular = $fonograma_titular->titular;
        $pseudonimos = $titular->combopseudonimos();
        return View::make('fonograma.titulares_adicionar')
                        ->with('instrumentos', $instrumentos)
                        ->with('id_instrumentos', $id_instrumentos)
                        ->with('titular', $titular)
                        ->with('categorias', $categorias)
                        ->with('pseudonimos', $pseudonimos)
                        ->with('fonograma_titular', $fonograma_titular)
                        ->with('titulo', 'Editar titular')
        ;
    }


    public function getEdit($id_fonograma){
        $fono = \Fonograma::find($id_fonograma);
        $fono_titulares = $fono->fonogramatitulares;
        $categorias = \Categoria::comboComDefault('(selecione...)');
        $instrumentos = \Instrumento::combo();

        $data = array(
                'fonograma'              => $fono
            ,   'fonograma_titulares'    => $fono_titulares
            ,   'instrumentos'    => $instrumentos
            ,   'categorias'        => $categorias
            ,   'titulo'            => 'Editar titulares do fonograma'
            );
        return View::make('fonograma.titulares_edit', $data);
    }

    public function postEditar($id_obra){
        $ret = array();
        try{
            $all = Input::all();
            $qtd = Input::get('qtd');
            if ($qtd > 0){
                for ($i=0; $i < $qtd; $i++) {
                    $id_obra_titular = Input::get('id_'.$i);
                    $id_titular = Input::get('id_titular_'.$i);
                    $id_categoria = Input::get('id_categoria_'.$i);
                    $id_pseudonimo = Input::get('id_pseudonimo_'.$i);
                    $pct_autoral = Input::get('pct_autoral_'.$i);
                    if ($id_obra_titular > 0){
                        $obratitular = ObraTitular::find($id_obra_titular);
                    } else {
                        $obratitular = new ObraTitular;
                        $obratitular->id_obra = Input::get('id_obra');
                    }
                    $obratitular->id_titular = $id_titular;
                    $obratitular->id_categoria = $id_categoria;
                    $obratitular->id_pseudonimo = $id_pseudonimo;
                    $obratitular->pct_autoral_fmt = $pct_autoral;
                    $obratitular->save();
                }
            }
            $ret['status'] = '1';
        } catch(Exception $e){
            $ret['status'] = '-1';
            $ret['erro'] = $e->getMessage().'<br/><br/>'.$e->getTraceAsString();
        }
        return json_encode($ret);
    }


    public function postAdicionar() {
        $ret = array();
        try {
            $rep = new \ISRC\Repositorios\RepositorioFonograma;
            $post = Input::all();
            if ($post['cod_categoria']!= ''){
                $categoria = \Categoria::find($post['cod_categoria']);
                $instrumentos = $post['id_instrumentos'];
                $x = count($instrumentos);
                if ($categoria->cod_categoria == 'MA' && $x == 1 ){
                    $ret['codret'] = -1;
                    $ret['msg'] = iconv('UTF-8', 'UTF-8', "É obrigatório informar o instrumento do músico acompanhante");
                } else {
                    $fonograma_titular = $rep->adicionarTitular($post);
                    $tabela = $this->tabela_de_titulares(Input::get('id_fonograma'), 'Titular adicionado com sucesso');
                    $ret['html'] = iconv('UTF-8', 'UTF-8', $tabela);
                    $ret['codret'] = 0;
                    $ret['msg'] = iconv('UTF-8', 'UTF-8', 'Titular adicionado com sucesso!');
                    $ret['id_fonograma_titular'] = $fonograma_titular->id_fonograma_titular;
                }
            }
        } catch (Exception $ex) {
            $ret['codret'] = -1;
            $ret['msg'] = iconv('UTF-8', 'UTF-8', "Não foi possível adicionar esse titular: " . $ex->getMessage() . $ex->getTraceAsString());
        }
        return json_encode($ret);
    }

    public function postAdicionartitularcoletivo() {
        $ret = array();
        try {
            $rep = new \ISRC\Repositorios\RepositorioFonograma;
            $post = Input::all();
            $fonograma_titular = $rep->adicionarTitularColetivo($post);

            $html = $this->tabela_de_titulares(Input::get('id_fonograma'), 'Titular adicionado com sucesso');
            $ret['html'] = iconv('UTF-8', 'UTF-8', $html);
            $ret['codret'] = 0;
            $ret['msg'] = iconv('UTF-8', 'UTF-8', 'Titular adicionado com sucesso!');
            $ret['id_fonograma_titular'] = $fonograma_titular->id_fonograma_titular;
        } catch (Exception $ex) {
            $ret['codret'] = -1;
            $ret['msg'] = iconv('UTF-8', 'UTF-8', "Não foi possível adicionar esse titular: " . $ex->getMessage() . $ex->getTraceAsString());
        }
        return json_encode($ret);
    }

    public function postExcluir() {
        $ret = array();
        try {
            $rep = new \ISRC\Repositorios\RepositorioFonograma;
            $post = Input::all();
            $id_fonograma = Input::get('id_fonograma');
            $rep->excluirTitular(Input::get('id_fonograma_titular'));

            $html = $this->tabela_de_titulares($id_fonograma, 'Titular excluído com sucesso');
            $ret['html'] = iconv('UTF-8', 'UTF-8',$html);
            $ret['codret'] = 0;
        } catch (Exception $ex) {
            $ret['codret'] = -1;
            $msg = "Não foi possível excluir esse titular: " . $ex->getMessage();
            $ret['msg'] = iconv('UTF-8', 'UTF-8', $msg);
        }
        return json_encode($ret);
    }

    public function getTitulares($id_fonograma){
        return $this->tabela_de_titulares($id_fonograma, 'Titular excluído com sucesso');
    }

    public function getRecalcula($id_fonograma){
        $rep = new \ISRC\Repositorios\RepositorioFonograma;
        $rep->calculaRateio($id_fonograma);
        return $this->tabela_de_titulares($id_fonograma, 'Precentuais recalculados com sucesso');
    }

    public function tabela_de_titulares($id_fonograma, $msg = ''){
        $objeto = \Fonograma::find($id_fonograma);
        $fonograma_titulares = $objeto->fonogramatitulares;
        return View::make('fonograma/titulares_tabela')
                ->with('fonograma_titulares', $fonograma_titulares)
                ->with('objeto', $objeto)
                ->with('msg', $msg)
        ;
    }
}
