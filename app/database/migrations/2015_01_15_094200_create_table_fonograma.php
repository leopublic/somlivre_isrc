<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFonograma extends Migration {

    public function up() {
        Schema::create('fonograma', function ($table) {
            $table->increments('id_fonograma');
            $table->integer('id_fonograma_original')->unsigned()->nullable();
            $table->integer('id_empresa')->unsigned()->nullable();
            $table->integer('id_poutporrit')->unsigned()->nullable();
            $table->integer('id_genero')->unsigned()->nullable();
            $table->integer('id_coletivo')->unsigned()->nullable();
            $table->integer('id_obra')->unsigned()->nullable();
            $table->integer('id_tipo_fonograma')->unsigned()->nullable();
            $table->integer('id_pais_origem')->unsigned()->nullable();
            $table->integer('id_pais_publicacao')->unsigned()->nullable();
            $table->integer('id_tipo_midia')->unsigned()->nullable();
            $table->integer('id_arranjo')->unsigned()->nullable();
            
            $table->string('cod_fonograma_ECAD', 20)->nullable();
            $table->string('cod_GRA', 8)->nullable();
            $table->string('num_catalogo', 20)->nullable();
            $table->string('Cod_ISRC', 12)->nullable();
            $table->string('Cod_ISRC_antes', 12)->nullable();
            $table->string('dsc_complemento_arranjo', 50)->nullable();

            $table->string('extensao_arquivo', 10)->nullable();
            $table->string('mimetype_arquivo', 200)->nullable();
            
            $table->boolean('is_nacional')->nullable();
            $table->boolean('is_instrumental')->nullable();
            $table->boolean('is_gravacao_propria')->nullable();
            $table->boolean('is_publsimult')->nullable();
            
            $table->integer('tmp_duracao_min')->unsigned()->nullable();
            $table->integer('tmp_duracao_seg')->unsigned()->nullable();
            
            $table->dateTime('dt_grav_original')->nullable();
            $table->dateTime('dt_lancamento')->nullable();
            $table->dateTime('dt_emissao')->nullable();

            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->text('obs')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('fonograma', function ($table) {
           $table->index('Cod_ISRC'); 
           $table->index('num_catalogo'); 
        });
}

    public function down() {
        if (Schema::hasTable('fonograma')) {
            Schema::drop('fonograma');
        }
    }

}
