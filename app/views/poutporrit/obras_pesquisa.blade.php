<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Adicionar obra</h4>
</div>
<div class="modal-body">
    <table class="table table-stripped table-condensed table-hover">
        <colgroup>
            <col style="width:60px;"/>
            <col style="width:40px;"/>
            <col style="width:auto;"/>
        </colgroup>
        <thead>
            <tr>
                <th class="center">Ações</th>
                <th class="center">Id</th>
                <th class="left">Título original</th>
            </tr>
        </thead>
        <tbody>
            @foreach($obras as $obra)
            <tr>
                <td class="center">
                    <a onClick="javascript:postAdicionarObra({{$id_poutporrit}},{{$obra->id_obra}});" class="btn btn-xs blue" title="Incluir esse titular no fonograma"><i class="fa fa-plus"></i></a>
                </td>
                <td>{{$obra->id_obra}}</td>
                <td style="text-align:left">{{$obra->titulo_original}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$msg_limite}}
</div>
<div class="modal-footer">
    <button type="button" onClick="btnObraAdicionarClick()" class="btn yellow-gold">Voltar</button>
    <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
</div>
