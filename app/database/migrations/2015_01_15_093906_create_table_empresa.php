<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmpresa extends Migration {

    public function up() {
        Schema::create('empresa', function ($table) {
            $table->increments('id_empresa');
            $table->string('nome', 200)->nullable();
            $table->integer('codigo')->unsigned()->nullable();
            $table->string('sigla',2)->nullable();
            $table->integer('id_produtor_fonografico')->nullable()->unsigned();
            $table->integer('id_pais')->nullable();
            $table->string('sgl_IFPI', 3)->nullable();
            $table->integer('ano_base')->unsigned()->nullable();
            $table->integer('seq_ISRC')->unsigned()->nullable();
            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->string('versao', 20)->nullable();
            $table->string('sgl_radical_IFPI')->nullable();
            $table->timestamps();
        });
        
        
    }

    public function down() {
        if (Schema::hasTable('empresa')) {
            Schema::drop('empresa');
        }
    }
    
}
