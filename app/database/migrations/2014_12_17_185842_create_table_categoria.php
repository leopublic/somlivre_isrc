<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCategoria extends Migration {

	public function up() {
		Schema::create ( 'categoria', function ($table) {
			$table->increments ( 'id_categoria' );
			$table->string ( 'cod_categoria',2 )->nullable();
			$table->string ( 'desc_categoria', 45)->nullable ();
			$table->string( 'tp_direito', 1)->nullable();
			$table->decimal('pct_max', 5,2)->nullable();
			$table->dateTime ( 'dt_inclusao' )->nullable ();
			$table->dateTime ( 'dt_alteracao' )->nullable ();
			$table->string ( 'usuario', 20 )->nullable ();
			$table->timestamps ();
			$table->softDeletes ();
		} );
	}
	public function down() {
		if (Schema::hasTable ( 'categoria' )) {
			Schema::drop ( 'categoria' );
		}
	}
	
}
