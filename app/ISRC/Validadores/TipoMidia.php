<?php
namespace ISRC\Validadores;
class TipoMidia extends Validador {
    public $id_unique;
    public static $rules = array(
    );
    public static $messages = array(
        'dsc_tipo_midia.required'       => 'O nome do tipo de mídia é obrigatório',
        'dsc_tipo_midia.unique'       => 'Já existe outro tipo de mídia com esse nome',
    );
    public function getRules(){
        return array('dsc_tipo_midia' => 'required|unique:tipo_midia,dsc_tipo_midia,'.$this->id_unique.",id_tipo_midia");
    }
}