<?
if (!isset($valor)){
    $valor = Input::old($nome, $reg->$nome);
}
if (!isset($class)){
    $class = '';
}
?>
<div class="form-group">
    <label class="col-md-3 control-label">{{$label}}</label>
    <div class="col-md-9">
        {{Form::text($nome, $valor, array("id" => $nome, "class" => "form-control ".$class))}}
    </div>
</div>