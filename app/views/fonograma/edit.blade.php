@extends('base')

@section('estilos')
@stop

@section('conteudo')
@parent
<div class="row">
    <div class="col-md-12">
        <h3 class="page-title">Editar fonograma</h3>
    </div>
</div>
@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <div class="portlet box {{$portlet_obra}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-music"></i> Obra/pot-pourrit</div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body">
                <div class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group" style="margin-bottom: 0px;">
                                <div class="col-md-10">
                                    {{Form::text('nome_obra', $objeto->nome_obra_poutporrit, array("class" => "form-control ", "disabled"))}}
                                </div>
        @if ($objeto->id_fonograma > 0)
                                <div class="col-md-2">
                                    <button id="btnObraAlterar" onclick="btnObraAlterarClick" class="btn btn-block blue"><i class="fa fa-chain"></i> Alterar a obra</button>
                                </div>
        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box {{$portlet_fonograma}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-microphone"></i> Dados do fonograma {{$objeto->id_fonograma}}</div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body form">
                {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                {{Form::hidden('id_fonograma', $objeto->id_fonograma, array('id'=> 'id_fonograma'))}}
                {{Form::hidden('id_obra', $objeto->id_obra, array('id'=> 'id_obra'))}}
                {{Form::hidden('id_poutporrit', $objeto->id_poutporrit, array('id'=> 'id_obra'))}}
                {{Form::hidden('Cod_ISRC_antes', $objeto->Cod_ISRC, array('id'=> 'Cod_ISRC_antes'))}}
                <div class="form-body">
                    <h4>Tipo de gravação</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">&nbsp;</label>
                                <div class="col-md-9">
                                {{Form::hidden('is_gravacao_propria', 0)}}
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            {{ Form::radio('is_gravacao_propria', 0, !Input::old('is_gravacao_propria', $objeto->is_gravacao_propria) )}} Terceiros
                                        </label>
                                        <label class="radio-inline">
                                            {{ Form::radio('is_gravacao_propria', 1, Input::old('is_gravacao_propria', $objeto->is_gravacao_propria) )}} Própria
                                        </label>
                                    </div>
                                </div>
                            </div>
                            </div>
                    </div>
                    <h4>Códigos</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">ISRC</label>
                                <div class="col-md-9">
                                    {{Form::text('Cod_ISRC', Input::old('Cod_ISRC', $objeto->Cod_ISRC), array("class" => "form-control input-medium", "id"=> "Cod_ISRC"))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">GRA</label>
                                <div class="col-md-9">
                                    {{Form::text('cod_GRA', Input::old('cod_GRA', $objeto->cod_GRA), array("class" => "form-control input-small"))}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Data de emissão</label>
                                <div class="col-md-8">
                                    {{Form::text('dt_emissao_fmt', Input::old('dt_emissao_fmt', $objeto->dt_emissao_fmt), array("class" => "form-control input-small mask_data"))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Catálogo</label>
                                <div class="col-md-8">
                                    {{Form::text('num_catalogo', Input::old('num_catalogo', $objeto->num_catalogo), array("class" => "form-control input-small"))}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <h4>Dados do fonograma</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Gênero</label>
                                <div class="col-md-9">
                                    {{Form::select('id_genero', $generos, Input::old('id_genero', $objeto->id_genero), array("class" => "form-control select2me"))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tipo mídia</label>
                                <div class="col-md-9">
                                    {{Form::select('id_tipo_midia', $tipos_midia, Input::old('id_tipo_midia', $objeto->id_tipo_midia), array("class" => "form-control select2me"))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">País origem</label>
                                <div class="col-md-9">
                                    {{Form::select('id_pais_origem', $paises, Input::old('id_pais_origem', $objeto->id_pais_origem), array("class" => "form-control select2me"))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">País lançamento</label>
                                <div class="col-md-9">
                                    {{Form::select('id_pais_publicacao', $paises, Input::old('id_pais_publicacao', $objeto->id_pais_publicacao), array("class" => "form-control select2me"))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tipo de produto</label>
                                <div class="col-md-9">
                                    {{Form::select('id_tipo_produto', $tipos_produto, Input::old('id_tipo_produto', $objeto->id_tipo_produto), array("class" => "form-control select2me"))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nome do produto</label>
                                <div class="col-md-9">
                                    {{Form::text('nome_produto', Input::old('nome_produto', $objeto->nome_produto), array("class" => "form-control"))}}
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Gravação original</label>
                                <div class="col-md-8">
                                    {{Form::text('dt_grav_original_fmt', Input::old('dt_grav_original_fmt', $objeto->dt_grav_original_fmt), array("class" => "form-control input-small mask_data"))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Lançamento</label>
                                <div class="col-md-8">
                                    {{Form::text('dt_lancamento_fmt', Input::old('dt_lancamento_fmt', $objeto->dt_lancamento_fmt), array("class" => "form-control input-small mask_data"))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Duração</label>
                                <div class="col-md-8">
                                    <div class="input-group" >
                                        <span class="input-group-addon">Min </span>
                                        {{Form::text('tmp_duracao_min', Input::old('tmp_duracao_min', $objeto->tmp_duracao_min), array("class" => "form-control  form-control-inline input-xsmall"))}}
                                        <span class="input-group-addon">Seg </span>
                                        {{Form::text('tmp_duracao_seg', Input::old('tmp_duracao_seg', $objeto->tmp_duracao_seg), array("class" => "form-control form-control-inline input-xsmall"))}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Classificação</label>
                                <div class="col-md-8">
                                    {{Form::select('id_tipo_fonograma', $tipos_fonograma, Input::old('id_tipo_fonograma', $objeto->id_tipo_fonograma), array("class" => "form-control select2me", "id"=>"id_tipo_fonograma"))}}
                                </div>
                            </div>
                            <div class="form-group" id="form-group-arranjo" style="display: none;">
                                <label class="col-md-4 control-label">Arranjo</label>
                                <div class="col-md-8">
                                    {{Form::select('id_arranjo', $arranjos, Input::old('id_arranjo', $objeto->id_arranjo), array("class" => "form-control select2me", "id"=>"id_arranjo"))}}
                                </div>
                            </div>
                            <div class="form-group" id="form-group-complemento" style="display: none;">
                                <label class="col-md-4 control-label">Complemento</label>
                                <div class="col-md-8">
                                    {{Form::text('dsc_complemento_arranjo', Input::old('dsc_complemento_arranjo', $objeto->dsc_complemento_arranjo), array("class" => "form-control", "id"=>"dsc_complemento_arranjo"))}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <h4>Observação</h4>
                    <div class="form-group">
                        <div class="col-md-12">
                            {{Form::textarea('obs', Input::old('obs', $objeto->obs), array("class" => "form-control"))}}
                        </div>
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green">Salvar</button>
                        <a href="{{URL::to('/admin/fonograma')}}" class="btn default">Cancelar</a>
                    </div>
                </div>
                {{Form::close()}}
                {{Form::hidden('tipos_fonograma_trilha', $tipos_fonograma_trilha, array("id"=> "tipos_fonograma_trilha"))}}
                {{Form::hidden('arranjos_precisam_complemento', $arranjos_precisam_complemento, array("id"=> "arranjos_precisam_complemento"))}}
            </div>
        </div>
        @if ($objeto->id_fonograma > 0)
        @include('fonograma.titulares_tabela')
        @endif
    </div>
</div>
        @if ($objeto->id_fonograma > 0)

<div class="row">
    <div class="col-md-12">
        <div class="portlet box {{$portlet_padrao}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-volume-up"></i> Arquivo</div>
            </div>
            <div class="portlet-body form">
                {{Form::open(array('role' => "form", "class" => "form-horizontal", "url" => "/admin/fonograma/arquivo", "files" => true))}}
                {{Form::hidden('id_fonograma', $objeto->id_fonograma, array('id'=> 'id_fonograma'))}}
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Arquivo atual</label>
                        <div class="col-md-9">
                            @if ($objeto->arquivoexiste)
                            <a class="btn green-seagreen" href="/admin/fonograma/download/{{$objeto->id_fonograma}}" target="_blank"><i class="fa fa-download"></i> Baixar</a>
                            @else
                            (nenhum arquivo carregado)
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Substituir arquivo</label>
                        <div class="col-md-9">
                            <input type="file" id="arquivo" name="arquivo"/>
                        </div>
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green">Enviar</button>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>

        @endif

<!-- Popups -->
<div id="modTitularPesquisar" class="modal fade" tabindex="-1"  data-width="650" data-height="480">
</div>


<div id="modColetivoPesquisar" class="modal fade" tabindex="-1"  data-width="650" data-height="650">
</div>

<div id="modObraPesquisar" class="modal fade" tabindex="-1"  data-width="650" data-height="650">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Selecionar obra</h4>
    </div>
    <div class="modal-body">
        {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "fmrObraPesquisar"))}}
        <div class="form-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Título</label>
                        <div class="col-md-9">
                            {{Form::text('titulo', '', array("class" => "form-control", "id"=> 'titulo'))}}
                        </div>
                    </div>
        </div>
        {{Form::close()}}
        <div id="msg" style="color:red;"></div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn blue" onclick="javascript:btnObraPesquisarClick();">Pesquisar</button>
        <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
    </div>
</div>
<div id="modObraListar" class="modal fade" tabindex="-1"  data-width="650" data-height="650">
</div>
@stop

@section('scripts_full')
<script>

    jQuery(document).ready(function () {
        $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
          '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
            '<div class="progress progress-striped active">' +
              '<div class="progress-bar" style="width: 100%;"></div>' +
            '</div>' +
          '</div>';

        $.fn.modalmanager.defaults.resize = true;

    });
    jQuery(document).ready(function () {
        $( "#btnObraAlterar" ).click(function( event ) {
            event.preventDefault();
            $('#modObraPesquisar').modal();
        });

        $("#Cod_ISRC").inputmask("**-***-99-99999", {
            "placeholder": "__-___-__-_____"
        }); //multi-char placeholder

        toggleISRC();

    });

    function noenter() {
        return !(window.event && window.event.keyCode == 13); 
    }

    $( "input[name=is_gravacao_propria]:radio" ).change(function() {
        toggleISRC();
    });

    function toggleISRC(){
        if ($('input[name=is_gravacao_propria]:checked').val() == '1'){
            $('#Cod_ISRC').prop('disabled', true);
            $('#Cod_ISRC').val($('#Cod_ISRC_antes').val());
        } else {
            $('#Cod_ISRC').prop('disabled', false);
            $('#Cod_ISRC').val($('#Cod_ISRC_antes').val());
        }
    }


    jQuery(document).ready(function () {
        toggleArranjo();
        toggleComplemento();
    });

    $('#id_tipo_fonograma').change(function(){
        toggleArranjo();
    });
    $('#id_arranjo').change(function(){
        toggleComplemento();
    });


    function toggleArranjo(){
        var sel = '#'+$('#id_tipo_fonograma').val()+'#';
//        console.log('Buscando '+sel);
        if (sel != '##'){
            var valores = $('#tipos_fonograma_trilha').val();
  //          console.log('Valores '+valores);
    //        console.log('Index '+valores.indexOf(sel));
            if (valores.indexOf(sel) >= 0){
                $('#form-group-arranjo').show();
            } else {
                $('#form-group-arranjo').hide();
                $('#id_arranjo').val('');
                $('#dsc_complemento_arranjo').val('');
                $('#form-group-complemento').hide();
            }
        } else {
            $('#form-group-arranjo').hide();
            $('#id_arranjo').val('');
            $('#dsc_complemento_arranjo').val('');
            $('#form-group-complemento').hide();
        }

    }

    function toggleComplemento(){
        var sel = '#'+$('#id_arranjo').val()+'#';
      //  console.log('Buscando '+sel);
        if (sel != '##'){
            var valores = $('#arranjos_precisam_complemento').val();
        //    console.log('Valores '+valores);
            if (valores.indexOf(sel) >=0){
                $('#form-group-complemento').show();
            } else {
                $('#dsc_complemento_arranjo').val('');
                $('#form-group-complemento').hide();
            }
        } else {
            $('#dsc_complemento_arranjo').val('');
            $('#form-group-complemento').hide();
        }
    }

    $('#itemFonogramas').addClass('active');

    // Exibe o painel de seleçao de obra
    function btnObraAlterarClick(){
        $('#modObraPesquisar').modal();
    }

    // Recupera a pesquisa de obras
    function btnObraPesquisarClick(){
        Metronic.blockUI({
            target: '#modObraPesquisar',
            boxed: true,
            message: '(aguarde...)',
        });
        var id_fonograma = $('#id_fonograma').val();
        var url = "/admin/obra/selecionarparafonograma/"+id_fonograma+"/"+$('#modObraPesquisar #titulo').val();

        $("#modObraListar" ).load( url,'' , function(){
            $('#modObraPesquisar').modal('hide');
            $('#modObraListar').modal();
            Metronic.unblockUI('#modObraPesquisar');
        });
    }

    function postObraAlterar(id_fonograma, id_obra){
        $.post( "/admin/fonograma/alterarobra", {id_fonograma: id_fonograma, id_obra:id_obra}
            , function(data){
                if (data.codret == 0){
                    $('#modObraListar').modal('hide');
                    location.reload(true);
                } else {
                    $('#modal-msg').html(data.msg);
                }
            }
            , "json" )
        ;
    }

    // Exibe o painel de inclusão de titular
    function btnTitularAdicionarClick(){
        Metronic.blockUI({boxed: true, overlayColor:'#acacac',message: 'Aguarde...'});
        var url = "/admin/fonogramatitulares/formpesquisa/";
        $("#modTitularPesquisar" ).load( url,'' , function(){
            Metronic.unblockUI();
            $('#modTitularPesquisar').modal();
        });
    }

    // Recupera a pesquisa de titulares
    function btnTitularPesquisarClick(){
        Metronic.blockUI({
            target: '#modTitularPesquisar',
            boxed: true,
            message: '(aguarde...)',
        });
        var id_fonograma = $('#id_fonograma').val();
        var tipo = $('#modTitularPesquisar #tipo').val();
        var url = "/admin/fonogramatitulares/gridresultadopesquisa/titular/"+id_fonograma+"/"+encodeURIComponent($('#modTitularPesquisar #nome_razao').val());
        $("#modTitularPesquisar" ).load( url,'' , function(){
            Metronic.unblockUI('#modTitularPesquisar');
        });
    }


    function getAdicionarTitular(id_fonograma, id_titular){
        Metronic.blockUI({
            target: '#modTitularPesquisar',
            boxed: true,
            message: '(aguarde...)'
        });
        var url = '/admin/fonogramatitulares/adicionar/'+id_fonograma+'/'+id_titular;
//        $('#modTitularPesquisar').empty();
        $('#modTitularPesquisar').load( url, '', function(){
            Metronic.unblockUI('#modTitularPesquisar');
            $("#modTitularPesquisar .mask_data").inputmask("d/m/y", {
                "placeholder": "dd/mm/aaaa"
            });

            $('#modTitularPesquisar .select2').select2();
        });
    }

    function getEditarTitular(id_fonograma_titular){
        var url = '/admin/fonogramatitulares/editar/'+id_fonograma_titular;
        Metronic.blockUI({boxed: true, overlayColor:'#acacac',message: 'Aguarde...'});
            $('#modTitularPesquisar').empty();
            $('#modTitularPesquisar').load( url, '', function(){
                Metronic.unblockUI();
                $("#modTitularPesquisar .mask_data").inputmask("d/m/y", {
                    "placeholder": "dd/mm/aaaa"
                }); //multi-char placeholder
                $('.select2').select2();
                $('#modTitularPesquisar').modal();
            });
    }

    function postAdicionarTitular(){
        Metronic.blockUI({
            target: '#modTitularPesquisar',
            boxed: true,
            message: '(aguarde...)'
        });
        $.post( "/admin/fonogramatitulares/adicionar", $( "#form_titular" ).serialize()
            , function(data){
                Metronic.unblockUI('#modTitularPesquisar');
                if (data.codret == 0){
                    $('#modTitularPesquisar').modal('hide');
                    $(document.body).animate({
                        'scrollTop':  $('#containerFonogramaTitulares').offset().top
                    }, 2000);
                    $('#containerFonogramaTitulares').html(data.html);
                    $('.popovers').popover();
                    $('#tabela'+data.id_fonograma_titular).stop().animate({backgroundColor: "#95A5A6"}, 100)
                        .animate({backgroundColor: "#FFFFFF"}, 4500);
                } else {
                    $('#modTitularPesquisar #modal-msg').html(data.msg);
                }
            }
            , "json" )
        ;
    }

    function resetPesquisaTitular(){
        $('#modTitularPesquisar').modal('hide');
    }

    // Exibe o painel de inclusão de coletivos
    function btnColetivoAdicionarClick(){
        Metronic.blockUI({boxed: true, overlayColor:'#acacac',message: 'Aguarde...'});
        var url = "/admin/fonogramacoletivos/formpesquisa/";
        $("#modColetivoPesquisar" ).load( url,'' , function(){
            Metronic.unblockUI();
            $('#modColetivoPesquisar').modal();
        });
    }

    // Recupera a pesquisa de coletivos
    function btnColetivoPesquisarClick(){
        Metronic.blockUI({
            target: '#modColetivoPesquisar',
            boxed: true,
            message: '(aguarde...)',
        });
        var id_fonograma = $('#id_fonograma').val();
        var url = "/admin/fonogramatitulares/gridresultadopesquisa/coletivo/"+id_fonograma+"/"+encodeURIComponent($('#modColetivoPesquisar #nome_razao').val());
        $("#modColetivoPesquisar" ).load( url,'' , function(){
            Metronic.unblockUI('#modColetivoPesquisar');
        });
    }
    // Recupera titulares do coletivo
    function getDetalharColetivo(id_fonograma, id_coletivo){
        Metronic.blockUI({
            target: '#modColetivoPesquisar',
            boxed: true,
            message: '(aguarde...)',
        });
        var url = "/admin/fonogramatitulares/detalharcoletivo/"+id_fonograma+"/"+id_coletivo;
        $("#modColetivoPesquisar" ).load( url,'' , function(){
            Metronic.unblockUI('#modColetivoPesquisar');
        });
    }

    function postAdicionarTitularColetivo(id_fonograma, id_titular, id_coletivo){
        Metronic.blockUI({
            target: '#modColetivoPesquisar',
            boxed: true,
            message: '(adicionando...)'
        });
        $.post( "/admin/fonogramatitulares/adicionartitularcoletivo", {id_fonograma: id_fonograma, id_titular:id_titular, id_coletivo: id_coletivo, pct_participacao_fmt: $('#pct_participacao_fmt'+id_titular).val(), id_categoria: $('#id_categoria'+id_titular).val()}
            , function(data){
                if (data.codret == 0){
                    $(document.body).animate({
                        'scrollTop':  $('#containerFonogramaTitulares').offset().top
                    }, 2000);
                    $('#acaoTitular'+id_titular).html($('#botaoOk').html());
                    $('#containerFonogramaTitulares').html(data.html);
                    $('#tabela'+data.id_fonograma_titular).stop().animate({backgroundColor: "#95A5A6"}, 100)
                        .animate({backgroundColor: "#FFFFFF"}, 4500);
                    Metronic.unblockUI('#modColetivoPesquisar');
                } else {
                    $('#modal-msg').html(data.msg);
                }
            }
            , "json" )
        ;
    }

    function resetPesquisaColetivo(){
        $('#modColetivoPesquisar').modal('hide');
    }

    function postExcluirTitular(id_fonograma_titular){
        Metronic.blockUI({
                target: '#portlet-body-titulares',
                boxed: true,
                message: 'Excluindo... (aguarde)'
        });
        $.post( "/admin/fonogramatitulares/excluir", {id_fonograma_titular:id_fonograma_titular, id_fonograma: $('#id_fonograma').val()}
            , function(data){
                Metronic.unblockUI('#portlet-body-titulares');
                if (data.codret == 0){
                    $('#ajax-modal').modal('hide');
                    $(document.body).animate({
                        'scrollTop':  $('#containerFonogramaTitulares').offset().top
                    }, 1000);
                    $('#containerFonogramaTitulares').html(data.html);
                } else {
                    $('#modal-msg').html(data.msg);
                }
            }
            , "json" )
        ;
    }
</script>
@stop
