@extends('base')

@section('estilos')
table tr.red td{
background-color:red;
color:#fff;
font-weight:bold;
}
@stop

@section('conteudo')
<div class="row">
    <div class="col-md-12">
        <h3 class="page-title">{{$titulo}}</h3>
    </div>
</div>

@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <div class="portlet box purple-studio">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-user"></i>Adicionar outro titular</div>
                <div class="tools">
                    <a href="javascript:;" class="expand"></a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "form_titulares"))}}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        {{Form::text('pesquisar', Input::old('pesquisar'), array("class" => "form-control", "id" => "txtPesquisarTitular"))}}
                                        <span class="input-group-btn"><button id="btnPesquisarTitular" type="button" class="btn"><i class="fa fa-search"></i> Pesquisar</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="containerTitulares" style="text-align: center;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12" id="containerObraTitulares" name="containerObraTitulares">
        <div class="portlet box {{$portlet_titular}}" id="containerFonogramaTitulares">
    <div class="portlet-title">
        <div class="caption">{{$titular_icon}} Titulares</div>
            <div class="actions">
                    <a href="javascript: postEditar();"  class="btn btn-sm green">Salvar</a>
            </div>
    </div>
    <div class="portlet-body" id="portlet-body-titulares">
        {{Form::open(array('id' => 'formObraTitulares'))}}
        {{Form::hidden('id_obra', $obra->id_obra, array('id' => 'id_obra'))}}
        {{Form::hidden('valtotal', '', array('id' => 'valtotal'))}}
        {{Form::hidden('qtd', '', array('id' => 'qtd'))}}
        <table class="table table-stripped table-condensed table-hover" id="tblObraTitular">
            <thead>
                <tr>
                    <th class="center">A&ccedil;&otilde;es</th>
                    <th class="center">Id</th>
                    <th class="left">Titular</th>
                    <th class="left">Pseud&ocirc;nimo</th>
                    <th class="left">Categoria</th>
                    <th class="right">Part. %</th>
                </tr>
            </thead>
            <tbody>
                <? $total = 0; 
                    $i = 0;?>
                @foreach($obra_titulares as $obra_titular)
                <? $total += $obra_titular->pct_autoral; ?>
                    @include('obra.linha_titular')
                <?php $i++;?>
                @endforeach
            </tbody>
        </table>
        {{Form::close()}}
        <div @if ($total != 100) class="red" @endif style="text-align: right; padding-right: 7px;">
            <span style="text-align: right;">Total:</span>
            <span id="total"><? print $total; ?>%</span>
        </div>
    </div>
</div>
<div id="stack1" class="modal fade" tabindex="-1" data-focus-on="input:first">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Atenção</h4>
    </div>
    <div class="modal-body" id="msg-erro">
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary">Ok</button>
    </div>
</div>

@stop
@section('scripts_full')
<script>
jQuery(document).ready(function () {
    $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
            '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar" style="width: 100%;"></div>' +
            '</div>' +
            '</div>';

    $.fn.modalmanager.defaults.resize = true;

    $('#txtPesquisarTitular').keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
                $('#btnPesquisarTitular').trigger('click');
            }
    });

    $('#btnPesquisarTitular').click(function (event) {
        event.preventDefault();
        $(this).html('<i class="fa fa-spin fa-spinner"></i> Aguarde');
        $(this).attr('disabled', 'disabled');
        $('#containerTitulares').html('<br/><i class="fa fa-spin fa-spinner"></i> carregando...');
        var id_obra = $('#id_obra').val();

        var url = "/admin/obratitulares/gridresultadopesquisa/" + id_obra + "/" + encodeURIComponent($('#txtPesquisarTitular').val());
        $("#containerTitulares").load(url);
        $(this).html('<i class="fa fa-search"></i> Pesquisar');
        $(this).removeAttr('disabled');
    });
    recalcula();
});

$('.pct_autoral_class').change(function(){
    recalcula();
});

function getAdicionarTitular(id_obra, id_titular) {
    var url = '/admin/obratitulares/adicionarnovo/'+ ($('.pct_autoral_class').size() ) + '/' + id_obra + '/' + id_titular;
    var ja_tem = false;
    $('.id_titular').each(function(){
        if ($(this).val() == id_titular){
            ja_tem = true;
        }
    });
    if (ja_tem){
        $('#msg-erro').html('Esse titular já foi adicionado à obra!');
        $('#stack1').modal();
    } else {
        $.get( url )
            .done(function( data ) {
                $('#tblObraTitular > tbody:last-child').append(data);
                $('.pct_autoral_class').change(function(){
                    recalcula();
                });
                $('#tblObraTitular tr:last').animate({backgroundColor: "#95A5A6"}, 100).animate({backgroundColor: "#FFFFFF"}, 4500);
            });
    }
}

function recalcula(){
    var total = 0;
    $('.pct_autoral_class').each(function(){
        valor = parseFloat($(this).val().replace(",", "."));
        if (valor == 0){
            $(this).css('color', 'red');
        } else {
            $(this).css('color', 'black');
        }
        total = total + valor;
    });
    $('#valtotal').val(total);
    if (total != 100){
        $('#total').css('color', 'red');
    } else {
        $('#total').css('color', 'black');
    }
    $('#total').html(total.toString().replace(".", ",") + "%");
}

function postEditar(){
    var url = '/admin/obratitulares/editar/'+$('#id_obra').val();
    $('#qtd').val($('.pct_autoral_class').size());
    if (postValido()){
        $.post( url
                , $( "#formObraTitulares" ).serialize()
                , function(data){
                    if (data.status == '1'){
                        $('#msg-erro').html('Titulares atualizados com sucesso!');
                        $('#stack1').modal();
                    } else {
                        $('#msg-erro').html(data.erro);
                        $('#stack1').modal();
                    }
                }
                , 'json');
    }
}

function postValido(){
    var ret = true;
    recalcula();
    msg_perc = validaPercentuais();
    msg_cat = validaCategorias();
    msg = msg_perc+msg_cat;
    if (msg != ''){
        $('#msg-erro').html('Não é possível salvar pois foram identificados erros:'+msg);
        $('#stack1').modal();
        return false;
    } else {
        return true;
    }
}

function validaCategorias(){
    var total = 0;
    var msg = '';
    var br = '<br/>';
    var valor = 0;
    var tem_produtor = false;
    var tem_em_branco = false;
    $('.id_categoria').each(function(){
        if ($(this).val() == 5){
            tem_produtor = true;
            $(this).css('color', 'black');
        } else {
            if ($(this).val() == ''){
                tem_em_branco = true;
                $(this).css('color', 'red');
            } else {
                $(this).css('color', 'black');
            }
        }
    });
    if (tem_em_branco){
        msg = msg+br+'- A categoria é obrigatória';
    }
    return msg;
}

function validaPercentuais(){
    var total = 0;
    var msg = '';
    var br = '<br/>';
    var valor = 0;
    var tem_zero = false;
    $('.pct_autoral_class').each(function(){
        valor = parseFloat($(this).val().replace(",", "."));
        if (valor == 0){
            tem_zero = true;
            $(this).css('color', 'red');
        } else {
            $(this).css('color', 'black');
        }
        total = total + valor;
    });
    $('#valtotal').val(total);
    if (total != 100){
        $('#total').css('color', 'red');
        msg = msg+br+ '- O percentual total deve ser 100%';
        br = '<br/>';
    } else {
        $('#total').css('color', 'black');
    }
    if (tem_zero){
        msg = msg+br+'- Nenhum percentual pode estar zerado';
        br = '<br/>';
    }
    $('#total').html(total.toString().replace(".", ",") + "%");
    return msg;
}

</script>
@stop
