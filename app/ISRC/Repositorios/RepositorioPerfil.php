<?php

namespace ISRC\Repositorios;

class RepositorioPerfil extends RepositorioCadastro {
    
    public function tituloModelo(){
        return "perfil";
    }
    
    public function nomeModelo(){
        return '\Perfil';
    }
    
    public function ordenacaoPadrao(){
        return 'descricao';
    }
    
}
