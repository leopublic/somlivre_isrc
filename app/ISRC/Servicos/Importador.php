<?php

namespace ISRC\Servicos;

class Importador {

    public static $repMDB;

    public function nomeTabela() {
        
    }

    public function sqlSelectMdb() {
        
    }

    public function novoObjeto() {
        
    }

    public function carregaObjeto($regMDB, $preservar_id) {
        
    }

    public function nome_id_local(){

    }
    public function nome_id_mdb(){
        return $this->nome_id_local();
    }


    public function tabela() {
        
    }

    public function recuperaExistenteOuNovo(){
        $obj = \Empresa::where('sgl_IFPI', '=', $regMDB['sgl_IFPI']);
        if (!$obj->get_id() > 0){
            $obj = $this->novoObjeto();
        }
        
    }
    
    public function dataHoraOk($campo) {
        if (trim($campo) == '') {
            return null;
        } else {
            return '20' . substr($campo, 6, 2) . '-' . substr($campo, 0, 2) . '-' . substr($campo, 3, 2) . ' ' . substr($campo, 9, 8);
        }
    }

    public function importa() {
        $msg = "Carregando tabela " . $this->tabela();
        print "\n" . $msg . "\r";
        // Abre tabela MDB
        $cursorMDB = self::$repMDB->cursor($this->sqlSelectMdb());
        $qtd = 0;

        foreach ($cursorMDB as $regMDB) {
            $obj = $this->carregaObjeto($regMDB);
            $obj->save();
            print $msg . " => " . $qtd . "\r";
            $qtd++;
        }
        $qtd--;
        print "Tabela " . $this->tabela() . " carregada com sucesso! (" . $qtd . " registros)";
    }

    public function importaRegistro($preservar_id) {
        $msg = "Carregando tabela " . $this->tabela();
//        print "\n" . $msg . "\r";
        print "\n\n" . $msg . "\r";
        // Abre tabela MDB
        $cursorMDB = self::$repMDB->query($this->sqlSelectMdb());
        $qtd = 0;

        while ($regMDB = $cursorMDB->fetch()) {
            $obj = $this->carregaObjeto($regMDB, $preservar_id);
            print "\n". $msg . " =>  chave " . $obj->get_id() . "   ind=". $qtd ;
            $obj->save();
            $qtd++;
        }
        $cursorMDB->closeCursor();
        $this->finaliza();
        $qtd--;
        print "\n";
        print "\nTabela " . $this->tabela() . " carregada com sucesso! (" . $qtd . " registros)";
        print "\n===================================================================";
        print "\n===================================================================";
        print "\n";
    }

    public function carregaCampos($obj, $reg, $campos, $preservar_id) {
        foreach ($campos as $campo) {
            $obj->$campo = $reg[$campo];
        }
        if ($preservar_id){
            $nome_id_local = $this->nome_id_local();
            $nome_id_mdb = $this->nome_id_mdb();
            if ($nome_id_local != '' && $nome_id_mdb != ''){
                $obj->$nome_id_local = $reg[$nome_id_mdb];
            }
        }
        return $obj;
    }

    public function finaliza(){

    }

    public function obtem_id_obra($id_obra, $id_entidade){
        $obra = \Obra::where('id_empresa', '=', \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa)
                        ->where('id_obra_original', '=', $id_obra)
                        ->first();
        if (count($obra) > 0){
            return $obra->id_obra;
        } else {
            \Log::error(get_class($this->novoObjeto()).' (id='.$id_entidade.' empresa='.\ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa.'): obra não encontrada id='.$id_obra);
            return null;
        }

    }

    public function obtem_id_poutporrit($id_poutporrit, $id_entidade){
        $poutporrit = \Poutporrit::where('id_empresa', '=', \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa)
                        ->where('id_poutporrit_original', '=', $id_poutporrit)
                        ->first();
        if (count($poutporrit) > 0){
            return $poutporrit->id_poutporrit;
        } else {
            \Log::error(get_class($this->novoObjeto()).' (id='.$id_entidade.' empresa='.\ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa.'): poutporrit não encontrado id='.$id_poutporrit);
            return null;
        }

    }

    public function obtem_id_coletivo($id_coletivo, $id_entidade){
        $coletivo = \Coletivo::where('id_empresa', '=', \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa)
                        ->where('id_coletivo_original', '=', $id_coletivo)
                        ->first();
        if (count($coletivo) > 0){
            return $coletivo->id_coletivo;
        } else {
            \Log::error(get_class($this->novoObjeto()).' (id='.$id_entidade.' empresa='.\ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa.'): coletivo não encontrado id='.$id_coletivo);
            return null;
        }
    }

    public function obtem_id_titular($id_titular, $id_entidade){
        $titular = \Titular::where('id_empresa', '=', \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa)
                        ->where('id_titular_original', '=', $id_titular)
                        ->first();
        if (count($titular) > 0){
            return $titular->id_titular;
        } else {
            \Log::error(get_class($this->novoObjeto()).' (id='.$id_entidade.' empresa='.\ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa.'): titular não encontrado id='.$id_titular);
            return null;
        }
    }
}
