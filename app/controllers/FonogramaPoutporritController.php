<?php

class FonogramaPoutporritController extends BaseController {
    public function getShow($id_fonograma) {
        return $this->show($id_fonograma);
    }
    
    public function postShow($id_fonograma){
        return $this->show($id_fonograma);
    }
    
    public function show($id_fonograma){
        Input::flash();
        $objeto = \Fonograma::find($id_fonograma);
        $pesquisar = Input::get('pesquisar', Input::old('pesquisar'));
        $regs = \PoutPorrit::where('nome', 'like', '%'.$pesquisar.'%')
                    ->with(array('obras', 'obras.obra'))
                    ->orderBy('nome')
                    ->paginate(10);
        return View::make('fonograma.poutporrit')
                ->with(compact('objeto', 'regs', 'pesquisar'))
            ;
    }
    
    public function getSet($id_fonograma, $id_poutporrit){
        try{
            $rep = new \ISRC\Repositorios\RepositorioFonograma;
            $rep->atualizaPoutporrit($id_fonograma, $id_poutporrit);
            Session::flash('msg', 'Fonograma atualizado com sucesso');
        } catch (Exception $ex) {
            Session::flash('error', $ex->getMessage());
        }
        return Redirect::to('/admin/fonogramaobras/show/'.$id_fonograma);
    }
}
