@extends('base')

@section('estilos')
@stop

@section('conteudo')
<div class="row">
    <div class="col-md-12">
        <h3 class="page-title">{{$titulo}}</h3>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-gift"></i>{{$titulo}}</div>
                <div class="tools">
                </div>
            </div>
            @include('_padroes.mensagens')
            <div class="portlet-body form">
                {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                {{Form::hidden('id_poutporrit', $objeto->id_poutporrit, array('id'=> 'id_poutporrit'))}}
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">ID</label>
                        <div class="col-md-9">
                            {{Form::text('id_poutporrit', $objeto->id_poutporrit, array("class" => "form-control", "disabled"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nome</label>
                        <div class="col-md-9">
                            {{Form::text('nome', Input::old('nome', $objeto->nome), array("class" => "form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Código ISWC</label>
                        <div class="col-md-9">
                            {{Form::text('cod_IWC', Input::old('cod_IWC', $objeto->cod_IWC), array("class" => "form-control"))}}
                        </div>
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green">Salvar</button>
                        <a href="{{URL::to('/poutporrit')}}" class="btn default">Cancelar</a>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
@if ($objeto->id_poutporrit > 0)
<div class="row">
    <div class="col-md-12" id="containerObras">
        @include ('poutporrit.obras_tabela')
    </div>
</div>

<div id="modal" class="modal fade" tabindex="-1"  data-width="650" data-height="650">
</div>
@endif
@stop

@section('scripts')
    $('#menuCadastros').addClass('active');
    $('#menuCadastros').addClass('open');
    $('#itemPotpourri').addClass('active');

            
    jQuery(document).ready(function () {
        $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner = 
          '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
            '<div class="progress progress-striped active">' +
              '<div class="progress-bar" style="width: 100%;"></div>' +
            '</div>' +
          '</div>';

        $.fn.modalmanager.defaults.resize = true;
    });

    function btnObraAdicionarClick(){
        var url = "/admin/potpourri/formpesquisa/";
        $('body').modalmanager('loading');
        $("#modal" ).load( url, '', function(){
            $('#modal').modal();
        });
    }
    // Recupera a pesquisa de titulares
    function btnObraPesquisarClick(){
        Metronic.blockUI({
            target: '#modal',
            boxed: true,
            message: '(aguarde...)',
        });
        var id_poutporrit = $('#id_poutporrit').val();
        var url = "/admin/potpourri/pesquisarobras/"+id_poutporrit+"/"+$('#modal #titulo_original').val();
        $("#modal" ).load( url,'' , function(){
            Metronic.unblockUI('#modal');    
        });
    }

        
    function postAdicionarObra(id_poutporrit, id_obra){
        Metronic.blockUI({
            target: '#modal',
            boxed: true,
            message: '(aguarde...)'
        });
        $.post( "/admin/potpourri/adicionarobra", {id_poutporrit: id_poutporrit, id_obra:id_obra}
            , function(data){
                if (data.codret == 0){
                    $('#modal').modal('hide');
                    $(document.body).animate({
                        'scrollTop':  $('#portlet-body-obras').offset().top
                    }, 2000);
                    $('#containerObras').html(data.html);
                    $('#linha'+data.id_poutporrit_obra).stop().animate({backgroundColor: "#95A5A6"}, 100)
                        .animate({backgroundColor: "#FFFFFF"}, 4500);
                } else {
                    $('#modal-msg').html(data.msg);
                }
            }        
            , "json" )
        ;
    }
    
    function postExcluirObra(id_poutporrit_obra){
        Metronic.blockUI({
                target: '#portlet-body-obras',
                boxed: true,
                message: 'Excluindo... (aguarde)'
        });
        $.post( "/admin/potpourri/excluirobra", {id_poutporrit_obra:id_poutporrit_obra, id_poutporrit: $('#id_poutporrit').val()}
            , function(data){
                Metronic.unblockUI('#portlet-body-obras');
                if (data.codret == 0){
                    $('#ajax-modal').modal('hide');
                    $(document.body).animate({
                        'scrollTop':  $('#containerObras').offset().top
                    }, 1000);
                    $('#containerObras').html(data.html);
                } else {
                    $('#modal-msg').html(data.msg);
                }
            }        
            , "json" )
        ;
    }


    
@stop
