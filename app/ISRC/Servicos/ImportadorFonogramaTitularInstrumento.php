<?php

namespace ISRC\Servicos;

class ImportadorFonogramaTitularInstrumento extends Importador{

    public function sqlSelectMdb() {
        return "select id_fono_titular_instru
            , id_categoria
            ,id_fonograma_titular
            ,id_instrumento
            ,dt_inclusao
            ,dt_alteracao
            ,usuario
            from ISRC_fono_titular_instru_tb where is_deleted = 0";
    }
    
    public function novoObjeto(){
        return new \FonogramaTitularInstrumento();
    }
    
    public function nome_id_local(){
        return 'id_fonograma_titular_instrumento';
    }
    
    public function nome_id_mdb(){
        return 'id_fono_titular_instru';
    }

    public function tabela(){
        return "ISRC_fono_titular_instru_tb";
    }
        
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            // 'id_fonograma_titular',
            'id_instrumento',
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);

        $fonograma_titular = \FonogramaTitular::where('id_empresa', '=', \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa)
                        ->where('id_fonograma_titular_original', '=', $regMDB['id_fonograma_titular'])
                        ->first();
        if (count($fonograma_titular) > 0){
            $obj->id_fonograma_titular = $fonograma_titular->id_fonograma_titular;
        } else {
            \Log::error('FonogramaTitularInstrumento (id='.$regMDB['id_fono_titular_instru'].' empresa='.\ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa.'): fonograma_titular não encontrada id='.$regMDB['id_fonograma_titular']);
        }
        //

        $obj->usuario = $regMDB['usuario'];
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        return $obj;
    }
}