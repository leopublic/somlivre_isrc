<?php

class ObraController extends BaseController {
    public function getIndex() {
        $rep = new \ISRC\Repositorios\RepositorioObra();
        $regs = $rep->registrosConsulta();

        return View::make('obra.index')
                ->with('regs', $regs);
    }

    public function postIndex(){
        Input::flash();
        $rep = new \ISRC\Repositorios\RepositorioObra();
        $regs = $rep->registrosConsulta(Input::get('pesquisar'));
        return View::make('obra.index')
                ->with('regs', $regs);

    }

    public function getEdit($id = 0) {
        $generos = \Genero::comboComDefault();
        if($id > 0){
            $reg = \Obra::find($id);
            $titulo = 'Editar obra';
        } else {
            $reg = new \Obra;
            $reg->is_nacional = 1;
            $titulo = 'Nova obra';
        }
        $obra_titulares = $reg->obratitulares;

        return View::make('obra.edit')
                ->with('reg', $reg)
                ->with('titulo', $titulo)
                ->with('generos', $generos)
                ->with('obra_titulares', $obra_titulares)
            ;
    }

    public function postEdit(){
        $post = Input::all();
        try{
            $rep = new \ISRC\Repositorios\RepositorioObra;
            $obra = $rep->salve($post);
            if ($post['id_obra'] == 0){
                Session::flash('msg', 'Obra cadastrada com sucesso');
            } else {
                Session::flash('msg', 'Obra atualizada com sucesso');
            }
            return Redirect::to('/admin/obra/edit/'.$obra->id_obra);
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', $ex->getMessage());
            return Redirect::to('/admin/obra/edit/'.$post['id_obra']);
        }
    }

    public function getSelecionarparafonograma($id_fonograma, $titulo){
        $sql = "select id_obra, titulo_original from obra where titulo_original like ?";
        $obras = \DB::select($sql, array('%'.$titulo.'%'));
        $msg_limite = '';
        return View::make('fonograma.obra_pesquisa')
                        ->with('id_fonograma', $id_fonograma)
                        ->with('obras', $obras)
                        ->with('msg_limite', $msg_limite)
                ;
    }
}