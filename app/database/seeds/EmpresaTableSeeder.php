<?php

class EmpresaTableSeeder extends Seeder {

    public function run() {
        // Uncomment the below to wipe the table clean before populating
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('empresa')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $empresa = array("nome" => "Som Livre", "codigo" => "SL", "sigla" => "SL", "created_at" => date("Y-m-d H:i:s"));
        DB::table('empresa')->insert($empresa);
        $empresa = array("nome" => "RGE", "codigo" => "RG", "sigla" => "RG", "created_at" => date("Y-m-d H:i:s"));
        DB::table('empresa')->insert($empresa);
    }

}
