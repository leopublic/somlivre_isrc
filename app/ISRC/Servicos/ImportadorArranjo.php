<?php

namespace ISRC\Servicos;

class ImportadorArranjo extends Importador{

    public function sqlSelectMdb() {
        return "select cod_arranjo
                    ,dsc_arranjo
                    ,ind_complemento
                    from ISRC_arranjo_tb ";
    }
    
    public function novoObjeto(){
        return new \Arranjo;
    }
            
    public function tabela(){
        return "ISRC_arranjo_tb";
    }
    
    public function nome_id_local(){
        return 'id_arranjo';
    }
    
    public function nome_id_mdb(){
        return 'cod_arranjo';
    }

    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            'dsc_arranjo',
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);

        $obj->ind_complemento       = $regMDB['ind_complemento'];
        if ($regMDB['ind_complemento'] == 'S'){
            $obj->ind_complemento = true;
        } else {
            $obj->ind_complemento = false;
        }
        return $obj;
    }
}