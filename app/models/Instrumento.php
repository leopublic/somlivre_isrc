<?php
/**
 * @property int $id_instrumento
 * @property int $id_grupo_instrumento
 * @property string $nome Nome 
 * @property date $dt_inclusao
 * @property date $dt_alteracao
 * @property string $usuario Usuário que cadastrou(ECAD)
 * @property datetime created_at 
 * @property datetime updated_at 
 * 
 */
class Instrumento extends Modelo {
    protected $table = 'instrumento';
    protected $primaryKey = 'id_instrumento';
    protected $guarded = array();

    public function grupo(){
    	return $this->belongsTo('GrupoInstrumento', 'id_grupo_instrumento');
    }
     
    public static function combo($filtro = ''){
        return \Instrumento::orderBy('nome')->lists('nome', 'id_instrumento');
    }
   
}