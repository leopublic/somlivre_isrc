<?php

namespace ISRC\Servicos;

class ImportadorTipoFonograma extends Importador{

    public function sqlSelectMdb() {
        return "select id_tipo_fonograma, dsc_tipo_fonograma, Ind_Situacao, ind_trilha from ISRC_tipo_fonograma_tb ";
    }
    
    public function novoObjeto(){
        return new \TipoFonograma();
    }
            
    public function tabela(){
        return "ISRC_tipo_fonograma_tb";
    }

    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
        		"id_tipo_fonograma",
        		"dsc_tipo_fonograma",
        		"Ind_Situacao",
        		"ind_trilha"
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);
        return $obj;
    }
}