@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-gift"></i> Alterar gênero</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
            {{Form::hidden('id_genero', $reg->id_genero)}}
            @include('_campos.texto_id', array('valor' => $reg->id_genero))
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Código</label>
                    <div class="col-md-9">
                        {{Form::text('cod_genero', Input::old('cod_genero', $reg->cod_genero), array("class" => "form-control"))}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Descrição</label>
                    <div class="col-md-9">
                        {{Form::text('nome', Input::old('nome', $reg->nome), array("class" => "form-control"))}}
                    </div>
                </div>
                    </div>
            <div class="form-actions fluid">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">Salvar</button>
                    <a href="{{URL::to('/genero')}}" class="btn default">Cancelar</a>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
</div>
@stop
@section('scripts')
    $('#menuTabelas').addClass('active');
    $('#menuTabelas').addClass('open');
    $('#itemGeneros').addClass('active');
@stop
