@extends('base')

@section('estilos')
@stop

@section('conteudo')
<div class="row">
    <div class="col-md-10">
        <h3 class="page-title">Selecione a empresa</h3>
    </div>
</div>
@include('_padroes.mensagens')

<div class="row">
    <div class="col-md-12">
            <div class="tiles">
            @foreach($empresas_contexto as $empresa)
                <div class="tile bg-blue-hoki">
                <a href="/admin/home/selecioneempresa/{{$empresa->id_empresa}}">
                    <div class="tile-body">
                        <i class="fa fa-bank"></i>
                    </div>
                    <div class="tile-object">
                        <div class="name">
                             {{$empresa->nome}}
                        </div>
                    </div>
                    </a>
                </div>
            @endforeach
            </div>
    </div>
</div>
@stop

@section('scripts')
@stop
