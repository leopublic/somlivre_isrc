<?php
/**
 * Perfis de usuário
 * @property int $id_perfil 
 * @property varchar $descricao 
 * @property tinyint $backoffice 
 * @property text $observacao 
 * @property timestamp $created_at 
 * @property timestamp $updated_at 
 */
class Perfil extends Modelo {

    protected $table = 'perfil';
    protected $primaryKey = 'id_perfil';
    protected $guarded = array();
    protected $connection = 'mysql';

    public function getQtdUsuariosAttribute($valor){
        return \UsuarioPerfil::where('id_perfil', '=', $this->id_perfil)->count();
    }

    public function getBackofficeFmtAttribute($valor){
        if ($this->backoffice){
            return "Sim";
        } else {
            return "Não";
        }
    }
}
