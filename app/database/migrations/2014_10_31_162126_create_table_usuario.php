<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableUsuario extends Migration {

    public function up() {
        Schema::create('usuario', function(Blueprint $table) {
            $table->increments('id_usuario');
            $table->integer('id_usuario_cadastro')->unsigned()->nullable();
            $table->integer('id_empresa')->unsigned()->nullable();
            $table->string('nome', 500);
            $table->string('email', 500)->nullable();
            $table->boolean('acessa_backoffice')->nullable();
            $table->string('password', 64)->nullable();
            $table->integer('qtd_tentativas_erradas')->nullable();
            $table->dateTime('dthr_primeiro_erro')->nullable();            
            $table->dateTime('dt_ultimo_acesso')->nullable();            
            $table->string('remember_token', 100)->nullable();
            $table->text('observacao')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down() {
        if (Schema::hasTable('usuario')){
            Schema::drop('usuario');            
        }
    }

}
