<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableColetivo extends Migration {

    public function up() {
        Schema::create('coletivo', function ($table) {
            $table->increments('id_coletivo');
            $table->integer('id_coletivo_original')->unsigned()->nullable();
            $table->integer('id_empresa')->unsigned()->nullable();
            $table->string('nome', 60)->nullable();
            $table->integer('cod_coletivo')->unsigned()->nullable();
            $table->integer('cod_coletivo_ECAD')->unsigned()->nullable();
            $table->string('pseudonimo', 60)->nullable();
            $table->boolean('is_nacional')->nullable();
            $table->string('cod_cae', 11)->nullable();
            $table->string('cod_old', 13)->nullable();
            $table->integer('id_tipo_coletivo')->unsigned()->nullable();
            $table->string('sigla', 2)->nullable();
            $table->integer('id_sigla_coletivo')->unsigned()->nullable();

            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::table('coletivo', function ($table) {
           $table->index('pseudonimo'); 
           $table->index('nome'); 
           $table->index('cod_coletivo_ECAD'); 
        });
    }

    public function down() {
        if (Schema::hasTable('coletivo')) {
            Schema::drop('coletivo');
        }
    }

}
