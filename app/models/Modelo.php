<?php

class Modelo extends Eloquent {

    public $errors;



    public function assumeDefaults() {

    }

    public static function combo($filtro = ''){
        throw new exception ('Combo não implementado');
    }

    public static function comboComDefault($default = '(não informado)'){
        return array('' => $default) + static::combo();
    }

    public function get_primaryKey(){
        return $this->primaryKey;
    }

    public function get_id(){
        $campo = $this->get_primaryKey();
        return $this->$campo;
    }

    public function recuperaOuNovo($chave, $key){
        if ($key == ''){
            $key = $this->primary_key;
        }
        $obj = self::where($key, '=', $chave);
        if (!$obj->get_id() > 0){
            return $obj;
        } else {
            return $this;
        }

    }

    public function valido($regras, $mensagens) {
        $validador = Validator::make($this->attributes, $regras, $mensagens);
        if ($validador->passes()) {
            return true;
        } else {
            $this->errors = $validador->messages();
            return false;
        }
    }

    public function dtFmt($nome_campo) {
        if (array_key_exists($nome_campo, $this->attributes)){
            $valor = substr($this->attributes[$nome_campo], 0, 10);
            if ($valor != '' && $valor != '00/00/0000' && $valor != '0000-00-00') {
                return Carbon::createFromFormat('Y-m-d', $valor)->format('d/m/y');
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function dtFmtSemSep($nome_campo) {
        if (array_key_exists($nome_campo, $this->attributes)){
            $valor = substr($this->attributes[$nome_campo], 0, 10);
            if ($valor != '' && $valor != '00/00/0000' && $valor != '0000-00-00') {
                return Carbon::createFromFormat('Y-m-d', $valor)->format('dmy');
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public function dtHrFmt($campo) {
        if ($this->$campo != '' && substr($this->$campo, 0, 8) != '00/00/0000' && substr($this->$campo, 0, 8) != '0000-00-00') {
            return \Carbon::createFromFormat('Y-m-d H:i:s', $this->$campo)->format('d/m/Y H:i:s');
        } else {
            return null;
        }
    }

    public function dtModelParaView($data) {
        if ($data != '' && $data != '00/00/0000' && $data != '0000-00-00') {
            return \Carbon::createFromFormat('Y-m-d', $data)->format('d/m/y');
        } else {
            return null;
        }
    }

    public function dtHrModelParaView($data) {
        if ($data != '') {
            return Carbon::createFromFormat('Y-m-d H:i:s', $data)->format('d/m/Y H:i:s');
        } else {
            return $data;
        }
    }

    public function dtViewParaModel($data) {
        if ($data != '' && $data != '00/00/0000' && $data != '0000-00-00') {
            return Carbon::createFromFormat('d/m/y', substr($data, 0, 10))->format('Y-m-d');
        } else {
            return null;
        }
    }

    public function getCaracteresTd($propriedade, $tam_max, $inicio = 0) {
        $cols = 0;
        $ret = '';
        $valor = trim($this->$propriedade);
        for ($index = $inicio; $index < strlen($valor); $index++) {
            $cols++;
            if ($cols <= $tam_max) {
                $ret .= '<td>' . mb_substr($valor, $index, 1) . '</td>';
            }
        }
        if ($cols < $tam_max) {
            $ret .= str_repeat('<td>&nbsp;</td>', $tam_max - $cols);
        }
        return $ret;
    }

    public static function dataOk($valor) {
        $dia = substr($valor, 0, 2);
        $mes = substr($valor, 3, 2);
        $ano = substr($valor, 6, 4);
        return checkdate($mes, $dia, $ano);
    }

    function mask($val, $mask) {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            }
            else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    public function relacaoComDefault($nome_relacao, $nome_atributo){
        if (count($this->$nome_relacao) > 0){
            return $this->$nome_relacao->$nome_atributo;
        } else {
            return '';
        }
    }

    public static function idNovo($entidade, $id_empresa,  $id_original){
        $x = new $entidade;
        $nome_chave_original = $x->primaryKey."_original";
        $obj = $entidade::where($nome_chave_original, '=', $id_original)
                    ->where('id_empresa', '=', $id_empresa)
                    ->first();
        if (count($obj) > 0){
            $chave = $x->primaryKey;
            return $obj->$chave;
        } else {
            return null;
        }
    }

    public function pode_excluir(){
        return true;
    }

}