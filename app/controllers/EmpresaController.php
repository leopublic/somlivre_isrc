<?php

class EmpresaController extends CadastroController {
    public function repo(){
        return new \ISRC\Repositorios\RepositorioEmpresa();
    }
    
    public function diretorioViews() {
        return 'empresa';
    }
    
    public function raizRota() {
        return '/admin/empresa';
    }
    
    public function msgSucesso(){
        return 'Parâmetros atualizados com sucesso';
    }    

}
