<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePerfil extends Migration {

    public function up() {
        Schema::create('perfil', function($table) {
            $table->increments('id_perfil');
            $table->string('descricao', 500)->nullable();
            $table->boolean('backoffice');
            $table->text('observacao')->nullable();
            $table->timestamps();
        });
    }

    public function down() {
        if (Schema::hasTable('perfil')) {
            Schema::drop('perfil');
        }
    }

}
