<?php
/**
 * @property int $id_pseudonimo
 * @property int $id_titular
 * @property string $pseudonimo
 * @property boolean $is_principal
 * @property datetime $dt_inclusao
 * @property datetime $dt_alteracao
 * @property string $usuario
 * @property datetime created_at 
 * @property datetime updated_at 
 * 
 */
class TitularPseudonimo extends Modelo {
    protected $table = 'titular_pseudonimo';
    protected $primaryKey = 'id_pseudonimo';
    protected $guarded = array();

}