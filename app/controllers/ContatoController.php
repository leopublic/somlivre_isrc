<?php
class ContatoController extends BaseController
{
    public function repo() {
        return new ISRC\Repositorios\RepositorioContato;
    }

    public function getIndex($id_pessoa) {
        $rep = $this->repo();
    }

    public function getEditar($id_pessoa, $id_contato) {
        if ($id_contato > 0) {
            $reg = \Contato::find($id_contato);
            $titulo = 'Alterar contato';
        }
        else {
            $reg = new \Contato;
            $reg->id_pessoa = $id_pessoa;
            $titulo = "Adicionar contato";
        }
        $telefones = \Telefone::where('id_pessoa', '=', $id_pessoa)->select(array('id_telefone', DB::raw("concat( if(coalesce(ddi, '') <> '', concat('+',ddi,' '), ''), if(coalesce(ddd, '') <> '', concat('(', ddd, ') '), '') , telefone, if(coalesce(ramal, '') <> '', concat(' - ramal ', ramal), '') ) telefone_completo")))->lists('telefone_completo', 'id_telefone');
        $data = array(
        	'telefones' => $telefones
        	, 'reg' => $reg
        	, 'titulo' => $titulo);
        return View::make('contato/edit', $data);
    }

    public function postEditar() {
        $ret = array();
        try {
            $rep = $this->repo();
            $post = Input::all();
            $contato = $rep->salve($post);

            $contatos = \Contato::where('id_pessoa', '=', $post['id_pessoa'])->get();
            $html = View::make('pessoa.tabela_contatos')->with('contatos', $contatos);
            $ret['html'] = iconv('UTF-8', 'UTF-8', $html);
            $ret['codret'] = 0;
            $ret['msg'] = iconv('UTF-8', 'UTF-8', 'Contato adicionado com sucesso!');
            $ret['id_contato'] = $contato->id_contato;
        }
        catch(Exception $ex) {
            $ret['codret'] = - 1;
            $ret['msg'] = "Não foi possível adicionar esse contato: " . $ex->getMessage() . $ex->getTraceAsString();
        }
        return json_encode($ret);
    }

    public function postExcluir() {
    	$id_pessoa = Input::get('id_pessoa');
    	$id_contato = Input::get('id_contato');
        $ret = array();
        try {
            $rep = $this->repo();
            $contato = $rep->exclua($id_contato);

            $contatos = \Contato::where('id_pessoa', '=', $id_pessoa)->get();
            $html = View::make('pessoa.tabela_contatos')->with('contatos', $contatos);
            $ret['html'] = iconv('UTF-8', 'UTF-8', $html);
            $ret['codret'] = 0;
            $ret['msg'] = iconv('UTF-8', 'UTF-8', 'Contato excluído com sucesso!');
        }
        catch(Exception $ex) {
            $ret['codret'] = - 1;
            $ret['msg'] = "Não foi possível adicionar esse contato: " . $ex->getMessage() . $ex->getTraceAsString();
        }
        return json_encode($ret);
    }
}
