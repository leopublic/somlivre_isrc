<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">{{$titulo}}</h4>
</div>
<div class="modal-body">
    {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "frmTelefone"))}}
    {{Form::hidden('id_pessoa', $reg->id_pessoa, array('id' => 'id_pessoa'))}}
    {{Form::hidden('id_telefone', $reg->id_telefone, array('id' => 'id_telefone'))}}
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @if ($reg->id_telefone > 0)
                    @include('_campos.texto_id', array('valor'=> $reg->id_telefone))
                @endif
                @include('_campos.texto', array('label' => 'DDI', 'nome' => 'ddi'))
                @include('_campos.texto', array('label' => 'DDD', 'nome' => 'ddd'))
                @include('_campos.texto', array('label' => 'Número', 'nome' => 'telefone'))
                @include('_campos.texto', array('label' => 'Ramal', 'nome' => 'ramal'))
                @include('_campos.select', array('label' => 'Tipo', 'nome' => 'id_tipo_telefone', 'valores' => $tipos_telefone))
            </div>
        </div>
    </div>
    {{Form::close()}}
    <div id="modal-msg" style="color:red;"></div>
</div>
<div class="modal-footer">
    <a href="javascript:postEditarTelefone();" class="btn blue">Salvar</a>
    <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
</div>
