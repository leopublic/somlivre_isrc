<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTitular extends Migration {

    public function up() {
        Schema::create('titular', function ($table) {
            $table->increments('id_titular');
            $table->integer('id_titular_original')->unsigned()->nullable();
            $table->integer('id_empresa')->unsigned()->nullable();
            $table->integer('id_sociedade')->unsigned()->nullable();
            $table->integer('id_pessoa')->unsigned()->nullable();
            $table->boolean('is_nacional')->nullable();
            $table->string('cod_cae', 11)->nullable();
            $table->string('cod_omb', 13)->nullable();
            $table->string('cod_old', 13)->nullable();
            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down() {
        if (Schema::hasTable('titular')) {
            Schema::drop('titular');
        }
    }

}
