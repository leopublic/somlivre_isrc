<?php

namespace ISRC\Repositorios;

class RepositorioCategoria extends RepositorioCadastro {

    public function tituloModelo(){
        return 'categoria';
    }
    
    public function nomeModelo(){
        return '\Categoria';
    }
    
    public function ordenacaoPadrao(){
        return 'desc_categoria';
    }
    public function registrosConsulta($filtros = null){
               
    return \Categoria::paginate(20);    
    }
}
