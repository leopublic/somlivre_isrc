<?php
/**
 * @property int $id_tipo_fonograma
 * @property string $dsc_tipo_fonograma Nome
 * @property string $Ind_Situacao
 * @property string $ind_trilha
 * @property datetime created_at
 * @property datetime updated_at
 *
 */
class TipoFonograma extends Modelo {
    protected $table = 'tipo_fonograma';
    protected $primaryKey = 'id_tipo_fonograma';
    protected $guarded = array();

    public function carregaPeloMDB($registro){

    }

    public static function combo($filtro = ''){
        return \TipoFonograma::where('Ind_Situacao', '=', 'A')->orderBy('dsc_tipo_fonograma')->lists('dsc_tipo_fonograma', 'id_tipo_fonograma');
    }

    public static function trilhas(){
        $tipos =  \TipoFonograma::where('ind_trilha', '=', 'S')->get();
        $ret = '';
        foreach($tipos as $tipo){
            $ret .= '#'.$tipo->id_tipo_fonograma.'#';
        }
        return $ret;
    }
}