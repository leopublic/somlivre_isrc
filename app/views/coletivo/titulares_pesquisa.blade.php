<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Adicionar titular</h4>
</div>
<div class="modal-body">
    <table class="table table-stripped table-condensed table-hover">
        <colgroup>
            <col style="width:60px;"/>
            <col style="width:40px;"/>
            <col style="width:auto;"/>
        </colgroup>
        <thead>
            <tr>
                <th class="center">Ações</th>
                <th class="center">Id</th>
                <th class="left">Titular</th>
            </tr>
        </thead>
        <tbody>
            @foreach($titulares as $titular)
            <tr>
                <td class="center">
                    <a onClick="javascript:getAdicionarTitular({{$id_coletivo}},{{$titular->id_titular}});" class="btn btn-xs blue" title="Incluir esse titular no coletivo"><i class="fa fa-plus"></i></a>
                </td>
                <td>{{$titular->id_titular}}</td>
                <td style="text-align:left">{{$titular->nome_razao}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$msg_limite}}
</div>
<div class="modal-footer">
    <button type="button" onClick="btnTitularAdicionarClick()" class="btn yellow-gold">Voltar</button>
    <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
</div>
