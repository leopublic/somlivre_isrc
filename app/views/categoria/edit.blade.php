@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.mensagens')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-tags"></i> {{$titulo}}</div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body form">
                {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                {{Form::hidden('id_categoria', $reg->id_categoria)}}
                <div class="form-body">
                    @include('_campos.texto_id', array('valor' => $reg->id_categoria))
                    <div class="form-group">
                        <label class="col-md-3 control-label">Código</label>
                        <div class="col-md-9">
                            {{Form::text('cod_categoria', Input::old('cod_categoria', $reg->cod_categoria), array("class" => "form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Descrição</label>
                        <div class="col-md-9">
                            {{Form::text('desc_categoria', Input::old('desc_categoria', $reg->desc_categoria), array("class" => "form-control"))}}
                        </div>
                    </div>
                    @include('_campos.select', array('label'=> 'Tipo direito', 'nome'=>'tp_direito', 'valores'=> array("A"=>"Autoral", "C"=>"Conexo")))
                    <div class="form-group">
                        <label class="col-md-3 control-label">Percentual máximo</label>
                        <div class="col-md-9">
                            {{Form::text('pct_max', Input::old('pct_max', $reg->pct_max), array("class" => "form-control"))}}
                        </div>
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green">Salvar</button>
                        <a href="{{URL::to('/categoria')}}" class="btn default">Cancelar</a>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
$('#menuTabelas').addClass('active');
$('#menuTabelas').addClass('open');
$('#itemCategorias').addClass('active');
@stop
