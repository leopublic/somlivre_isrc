<?php

class TipoMidiaController extends CadastroController {
    public function repo(){
        return new \ISRC\Repositorios\RepositorioTipoMidia();
    }
    
    public function diretorioViews() {
        return 'tipomidia';
    }
    
    public function raizRota() {
        return '/admin/tipomidia';
    }
    
    public function msgSucesso(){
        return 'Tipo de mídia atualizado com sucesso';
    }    

}
