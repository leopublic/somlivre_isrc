<?php

class PerfilController extends BaseController {
    public function getIndex() {
        $rep = new \ISRC\Repositorios\RepositorioPerfil;
        $perfis = $rep->registrosConsulta();

        return View::make('perfil.index')
                ->with('registros', $perfis);
    }

    public function getEdit($id = 0) {
        $rep = new \ISRC\Repositorios\RepositorioPerfil();
        $objeto = $rep->instanciaEditado($id);
        $titulo = $rep->tituloTelaEdicao($objeto);
        return View::make('perfil.edit')
                ->with('objeto', $objeto)
                ->with('titulo', $titulo)
            ;
    }
    
    public function postEdit($id_perfil = ''){
        $post = Input::all();
        try{
            $rep = new \ISRC\Repositorios\RepositorioPerfil();
            $rep->salve($post);
            Session::flash('msg', 'Perfil atualizado com sucesso');
            return Redirect::to('/admin/perfil/');
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', $ex->getMessage());
            return Redirect::to('/admin/perfil/edit/'.$post['id_perfil']);
        }
    }

}
