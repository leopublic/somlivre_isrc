<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTipoCpf extends Migration {

    public function up() {
        Schema::create('tipo_cpf', function($table) {
            $table->increments('id_tipo_cpf');
            $table->string('descricao', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        
        $obj = array("descricao" => "Próprio");
        DB::table('tipo_cpf')->insert($obj);
        $obj = array("descricao" => "Filiação");
        DB::table('tipo_cpf')->insert($obj);
        $obj = array("descricao" => "Responsável");
        DB::table('tipo_cpf')->insert($obj);
        $obj = array("descricao" => "Estrangeiro");
        DB::table('tipo_cpf')->insert($obj);
    }

    public function down() {
        Schema::dropIfExists('tipo_cpf');
    }

}
