<?php
namespace ISRC\Repositorios;

class RepositorioTelefone extends RepositorioCadastro {

    public function nomeModelo(){
        return '\Telefone';
    }

    public function ordenacaoPadrao(){
        return 'telefone';
    }
}