<?php

namespace ISRC\Servicos;

class ImportadorTitularPseudonimo extends Importador{

    public function sqlSelectMdb() {
        return "select id_titular, id_pseudonimo, pseudonimo, is_principal, dt_inclusao, dt_alteracao, usuario from ISRC_titular_pseudonimo_tb";
    }
    
    public function novoObjeto(){
        return new \TitularPseudonimo();
    }
            
    public function tabela(){
        return "ISRC_titular_pseudonimo_tb";
    }

    public function nome_id_local(){
        return 'id_pseudonimo';
    }
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            // 'id_titular',
            'pseudonimo', 
            'is_principal', 
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);
        $obj->id_titular = $this->obtem_id_titular($regMDB['id_titular'], $regMDB['id_pseudonimo']);

        $obj->usuario = $regMDB['usuario'];
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);

        $obj->id_pseudonimo_original = $regMDB['id_pseudonimo'];
        $obj->id_empresa = \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa;
        return $obj;
    }
}