        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">{{$titulo}}</h4>
        </div>
        <div class="modal-body">
            {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "form_titular"))}}
            {{Form::hidden('id_fonograma_titular', $fonograma_titular->id_fonograma_titular)}}
            {{Form::hidden('id_fonograma', $fonograma_titular->id_fonograma)}}
            {{Form::hidden('id_titular', $fonograma_titular->id_titular)}}
            <div class="form-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Titular</label>
                            <div class="col-md-9">
                                {{Form::text('nome_razao', $titular->pessoa->nome_razao, array("class" => "form-control", 'disabled'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pseudônimo</label>
                            <div class="col-md-9">
                                {{Form::select('id_pseudonimo', $pseudonimos, Input::old('id_pseudonimo', $fonograma_titular->id_pseudonimo), array("class" => "form-control select2", 'id' => 'id_pseudonimo'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Categoria</label>
                            <div class="col-md-9">
                                {{Form::select('cod_categoria', $categorias, Input::old('cod_categoria', $fonograma_titular->id_categoria), array("class" => "form-control select2", "id"=>"cod_categoria"))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">% partic.</label>
                            <div class="col-md-9">
                                {{Form::text('pct_participacao_fmt', $fonograma_titular->pct_participacao_fmt, array("class" => "form-control"))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Início contrato</label>
                            <div class="col-md-9">
                                {{Form::text('dt_inicontrato_fmt', Input::old('dt_inicontrato_fmt', $fonograma_titular->dt_inicontrato_fmt), array("class" => "form-control input-small mask_data"))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Fim contrato</label>
                            <div class="col-md-9">
                                {{Form::text('dt_fimcontrato_fmt', Input::old('dt_fimcontrato_fmt', $fonograma_titular->dt_fimcontrato_fmt), array("class" => "form-control input-small mask_data"))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Instrumentos</label>
                            <div class="col-md-9">
                                {{Form::hidden('id_instrumentos[]');}}
                                {{Form::select('id_instrumentos[]', $instrumentos, Input::old('id_instrumentos', $id_instrumentos), array("class" => "form-control select2", "multiple", 'id' => 'id_instrumentos[]'))}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{Form::close()}}
            <div id="modal-msg" style="color:red;"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn blue" onclick="javascript:postAdicionarTitular();">Salvar</button>
            <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
        </div>
