<?php
/**
 * @property int $id_categoria
 * @property string $cod_categoria Código
 * @property string $desc_categoria
 * @property string $tp_direito
 * @property decimal $pct_max 
 * @property date $dt_inclusao
 * @property date $dt_alteracao
 * @property string $usuario Usuário que cadastrou(ECAD)
 * @property datetime created_at 
 * @property datetime updated_at 
 */
class Categoria extends Modelo {
    protected $table = 'categoria';
    protected $primaryKey = 'id_categoria';
    protected $guarded = array();

    
    public static function combo($filtro = ''){
        return \Categoria::orderBy('desc_categoria')->lists('desc_categoria', 'id_categoria');
    }

    public static function comboTitular($default){
        return array('' => $default) + \Categoria::where('tp_direito', '=', 'C')->orderBy('desc_categoria')->lists('desc_categoria', 'id_categoria');

    }
}