<?php

/*
  |--------------------------------------------------------------------------
  | Application & Route Filters
  |--------------------------------------------------------------------------
  |
  | Below you will find the "before" and "after" events for the application
  | which may be used to do any work before or after a request into your
  | application. Here you may also register your custom route filters.
  |
 */

App::before(function($request) {
    //
});


App::after(function($request, $response) {
    //
});

Route::filter('guest', function() {
    if (Auth::guest()) {
        return Redirect::to('auth/login');
    }
});

Route::filter('usuario-externo', function() {
    if (Auth::guest()) {
        return Redirect::to('auth/login')->with('login-error', true)->with('msg', 'Usuário não autenticado.');
    }
});

Route::filter('backoffice', function() {
    if (Auth::guest()) {
        return Redirect::to('auth/login')->with('msg', 'Você precisa se identificar antes para acessar o sistema.');
    } else {
        if (!Auth::user()->acessa_backoffice) {
            return Redirect::to('auth/login')->with('msg', 'Seu login não tem acesso à função solicitada.');
        }
    }
});

Route::filter('empresa', function() {
    if (Auth::user()->id_empresa == '') {
        return Redirect::to('/admin/home/empresa');
    }
});


/*
  |--------------------------------------------------------------------------
  | Guest Filter
  |--------------------------------------------------------------------------
  |
  | The "guest" filter is the counterpart of the authentication filters as
  | it simply checks that the current user is not logged in. A redirect
  | response will be issued if they are, which you may freely change.
  |
 */
Route::filter('guest', function() {
    if (Auth::check())
        return Redirect::to('/');
});

/*
  |--------------------------------------------------------------------------
  | CSRF Protection Filter
  |--------------------------------------------------------------------------
  |
  | The CSRF filter is responsible for protecting your application against
  | cross-site request forgery attacks. If this special token in a user
  | session does not match the one given in this request, we'll bail.
  |
 */

Route::filter('csrf', function() {
    if (Session::token() != Input::get('_token')) {
        throw new Illuminate\Session\TokenMismatchException;
    }
});
