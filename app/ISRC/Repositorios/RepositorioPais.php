<?php

namespace ISRC\Repositorios;

class RepositorioPais extends RepositorioCadastro {
    public function tituloModelo(){
        return "pais";
    }

    public function nomeModelo(){
        return '\Pais';
    }
    
    public function ordenacaoPadrao(){
        return 'nome';
    }
        public function registrosConsulta($filtros = null){
               
    	return \Pais::paginate(20);    
    }

}
