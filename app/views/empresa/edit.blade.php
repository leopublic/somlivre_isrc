@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.mensagens')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-tags"></i> {{$titulo}}</div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body form">
                {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                {{Form::hidden('id_empresa', $reg->id_empresa)}}
                <div class="form-body">
                    @include('_campos.texto_id', array('valor' => $reg->id_empresa))
                    @include('_campos.texto', array("label"=> "Nome", "nome"=> "nome"))
                    @include('_campos.texto', array("label"=> "Prefixo IFPI", "nome"=> "sgl_radical_IFPI"))
                    @include('_campos.texto', array("label"=> "Sigla IFPI", "nome"=> "sgl_IFPI"))
                    @include('_campos.texto', array("label"=> "Ano base", "nome"=> "ano_base"))
                    @include('_campos.texto', array("label"=> "Sequencial ISRC", "nome"=> "seq_ISRC"))
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green">Salvar</button>
                        <a href="{{URL::to('/empresa')}}" class="btn default">Cancelar</a>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
    $('#itemEmpresas').addClass('active');
@stop
