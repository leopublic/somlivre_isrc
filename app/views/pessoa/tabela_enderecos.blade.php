<table class="table table-stripped table-condensed table-hover">
    <colgroup>
        <col style="width:100px;"/>
        <col style="width:50px;"/>
        <col style="width:auto;"/>
        <col style="width:auto;"/>
        <col style="width:auto;"/>
        <col style="width:auto;"/>
        <col style="width:auto;"/>
        <col style="width:auto;"/>
    </colgroup>
    <thead>
        <tr>
            <th class="center">Acoes</th>
            <th class="center">Id</th>
            <th class="left">Endereco</th>
            <th class="left">Complemento</th>
            <th class="left">Bairro</th>
            <th class="left">Cidade</th>
            <th class="center">CEP</th>
            <th class="center">UF</th>
        </tr>
    </thead>
    <tbody>
        @foreach($enderecos as $end)
        <tr id="endereco{{$end->id_endereco}}">
            <td class="center">
                <a href="javascript:getEditarEndereco('{{$end->id_pessoa}}', '{{$end->id_endereco}}');" class="btn btn-xs {{$botao_edicao_cor}}" title="Editar esse endereço">{{$botao_edicao_icon}}</a>
                <a href="javascript:postExcluirEndereco('{{$end->id_pessoa}}', '{{$end->id_endereco}}');" class="btn btn-xs {{$botao_exclusao_cor}}" title="Excluir esse endereço">{{$botao_exclusao_icon}}</a>
            </td>
            <td class="center">{{$end->id_endereco}}</td>
            <td class="left">{{$end->endereco}}</td>
            <td class="left">{{$end->complemento}}</td>
            <td class="left">{{$end->bairro}}</td>
            <td class="left">{{$end->cidade}}</td>
            <td class="center">{{$end->cep}}</td>
            <td class="center">@if (count($end->uf)){{$end->uf->sigla}}@endif</td>
        </tr>
        @endforeach
    </tbody>
</table>