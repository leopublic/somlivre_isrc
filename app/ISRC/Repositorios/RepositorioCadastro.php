<?php
/**
 * Classe padrao para repositórios de cadastros
 */
namespace ISRC\Repositorios;

class RepositorioCadastro extends Repositorio {

    public function tituloModelo(){
        return '-- informe o titulo do modelo no RepositorioModelo --';
    }

    public function nomeModelo(){

    }
    public function ordenacaoPadrao(){

    }

    public function instanciaEditado($id){
        $nomeModelo = $this->nomeModelo();
        if (intval($id) > 0) {
            $instancia = $nomeModelo::find($id);
        } else {
            $instancia = new $nomeModelo;
        }
        return $instancia;
    }

    public function tituloTelaEdicao($modelo){
        $chave = $modelo->get_primaryKey();
        if ($modelo->$chave > 0){
            return 'Alterar '.$this->tituloModelo();
        } else {
            return 'Adicionar '.$this->tituloModelo();
        }
    }

    /**
     * Recupera todos os registros do cadastro para alimentar uma consulta
     * @param type $filtros
     */
    public function registrosConsulta($filtros = null){
        $nomeModelo = $this->nomeModelo();
        return $nomeModelo::orderBy($this->ordenacaoPadrao())
                ->get();

    }

    public function extraiId($post){
        $nomeModelo = $this->nomeModelo();
        $modelo = new $nomeModelo;
        $chave = $modelo->get_primaryKey();

        return $post[$chave];
    }

    public function salve($post) {
        $nomeModelo = $this->nomeModelo();
        $modelo = new $nomeModelo;
        $chave = $modelo->get_primaryKey();

        $id = $post[$chave];
        if (intval($id) > 0) {
            $instancia = $nomeModelo::find($id);
        } else {
            $instancia = new $nomeModelo;
        }
        $nomeValidador = "\ISRC\Validadores".$nomeModelo;
        $this->validador = new $nomeValidador($post);
        $this->validador->id_unique = $id;
        if ($this->validador->passes()) {
            if ($this->validacao_adicional($post)){
                $instancia = $this->preencheCampos($instancia, $post);
                $instancia->save();
                $this->aposSalvar($instancia, $post);
                return $instancia;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function validacao_adicional($post){
        return true;
    }

    public function preencheCampos($instancia, $post){
        $instancia->fill($post);
        return $instancia;
    }

    public function aposSalvar($instancia, $post){

    }

    public function exclua($id){
        $nomeModelo = $this->nomeModelo();
        $modelo = new $nomeModelo;
        $chave = $modelo->get_primaryKey();
        $nomeModelo::where($chave, '=', $id)->delete();
    }



}