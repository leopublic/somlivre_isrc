<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInstrumento extends Migration {
	public function up() {
		Schema::create ( 'instrumento', function ($table) {
			$table->increments ( 'id_instrumento' );
			$table->integer ( 'id_grupo_instrumento' )->unsigned()->nullable();
			$table->string ( 'nome', 100 )->nullable ();
			$table->dateTime ( 'dt_inclusao' )->nullable ();
			$table->dateTime ( 'dt_alteracao' )->nullable ();
			$table->string ( 'usuario', 20 )->nullable ();
			$table->timestamps ();
			$table->softDeletes ();
		} );
	}
	public function down() {
		if (Schema::hasTable ( 'instrumento' )) {
			Schema::drop ( 'instrumento' );
		}
	}
}
