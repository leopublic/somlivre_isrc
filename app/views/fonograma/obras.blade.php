@extends('base')

@section('estilos')
@stop

@section('conteudo')
<div class="row">
    <div class="col-md-12">
        <h3 class="page-title">Editar fonograma - Obra</h3>
    </div>
</div>
@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <div class="tabbable-custom nav-justified">
            <ul class="nav nav-tabs nav-justified">
                <li><a href="/admin/fonograma/edit/{{$objeto->id_fonograma}}" > Fonograma</a></li>
                <li class="active"><a href="#tab_1_1_1" data-toggle="tab"> Obra  </a></li>
                <li><a href="/admin/fonogramapoutporrit/show/{{$objeto->id_fonograma}}" > Poutporrit </a></li>
                <li><a href="/admin/fonogramatitulares/show/{{$objeto->id_fonograma}}" > Titulares conexos </a></li>
                <li><a href="/admin/fonogramacoletivos/show/{{$objeto->id_fonograma}}" > Coletivo </a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1_1">

                    <div class="portlet box yellow-gold">
                        <div class="portlet-title">
                            <div class="caption"><i class="fa fa-gift"></i> Obra atual</div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{Form::text('nome_obra', $objeto->nome_obra, array("class" => "form-control", "disabled"))}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="portlet box yellow-gold">
                        <div class="portlet-title">
                            <div class="caption"><i class="fa fa-gift"></i> Alterar obra</div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body">
                            {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                            <div class=" form form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            {{Form::text('pesquisar', Input::old('pesquisar'), array("class" => "form-control"))}}
                                            <span class="input-group-btn"><button class="btn" ><i class="fa fa-search"></i> Filtrar</button></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if ($obras)
                            {{$obras->appends(array('pesquisar' => $pesquisar))->links()}}
                            @endif
                            {{Form::close()}}
                            @include('_padroes.table')
                            <colgroup>
                                <col style="width:60px;"/>
                                <col style="width:100px;"/>
                                <col style="width:auto;"/>                                
                            </colgroup>
                            <thead>
                                <tr>
                                    <th class="center">Ações</th>
                                    <th class="center">Id</th>
                                    <th class="left">Nome</th>
                                    <th class="left">Artistas</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($obras as $obra)
                                <tr>
                                    <td class="center">
                                        <a href="{{URL::to('/admin/fonogramaobras/set/'.$objeto->id_fonograma.'/'.$obra->id_obra)}}" class="btn btn-sm purple-studio" title="Editar"><i class="fa fa-thumb-tack"></i></a>
                                    </td>
                                    <td class="center">{{$obra->id_obra}}</td>
                                    <td>{{$obra->titulo_original}}</td>
                                    <td>{{$obra->nomes_titulares}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ajax" role="basic" aria-hidden="true">
    <div class="page-loading page-loading-boxed">
        <img src="/assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
        <span>
            &nbsp;&nbsp;Loading... </span>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>
@stop
@section('scripts')
ComponentsPickers.init();
$('#menuAcesso').addClass('active');
$('#menuAcesso').addClass('open');
$('#itemPerfis').addClass('active');
@stop
