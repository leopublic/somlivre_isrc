@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.titulo_cadastro', array('titulo'=> 'Titulares', 'rota'=>'pessoa'))
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>Registros encontrados
            </div>
        </div>
        <div class="portlet-body">
            {{Form::open()}}
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group">
                        <input type="search" id="filtro" name="filtro" class="form-control" value="{{Input::old('filtro')}}">
                        <span class="input-group-btn"><button type="submit" class="btn">Buscar</button></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">{{$regs->appends(Input::get())->links()}}</div>
            </div>
            {{Form::close()}}
       		
            <div class="table-scrollable">
            <table class="table table-striped dataTable table-condensed table-hover no-footer">
                <thead>
                    <tr>
                        <th class="center">Ações</th>
                        <th class="center">Id</th>
                        <th class="left">Nome</th>
                        <th class="left">Pseudônimo</th>
                        <th class="center">CPF/CNPJ</th>
                        <th class="center">Data cadastro</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($regs as $reg)
                    <tr>
                        <td class="center">
                            <a href="{{URL::to('/admin/pessoa/edit/'.$reg->id_pessoa)}}" class="btn btn-sm green-seagreen" title="Editar"><i class="fa fa-edit"></i></a>
                                @if($reg->pode_excluir())
                                    <div class="btn-group" style="display: ">
                                        <button class="btn btn-danger btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-trash-o"></i></button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="/admin/titular/excluir/{{$reg->id_titular}}" class="alert-danger">Clique para confirmar a exclusão desse titular</a></li>
                                        </ul>
                                    </div>
                                @endif
                        </td>
                        <td class="center">{{$reg->id_pessoa}}</td>
                        <td>{{$reg->pessoa->nome_razao}}</td>
                        <td>{{$reg->nomepseudonimoprincipal()}}</td>
                        <td class="center">{{$reg->pessoa->cpf_cnpj_formatado}}</td>
                        <td class="center">{{$reg->dtHrFmt('dt_inclusao')}}</td>
                    </tr>
                @endforeach
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td class="center">&nbsp;</td>
                        <td class="center">&nbsp;</td>
                    </tr>
                </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END BORDERED TABLE PORTLET-->
</div>
@stop
@section('scripts')
$('#itemTitulares').addClass('active');
@stop
