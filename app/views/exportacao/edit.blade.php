@extends('base')

@section('estilos')
table tr.red td{
background-color:red;
color:#fff;
font-weight:bold;
}
@stop

@section('conteudo')
<div class="row">
    <div class="col-md-12">
        <h3 class="page-title">Exportar</h3>
    </div>
</div>

@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        @if (intval($exp->id_exportacao) == 0)
        <div class="portlet box {{$portlet_obra}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-music"></i>Gerar exportação para ECAD</div>
            </div>
            <div class="portlet-body form">
                {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                {{Form::hidden('id_exportacao', $exp->id_exportacao, array('id' => 'id_exportacao'))}}
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Empres</label>
                        <div class="col-md-9">
                            {{Form::select('id_empresa', $empresas, Input::old('id_empresa', $exp->id_empres), array("class" => "bs-select form-control"))}}
                        </div>
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green">Salvar</button>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
        @else
        <div class="portlet box {{$portlet_obra}}">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-music"></i>Exportação em andamento</div>
            </div>
            <div class="portlet-body form">
                {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                {{Form::hidden('id_exportacao', $exp->id_exportacao, array('id' => 'id_exportacao'))}}
                <div class="form-body">
                Aguarde...<i class="fa fa-spin fa-spinner"></i>
                </div>
                <div class="form-actions fluid">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green">Salvar</button>
                        <a href="{{URL::to('/admin/obra')}}" class="btn default">Cancelar</a>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>

        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_obra}}">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>Exportações anteriores
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    @include('_padroes.table')
                    <thead>
                        <tr>
                            <th class="center" style="width:100px;">Ações</th>
                            <th class="center" >Id</th>
                            <th class="center" >Empresa</th>
                            <th class="left" >Usuário</th>
                            <th class="center" >Data</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($exps as $exportacao)
                        <tr>
                            <td class="center">&nbsp;</td>
                            <td class="center">{{$exportacao->id_exportacao}}</td>
                            <td class="center">{{$exportacao->empresa->sgl_IFPI}}</td>
                            <td class="center">{{$exportacao->usuario->nome}}</td>
                            <td class="center">{{$exportacao->created_at_fmt}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END BORDERED TABLE PORTLET-->
    </div>

@stop
@section('scripts_full')
<script>
@if ($exp->id_exportacao > 0)
    $.get('/exportacao/processar', '');
@endif
</script>
@stop
