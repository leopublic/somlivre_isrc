<?php
/**
 * @property int $id_tipo_produto
 * @property string $nome Nome
 * @property date $dt_inclusao
 * @property date $dt_alteracao
 */
class TipoProduto extends Modelo {
    protected $table = 'tipo_produto';
    protected $primaryKey = 'id_tipo_produto';
    protected $guarded = array();

    public static function combo($filtro = ''){
        return \TipoProduto::orderBy('nome')->lists('nome', 'id_tipo_produto');
    }
}