<?php

class CadastroController extends BaseController {
   // Retorna o repositório. Deve ser implementada em cada controller filho
    public function repo(){
    }

    public function diretorioViews(){
        
    }

    public function raizRota(){
        
    }
    
    public function getIndex() {
        Input::flash();
        $post = Input::all();
        return $this->lista($post);
    }
    
    public function postIndex() {
        Input::flash();
        $post = Input::all();
        return $this->lista($post);
    }

    public function lista($post){
        $rep = $this->repo();
        $regs = $rep->registrosConsulta($post);
        return View::make($this->diretorioViews().'.index')
                ->with('regs', $regs);
        
    }
    
    public function getEdit($id = 0) {
        return $this->show($id);
    }
    
    public function show($id){
        $rep = $this->repo();
        $objeto = $rep->instanciaEditado($id);
        $titulo = $rep->tituloTelaEdicao($objeto);
        $dados = array(
            'reg' => $objeto
            ,'titulo' => $titulo
        );
        $dados = $this->complementaDadosShow($dados);
        return View::make($this->diretorioViews().'.edit', $dados);
    }
    
    public function complementaDadosShow($dados){
        return $dados;
    }
    
    public function msgSucesso(){
        return '';
    }
    
    public function postEdit($id = ''){
        Input::flash();
        $post = Input::all();
        $rep = $this->repo();
        try{
            $obj = $rep->salve($post);
            if ($obj){
                Session::flash('msg', $this->msgSucesso());
                return Redirect::to($this->raizRota());
                return $this->lista(array());
            } else {
                $erro = $rep->getMsgErro();
                Session::flash('error',$erro );                
                return $this->show($rep->extraiId($post));
            }
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', $ex->getMessage());
            return $this->show($id);
        }
    }

    public function getExcluir($id){
        $rep = $this->repo();
        $rep->excluir($id);
        return Redirect::to($this->raizRota());
    }
    
}
