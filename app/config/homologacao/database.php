<?php

return array(

	'fetch' => PDO::FETCH_CLASS,
	'default' => 'mysql',
	'connections' => array(

		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'somlivre_isrc',
			'username'  => 'root',
			'password'  => 'gg0rezqn',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),

		'mysql_br' => array(
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'somlivre_isrc_br',
			'username'  => 'root',
			'password'  => 'gg0rezqn',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),

		'mysql_sl' => array(
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'somlivre_isrc_sl',
			'username'  => 'root',
			'password'  => 'gg0rezqn',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),

		'mysql_ab' => array(
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'somlivre_isrc_ab',
			'username'  => 'root',
			'password'  => 'gg0rezqn',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),
	),

	'migrations' => 'migrations',

	'redis' => array(

		'cluster' => false,

		'default' => array(
			'host'     => '127.0.0.1',
			'port'     => 6379,
			'database' => 0,
		),
	),

);
