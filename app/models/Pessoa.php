<?php
/**
 * 
 * @property int $id_pessoa
 * @property string $tp_pessoa
 * @property int $cod_pessoa
 * @property string $cod_ecad
 * @property string $nome_razao Nome 
 * @property string $fantasia
 * @property string $tp_cpf
 * @property string $cpf_cnpj
 * @property string $tp_pessoa
 * @property date $dt_inclusao
 * @property date $dt_alteracao
 * @property string $email
 * @property date $dt_nascimento
 * @property string $usuario Usuário que cadastrou(ECAD)
 * @property string $estado_civil
 * @property string $sexo
 * @property string $insc_estadual
 * @property string $insc_municipal
 * @property string $identidade
 * @property string $orgao_emissor
 * @property string $profissao
 * @property string $nacionalidade
 * @property string $is_editora
 * @property string $is_produtor_fono
 * @property string $is_autor
 * @property string $is_interprete
 * @property string $is_musico
 * @property string $observacao
 * @property datetime created_at 
 * @property datetime updated_at 
 */
class Pessoa extends Modelo {
    protected $table = 'pessoa';
    protected $primaryKey = 'id_pessoa';
    protected $guarded = array();
    protected $fillable = array("id_pessoa", "tp_pessoa", "cod_pessoa", "cod_ecad", "nome_razao", "fantasia", "tp_cpf"
        , "cpf_cnpj", "tp_pessoa", "email", "dt_nascimento_fmt", "id_estado_civil", "sexo", "insc_estadual", "insc_municipal", "identidade"
        , "orgao_emissor", "profissao", "nacionalidade", "is_editora", "is_produtor_fono", "is_autor", "is_interprete", "is_musico"
        , "observacao");

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            $model->id_empresa = \Auth::user()->id_empresa;
        });
    }
   /**
     * Atributos calculados
     */
    public function getCpfCnpjFormatadoAttribute($valor) {
        $valor = $this->attributes['cpf_cnpj'];
        if ($this->tp_pessoa == 'F') {
            return $this->mask($valor, '###.###.###-##');
        } else {
            return $this->mask($valor, '##.###.###/####-##');            
        }
    }
    
    public function getIsPfAttribute($valor){
        if (key_exists('tp_pessoa', $this->attributes) && $this->attributes['tp_pessoa'] == 'J'){
            return false;
        } else {
            return true;
        }
    }
    
    public function setIsPfAttribute($valor){
        if ($valor){
            $this->tp_pessoa = 'F';
        } else {
            $this->tp_pessoa = 'J';
        }
    }
    
    public function getDtNascimentoFmtAttribute($valor){
        return $this->dtFmt('dt_nascimento');
    }
    
    public function setDtNascimentoFmtAttribute($valor){
        return $this->attributes['dt_nascimento'] = $this->dtViewParaModel($valor);
    }

    public function estadocivil(){
        return $this->belongsTo('EstadoCivil', 'id_estado_civil', 'id_estado_civil');
    }
    

}