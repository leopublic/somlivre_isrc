<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEstadoCivil extends Migration {

    public function up() {
        Schema::create('estado_civil', function($table) {
            $table->increments('id_estado_civil');
            $table->string('descricao', 100)->nullable();
            $table->string('codigo', 2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        $obj = array("codigo" => "C", "descricao" => "Casado");
        DB::table('estado_civil')->insert($obj);
        $obj = array("codigo" => "DE", "descricao" => "Desquitado");
        DB::table('estado_civil')->insert($obj);
        $obj = array("codigo" => "DI", "descricao" => "Divorciado");
        DB::table('estado_civil')->insert($obj);
        $obj = array("codigo" => "V", "descricao" => "Viúvo");
        DB::table('estado_civil')->insert($obj);
        $obj = array("codigo" => "S", "descricao" => "Solteiro");
        DB::table('estado_civil')->insert($obj);
        $obj = array("codigo" => "O", "descricao" => "Separado");
        DB::table('estado_civil')->insert($obj);
        if (!Schema::hasColumn('pessoa', 'id_estado_civil')) {
            Schema::table('pessoa', function($table) {
                $table->integer('id_estado_civil')->nullable();
            });
        }
        $sql = "update pessoa, estado_civil set pessoa.id_estado_civil = estado_civil.id_estado_civil where estado_civil.codigo = pessoa.estado_civil";
        \DB::update(\DB::raw($sql));
    }

    public function down() {
        Schema::dropIfExists('estado_civil');
    }

}
