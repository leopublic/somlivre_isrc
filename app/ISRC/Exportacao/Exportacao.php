<?php
namespace ISRC\Exportacao;
class Exportacao
{
    public $campos;
    public $empresa;

    public $reg;
    public static $file;
    public $totalLinhas;
    public $totalGrupos;
    public $newLine;

    public $saidaDebug;

    public $debug;

    const tpoABRIR = '0';
    const tpoINCLUIR = '1';
    const tpoALTERAR = '2';
    const tpoEXCLUIR = '3';
    const tpoFECHAR = '9';

    const tpcTITULAR = 'TIT';
    const tpcOBRAS = 'OBM';
    const tpcPPOURRIT = 'POP';
    const tpcFONOGRAMA = 'FON';

    public function __construct($empresa) {
        $this->empresa = $empresa;
        $this->debug = false;
    }

    public function abreArquivo() {
        self::$file = fopen($this->nome_arquivo(), 'w');
        $this->newLine = false;
    }
    public function fechaArquivo(){
        fclose(self::$file);
    }

    public function tipo_cadastro() {
        throw new Exception('Tipo de cadastro não definido');
    }

    public function header() {
        $this->zera_registro();
        $this->adicionaCampo($this->empresa->sgl_IFPI, 'A', 3);
        $this->adicionaCampo($this->nome_arquivo(), 'A', 8);
        $this->adicionaCampo(date('dmY'), 'A', 8);
        $this->adicionaCampo($this->empresa->versao_sistema, 'A', 10);
        $this->grava_registro();
    }

    public function trailler() {
        $this->zera_registro();
        $this->adicionaCampo($this->empresa->sgl_IFPI, 'A', 3);
        $this->adicionaCampo($this->nome_arquivo(), 'A', 8);
        $this->adicionaCampo($this->totalLinhas, 'N', 8);
        $this->adicionaCampo($this->totalGrupos, 'N', 8);
        $this->grava_registro();
    }

    public function novo_reg($tipo_operacao, $tipo_registro) {
        $this->zera_registro();
        $this->adicionaCampo($this->empresa->sgl_IFPI, 'A', 3);
        $this->adicionaCampo($tipo_operacao, 'N', 1);
        $this->adicionaCampo($this->tipo_cadastro(), 'A', 3);
        $this->adicionaCampo($tipo_registro, 'N', 1);
    }

    public function abreTransacao() {
        $this->novo_reg('0', '0');
        $this->adicionaCampo('00000', 'A', 5);
        $this->grava_registro();
    }

    public function fechaTransacao() {
        $this->novo_reg('0', '9');
        $this->grava_registro();
    }

    public function grava_registro() {
    	if (!$this->debug){
	    	$nl = '';
	    	if ($this->newLine){
	    		$nl = "\n";
	    	}
	    	$this->newLine = true;
	        fwrite(self::$file, $nl.$this->reg);
    	}
    }

    public function nome_arquivo() {
        return $this->empresa->sgl_IFPI . substr('00000' . $this->empresa->seq_exportacao, -5) . '.imp';
    }

    public function zera_registro() {
        $this->reg = '';
    }

    public function adicionaCampo($valor, $tipo, $tamanho, $comentario = '') {
        $cmp = '';
        switch ($tipo) {
            case 'A':
                $cmp = substr($valor . str_repeat(' ', $tamanho), 0, $tamanho);
                break;
            case 'D':
            	if (trim($valor) == ''){
            		$cmp = '00000000';
            	} else {
                	$cmp = substr($valor . str_repeat(' ', $tamanho), 0, $tamanho);
            	}
                break;
            case 'H':
            	if (trim($valor) == ''){
            		$cmp = '000000';
            	} else {
                	$cmp = substr($valor . str_repeat(' ', $tamanho), 0, $tamanho);
            	}
                break;

            case 'N':
                $cmp = substr(str_repeat('0', $tamanho) . $valor, ($tamanho * -1));
                break;

            case 'B':
                if ($valor) {
                    $cmp = 'S';
                }
                else {
                    $cmp = 'N';
                }
                break;
        }
    	if ($this->debug){
    		$campo = array(strlen($this->reg)+1, $tamanho, $comentario, $valor, $cmp);
    		$this->saidaDebug[] = $campo;
    	}
        $this->reg.= $cmp;
    }

    public static function nomeEntidade(){
        return '';
    }

    public static function pendentes($id_empresa){
        $nome = self::nomeEntidade();
        return $nome::where('id_empresa', '=', $id_empresa)
            ->where('exportar', '=', 1)
            ->get();

    }
    public function exportaPendentes($id_empresa){
        $entidades = self::pendentes($id_empresa);
        if (count($entidades) > 0){
            foreach($entidades as $entidade){
                $this->exporta($entidade);
            }
        }
    }
}