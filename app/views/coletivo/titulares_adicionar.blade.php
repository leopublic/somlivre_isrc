        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">{{$titulo}}</h4>
        </div>
        <div class="modal-body">
            {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "form_titular"))}}
            {{Form::hidden('id_titular_coletivo', $titular_coletivo->id_titular_coletivo)}}
            {{Form::hidden('id_coletivo', $titular_coletivo->id_coletivo)}}
            {{Form::hidden('id_titular', $titular_coletivo->id_titular)}}
            <div class="form-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Titular</label>
                            <div class="col-md-9">
                                {{Form::text('nome_razao', $titular->pessoa->nome_razao, array("class" => "form-control", 'disabled'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Data entrada</label>
                            <div class="col-md-9">
                                {{Form::text('dt_entrada_fmt', Input::old('dt_entrada_fmt', $titular_coletivo->dt_entrada_fmt), array("class" => "form-control form-control-inline input-small date-pickerx"))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Data saída</label>
                            <div class="col-md-9">
                                {{Form::text('dt_saida_fmt', Input::old('dt_saida_fmt', $titular_coletivo->dt_saida_fmt), array("class" => "form-control form-control-inline input-small date-pickerx"))}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{Form::close()}}
            <div id="modal-msg" style="color:red;"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn blue" onclick="javascript:postAdicionarTitular();">Salvar</button>
            <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
        </div>
