<?php
/**
 * @property int $id_fono_titular_instrumento 
 * @property int $id_fonograma_titular 
 * @property int $id_instrumento 
 * @property datetime $dt_inclusao 
 * @property datetime $dt_alteracao 
 * @property varchar $usuario 
 * @property timestamp $created_at 
 * @property timestamp $updated_at 
 */
class FonogramaTitularInstrumento extends Modelo {
    protected $table = 'fonograma_titular_instrumento';
    protected $primaryKey = 'id_fonograma_titular_instrumento';
    protected $guarded = array();

    /**
     * 
     * @return \FonogramaTitular
     */
    public function fonogramatitular(){
        return $this->belongsTo('FonogramaTitular', 'id_fonograma_titular', 'id_fonograma_titular');
    }
    
    /**
     * 
     * @return \Instrumento
     */
    public function instrumento(){
        return $this->belongsTo('Instrumento', 'id_instrumento', 'id_instrumento');
    }
}