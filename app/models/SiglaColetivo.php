<?php
/**
 * @property int $id_sigla_coletivo
 * @property string $descricao
 * @property datetime created_at 
 * @property datetime updated_at 
 * 
 */
class SiglaColetivo extends Modelo {
    protected $table = 'sigla_coletivo';
    protected $primaryKey = 'id_sigla_coletivo';
    protected $guarded = array();

   
    public static function combo($filtro = ''){
        return \SiglaColetivo::orderBy('descricao')->lists('descricao', 'id_sigla_coletivo');
    }
     /**
     * Relacionamentos
     */
    
}