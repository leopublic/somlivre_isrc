<?php

namespace ISRC\Servicos;

class ImportadorPais extends Importador{

    public function sqlSelectMdb() {
        return "select id_pais, nome, sigla, dt_inclusao, dt_alteracao, usuario, is_convroma from ISRC_pais_tb where is_deleted = 0";
    }
    
    public function novoObjeto(){
        return new \Pais;
    }
    
    public function nome_id_local(){
        return 'id_pais';
    }
            
    public function tabela(){
        return "ISRC_pais_tb";
    }
        
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            'is_convroma',
            'nome',
            'sigla',
            'usuario'
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);

        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        return $obj;
    }
}