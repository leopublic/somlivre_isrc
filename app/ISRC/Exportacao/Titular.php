<?php
namespace ISRC\Exportacao;
class Titular extends Exportacao{
	const tprDETALHE = '1';
	const tprDOCUMENTACAO = '2';
	const tprPSEUDONIMO = '4';
	const tprSUBSTITUICAO = '9';

	public static function nomeEntidade(){
		return '\Titular';
	}
	public function tipo_cadastro(){
		return 'TIT';
	}

	public function exporta($titular){
		$this->abreTransacao();
		$this->detalhe($titular);
		$this->documentacao($titular);
		$pseudonimos = $titular->pseudonimo;
		foreach($pseudonimos as $pseudonimo){
			$this->pseudonimo($titular, $pseudonimo);
		}
		$this->fechaTransacao();
	}

	public function detalhe($titular){
		$this->novo_reg(self::tpoINCLUIR, self::tprDETALHE);
		$this->adicionaCampo($titular->pessoa->cod_ecad, 'N', 13, 'Código ECAD do titular');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC do titular');
		$this->adicionaCampo($titular->pessoa->tp_pessoa, 'A', 1, 'Tipo de pessoa');
		$this->adicionaCampo($titular->pessoa->nome_razao, 'A', 45, 'Nome do titular');
		$this->adicionaCampo($titular->is_nacional, 'B', 1, 'É nacional');
		$this->adicionaCampo($titular->pessoa->dtFmtSemSep('dt_nascimento'), 'D', 8, 'Data de nascimento');
		$this->adicionaCampo('', 'D', 8, 'Data de falecimento');// Data falecimento
		if ($titular->pessoa->tp_pessoa == 'J'){
			$this->adicionaCampo($titular->pessoa->fantasia, 'A', 45, 'Nome fantasia');
		} else {
			$this->adicionaCampo('', 'A', 45, 'Nome fantasia');
		}
		$this->adicionaCampo($titular->pessoa->profissao, 'A', 45, 'Profissão');
		$this->adicionaCampo('', 'A', 45,'Nome do pai'); // Nome do pai
		$this->adicionaCampo('', 'A', 45, 'Nome da mãe'); // Nome da mae
		$this->adicionaCampo($titular->pessoa->relacaoComDefault('estadocivil', 'codigo'), 'A', 2, 'Estado civil');
		$this->adicionaCampo($titular->pessoa->sexo, 'A', 1, 'Sexo');
		$this->adicionaCampo(0, 'N', 2, 'Qtd filhos'); // Qtd filhos
		$this->adicionaCampo('', 'A', 45, 'Nome do conjuge'); // Nome conjuge
		$this->adicionaCampo('', 'A', 2, 'Naturalidade'); // Naturalidade
		$this->adicionaCampo($titular->pessoa->nacionalidade, 'A', 2, 'Nacionalidade'); // Nacionalidade
		$this->adicionaCampo(false, 'B', 1, 'Tem sucessores'); // Tem sucessores
		$this->adicionaCampo('', 'A', 2, 'Código Quality Code'); // Código quality code
		$this->adicionaCampo($titular->cod_cae, 'N', 11, 'Código CAE'); // Código CAE
		$this->adicionaCampo('', 'A', 3, 'Sigla IFPI'); // Sigla IFPI
		$this->grava_registro();
	}

	public function documentacao($titular){
		$this->novo_reg(self::tpoINCLUIR, self::tprDOCUMENTACAO);
		$this->adicionaCampo($titular->pessoa->cod_ecad, 'N', 13, 'Código ECAD do titular');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC do titular');			// Cod ISRC
		if ($titular->pessoa->tp_pessoa == 'J'){
			$this->adicionaCampo(0, 'N', 11, 'CPF');
			$this->adicionaCampo($titular->pessoa->cpf_cnpj, 'N', 14, 'CNPJ');
		} else {
			$this->adicionaCampo($titular->pessoa->cpf_cnpj, 'N', 11, 'CPF');
			$this->adicionaCampo(0, 'N', 14, 'CNPJ');
		}
		$this->adicionaCampo($titular->pessoa->orgao_emissor, 'A', 7, 'Órgão emissor');
		$this->adicionaCampo(0, 'N', 8, 'Data de emissão do RG'); // Data de emissão do RG
		$this->adicionaCampo($titular->pessoa->identidade, 'N', 12, 'Registro geral (RG)');
		$this->adicionaCampo(0, 'N', 2, 'UF do RG'); // UF do RG
		// Endereço
		$this->adicionaCampo('', 'A', 3,'Tipo de endereço');				// Tipo de endereço
		$this->adicionaCampo('', 'N', 3,'Código do tipo de logradouro');				// Código do tipo de logradouro
		$this->adicionaCampo('', 'A', 65, 'Logradouro');				// Logradouro
		$this->adicionaCampo('', 'N', 5,'Número');				// Número
		$this->adicionaCampo('', 'A', 15,'Complemento');				// Complemento
		$this->adicionaCampo('', 'N', 8, 'CEP');				// CEP
		$this->adicionaCampo('', 'N', 5, 'Código município');				// Código municipio
		$this->adicionaCampo('', 'N', 5, 'Código bairro');				// Código Bairro
		$this->adicionaCampo('', 'A', 2,'Sigla UF');				// Sigla UF
		$this->adicionaCampo('', 'A', 2, 'Sigla país');				// Sigla do país
		// Telefones
		$this->adicionaCampo('', 'A', 16,'Número telefone 1');				// Núm telefone 1
		$this->adicionaCampo('', 'A', 16, 'Número telefone 2');				// Núm telefone 2
		$this->adicionaCampo('', 'A', 16,'Número fax');				// Núm fax
		// Contato
		$this->adicionaCampo('', 'A', 65, 'Referência contato');				// Referencia contato
		$this->adicionaCampo($titular->pessoa->email, 'A', 45,'Email');				// email
		$this->adicionaCampo('', 'A', 45, 'Url');				// Url
		$this->adicionaCampo($titular->cod_omb, 'A', 13, 'Cód OMB');				// Cód OMB
		$this->adicionaCampo($titular->pessoa->tp_cpf, 'A', 1, 'Tipo CPF/CNPJ');				// Tipo CPF/CNPJ

		$this->grava_registro();
	}

	public function pseudonimo($titular, $pseudonimo){
		$this->novo_reg(self::tpoINCLUIR, self::tprPSEUDONIMO);
		$this->adicionaCampo($titular->pessoa->cod_ecad, 'N', 13, 'Código ECAD do titular');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC do titular');				//Cod ISRC
		$this->adicionaCampo('', 'A', 2, 'Código categoria');				// Código categoria
		$this->adicionaCampo('', 'A', 2, 'Código quality code');				// Código quality code
		$this->adicionaCampo($titular->cod_cae, 'N', 11, 'Código CAE');				// Código CAE
		$this->adicionaCampo($pseudonimo->pseudonimo, 'A', 45, 'Pseudônimo');				// Pseudonimo
		$this->adicionaCampo($pseudonimo->is_principal, 'B', 1, 'É principal');				// É principal

		$this->grava_registro();
	}
}