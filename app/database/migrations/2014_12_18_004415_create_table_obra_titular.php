<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableObraTitular extends Migration {

    public function up() {
        Schema::create('obra_titular', function ($table) {
            $table->increments('id_obra_titular');
            $table->integer('id_categoria')->unsigned()->nullable();
            $table->integer('id_obra')->unsigned()->nullable();
            $table->integer('id_titular')->unsigned()->nullable();
            $table->integer('id_pseudonimo')->unsigned()->nullable();
            $table->decimal('pct_autoral', 5, 2)->unsigned()->nullable();

            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down() {
        if (Schema::hasTable('obra_titular')) {
            Schema::drop('obra_titular');
        }
    }

}
