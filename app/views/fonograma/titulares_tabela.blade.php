<div class="portlet box {{$portlet_titular}}" id="containerFonogramaTitulares">
    <div class="portlet-title">
        <div class="caption">{{$titular_icon}} Titulares</div>
            <div class="actions">
                    <a href="javascript:btnTitularAdicionarClick();" class="btn btn-sm {{$botao_novo_cor}}">{{$botao_novo_icon}} Adicionar titular</a>
                    <a href="javascript:btnColetivoAdicionarClick();" class="btn btn-sm {{$botao_novo_cor}}">{{$botao_novo_icon}} Adicionar coletivo</button>
                    <a href="/admin/fonograma/recalcular/{{$objeto->id_fonograma}}" class="btn btn-sm yellow-gold"><i class="fa fa-refresh"></i>Recalcular</a>
            </div>
    </div>
    <div class="portlet-body" id="portlet-body-titulares">
        <table class="table table-stripped table-condensed table-hover">
            <thead>
                <tr>
                    <th class="left" style="width:120px;">Ações</th>
                    <th class="center">Id</th>
                    <th class="left">Titular</th>
                    <th class="left">Pseudônimo</th>
                    <th class="left">Categoria</th>
                    <th class="left">Coletivo</th>
                    <th class="right">Part. %</th>
                </tr>
            </thead>
            <tbody>
                <? $total = 0; ?>
                @foreach($fonograma_titulares as $fonograma_titular)
                <? $total += $fonograma_titular->pct_participacao; ?>
                <tr id="tabela{{$fonograma_titular->id_fonograma_titular}}">
                    <td class="left">
                        <a href="javascript:getEditarTitular({{$fonograma_titular->id_fonograma_titular}});" class="btn btn-xs {{$botao_edicao_cor}}" title="Alterar esse titular no fonograma">{{$botao_edicao_icon}}</a>
                        <a href="javascript:postExcluirTitular({{$fonograma_titular->id_fonograma_titular}});" class="btn btn-xs {{$botao_exclusao_cor}}" title="Retirar esse titular do fonograma">{{$botao_exclusao_icon}}</a>
                        @if($fonograma_titular->categoria->cod_categoria == 'MA')
                            <a href="#"  class="btn btn-xs yellow-casablanca popovers" data-title="Instrumentos" data-placement="top" data-html="true" data-container="body" data-trigger="hover" data-content="{{ $fonograma_titular->lista_instrumentos() }}"><i class="fa fa-microphone"></i></a>
                        @endif
                    </td>
                    <td class="center">{{$fonograma_titular->id_titular}}</td>
                    <td>
                        @if ($fonograma_titular->id_coletivo > 0)
                            <i class="fa fa-group"></i>                        
                        @else
                            <i class="fa fa-user"></i>
                        @endif
                        {{$fonograma_titular->nome_razao}}</td>
                    <td>{{$fonograma_titular->nome_pseudonimo}}</td>
                    <td>{{$fonograma_titular->desc_categoria}}</td>
                    <td>{{$fonograma_titular->nome_coletivo}}</td>
                    <td class="right">{{$fonograma_titular->pct_participacao_fmt}}</td>
                </tr>
                @endforeach
                @if ($total > 100)
                <tr style="background-color:#E33A0C; color: #fff; font-weight: bold;">
                    <td colspan="3" style="text-align: right;">ATENÇAO: TOTAL EXCEDE 100%!!! Verifique!</td>
                @else 
                <tr>
                    <td colspan="3" style="text-align: right;">&nbsp;</td>
                @endif
                    <td colspan="3" style="text-align: right;">Total:</td>
                    <td class="right"><? print $total; ?>%</td>
                </tr>
            </tbody>
        </table>
        @include('_padroes.mensagens_locais')
    </div>
</div>
