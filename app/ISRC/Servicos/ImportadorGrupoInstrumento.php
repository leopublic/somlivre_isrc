<?php

namespace ISRC\Servicos;

class ImportadorGrupoInstrumento extends Importador{

    public function sqlSelectMdb() {
        return "select id_grupo_instrumento, nome, dt_inclusao, dt_alteracao, usuario, ind_part_calc_musico_acomp from ISRC_grupo_instrumento_tb where is_deleted = 0";
    }
    
    public function novoObjeto(){
        return new \GrupoInstrumento();
    }
    
    public function nome_id_local(){
        return 'id_grupo_instrumento';
    }

    public function tabela(){
        return "ISRC_grupo_instrumento_tb";
    }
        
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            'nome',
            'usuario',
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);

        $obj->ind_part_calc_musico_acomp   = $regMDB['ind_part_calc_musico_acomp'];
        if ($regMDB['ind_part_calc_musico_acomp'] == 'S'){
        	$obj->fl_ind_part_calc_musico_acomp = 1;
        } else {
        	$obj->fl_ind_part_calc_musico_acomp = 0;
        }
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        return $obj;
    }
}