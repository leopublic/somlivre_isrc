<?php

class TitularController extends CadastroController {
    public function repo(){
        return new \ISRC\Repositorios\RepositorioTitular;
    }
    
    public function diretorioViews() {
        return 'titular';
    }
    
    public function msgSucesso(){
        return 'Titular atualizado com sucesso';
    }    
}
