<?php
View::share('portlet_obra', 'green');
View::share('portlet_fonograma', 'yellow-gold');
View::share('portlet_titular', 'blue-madison');
View::share('portlet_padrao', 'blue');

View::share('botao_edicao_icon', '<i class="fa fa-pencil"></i>');
View::share('botao_edicao_cor', 'blue');
View::share('botao_exclusao_icon', '<i class="fa fa-trash-o"></i>');
View::share('botao_exclusao_cor', 'red');
View::share('botao_novo_icon', '<i class="fa fa-plus"></i>');
View::share('botao_novo_cor', 'green-jungle');

View::share('fonograma_icon', '<i class="fa fa-microphone"></i>');
View::share('obra_icon', '<i class="fa fa-music"></i>');
View::share('titular_icon', '<i class="fa fa-user"></i>');
View::share('coletivo_icon', '<i class="fa fa-group"></i>');
