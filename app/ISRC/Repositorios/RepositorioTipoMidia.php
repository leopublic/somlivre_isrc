<?php

namespace ISRC\Repositorios;

class RepositorioTipoMidia extends RepositorioCadastro {

    public function tituloModelo() {
        return "tipo de mídia";
    }
    
    public function nomeModelo(){
        return '\TipoMidia';
    }
    
    public function ordenacaoPadrao(){
        return 'dsc_tipo_midia';
    }

}
