<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <li id="itemTitulares" class="start"><a href="{{URL::to('/titular')}}"><i class="fa fa-users"></i><span class="title">Titulares</span></a></li>
            <li id="itemObra"><a href="{{URL::to('/obra')}}"><i class="fa fa-music"></i><span class="title">Obras</span></a></li>
            <li id="itemFonogramas"><a href="{{URL::to('/fonograma')}}"><i class="fa fa-music"></i><span class="title">Fonogramas</span></a></li>
            <li id="menuCadastros">
                <a href="javascript:;"><i class="fa fa-database"></i><span class="title">Cadastros</span><span class="arrow "></span></a>
                <ul class="sub-menu">
                    <li id="itemSociedadesAutorais"><a href="{{URL::to('/sociedade')}}"><i class="icon-home"></i>Sociedades autorais</a></li>
                    <li id="itemColetivos"><a href="{{URL::to('/coletivo')}}"><i class="icon-home"></i>Coletivos</a></li>
                    <li><a href="{{URL::to('/poutporrit')}}"><i class="icon-home"></i>Pout-pourri</a></li>
                </ul>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->
