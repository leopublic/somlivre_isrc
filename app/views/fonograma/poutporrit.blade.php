@extends('base')

@section('estilos')
@stop

@section('conteudo')
<div class="row">
    <div class="col-md-12">
        <h3 class="page-title">Editar fonograma - Poutporrit</h3>
    </div>
</div>
@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <div class="tabbable-custom nav-justified">
            @include('fonograma.abas', array('abaAtiva' => 'Pot-pourri'))
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1_1">

                    <div class="portlet box yellow-gold">
                        <div class="portlet-title">
                            <div class="caption"><i class="fa fa-gift"></i> Poutporrit atual</div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{Form::text('nome_poutporrit', $objeto->nome_poutporrit, array("class" => "form-control", "disabled"))}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="portlet box yellow-gold">
                        <div class="portlet-title">
                            <div class="caption"><i class="fa fa-gift"></i> Alterar poutporrit</div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body form">
                            {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            {{Form::text('pesquisar', Input::old('pesquisar'), array("class" => "form-control"))}}
                                            <span class="input-group-btn"><button class="btn" ><i class="fa fa-search"></i> Filtrar</button></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{Form::close()}}
                            @if ($regs)
                            {{$regs->appends(array('pesquisar'=> $pesquisar))->links()}}
                            @endif
                            <div class="table-scrollable">
                                @include('_padroes.table')
                                <thead>
                                    <tr>
                                        <th class="center">Ações</th>
                                        <th class="center">Id</th>
                                        <th class="left">Nome</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($regs as $reg)
                                    <tr>
                                        <td class="center">
                                            <a href="{{URL::to('/admin/fonogramapoutporrit/set/'.$objeto->id_fonograma.'/'.$reg->id_poutporrit)}}" class="btn btn-sm green-seagreen" title="Editar"><i class="fa fa-edit"></i></a>
                                        </td>
                                        <td class="center">{{$reg->id_poutporrit}}</td>
                                        <td>{{$reg->nome}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ajax" role="basic" aria-hidden="true">
        <div class="page-loading page-loading-boxed">
            <img src="/assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
            <span>
                &nbsp;&nbsp;Loading... </span>
        </div>
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
ComponentsPickers.init();
$('#menuAcesso').addClass('active');
$('#menuAcesso').addClass('open');
$('#itemPerfis').addClass('active');
@stop
