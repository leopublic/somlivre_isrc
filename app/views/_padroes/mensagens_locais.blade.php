@if (isset($error) || isset($msg) || isset($info))
<div class="row">
    <div class="col-md-12">
        @if (isset($error))
        <div id="prefix_74553407" class="Metronic-alerts alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            {{$error}}
        </div>
        @endif
        @if (isset($msg))
        <div id="prefix_74553407" class="Metronic-alerts alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            {{$msg}}
        </div>
        @endif
        @if (isset($info))
        <div id="prefix_74553407" class="Metronic-alerts alert alert-info fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            {{$info}}
        </div>
        @endif
    </div>
</div>
@endif