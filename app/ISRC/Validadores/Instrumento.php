<?php
namespace ISRC\Validadores;
class Instrumento extends Validador {
    public $id_unique;
    public static $rules = array(
        'nome'       => 'required',
        'id_grupo_instrumento'       => 'required',
    );
    public static $messages = array(
        'nome.required'       => 'O nome é obrigatório',
        'id_grupo_instrumento.required'       => 'O grupo do instrumento é obrigatório',
        'nome.unique'       => 'Já existe outro instrumento com esse nome',
    );
    public function getRules(){
        $x = array_merge(static::$rules , array('nome' => 'required|unique:instrumento,nome,'.$this->id_unique.",id_instrumento"));
        return $x;
    }
}