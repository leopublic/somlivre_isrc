<?php

namespace ISRC\Repositorios;

class RepositorioSociedade extends RepositorioCadastro {

    public function tituloModelo() {
        return "sociedade";
    }

    public function nomeModelo() {
        return '\Sociedade';
    }

    public function ordenacaoPadrao() {
        return 'nome';
    }

    public function registrosConsulta($filtros = null) {
        if (key_exists('filtro', $filtros)) {
            $filtro = $filtros['filtro'];
        } else {
            $filtro = '';
        }

        return \Sociedade::wherefiltro($filtro)
                        ->orderBy('nome')
                        ->paginate(20);
    }

}
