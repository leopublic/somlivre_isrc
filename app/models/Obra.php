<?php
/**
 * @property int $id_tipo_fonograma
 * @property string $dsc_tipo_fonograma Nome
 * @property string $Ind_Situacao
 * @property string $ind_trilha
 * @property datetime created_at
 * @property datetime updated_at
 *
 */
class Obra extends Modelo {
    protected $table = 'obra';
    protected $primaryKey = 'id_obra';
    protected $guarded = array();

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            $model->id_empresa = \Auth::user()->id_empresa;
        });
    }

    public function obratitulares(){
        return $this->hasMany('ObraTitular', 'id_obra', 'id_obra');
    }

    public function getNomesTitularesAttribute(){
        $obraTitulares = $this->obratitulares();
        $ret = '';
        $br = '';
        foreach($obraTitulares as $obraTitular){
            $ret .= $br.$obraTitular->titular->pessoa->fantasia;
            $br = '<br/>';
        }
        return $ret;
    }

    public function genero(){
      return $this->belongsTo('Genero', 'id_genero', 'id_genero');
    }

    public function scopeTitulo($query, $filtro){
        return $query->orWhere('titulo_original', 'like', '%'.$filtro.'%');
    }

    public function scopeFiltro($query, $filtro){
        if (trim($filtro) != ''){
            $filtro = "%".$filtro."%";
            $sql = " (  titulo_original like '".$filtro."'
                             or subtitulo like '".$filtro."'
                             or cod_obra_ecad like '".$filtro."'
                             or cod_ISWC  like '".$filtro."'
                             or id_obra in (select obra.id_obra
                                              from obra_titular, titular, pessoa, obra
                                             where titular.id_titular = obra_titular.id_titular
                                               and pessoa.id_pessoa = titular.id_pessoa
                                               and obra_titular.id_obra = obra.id_obra
                                               and pessoa.nome_razao like '".$filtro."')
                            or id_obra in (select obra.id_obra
                                            from obra_titular, titular, pessoa, titular_pseudonimo, obra
                                           where titular.id_titular = obra_titular.id_titular
                                             and pessoa.id_pessoa = titular.id_pessoa
                                             and obra_titular.id_obra = obra.id_obra
                                             and titular_pseudonimo.id_titular = titular.id_titular
                                             and titular_pseudonimo.pseudonimo like '".$filtro."')
                            )";
            $query->whereRaw($sql);
        }

        return $query;

    }

    function pode_excluir(){
        $qtd_fonograma = \Fonograma::where('id_obra', '=', $this->id_obra)->count();
        if ($qtd_fonograma > 0){
            return false;
        } else {
            return true;
        }
    }
}