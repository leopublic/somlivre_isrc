<?php

class SociedadeController extends BaseController {

    public function getIndex() {
        $rep = new \ISRC\Repositorios\RepositorioSociedade;
        $regs = $rep->registrosConsulta(array());

        return View::make('sociedade.index')
                        ->with('regs', $regs);
    }

    public function postIndex(){
        Input::flash();
        $rep = new \ISRC\Repositorios\RepositorioSociedade;
        $post = Input::all();
        $regs = $rep->registrosConsulta($post);

        return View::make('sociedade.index')
                        ->with('regs', $regs);
        
    }
    
    public function getEdit($id = 0) {
        $rep = new \ISRC\Repositorios\RepositorioSociedade();
        $objeto = $rep->instanciaEditado($id);
        $titulo = $rep->tituloTelaEdicao($objeto);
        return View::make('sociedade.edit')
                        ->with('objeto', $objeto)
                        ->with('titulo', $titulo)
        ;
    }

    public function postEdit($id_sociedade = '') {
        $post = Input::all();
        try {
            $rep = new \ISRC\Repositorios\RepositorioSociedade();
            if ($rep->salve($post)) {
                Session::flash('msg', 'Sociedade atualizada com sucesso');
                return Redirect::to('/admin/sociedade/');
            } else {
                Input::flash();
                Session::flash('error', $rep->getErrors());
                return Redirect::to('/admin/sociedade/edit/' . $post['id_sociedade']);
            }
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', $ex->getMessage());
            return Redirect::to('/admin/sociedade/edit/' . $post['id_sociedade']);
        }
    }

}
