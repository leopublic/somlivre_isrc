<?php
namespace ISRC\Exportacao;
class Poutporrit extends Exportacao{
	const tprDETALHE = '1';
	const tprOBRAS = '2';

	public static function nomeEntidade(){
		return '\Poutporrit';
	}
	public function tipo_cadastro(){
		return 'POP';
	}

	public function exporta($poutporrit){
		$this->abreTransacao();
		$this->detalhe($poutporrit);
		$obras = $poutporrit->obras;
		foreach($obras as $obra){
			$this->obra($poutporrit, $obra);
		}
		$this->fechaTransacao();
	}

	public function detalhe($poutporrit){
		$this->novo_reg(self::tpoINCLUIR, self::tprDETALHE);
		$this->adicionaCampo($poutporrit->cod_poutporrit_ECAD, 'N', 9, 'Código ECAD poutporrit');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC poutporrit');			//  Código ISRC
		$this->adicionaCampo($poutporrit->nome, 'A', 90, 'Título do poutporrit');
		$this->adicionaCampo('', 'H', 6, 'Duração total');
		$this->adicionaCampo($poutporrit->cod_IWC, 'A', '11', 'Código ISWC');
		$this->grava_registro();
	}

	public function obra($poutporrit, $poutporrit_obra){
		$this->novo_reg(self::tpoINCLUIR, self::tprOBRAS);
		$this->adicionaCampo($poutporrit->cod_poutporrit_ECAD, 'N', 9, 'Código ECAD poutporrit');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC do poutporrit');	
		$this->adicionaCampo($poutporrit_obra->obra->cod_obra_ecad, 'N', 13, 'Código ECAD da obra');
		$this->adicionaCampo('', 'A', 15, 'Código ISRC da obra');	
		$this->adicionaCampo(0, 'N', 3, 'Número de execuções');			// Número de execuções
		$this->adicionaCampo('', 'H', 6, 'Duração');			// Duração

		$this->grava_registro();
	}


}