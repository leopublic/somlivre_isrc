<?php
namespace ISRC\Validadores;
class Sociedade extends Validador {
    public $id_unique;
    public static $rules = array(
        'sgl_sociedade'       => 'required',
        'nome'       => 'required',
    );
    public static $messages = array(
        'sgl_sociedade.required'       => 'A sigla da sociedade é obrigatória',
        'nome.required'       => 'O nome da sociedade é obrigatório',
        'nome.unique'       => 'Já existe outra sociedade com esse nome',
    );

    public function getRules(){
        $x = array_merge(static::$rules , array('nome' => 'required|unique:sociedade,nome,'.$this->id_unique.",id_sociedade"));
        return $x;
    }

}
