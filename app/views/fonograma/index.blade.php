@extends('base')

@section('estilos')
@stop

@section('conteudo')
<div class="col-md-12">
    <h3 class="page-title">Fonogramas</h3>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
    <div class="portlet box {{$portlet_fonograma}}">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>Registros encontrados
            </div>
    <!--            <div class="actions">
                    <a href="/admin/fonograma/criar/0" class="btn btn-sm {{$botao_novo_cor}}">{{$botao_novo_icon}} Novo</a>
                </div>-->
        </div>
        <div class="portlet-body">
            {{Form::open()}}
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group">
                        <input type="search" id="filtro" name="filtro" class="form-control" value="{{Input::old('filtro')}}">
                        <span class="input-group-btn"><button type="submit" class="btn">Buscar</button></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">{{$regs->appends(Input::get())->links()}}</div>
            </div>
            {{Form::close()}}
            @include('_padroes.table')
            <colgroup>
                <col style="width:80px;"/>
                <col style="width:80px;"/>
                <col style="width:150px;"/>
                <col style="width:150px;"/>
                <col style="width:auto;"/>
            </colgroup>
            <thead>
                <tr>
                    <th class="center">Ações</th>
                    <th class="center">Id</th>
                    <th class="center">Cód.<br/>Gravadora</th>
                    <th class="center">Cód. ISRC</th>
                    <th class="left">Título</th>
                </tr>
            </thead>
            <tbody>
                @foreach($regs as $reg)
                <tr>
                    <td class="center">
                        <a href="{{URL::to('/admin/fonograma/edit/'.$reg->id_fonograma)}}" class="btn btn-sm {{$botao_edicao_cor}}" title="Editar">{{$botao_edicao_icon}}</a>
                    </td>
                    <td class="center">{{$reg->id_fonograma}}</td>
                    <td class="center">{{$reg->cod_GRA}}</td>
                    <td class="center">{{$reg->Cod_ISRC_fmt}}</td>
                    <td>{{$reg->titulo}}</td>
                </tr>
                @endforeach
            </tbody>
            </table>
            {{$regs->links()}}
        </div>
    </div>
    <!-- END BORDERED TABLE PORTLET-->
</div>
@stop
@section('scripts')
$('#itemFonogramas').addClass('active');
@stop
