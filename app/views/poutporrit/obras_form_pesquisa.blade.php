<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Adicionar obra</h4>
</div>
<div class="modal-body">
    {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "fmrTitularPesquisar"))}}
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-md-3 control-label">Título original</label>
                    <div class="col-md-9">
                        {{Form::text('titulo_original', '', array("class" => "form-control", "id"=> 'titulo_original'))}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}
    <div id="msg" style="color:red;"></div>
</div>
<div class="modal-footer">
    <button type="button" class="btn blue" onclick="javascript:btnObraPesquisarClick();">Pesquisar</button>
    <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
</div>  
