@extends('base')

@section('estilos')
@stop

@section('conteudo')
@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box {{$portlet_padrao}}">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-gift"></i> {{$titulo}}</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
            {{Form::hidden('id_tipo_midia', $reg->id_tipo_midia)}}
            <div class="form-body">
                    @include('_campos.texto_id', array('valor' => $reg->id_tipo_midia))
                <div class="form-group">
                    <label class="col-md-3 control-label">Descrição</label>
                    <div class="col-md-9">
                        {{Form::text('dsc_tipo_midia', Input::old('dsc_tipo_midia', $reg->dsc_tipo_midia), array("class" => "form-control"))}}
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">Salvar</button>
                    <a href="{{URL::to('/categoria')}}" class="btn default">Cancelar</a>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>
</div>
@stop
@section('scripts')
    $('#menuTabelas').addClass('active');
    $('#menuTabelas').addClass('open');
    $('#itemTipomidias').addClass('active');
@stop
