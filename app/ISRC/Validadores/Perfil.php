<?php
namespace ISRC\Validadores;
class Perfil extends Validador {
    public $id_unique;
    public static $rules = array(
        'descricao'       => 'required',
    );
    public static $messages = array(
        'descricao.required'       => 'O nome do perfil é obrigatório',
        'descricao.unique'       => 'Já existe outro perfi com esse nome',
    );

    public function getRules(){
        $x = array_merge(static::$rules , array('descricao' => 'unique:perfil,descricao,'.$this->id_unique.", id_perfil"));
        return $x;
    }

}
