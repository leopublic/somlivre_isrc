<?php
/**
 * @property int $id_grupo_instrumento
 * @property string $nome Nome 
 * @property string $ind_part_calc_musico_acomp
 * @property boolean $fl_ind_part_calc_musico_acomp
 * @property date $dt_inclusao
 * @property date $dt_alteracao
 * @property string $usuario Usuário que cadastrou(ECAD)
 * @property datetime created_at 
 * @property datetime updated_at 
 * 
 */
class GrupoInstrumento extends Modelo {
    protected $table = 'grupo_instrumento';
    protected $primaryKey = 'id_grupo_instrumento';
    protected $guarded = array();

    public function carregaPeloMDB($registro){
        
    }
    
    public static function combo($filtro = ''){
        return \GrupoInstrumento::orderBy('nome')->lists('nome', 'id_grupo_instrumento');
    }
}