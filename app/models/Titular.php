<?php

/**
 * @property int(10) unsigned $id_titular
 * @property int(10) unsigned $id_empresa
 * @property int(10) unsigned $id_sociedade
 * @property int(10) unsigned $id_pessoa
 * @property tinyint(1) $is_nacional
 * @property varchar(11) $cod_cae
 * @property varchar(13) $cod_omb
 * @property varchar(13) $cod_old
 * @property datetime $dt_inclusao
 * @property datetime $dt_alteracao
 * @property varchar(20) $usuario
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property timestamp $deleted_at
 */
class Titular extends Modelo {

    protected $table = 'titular';
    protected $primaryKey = 'id_titular';
    protected $guarded = array('id_empresa');

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            $model->id_empresa = \Auth::user()->id_empresa;
        });
    }

    /**
     * Relacionamentos
     */
    public function pessoa() {
        return $this->belongsTo('Pessoa', 'id_pessoa');
    }

    public function pseudonimo() {
        return $this->hasMany('TitularPseudonimo', 'id_titular', 'id_titular');
    }

    public function pseudonimoprincipal() {
        return $this->hasOne('TitularPseudonimo', 'id_titular', 'id_titular')->where('is_principal', '=', 1);
    }

    public function combopseudonimos() {
        return \TitularPseudonimo::where('id_titular', '=', $this->id_titular)->orderBy('pseudonimo')->lists('pseudonimo', 'id_pseudonimo');
    }

    public function nomepseudonimoprincipal() {
        if (is_object($this->pseudonimoprincipal)) {
            return $this->pseudonimoprincipal->pseudonimo;
        } else {
            return '--';
        }
    }

    public function scopeWherefiltro($query, $filtro) {
        if ($filtro != ''){
            $numerico = preg_replace('/\D/', '', $filtro);

            $sql = "( id_pessoa in (select id_pessoa from pessoa "
                    . " where ( nome_razao like ?"
                    . " or fantasia like ? ";
            if ($numerico != ''){
                $sql .= " or cpf_cnpj like '%".$numerico."%'";
            }

            $sql .= " )  )";
            $sql .= " or id_titular in (select id_titular from titular_pseudonimo "
                    . " where pseudonimo like ?"
                    . " and id_titular = titular.id_titular)";
            if (is_numeric($filtro)){
                $sql .= " or id_titular = ".$filtro;
            }
            $sql .= ")";
            $query->whereRaw($sql, array('%' . $filtro . '%', '%' . $filtro . '%', '%' . $filtro . '%'));
        }
        return $query;
    }

    public function pode_excluir(){
        $qtd_obra = \ObraTitular::where('id_titular', '=', $this->id_titular)->count();
        $qtd_fonograma = \FonogramaTitular::where('id_titular', '=', $this->id_titular)->count();
        $qtd_coletivo = \TitularColetivo::where('id_titular', '=', $this->id_titular)->count();
        $qtd_total = intval($qtd_obra) + intval($qtd_fonograma) + intval($qtd_coletivo);
        if ($qtd_total > 0){
            return false;
        } else {
            return true;
        }
    }
}
