<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEmpresaSeqExportacao extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (!Schema::hasColumn('empresa', 'seq_exportacao')) {
            Schema::table('empresa', function ($table) {
                $table->string('seq_exportacao');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if (Schema::hasColumn('empresa', 'seq_exportacao')) {
            Schema::table('empresa', function ($table) {
                $table->dropColumn(array('seq_exportacao'));
            });
        }
    }
}
