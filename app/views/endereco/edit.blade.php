<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">{{$titulo}}</h4>
</div>
<div class="modal-body">
    {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "frmEndereco"))}}
    {{Form::hidden('id_pessoa', $reg->id_pessoa, array('id' => 'id_pessoa'))}}
    {{Form::hidden('id_endereco', $reg->id_endereco, array('id' => 'id_endereco'))}}
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                @if ($reg->id_endereco > 0)
                    @include('_campos.texto_id', array('valor'=> $reg->id_endereco))
                @endif
                @include('_campos.texto', array('label' => 'Endereço', 'nome' => 'endereco'))
                @include('_campos.texto', array('label' => 'Complemento', 'nome' => 'complemento'))
                @include('_campos.texto', array('label' => 'Bairro', 'nome' => 'bairro'))
                @include('_campos.texto', array('label' => 'Cidade', 'nome' => 'cidade'))
                @include('_campos.texto', array('label' => 'CEP', 'nome' => 'cep'))
                @include('_campos.select', array('label' => 'UF', 'nome' => 'id_uf', 'valores' => $ufs))
                @include('_campos.select', array('label' => 'Pais', 'nome' => 'id_pais', 'valores' => $paises))
            </div>
        </div>
    </div>
    {{Form::close()}}
    <div id="modal-msg" style="color:red;"></div>
</div>
<div class="modal-footer">
    <a href="javascript:postEditarEndereco();" class="btn blue" onclick="">Salvar</a>
    <button type="button" data-dismiss="modal" class="btn red">Cancelar</button>
</div>
