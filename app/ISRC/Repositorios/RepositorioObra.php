<?php

namespace ISRC\Repositorios;

class RepositorioObra extends RepositorioCadastro {
    
    public function nomeModelo(){
        return '\Obra';
    }
    
    public function ordenacaoPadrao(){
        return 'titulo_original';
    }
    public function registrosConsulta($filtros = null){
               
    	$ret =  \Obra::filtro($filtros)
                ->with('obratitulares', 'obratitulares.titular', 'obratitulares.titular.pessoa', 'obratitulares.categoria', 'obratitulares.titular.pseudonimoprincipal')
                ->orderBy('titulo_original')
                ->paginate(20);
        return $ret;
    }
    
    public function preencheCampos($instancia, $post){
        $instancia = parent::preencheCampos($instancia, $post);
        $instancia->is_nacional = !$post['is_nacional'];
        return $instancia;
    }

    public function adicionarTitular($post){
        if($post['id_obra_titular'] == 0){
            $obra_tit = new \ObraTitular;            
            $obra_tit->id_obra = $post['id_obra'];
            $obra_tit->id_titular = $post['id_titular'];
        } else {
            $obra_tit = \ObraTitular::find($post['id_obra_titular']);
        }
        $obra_tit->id_categoria = $post['id_categoria'];
        $obra_tit->id_pseudonimo = $post['id_pseudonimo'];
        $obra_tit->pct_autoral_fmt = $post['pct_autoral_fmt'];
        $obra_tit->save();
        return $obra_tit;
    }
    
}
