<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaNovosIndices extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('obra', function ($table) {
           $table->index('subtitulo'); 
           $table->index('cod_ISWC'); 
           $table->index('id_empresa'); 
           $table->index('id_obra'); 
        });

		Schema::table('fonograma', function ($table) {
           $table->index('cod_GRA'); 
           $table->index('id_empresa'); 
        });

		Schema::table('pessoa', function ($table) {
           $table->index('cpf_cnpj'); 
           $table->index('id_empresa'); 
        });

		Schema::table('obra_titular', function ($table) {
           $table->index('id_obra'); 
           $table->index('id_titular'); 
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
