<?php
/**
 * @property int $id_pais
 * @property string $nome Nome completo do pais
 * @property string $sigla Sigla de duas letras
 * @property date $data 
 * @property boolean $is_convroma Indica se o país é signatário da convenção de Roma (ECAD)
 * @property string $usuario Usuário que cadastrou o pais (ECAD)
 * @property datetime created_at 
 * @property datetime updated_at 
 * 
 * 
 */
class Pais extends Modelo {
    protected $table = 'pais';
    protected $primaryKey = 'id_pais';
    protected $guarded = array();

    public function carregaPeloMDB($registro){
        
    }
    
    public static function combo($filtro = ''){
        return \Pais::orderBy('nome')->lists('nome', 'id_pais');
    }
}