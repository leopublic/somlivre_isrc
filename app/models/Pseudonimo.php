<?php
/**
 * 
 * @property int $id_pseudonimo
 * @property int $id_titular
 * @property int $is_principal
 * @property string $pseudonimo
 * @property datetime created_at 
 * @property datetime updated_at 
 */
class Pseudonimo extends Modelo {
    protected $table = 'titular_pseudonimo';
    protected $primaryKey = 'id_pseudonimo';
    protected $guarded = array();


    public function titular(){
    	return $this->belongsTo('Titular', 'id_titular', 'id_titular');
    }
}