<?php
namespace ISRC\Validadores;
class Empresa extends Validador {
    public $id_unique;
    public static $rules = array(
        'nome'       => 'required',
        'sgl_IFPI'       => 'required',
        'sgl_radical_IFPI'       => 'required',
        'ano_base'       => 'required',
        'seq_ISRC'       => 'required',
    );
    public static $messages = array(
        'nome.required'       => 'Informe um nome para a empresa',
        'nome.unique'       => 'Já existe outra empresa cadastrada com esse nome. Verifique.',
        'sgl_IFPI.required'       => 'A sigla IFPI é obrigatória',
        'sgl_IFPI.unique'       => 'Já existe outra empresa com essa sigla. Verifique.',
        'sgl_radical_IFPI.required'       => 'O prefixo IFPI é obrigatório',
        'ano_base.required'       => 'O ano base é obrigatório',
        'seq_ISRC.required'       => 'O sequencial do próximo ISRC é obrigatório',
        'desc_categoria.unique'       => 'Já existe outra categoria com esse nome',
    );
    public function getRules(){
        $x = array_merge(static::$rules , array('nome' => 'required|unique:empresa,nome,'.$this->id_unique.",id_empresa"));
        $y = array_merge($x , array('sgl_IFPI' => 'required|unique:empresa,sgl_IFPI,'.$this->id_unique.",id_empresa"));
        return $y;
    }
}