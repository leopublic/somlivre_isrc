<?php

class BaseController extends Controller {

	protected $empresas;
	public function __construct(){
		if (Auth::user()){
			$id = \Auth::user()->id_empresa;
			if (!$id){
				$id = '0';
			}
			$this->empresas = \Empresa::outrasEmpresas($id);
			View::share('empresas_contexto', $this->empresas);
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
