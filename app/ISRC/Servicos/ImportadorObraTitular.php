<?php

namespace ISRC\Servicos;

class ImportadorObraTitular extends Importador{

    public function sqlSelectMdb() {
        return "select id_obra_titular, id_categoria, id_titular, id_obra, id_pseudonimo, dt_inclusao, dt_alteracao, usuario, pct_autoral from ISRC_obra_titular_tb where is_deleted = 0";
    }
    
    public function novoObjeto(){
        return new \ObraTitular();
    }
    
    public function nome_id_local(){
        return 'id_obra_titular';
    }
            
    public function tabela(){
        return "ISRC_obra_titular_tb";
    }
    
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            'id_categoria', 
            // 'id_obra', 
            // 'id_titular',
            // 'id_pseudonimo',
            'pct_autoral',
            'usuario'
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);

        //
        $pseudonimo = \Pseudonimo::where('id_empresa', '=', \ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa)
                        ->where('id_pseudonimo_original', '=', $regMDB['id_pseudonimo'])
                        ->first();
        if (count($pseudonimo) > 0){
            $obj->id_pseudonimo = $pseudonimo->id_pseudonimo;
        } else {
            \Log::error('ObraTitular (id='.$regMDB['id_obra_titular'].' empresa='.\ISRC\Repositorios\RepositorioMDB::$empresa->id_empresa.'): pseudonimo não encontrada id='.$regMDB['id_pseudonimo']);
        }
        //
        $this->id_titular = $this->obtem_id_titular($regMDB['id_titular'], $regMDB['id_obra_titular']);
        $this->id_obra = $this->obtem_id_obra($regMDB['id_obra'], $regMDB['id_obra_titular']);


        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        return $obj;
    }
}