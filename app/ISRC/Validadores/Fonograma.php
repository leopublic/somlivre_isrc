<?php
namespace ISRC\Validadores;
class Fonograma extends Validador {
    public $id_unique;
    public static $rules = array(
        'tmp_duracao_min'               => 'required',
        'tmp_duracao_seg'               => 'required',
//        'email'                  => array('required', 'unique:usuario,email', 'regex:/^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,3})$/'),
    );
    public static $messages = array(
        'tmp_duracao_min.required'      => 'A quantidade de minutos da duração é obrigatória',
        'tmp_duracao_seg.required'         => 'A quantidade de segundos da duração é obrigatória',
    );
}
