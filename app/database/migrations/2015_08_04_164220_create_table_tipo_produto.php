<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTipoProduto extends Migration
{

    public function up() {
        Schema::create('tipo_produto', function ($table) {
            $table->increments('id_tipo_produto');
            $table->string('nome', 500);
            $table->timestamps();
        });

        if (!Schema::hasColumn('fonograma', 'id_tipo_produto')) {
            Schema::table('fonograma', function ($table) {
                $table->integer('id_tipo_produto')->unsigned()->nullable();
            });
        }
    }
    public function down() {
        Schema::dropIfExists('tipo_produto');
    }
}
