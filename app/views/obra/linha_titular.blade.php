<tr id="tabela_{{$obra_titular->id_obra_titular}}">
    <input type="hidden" id="id_{{$i}}" name="id_{{$i}}" class="id_titular" value="{{$obra_titular->id_obra_titular}}"/>
    <input type="hidden" id="id_titular_{{$i}}" name="id_titular_{{$i}}" class="id_titular" value="{{$obra_titular->id_titular}}"/>
    <td class="center">
        <a href="javascript:postExcluiTitular({{$obra_titular->id_obra_titular}});" class="btn btn-xs  {{$botao_exclusao_cor}}" title="Retirar esse titular da obra"> {{$botao_exclusao_icon}}</a>
    </td>
    <td class="center">{{$obra_titular->id_titular}}</td>
    <td>
        @if ($obra_titular->id_coletivo > 0)
            <i class="fa fa-group"></i>
        @else
            <i class="fa fa-user"></i>
        @endif
        {{$obra_titular->nome_razao}}</td>
    <td>{{Form::select('id_pseudonimo_'.$i, $obra_titular->titular->combopseudonimos(), $obra_titular->id_pseudonimo, array("class" => "form-control select2"))}}</td>
    <td>{{Form::select('id_categoria_'.$i, $categorias, $obra_titular->id_categoria, array("class" => "form-control select2 id_categoria"))}}</td>
    <td class="right"><input type="text" value="{{$obra_titular->pct_autoral_fmt}}" name="pct_autoral_{{ $i }}" id="pct_autoral_{{ $i }}" class="pct_autoral_class" style="text-align: right;"/></td>
</tr>