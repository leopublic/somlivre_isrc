<?php
/**
 *
 */
class Uf extends Modelo {
    protected $table = 'uf';
    protected $primaryKey = 'id_uf';
    protected $guarded = array();

    public function carregaPeloMDB($registro){

    }

    public static function combo($filtro = ''){
        return \Uf::orderBy('descricao')->lists('descricao', 'id_uf');
    }
}