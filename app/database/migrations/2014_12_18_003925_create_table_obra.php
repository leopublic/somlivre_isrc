<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableObra extends Migration {

    public function up() {
        Schema::create('obra', function ($table) {
            $table->increments('id_obra');
            $table->integer('id_obra_original')->unsigned()->nullable();
            $table->integer('id_empresa')->unsigned()->nullable();
            $table->integer('id_genero')->unsigned()->nullable();
            $table->string('titulo_original', 90)->nullable();
            $table->integer('cod_obra')->unsigned()->nullable();
            $table->string('cod_obra_ecad', 20)->nullable();
            $table->string('cod_old', 13)->nullable();
            $table->string('subtitulo', 90)->nullable();
            $table->string('versao', 90)->nullable();
            $table->string('cod_ISWC', 11)->nullable();
            $table->boolean('is_nacional')->nullable();
            $table->boolean('is_instrumental')->nullable();

            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::table('obra', function ($table) {
           $table->index('titulo_original'); 
           $table->index('id_genero'); 
           $table->index('cod_obra_ecad'); 
        });
    }

    public function down() {
        if (Schema::hasTable('obra')) {
            Schema::drop('obra');
        }
    }

}
