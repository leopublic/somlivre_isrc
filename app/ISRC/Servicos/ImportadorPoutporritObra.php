<?php

namespace ISRC\Servicos;

class ImportadorPoutporritObra extends Importador{

    public function sqlSelectMdb() {
        return "select id_poutporrit_obra, id_obra, id_poutporrit, dt_inclusao, dt_alteracao, usuario from ISRC_poutporrit_obra_tb where is_deleted = 0";
    }
    
    public function nome_id_local(){
        return 'id_poutporrit_obra';
    }

    public function novoObjeto(){
        return new \PoutporritObra();
    }
            
    public function tabela(){
        return "ISRC_poutporrit_obra_tb";
    }
        
    public function carregaObjeto($regMDB, $preservar_id){
        $obj = $this->novoObjeto();
        $campos = array(
            // 'id_poutporrit_obra',
            // 'id_obra', 
            // 'id_poutporrit', 
        );
        $obj = $this->carregaCampos($obj, $regMDB, $campos, $preservar_id);

        $obj->id_obra = $this->obtem_id_obra($regMDB['id_obra'], $regMDB['id_poutporrit_obra']);
        $obj->id_poutporrit = $this->obtem_id_poutporrit($regMDB['id_poutporrit'], $regMDB['id_poutporrit_obra']);

        $obj->usuario = $regMDB['usuario'];
        $obj->dt_inclusao =  $this->dataHoraOk($regMDB['dt_inclusao']);
        $obj->dt_alteracao =  $this->dataHoraOk($regMDB['dt_alteracao']);
        return $obj;
    }
}