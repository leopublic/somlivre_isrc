@extends('base')

@section('estilos')
@stop

@section('conteudo')
<div class="col-md-12">
    <h3 class="page-title">Debug exportação</h3>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
    <div class="portlet box {{$portlet_fonograma}}">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>Layout do registro "{{$entidade}}"/"{{$reg}}"  - ID:{{$id}}
            </div>
        </div>
        <div class="portlet-body">
            {{Form::close()}}
            @include('_padroes.table')
            <colgroup>
                <col style="width:80px;"/>
                <col style="width:80px;"/>
                <col style="width:400px;"/>
                <col style="width:auto;"/>
            </colgroup>
            <thead>
                <tr>
                    <th class="center">Início</th>
                    <th class="center">Tam</th>
                    <th class="center">Comentário</th>
                    <th class="center">Valor no registro</th>
                    <th class="center">Valor enviado (espaços trocados por ".")</th>
                </tr>
            </thead>
            <tbody>
                @foreach($regs as $reg)
                <tr>
                    <td class="center">{{substr('000'.$reg[0], -3)}}</td>
                    <td class="center">{{substr('000'.$reg[1], -3)}}</td>
                    <td class="left">{{$reg[2]}}</td>
                    <td class="left">"{{str_replace(' ', '.', $reg[3])}}"</td>
                    <td class="left" ><pre>{{str_replace(' ', '.', $reg[4])}}</pre></td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
    <!-- END BORDERED TABLE PORTLET-->
</div>
@stop
