<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTentativaAcesso extends Migration {

    public function up() {
        Schema::create('tentativa_acesso', function(Blueprint $table) {
            $table->string('ip', 25);
            $table->integer('qtd')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down() {
        if (Schema::hasTable('tentativa_acesso')){
            Schema::drop('tentativa_acesso');
        }
    }

}
