<div class="radio-list">
    <label class="radio-inline">
        {{ Form::radio($nome, 0, !Input::old($nome, $modelo->$nome) )}} Não
    </label>
    <label class="radio-inline">
        {{ Form::radio($nome, 1, Input::old($nome, $modelo->$nome) )}} Sim
    </label>
</div>
