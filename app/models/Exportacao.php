<?php
/**
 * @property int $id_exportacao
 * @property int $sequencial
 * @property int $id_usuario
 * @property int $id_empresa
 * @property int $id_status_exportacao
 * @property timestamp $created_at
 * @property timestamp $updated_at
 */
class Exportacao extends Modelo {
    protected $table = 'exportacao';
    protected $primaryKey = 'id_exportacao';
    protected $guarded = array();

    public function usuario(){
        return  $this->belongsTo('Usuario', 'id_usuario', 'id_usuario');
    }

    public function empresa(){
        return  $this->belongsTo('Empresa', 'id_empresa', 'id_empresa');
    }
    /**
     * Recupera o último sequencial da empresa, incrementa, atualiza a 
     * exportação e atualiza a empresa
     * @return [type] [description]
     */
    public function atribuiProximoSequencial(){
    	try{
	    	DB::beginTransaction();
	    	$seq = $this->empresa->seq_exportacao;
	    	$this->sequencial = $seq+1;
	    	$this->empresa->incrementaExportacao();
	    	$this->save();
    		DB::commit();
    	} catch(Exception $e){
    		DB::rollback();
    		throw $e;
    	}
    }
}