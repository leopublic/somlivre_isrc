@extends('base')

@section('estilos')
@stop

@section('conteudo')
<div class="row">
    <div class="col-md-12">
        <h3 class="page-title">Editar fonograma</h3>
    </div>
</div>
@include('_padroes.mensagens')
<div class="row">
    <div class="col-md-12">
        <div class="tabbable-custom nav-justified">
            <ul class="nav nav-tabs nav-justified">
                <li><a href="/admin/fonograma/edit/{{$objeto->id_fonograma}}" > Fonograma</a></li>
                <li><a href="/admin/fonogramaobras/show/{{$objeto->id_fonograma}}" > Obras </a></li>
                <li><a href="/admin/fonogramatitulares/show/{{$objeto->id_fonograma}}" > Titulares conexos </a></li>
                <li class="active"><a href="#tab_1_1_1" data-toggle="tab"> Coletivos </a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1_1">

                    <div class="portlet box yellow-gold">
                        <div class="portlet-title">
                            <div class="caption"><i class="fa fa-gift"></i> Titulares conexos</div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="table-scrollable">
                                @include('_padroes.table')
                                <thead>
                                    <tr>
                                        <th class="center">Ações</th>
                                        <th class="center">Id</th>
                                        <th class="left">Título</th>
                                        <th class="center">Cód.<br/>Gravadora</th>
                                        <th class="center">Cód. ISRC</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($regs as $reg)
                                    <tr>
                                        <td class="center">
                                            <a href="{{URL::to('/admin/fonograma/edit/'.$reg->id_fonograma)}}" class="btn btn-sm green-seagreen" title="Editar"><i class="fa fa-edit"></i></a>
                                        </td>
                                        <td class="center">{{$reg->id_fonograma}}</td>
                                        <td>{{$reg->titulo}}</td>
                                        <td class="center">{{$reg->cod_GRA}}</td>
                                        <td class="center">{{$reg->Cod_ISRC_fmt}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="portlet box yellow-gold">
                        <div class="portlet-title">
                            <div class="caption"><i class="fa fa-gift"></i> Titulares conexos</div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body form">
                            {{Form::open(array('role' => "form", "class" => "form-horizontal"))}}
                            {{Form::hidden('id_fonograma', $objeto->id_fonograma)}}
                            <div class="form-body">
                                <a class=" btn default" href="/admin/fonograma/selecionarobra/{{$objeto->id_fonograma}}" data-target="#ajax" data-toggle="modal">
                                    View Demo </a>
                                <h4>Códigos</h4>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">ISRC</label>
                                    <div class="col-md-9">
                                        {{Form::text('Cod_ISRC', Input::old('Cod_ISRC', $objeto->Cod_ISRC), array("class" => "form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Gravadora</label>
                                    <div class="col-md-9">
                                        {{Form::text('cod_GRA', Input::old('cod_GRA', $objeto->cod_GRA), array("class" => "form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Data de emissão</label>
                                    <div class="col-md-9">
                                        {{Form::text('dt_emissao_fmt', Input::old('dt_emissao_fmt', $objeto->dt_emissao_fmt), array("class" => "form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Catálogo</label>
                                    <div class="col-md-9">
                                        {{Form::text('num_catalogo', Input::old('num_catalogo', $objeto->num_catalogo), array("class" => "form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Obra</label>
                                    <div class="col-md-9">
                                        {{Form::text('num_catalogo', Input::old('num_catalogo', $objeto->num_catalogo), array("class" => "form-control"))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ajax" role="basic" aria-hidden="true">
    <div class="page-loading page-loading-boxed">
        <img src="/assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
        <span>
            &nbsp;&nbsp;Loading... </span>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>
</div>
@stop
@section('scripts')
ComponentsPickers.init();
$('#menuAcesso').addClass('active');
$('#menuAcesso').addClass('open');
$('#itemPerfis').addClass('active');
@stop
