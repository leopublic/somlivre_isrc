<table class="table table-stripped table-condensed table-hover">
    <colgroup>
        <col style="width:100px;"/>
        <col style="width:50px;"/>
        <col style="width:auto;"/>
        <col style="width:auto;"/>
        <col style="width:auto;"/>
        <col style="width:auto;"/>
    </colgroup>
    <thead>
        <tr>
            <th class="center">Ações</th>
            <th class="center">Id</th>
            <th class="left">Nome</th>
            <th class="left">Email</th>
            <th class="left">Telefone</th>
            <th class="left">Obs</th>
        </tr>
    </thead>
    <tbody>
        @foreach($contatos as $contato)
        <tr id="contato{{$contato->id_contato}}">
            <td class="center">
                <a href="javascript:getEditarContato('{{$contato->id_pessoa}}', '{{$contato->id_contato}}');" class="btn btn-xs {{$botao_edicao_cor}}" title="Editar esse Contato">{{$botao_edicao_icon}}</a>
                <a href="javascript:postExcluirContato('{{$contato->id_pessoa}}', '{{$contato->id_contato}}');" class="btn btn-xs {{$botao_exclusao_cor}}" title="Excluir esse contato">{{$botao_exclusao_icon}}</a>
            </td>
            <td class="center">{{$contato->id_contato}}</td>
            <td class="left">{{$contato->nome}}</td>
            <td class="left">{{$contato->email}}</td>
            <td class="left">@if(count($contato->telefone)>0){{$contato->telefone->numero_completo}}@else--@endif</td>
            <td class="left">{{$contato->observacao}}</td>
        </tr>
        @endforeach
    </tbody>
</table>