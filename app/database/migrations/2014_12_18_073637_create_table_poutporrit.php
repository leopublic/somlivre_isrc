<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePoutporrit extends Migration {

    public function up() {
        Schema::create('poutporrit', function ($table) {
            $table->increments('id_poutporrit');
            $table->integer('id_poutporrit_original')->unsigned()->nullable();
            $table->integer('id_empresa')->unsigned()->nullable();
            $table->string('nome', 90)->nullable();
            $table->integer('cod_poutporrit')->unsigned()->nullable();
            $table->string('cod_poutporrit_ECAD', 20)->nullable();
            $table->string('cod_IWC', 11)->nullable();
            $table->dateTime('dt_inclusao')->nullable();
            $table->dateTime('dt_alteracao')->nullable();
            $table->string('usuario', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::table('poutporrit', function ($table) {
           $table->index('nome'); 
           $table->index('cod_IWC'); 
           $table->index('cod_poutporrit_ECAD'); 
        });
    }

    public function down() {
        if (Schema::hasTable('poutporrit')) {
            Schema::drop('poutporrit');
        }
    }

}
